using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using System;
using System.Threading.Tasks;
using Tams.Web.BusinessLayer;

namespace Tams.Web.Shared
{
    public class LoginDisplayBase : ComponentBase
    {
        [Inject]
        public AuthenticationStateProvider provider {get; set;}    

        [Inject]
        public IDataService dataService {get; set;}  
        public string UserName {get; set;}    
        
        protected override async Task OnInitializedAsync()
        {    
            var user = (await provider.GetAuthenticationStateAsync()).User;

            if(!user.Identity.IsAuthenticated) 
                return;

            var userId = Guid.Parse(user.Identity.Name);
            var person = dataService.GetPerson(userId);
            UserName = string.IsNullOrWhiteSpace(person.MiddleName) ? 
                $"{person.FirstName} {person.LastName}" : $"{person.FirstName} {person.MiddleName} {person.LastName}";
        } 
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.