using AutoMapper;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;
using Tams.Web.ViewModels.DisplayViewModels;
using Tams.Web.ViewModels.ReportViewModels;

namespace Tams.Web
{
    public class MappingProfile : Profile 
    {
        public MappingProfile() 
        {   
            //Mapping display viewmodels
            CreateMap<Questionnaire, QuestionnaireViewModel>()
                .ReverseMap();

            CreateMap<Category, CategoryViewModel>()
                .ReverseMap();

            CreateMap<Subcategory, SubcategoryViewModel>()
                .ReverseMap();

            CreateMap<Question, QuestionViewModel>()
                .ReverseMap();   

            CreateMap<Response, ResponseViewModel>()
                .ReverseMap();   

            //Mapping report viewmodels
            CreateMap<Questionnaire, ReportQuestionnaireViewModel>()
                .ForMember(dest => dest.ReportCategories, opt => opt.MapFrom(src => src.Categories));;

            CreateMap<Category, ReportCategoryViewModel>()
                .ForMember(dest => dest.ReportSubcategories, opt => opt.MapFrom(src => src.Subcategories));

            CreateMap<Subcategory, ReportSubcategoryViewModel>()
                .ForMember(dest => dest.ReportCategory, opt => opt.MapFrom(src => src.Category))
                .ForMember(dest => dest.ReportQuestions, opt => opt.MapFrom(src => src.Questions));

            CreateMap<Question, ReportQuestionViewModel>()
                .ForMember(dest => dest.ReportResponses, opt => opt.MapFrom(src => src.Responses));     

            CreateMap<Response, ReportResponseViewModel>();  

            CreateMap<CybersecurityStandard, NistMappingViewModel>();

            CreateMap<ScoringRubric, ReportScoringRubricViewModel>();
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.