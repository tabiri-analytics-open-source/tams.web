# TAMS

This is the Tabiri Analytics Management System (TAMS) that is used for conducting technical and operational assessments.

## Setting up Development Environment on Ubuntu

### 1. Run script below
```
#!/bin/bash
# Give this script execute permission before executing it by running the chmod in the script's directory
# chmod +x dev_install.sh

echo
echo "Updating the apt package index..."
echo
apt update

echo
echo "Installing curl..."
echo
apt install -y curl

echo
echo "Creating folder for AWS credential file and git projects..."
echo
sudo -u $SUDO_USER mkdir /home/$SUDO_USER/.aws
sudo -u $SUDO_USER mkdir /home/$SUDO_USER/git

echo
echo "Installing AWS CLI..."
echo
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "/tmp/awscliv2.zip"
unzip /tmp/awscliv2.zip
sudo ./tmp/aws/install

echo
echo "Installing .NET Core 5.0 CLI..."
echo
wget https://packages.microsoft.com/config/ubuntu/$(lsb_release -rs)/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb

add-apt-repository universe

apt-get install -y apt-transport-https && \
    apt-get update && \
    apt-get install -y dotnet-sdk-5.0

# https://docs.microsoft.com/en-us/dotnet/core/install/linux-ubuntu

echo
echo "Installing PostgreSQL..."
echo
echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" | tee /etc/apt/sources.list.d/pgdg.list 
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

apt update
apt install -y postgresql-13 \
    postgresql-client-13
    
# https://www.postgresql.org/download/linux/ubuntu/

sudo -u postgres psql -c "ALTER USER postgres PASSWORD 'YOUR_TAMS_DB_PASSWORD';"
sudo /etc/init.d/postgresql restart

echo
echo "Installing pgAdmin..."
echo
curl https://www.pgadmin.org/static/packages_pgadmin_org.pub | sudo apt-key add
sh -c 'echo "deb https://ftp.postgresql.org/pub/pgadmin/pgadmin4/apt/$(lsb_release -cs) pgadmin4 main" > /etc/apt/sources.list.d/pgadmin4.list && apt update'
apt install -y pgadmin4

echo
echo "Installing Visual Studio Code..."
echo
snap install code --classic

echo
echo "Installing Visual Studio Code extentions as "$SUDO_USER"..."
echo
sudo -u $SUDO_USER code --install-extension ms-vscode.csharp
sudo -u $SUDO_USER code --install-extension amazonwebservices.aws-toolkit-vscode
sudo -u $SUDO_USER code --install-extension ms-azuretools.vscode-docker
sudo -u $SUDO_USER code --install-extension ms-vscode-remote.vscode-remote-extensionpack
sudo -u $SUDO_USER code --install-extension formulahendry.auto-rename-tag
sudo -u $SUDO_USER code --install-extension thekalinga.bootstrap4-vscode
sudo -u $SUDO_USER code --install-extension coenraads.bracket-pair-colorizer-2
sudo -u $SUDO_USER code --install-extension pranaygp.vscode-css-peek
sudo -u $SUDO_USER code --install-extension davidanson.vscode-markdownlint

echo
echo "Installing Git..."
echo
apt install -y git

sudo -u $SUDO_USER cat << EOF >> /home/$SUDO_USER/.gitmessage
# Hey there o/! 
# 
# We just wanted to let you know that we care a great deal about    
# making our git history clean, maintainable and easy to access for 
# all our contributors. Commit messages are very important to us,  
# which is why we have a strict commit message policy in place.     
# Please use the following guidelines to format all your commit     
# messages:
# 
#     <type>(<scope>): <subject>
#     <BLANK LINE>
#     <body>
#     <BLANK LINE>
#     <footer>
# 
# Please note that:
#  - The HEADER is a single line of max. 50 characters that         
#    contains a succinct description of the change. It contains a   
#    type, an optional scope, and a subject
#       + <type> describes the kind of change that this commit is   
#                providing. Allowed types are:
#             * feat (feature)
#             * fix (bug fix)
#             * docs (documentation)
#             * style (formatting, missing semicolons, …)
#             * refactor
#             * test (when adding missing tests)
#             * chore (maintain)
#       + <scope> can be anything specifying the place of the commit    
#                 change
#       + <subject> is a very short description of the change, in   
#                   the following format:
#             * imperative, present tense: “change” not             
#               “changed”/“changes”
#             * no capitalised first letter
#             * no dot (.) at the end
#  - The BODY should include the motivation for the change and      
#    contrast this with previous behavior and must be phrased in   
#    imperative present tense 
#  - The FOOTER should contain any information about Breaking       
#    Changes and is also the place to reference GitHub issues that   
#    this commit closes
# 
# Thank you <3
EOF

echo
echo "Configuring git..."
echo
sudo -u $SUDO_USER git config --global user.name "YOUR NAME GOES HERE"
sudo -u $SUDO_USER git config --global user.email "YOUR EMAIL GOES HERE"
sudo -u $SUDO_USER git config --global commit.template "~/.gitmessage"
# git config --list # View set configurations

echo
echo "Updating the apt package index..."
echo
apt update

echo
echo "Installing the newest versions of available packages..."
echo
apt upgrade -y

echo
echo "Removing packages that are no longer required..."
echo
apt autoremove -y

echo
echo "Deleting installation files..."
rm /tmp/*

echo
echo "Finished running script..."
echo
echo "Rebooting system..."
reboot
```
### 2. Post Install Steps

1. [Create AWS account](https://aws.amazon.com/premiumsupport/knowledge-center/create-and-activate-aws-account/)

2. Configure AWS Access key
    - [Create AWS Access key](https://aws.amazon.com/premiumsupport/knowledge-center/create-access-key/) 
    - Launch Visual Studio Code
    - Click on the AWS Extension on the left
    - Click on hamburger menu > Connect to AWS
    - Enter Access Key Id then press "ENTER"
    - Enter Secret Access Key then press "ENTER"

### 3. Configuring AWS Cognito
##### 1. Create a User Pool
![](readme_images/dd0c1df3-ebea-42ed-878b-9bc2a97e0437.png)
##### 2. Select a Pool Name
![](readme_images/f53d9ab0-aa21-41a3-b3d3-d9c790ecb3fe.png)
##### 3. Fill out the User Pool's Attributes
![](readme_images/7e17688d-4a7c-4ae0-84d7-7b073c49ed29.png)
##### 4. Fill out the User Pool's Policies
![](readme_images/7612673b-9664-4fee-aa15-01bb31f0ee17.png)
##### 5. Fill out the User Pool's MFA Verification details
![](readme_images/783899dd-cadd-4c3f-b6d0-087f599af9a1.png)
##### 6. Fill out the User Pool's Message Customization
![](readme_images/101396ca-cd98-4b65-a24c-86bdecb7d352.png)

![](readme_images/3c395a82-0710-4c36-8d7f-f7ceff58eab5.png)

![](readme_images/7326ee09-ce15-40ec-b938-d118aa4c9ed1.png)
##### 7. Create App Client
![](readme_images/44d854b2-e6e0-4871-94de-d5ecce3904d3.png)

![](readme_images/cc9d5ebe-a282-4670-9ae1-12d84eea7f31.png)

### 4. Configuring TAMS to use AWS Cognito Authentication

#### 1. Create tamssettings.json file and move it to /
![](readme_images/384d695f-fb18-40fe-bc9f-5eb9eb51646a.png)

Obtain the UserPoolId, UserPoolClientId and UserPoolClientSecret from the AWS Cognito screens shown below:

![](readme_images/b562f131-b1c1-4f8f-acab-623fc6a7cead.png)

![](readme_images/7c271e25-2f88-4f08-9500-bbda0eed36a5.png)

![](readme_images/961d442f-dc80-4131-9dbc-4825733a7892.png)

#### 1. Create Admin and Auditor Groups

![](readme_images/72fa91c7-d7c5-4fd3-a5d6-2585ae952dd2.png)

#### 2. Create User(s) and add them Admin and Auditor Groups

![](readme_images/d29d276d-3406-4c54-b68b-31b91f218ea1.png)


