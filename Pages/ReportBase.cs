using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Tams.Web.BusinessLayer;

namespace Tams.Web.Pages
{
    public class ReportBase : ComponentBase
    {
        [Inject]
        public IDataService DataService { get; set; } 

        [Inject]
        public IStateContainer StateContainer { get; set; } 

        [Inject]
        public AuthenticationStateProvider Provider { get; set; }  

        [Inject]
        public NavigationManager NavigationManager { get; set; }
        public string OrgName { get; set; }
        protected Guid _orgId, _questionnaireId;    
        protected override void OnInitialized()
        {  
            var user = Task.Run<AuthenticationState>(async () => await Provider.GetAuthenticationStateAsync())
                .Result
                .User; 

            if (user.Identity.IsAuthenticated)
            {
                var userId = Guid.Parse(user.Identity.Name);

                NavigationManager.TryGetQueryString<Guid>("orgId", out _orgId);
                NavigationManager.TryGetQueryString<Guid>("questionnaireId", out _questionnaireId);

                OrgName = DataService.GetOrganizationByOrgId(_orgId)?.Name;

                StateContainer.Init(userId, _orgId, _questionnaireId);
            }   
        }                      
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.