using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.AspNetCore.Identity.Cognito;
using Amazon.Extensions.CognitoAuthentication;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Tams.Web.BusinessLayer;

namespace Tams.Web.Pages
{
    public class IndexBase : ComponentBase
    {
        [Inject]
        public IStateContainer StateContainer { get; set; }  
        
        [Inject]
        protected NavigationManager NavigationManager { get; set; }

        [Inject]
        public AuthenticationStateProvider Provider { get; set; }       
       
        [Inject]
        public CognitoUserManager<CognitoUser> UserManager { get; set; }      

        protected override void OnInitialized()
        {           
            var user = Task.Run<AuthenticationState>(async () => await Provider.GetAuthenticationStateAsync())
                .Result
                .User;           

            if (user.Identity.IsAuthenticated)
            {
                var cognitoUser = Task.Run<CognitoUser>(async () => await UserManager.GetUserAsync(user))
                    .Result; 
                
                var roles = Task.Run<IList<string>>(async () => await UserManager.GetRolesAsync(cognitoUser))
                    .Result;
                
                //var isCurrentUserAuditor = roles.Any(y => y.ToLower() == "Auditor".ToLower());
                var isCurrentUserTabiriAuditor = roles.Any(y => y.ToLower() == "TabiriAuditor".ToLower());

                var isCurrentUserCmuAuditor = roles.Any(y => y.ToLower() == "CmuAuditor".ToLower());
                                
                var userId = Guid.Parse(user.Identity.Name);

                StateContainer.Init(userId);

                if(isCurrentUserTabiriAuditor || isCurrentUserCmuAuditor)
                {
                    NavigationManager.NavigateTo("/auditor-dashboard");
                }
                else
                {
                    NavigationManager.NavigateTo("/operational-assessment/questionnaire");
                }
            }    
            else
            {
                NavigationManager.NavigateTo("/Identity/Account/Login");
            }
        }      
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.