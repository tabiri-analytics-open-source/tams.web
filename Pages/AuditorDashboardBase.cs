
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Amazon.AspNetCore.Identity.Cognito;
using Amazon.Extensions.CognitoAuthentication;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Tams.Web.BusinessLayer;

namespace Tams.Web.Pages
{
    public class AuditorDashboardBase : ComponentBase
    {
        [Inject]
        public IDataService DataService { get; set; }    

        [Inject]
        public IStateContainer StateContainer { get; set; }

        [Inject]
        public CognitoUserManager<CognitoUser> UserManager { get; set; } 

        [CascadingParameter]
        private Task<AuthenticationState> authState { get; set; }    
            
        public bool IsCurrentUserTabiriAuditor { get; set; }
        public bool IsCurrentUserCmuAuditor { get; set; }
        public ClaimsPrincipal currentUser;
        protected async override void OnParametersSet()
        {
            if (authState != null)
            {
                currentUser = (await authState).User;  

                var cognitoUser = Task.Run<CognitoUser>(async () => await UserManager.GetUserAsync(currentUser))
                    .Result;

                if(cognitoUser != null)
                {
                    var roles = Task.Run<IList<string>>(async () => await UserManager.GetRolesAsync(cognitoUser))
                        .Result;            
                    
                    IsCurrentUserTabiriAuditor = roles.Any(y => y.ToLower() == "TabiriAuditor".ToLower());

                    IsCurrentUserCmuAuditor = roles.Any(y => y.ToLower() == "CmuAuditor".ToLower());
                }                                               
            }           
        }                   
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.