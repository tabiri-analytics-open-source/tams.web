using System;
using System.Collections.Generic;
using System.Linq;

namespace Tams.Web.ViewModels.DisplayViewModels
{
  public class SubcategoryViewModel : INavigationViewModel
  {
    public Guid Id { get; set; }  
    public string Title { get; set; }
    public string Objective { get; set; }   
    public string IsoMapping { get; set; }
    public int Order { get; set; }
    public Guid CategoryId { get; set; }

    #region Logging properties
    public Guid CreatedBy { get; set; }
    public DateTime CreatedTimestamp { get; set; }
    public Guid? LastUpdatedBy { get; set; }
    public DateTime? LastUpdatedTimestamp { get; set; }

    #endregion 

    public CategoryViewModel Category { get; set; }

    #region Question properties

    public List<QuestionViewModel> Questions { get; set; }

    public List<QuestionViewModel> RootQuestions => Questions.Where(x => x.IsTopLevelQuestion)
          .ToList();
    
    public QuestionViewModel CurrentRootQuestion => RootQuestions[currentQuestionIndex];
                
    public List<QuestionViewModel> CurrentQuestionsChildren
    { 
      get
      {
        if(CurrentRootQuestion.HasChildren)
        {
          return CurrentRootQuestion.Children
            .OrderBy(x => x.Order)
            .ToList();
        }

        return null;
      }
    } 
    public int CurrentQuestionIndex => currentQuestionIndex;        
    private int currentQuestionIndex;
    public int MaxQuestionIndex => RootQuestions.Count() - 1;
    public bool HasNextQuestion => currentQuestionIndex < MaxQuestionIndex;
    public bool HasPreviousQuestion => currentQuestionIndex > 0;        

    #endregion      

    public void NextQuestion() => currentQuestionIndex++;  

    public void PreviousQuestion() => currentQuestionIndex--;    
  }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.