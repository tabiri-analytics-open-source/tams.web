using System;

namespace Tams.Web.ViewModels.DisplayViewModels
{
    public class NistMappingViewModel : INavigationViewModel
    {
        public Guid Id { get; set; }  
        public string Title { get; set; }    
        public int Order { get; set; }

        #region Logging properties
        public Guid CreatedBy { get; set; }
        public DateTime CreatedTimestamp { get; set; }
        public Guid? LastUpdatedBy { get; set; }
        public DateTime? LastUpdatedTimestamp { get; set; }

        #endregion 
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.