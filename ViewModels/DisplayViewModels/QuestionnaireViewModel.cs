using System;
using System.Collections.Generic;
using System.Linq;

namespace Tams.Web.ViewModels.DisplayViewModels
{
  public class QuestionnaireViewModel
  {
    public Guid Id { get; set; }
    public string Title { get; set; }   
    
    #region Logging properties
    public Guid CreatedBy { get; set; }
    public DateTime CreatedTimestamp { get; set; }
    public Guid? LastUpdatedBy { get; set; }
    public DateTime? LastUpdatedTimestamp { get; set; }

    #endregion
    
    #region Category properties
    public List<CategoryViewModel> Categories { get; set; }
    public CategoryViewModel CurrentCategory => Categories[currentCategoryIndex];
    private int currentCategoryIndex;
    public int MaxCategoryIndex => Categories.Count() - 1;
    public bool HasNextCategory => currentCategoryIndex < MaxCategoryIndex;
    public bool HasPreviousCategory => currentCategoryIndex > 0;        

    #endregion 

    public void NextCategory()
    {
      if(!HasNextCategory)
          return;       

      currentCategoryIndex++;            
    }

    public void PreviousCategory()
    {
      if(!HasPreviousCategory)
          return;   

      currentCategoryIndex--;          
    }

    public void SetCurrentCategory(Guid categoryId) => currentCategoryIndex = Categories.FindIndex(x => x.Id == categoryId);
   
  }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.