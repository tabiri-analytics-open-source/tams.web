using System;
using System.Collections.Generic;
using System.Linq;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.ViewModels.DisplayViewModels
{
    public class QuestionViewModel
  {
    private const string UNDERSCORE = "_";
    private const string HYPHEN = "-";    
    char[] alphabet = "abcdefghijklmnopqrstuvwxyz".ToCharArray();
    string[] romanNumerals = "i,ii,iii,iv,v,vi,vii,viii,ix,x".Split(',');    
    public Guid Id { get; set; }  
    public string MainText { get; set; }
    public string HintText { get; set; }
    public string OptionChoices { get; set; }
    public int? ActiveOptionIndex { get; set; }
    public OptionsOrientation OptionsOrientation { get; set; }
    public int Order { get; set; }
    public Guid? ParentId { get; set; }    
    public Guid SubcategoryId { get; set; }
    public QuestionType QuestionType { get; set; }   

    #region Derived properties

    public string FullQuestionText => $"{BulletNumber}. {MainText}";

    public string ResponseText 
    {
      get
      {
        var response = Responses?.FirstOrDefault();
      
        return (response != null) ? 
          response.ResponseText : null;
      }
    }

    public List<string> Options => OptionChoices
      .Split(',')
      .Select(x => x.Trim())
      .ToList();  

    public string ActiveOption 
    {
      get 
      {
        if(ActiveOptionIndex == null)
          return null;
        
        return Options[(int) ActiveOptionIndex];                            
      }
    }

    #endregion

    #region Logging properties
    public Guid CreatedBy { get; set; }
    public DateTime CreatedTimestamp { get; set; }
    public Guid? LastUpdatedBy { get; set; }
    public DateTime? LastUpdatedTimestamp { get; set; }

    #endregion

    #region Display properties
    public bool IsTopLevelQuestion => Parent == null;
    public bool HasChildren => Children == null ? false : Children.Any();    
    public bool HasResponse => (Responses == null) ? false : Responses.Any();    
    public string DisplayInputType
    {
      get
      {
        if(QuestionType == QuestionType.SelectSingle)
          return "radio";

        if(QuestionType == QuestionType.SelectMultiple)
          return "checkbox";

        return null;
      }
    }
    public string DisplayInputId => $"response_{Id.ToString().Replace(HYPHEN,UNDERSCORE)}";
    
    public bool IsOptionQuestion => DisplayInputType != null;    
    public bool IsDisplayOptionChecked(string option)
    {
      if (!HasResponse)
        return false;

      return Responses
        .Select(x => x.ResponseText)
        .Any(y => y?.ToLower().Trim() == option.ToLower().Trim()); 
    }

    public string DisplayResponse => (Responses == null || !Responses.Any()) ? 
      null : Responses.FirstOrDefault().ResponseText;     

    public bool IsNumericInputType => QuestionType == QuestionType.Integer || 
      QuestionType == QuestionType.Percentage || 
      QuestionType == QuestionType.Currency; 
   
    public bool IsDateInputType => QuestionType == QuestionType.Date;

    public bool HasParent => Parent != null;

    public DisplayIndentLevel DisplayIndentLevel
    {
      get
      {
        if(!HasParent)        
          return DisplayIndentLevel.One;        

        if(!Parent.HasParent)
          return DisplayIndentLevel.Two;
        
        if(!Parent.Parent.HasParent)
          return DisplayIndentLevel.Three;

        if(!Parent.Parent.Parent.HasParent)
          return DisplayIndentLevel.Four;

        return DisplayIndentLevel.Invalid;
      }
    }

    public string BulletNumber
    {
      get
      {
        if(DisplayIndentLevel == DisplayIndentLevel.One)
          return Order.ToString();

        if(DisplayIndentLevel == DisplayIndentLevel.Two)
          return alphabet[Order - 1].ToString();

        if(DisplayIndentLevel == DisplayIndentLevel.Three)
          return romanNumerals[Order - 1].ToString();
        
        return Order.ToString();
        
      }
    }

    public bool ShowChildren 
    { 
      get
      {
        if (!HasChildren)
          return false;

        if(ActiveOption == null)
          return true;

        if(QuestionType == QuestionType.SelectSingle)
        {     
          return HasResponse ? 
            ResponseText == ActiveOption : false;
        }

        if(QuestionType == QuestionType.SelectMultiple)
        {    
          return Responses
            .Select(x => x.ResponseText)
            .Any(y => y == ActiveOption);
        }

        return true;
      }
    }

    public bool IsLastQuestion { get; set; }  

    #endregion 

    #region Navigation properties
    public SubcategoryViewModel Subcategory { get; set; }   
    public List<ResponseViewModel> Responses { get; set; }    
    public QuestionViewModel Parent { get; set; }
    public List<QuestionViewModel> Children { get; set; }

    #endregion

    public List<ResponseViewModel> FlattenResponses()
    {      
      var responses = new List<ResponseViewModel>();

      if(this.Responses.Any())
      {
        responses.AddRange(this.Responses);
      }
      else
      {
        var response = new ResponseViewModel
        { 
          QuestionId = this.Id,
          ResponseText = null,
          CreatedTimestamp = DateTime.UtcNow
        };

        responses.Add(response);
      }      

      if(this.HasChildren)
      {
        var childResponses = Children
          .SelectMany(x => x.FlattenResponses())
          .ToList();

        responses.AddRange(childResponses);
      }      

      return responses;
    }   

    public string ClearChildResponses()
    {
      if(!HasChildren)
        return null;   

      foreach(var child in Children)
      {
        child.Responses
          .ForEach(x => x.ResponseText = null);

        child.ClearChildResponses();
      }  

      return null;
    }                   
  }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.