using System.Collections.Generic;
using System.Linq;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.ViewModels.ReportViewModels
{
    public class ReportQuestionViewModel
    {
        private char[] alphabet = "abcdefghijklmnopqrstuvwxyz".ToCharArray();
        private string[] romanNumerals = "i,ii,iii,iv,v,vi,vii,viii,ix,x".Split(',');   
        public string MainText { get; set; }
        public string HintText { get; set; }
        public string OptionChoices { get; set; }
        public int? ActiveOptionIndex { get; set; }
        public int Order { get; set; }     
        public QuestionType QuestionType { get; set; }   

        #region Derived properties

        public string FullQuestionText => $"{BulletNumber}. {MainText}";

        public string ResponseText => (QuestionType == QuestionType.SelectMultiple) ?  
            GetConcatenatedResponseText(ReportResponses) : GetResponseText(ReportResponses);

        public List<string> Options => OptionChoices
            .Split(',')
            .Select(x => x.Trim())
            .ToList();  

        public string ActiveOption => (ActiveOptionIndex != null) ? 
            Options[(int) ActiveOptionIndex] : null;      

        #endregion     

        #region Display properties
        public bool HasParent => Parent != null;
        public bool HasChildren => (Children != null) ? 
            Children.Any() : false;    
        public bool HasResponse => (ReportResponses != null) ? 
            ReportResponses.Any() : false;
        public DisplayIndentLevel DisplayIndentLevel
        {
            get
            {
                if(!HasParent)        
                    return DisplayIndentLevel.One;        

                if(!Parent.HasParent)
                    return DisplayIndentLevel.Two;
                
                if(!Parent.Parent.HasParent)
                    return DisplayIndentLevel.Three;

                if(!Parent.Parent.Parent.HasParent)
                    return DisplayIndentLevel.Four;

                return DisplayIndentLevel.Invalid;
            }
        }

        public string BulletNumber
        {
            get
            {
                if(DisplayIndentLevel == DisplayIndentLevel.One)
                    return Order.ToString();

                if(DisplayIndentLevel == DisplayIndentLevel.Two)
                    return alphabet[Order - 1].ToString();

                if(DisplayIndentLevel == DisplayIndentLevel.Three)
                    return romanNumerals[Order - 1].ToString();
                
                return Order.ToString();                
            }
        }

        public bool ShowChildren 
        { 
            get
            {
                if (!HasChildren)
                    return false;

                if(ActiveOption == null)
                    return true;

                if(QuestionType == QuestionType.SelectSingle)
                {     
                    return HasResponse ? 
                        ResponseText == ActiveOption : false;
                }

                if(QuestionType == QuestionType.SelectMultiple)
                {    
                    return ReportResponses
                        .Select(x => x.ResponseText)
                        .Any(y => y == ActiveOption);
                }

                return true;
            }
        }        

        #endregion 

        #region Navigation properties

        public List<ReportResponseViewModel> ReportResponses { get; set; }    
        public ReportQuestionViewModel Parent { get; set; }
        public List<ReportQuestionViewModel> Children { get; set; }

        #endregion  

        #region Helper functions

        private string GetConcatenatedResponseText(List<ReportResponseViewModel> reportResponses)
        {
            string responseText = null;

            for (int i = 0; i < reportResponses.Count(); i++)
            {
                if(i == 0)
                {
                    responseText = reportResponses[i].ResponseText;
                }
                else
                {
                    responseText += $", {reportResponses[i].ResponseText}";
                }
            }                 

            return responseText;
        }  

        private string GetResponseText(List<ReportResponseViewModel> reportResponses)
        {
           var response = ReportResponses?.FirstOrDefault();
            
            return (response != null) ? 
                response.ResponseText : null;  
        }    

        #endregion          
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.