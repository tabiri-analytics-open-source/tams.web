using Amazon.AspNetCore.Identity.Cognito;
using Amazon.Extensions.CognitoAuthentication;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Tams.Web.BusinessLayer;

namespace Tams.Web.Areas.Identity.Pages.Account
{
  [AllowAnonymous]  
  public class LoginModel : PageModel
  {
    private readonly CognitoUserManager<CognitoUser> _userManager;
    private readonly SignInManager<CognitoUser> _signInManager;
    private IDataService _dataService; 
    private readonly ILogger<LoginModel> _logger;

    public LoginModel(SignInManager<CognitoUser> signInManager, 
        UserManager<CognitoUser> userManger, 
        IDataService dataService,
        ILogger<LoginModel> logger)
    {
      _signInManager = signInManager;
      _userManager = userManger as CognitoUserManager<CognitoUser>;
      _dataService = dataService;
      _logger = logger;
    }

    [BindProperty]
    public InputModel Input { get; set; }

    public IList<AuthenticationScheme> ExternalLogins { get; set; }

    public string ReturnUrl { get; set; }

    [TempData]
    public string ErrorMessage { get; set; }

    public class InputModel
    {
      [Required]
      public string UserEmail { get; set; }

      [Required]
      [DataType(DataType.Password)]
      public string Password { get; set; }

      [Display(Name = "Remember me?")]
      public bool RememberMe { get; set; }
    }

    public async Task OnGetAsync(string returnUrl = null)
    {
      if (!string.IsNullOrEmpty(ErrorMessage))
      {
        ModelState.AddModelError(string.Empty, ErrorMessage);
      }

      returnUrl = returnUrl ?? Url.Content("~/");

      // Clear the existing external cookie to ensure a clean login process
      await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme).ConfigureAwait(false);

      ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync().ConfigureAwait(false)).ToList();

      ReturnUrl = returnUrl;
    }

    public async Task<IActionResult> OnPostAsync(string returnUrl = null)
    {
      returnUrl = returnUrl ?? Url.Content("~/");

      if (ModelState.IsValid)
      {
        var user = await _userManager.FindByEmailAsync(Input.UserEmail);
        if(user == null)
        {
          ModelState.AddModelError(string.Empty, "Invalid login attempt.");
          return Page();
        }

        var userId = Guid.Parse(user.UserID);
        var person = _dataService.GetPerson(userId);
        var userName = $"{person.FirstName} {person.LastName}";

        var result = await _signInManager.PasswordSignInAsync(user, Input.Password, Input.RememberMe, lockoutOnFailure: false);

        if (result.Succeeded)
        {          
          _logger.LogInformation($"User {userName} (UserId {user.UserID}) logged in.");
          
          return LocalRedirect(returnUrl);
        }
        else if (result.RequiresTwoFactor)
        {
          return RedirectToPage("./LoginWith2fa", new { ReturnUrl = returnUrl, RememberMe = Input.RememberMe });
        }
        else if (result.IsCognitoSignInResult())
        {
          if (result is CognitoSignInResult cognitoResult)
          {
            if (cognitoResult.RequiresPasswordChange)
            {
              _logger.LogWarning($"User password for user {userName} (UserId {user.UserID}) needs to be changed");
              return RedirectToPage("./Manage/ChangePassword");
            }
            else if (cognitoResult.RequiresPasswordReset)
            {
              _logger.LogWarning($"User password for user {userName} (UserId {user.UserID}) needs to be reset");
              return RedirectToPage("./ResetPassword");
            }
          }

        }

        ModelState.AddModelError(string.Empty, "Invalid login attempt.");
        return Page();
      }

      // If we got this far, something failed, redisplay form
      return Page();
    }
  }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.