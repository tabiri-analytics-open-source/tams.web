using System;
using System.Threading.Tasks;
using Amazon.Extensions.CognitoAuthentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Tams.Web.BusinessLayer;

namespace Tams.Web.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    [IgnoreAntiforgeryToken]
    public class LogoutModel : PageModel
    {
        private readonly SignInManager<CognitoUser> _signInManager;
        private IDataService _dataService; 
        private readonly ILogger<LogoutModel> _logger;

        public LogoutModel(SignInManager<CognitoUser> signInManager, 
            IDataService dataService,
            ILogger<LogoutModel> logger)
        {
            _signInManager = signInManager;
            _dataService = dataService;
            _logger = logger;
        }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPost(string returnUrl = null)
        {
            var userId = Guid.Parse(User.Identity.Name);
            var person = _dataService.GetPerson(userId);
            var userName = $"{person.FirstName} {person.LastName}";

            await _signInManager.SignOutAsync();

            _logger.LogInformation($"User {userName} (UserId {User.Identity.Name}) logged out.");
            
            if (returnUrl != null)
            {
                return LocalRedirect(returnUrl);
            }
            else
            {
                return RedirectToPage();
            }
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.