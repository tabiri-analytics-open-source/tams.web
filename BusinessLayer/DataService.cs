using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Tams.Web.DataLayer;
using Tams.Web.DataLayer.DomainModels;
using System.Linq;
using System;
using AutoMapper;
using Tams.Web.ViewModels.DisplayViewModels;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.BusinessLayer
{
    public class DataService : IDataService
    {
        private readonly ILogger<DataService> _logger;
        private readonly OperationalAssessmentContext _context;
        private readonly IMapper _mapper;
        public DataService(
            ILogger<DataService> logger, 
            OperationalAssessmentContext context, 
            IMapper mapper)
        {
            _logger = logger;
            _context = context;
            _mapper = mapper;

            context.Database.EnsureCreated();
        }

        public Person GetPerson(Guid personId) =>
            _context.Person
                .SingleOrDefault(x => x.Id == personId);

        public Questionnaire GetQuestionnaire(Guid orgId, Guid questionnaireId = default(Guid))
        {
            var org = GetOrganizationGraph(orgId);

            Questionnaire questionnaire;

            if(questionnaireId == Guid.Empty)
            {
                questionnaire = org
                    .Questionnaires
                    .FirstOrDefault();
            }
            else
            {
                questionnaire = org
                    .Questionnaires
                    .SingleOrDefault(x => x.Id == questionnaireId);
            } 

            return questionnaire;
        }       

        public Organization GetOrganizationByUserId(Guid userId)
        {
            var person = _context.Person
              .Include(x => x.Organization)
              .SingleOrDefault(y => y.Id == userId);

            return (person != null) ?
                person.Organization : null;
        }   

        public Organization GetOrganizationByOrgId(Guid orgId) => 
            _context.Organizations.FirstOrDefault(x => x.Id == orgId);        

        public List<Organization> GetOrganizationsWithQuestionnaires(OrganizationType orgType) =>
            _context.Organizations
                .Include(x => x.Questionnaires)
                .Where(x => x.OrganizationType == orgType)
                .Where(x => x.Questionnaires.Any())
                .ToList();

        public List<CybersecurityStandard> GetCybersecurityStandards(Guid subcategoryId, CybersecurityStandardsType standardsType) =>
            _context.CybersecurityStandards
                .Include(x => x.Subcategory)
                .Where(x => x.StandardsType == standardsType)
                .Where(x => x.SubcategoryId == subcategoryId)
                .OrderBy(x => x.Order)
                .ToList();

        public List<ScoringRubric> GetScoringRubrics(Guid subcategoryId) => 
            _context.ScoringRubrics
                .Include(x => x.Subcategory)
                .Where(x => x.SubcategoryId == subcategoryId)
                .OrderBy(x => x.Order)
                .ToList();

        public void PersistResponses(List<ResponseViewModel> responses, Guid userId)
        {
            var insertionViewModels = responses
                .Where(x => x.HasValue)
                .ToList();

            var insertionEntities = _mapper.Map<List<Response>>(insertionViewModels);
            _context.AddOrUpdate<Response>(insertionEntities, userId);

            var deletionViewModels = responses
                .Where(x => !x.HasValue)
                .ToList();

            var deletionEntities = _mapper.Map<List<Response>>(deletionViewModels);
            _context.Responses.DeleteIfExists(deletionEntities);

            _context.SaveChanges();
        }

        #region Helper functions
        private Organization GetOrganizationGraph(Guid orgId)
        {
            var org = _context.Organizations
                .Include(x => x.Questionnaires)
                    .ThenInclude(x => x.Categories.OrderBy(y => y.Order))
                    .ThenInclude(x => x.Subcategories.OrderBy(y => y.Order))
                    .ThenInclude(x => x.Questions)
                    .ThenInclude(x => x.Parent)
                .Include(x => x.Questionnaires)
                    .ThenInclude(x => x.Categories.OrderBy(y => y.Order))
                    .ThenInclude(x => x.Subcategories.OrderBy(y => y.Order))
                    .ThenInclude(x => x.Questions)
                    .ThenInclude(x => x.Responses)                            
                .SingleOrDefault(x => x.Id == orgId);

            return new Organization
            {
                Name = org.Name,
                CurrentQuestionnaireId = org.CurrentQuestionnaireId,
                OrganizationType = org.OrganizationType,
                Persons = org.Persons,                
                Questionnaires = org
                    .Questionnaires
                    .Select(x => OrderAndFilterQuestionnaireByOrgId(x, orgId))
                    .ToList()
            };           
        }

        private Questionnaire OrderAndFilterQuestionnaireByOrgId(Questionnaire questionnaire, Guid orgId)
        {
            var prunedCategories = questionnaire.Categories
                .Select(x => new Category
                {
                    Id = x.Id,
                    Title = x.Title,
                    Objective = x.Objective,
                    ExecutiveSummary = x.ExecutiveSummary,
                    Order = x.Order,
                    QuestionnaireId = x.QuestionnaireId,
                    Questionnaire = x.Questionnaire,
                    Subcategories = x.Subcategories.Select(y => new Subcategory
                    {
                        Id = y.Id,
                        Title = y.Title,
                        Objective = y.Objective,
                        CybersecurityStandards = y.CybersecurityStandards,
                        Order = y.Order,
                        CategoryId = y.CategoryId,
                        Questions = FilterResponsesByOrgId(y.Questions, orgId),
                        CreatedBy = y.CreatedBy,
                        CreatedTimestamp = y.CreatedTimestamp,
                        LastUpdatedBy = y.LastUpdatedBy,
                        LastUpdatedTimestamp = y.LastUpdatedTimestamp
                    })                
                .ToList(),
                    CreatedBy = x.CreatedBy,
                    CreatedTimestamp = x.CreatedTimestamp,
                    LastUpdatedBy = x.LastUpdatedBy,
                    LastUpdatedTimestamp = x.LastUpdatedTimestamp
                })                
                .ToList();           

            var categories = prunedCategories
                .Select(x => new Category
                {
                    Id = x.Id,
                    Title = x.Title,
                    Objective = x.Objective,
                    ExecutiveSummary = x.ExecutiveSummary,
                    Order = x.Order,
                    QuestionnaireId = x.QuestionnaireId,
                    Questionnaire = x.Questionnaire,
                    Subcategories = x.Subcategories.Select(y => new Subcategory
                    {
                        Id = y.Id,
                        Title = y.Title,
                        Objective = y.Objective,
                        CybersecurityStandards = y.CybersecurityStandards,
                        Order = y.Order,
                        CategoryId = y.CategoryId,
                        Questions = OrderQuestions(y.Questions),
                        CreatedBy = y.CreatedBy,
                        CreatedTimestamp = y.CreatedTimestamp,
                        LastUpdatedBy = y.LastUpdatedBy,
                        LastUpdatedTimestamp = y.LastUpdatedTimestamp
                    })
                    .OrderBy(x => x.Order)
                    .ToList(),
                    CreatedBy = x.CreatedBy,
                    CreatedTimestamp = x.CreatedTimestamp,
                    LastUpdatedBy = x.LastUpdatedBy,
                    LastUpdatedTimestamp = x.LastUpdatedTimestamp
                })
                .OrderBy(x => x.Order)
                .ToList();

            return new Questionnaire
            {
                Id = questionnaire.Id,
                Title = questionnaire.Title,
                CreatedBy = questionnaire.CreatedBy,
                CreatedTimestamp = questionnaire.CreatedTimestamp,
                LastUpdatedBy = questionnaire.LastUpdatedBy,
                LastUpdatedTimestamp = questionnaire.LastUpdatedTimestamp,
                Categories = categories
            };
        }

        private ICollection<Question> OrderQuestions(ICollection<Question> questions)
        {
            if (questions == null)
                return null;

            return questions.Select(x => new Question
            {
                Id = x.Id,
                MainText = x.MainText,
                HintText = x.HintText,
                OptionChoices = x.OptionChoices,
                ActiveOptionIndex = x.ActiveOptionIndex,
                OptionsOrientation = x.OptionsOrientation,
                Order = x.Order,
                ParentId = x.ParentId,
                SubcategoryId = x.SubcategoryId,
                QuestionType = x.QuestionType,
                Subcategory = x.Subcategory,
                Responses = x.Responses,
                Parent = x.Parent,
                Children = OrderQuestions(x.Children),
                CreatedBy = x.CreatedBy,
                CreatedTimestamp = x.CreatedTimestamp,
                LastUpdatedBy = x.LastUpdatedBy,
                LastUpdatedTimestamp = x.LastUpdatedTimestamp
            })
            .OrderBy(x => x.Order)
            .ToList();
        }

        private ICollection<Question> FilterResponsesByOrgId(ICollection<Question> questions, Guid organizationId)
        {
            if (questions == null)
                return null;

            return questions.Select(x => new Question
            {
                Id = x.Id,
                MainText = x.MainText,
                HintText = x.HintText,
                OptionChoices = x.OptionChoices,
                ActiveOptionIndex = x.ActiveOptionIndex,
                OptionsOrientation = x.OptionsOrientation,
                Order = x.Order,
                ParentId = x.ParentId,
                SubcategoryId = x.SubcategoryId,
                QuestionType = x.QuestionType,
                Subcategory = x.Subcategory,                
                Responses = x.Responses
                  .Where(y => y.OrgId == organizationId)
                  .ToList(),
                Parent = x.Parent,
                Children = FilterResponsesByOrgId(x.Children, organizationId),
                CreatedBy = x.CreatedBy,
                CreatedTimestamp = x.CreatedTimestamp,
                LastUpdatedBy = x.LastUpdatedBy,
                LastUpdatedTimestamp = x.LastUpdatedTimestamp
            })             
            .ToList();
        }

        #endregion          
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.