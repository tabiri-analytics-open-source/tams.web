using System;
using System.Collections.Generic;
using AutoMapper;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;
using Tams.Web.ViewModels.DisplayViewModels;
using Tams.Web.ViewModels.ReportViewModels;

namespace Tams.Web.BusinessLayer
{
    public class StateContainer : IStateContainer
    {        
        public Guid UserId { get; set; }
        public Guid OrgId { get; set; }
        public Guid QuestionnaireId { get; set; } 
        public QuestionnaireViewModel Questionnaire { get; set; }        
        public event Action OnChange;

        #region Category properties
        public List<CategoryViewModel> Categories 
            => Questionnaire?.Categories;       
        public CategoryViewModel CurrentCategory 
            => Questionnaire.CurrentCategory; 
        public bool HasNextCategory 
            => Questionnaire.HasNextCategory;
        public bool HasPreviousCategory 
            => Questionnaire.HasPreviousCategory;   

        #endregion 
        
        #region Subcategory properties
        public List<SubcategoryViewModel> Subcategories 
            => CurrentCategory.Subcategories;       
        public SubcategoryViewModel CurrentSubcategory 
            => CurrentCategory.CurrentSubcategory;
        public bool HasNextSubcategory 
            => CurrentCategory.HasNextSubcategory;
        public bool HasPreviousSubcategory 
            => CurrentCategory.HasPreviousSubcategory;         

        #endregion

        #region Question properties
        public List<QuestionViewModel> RootQuestions 
            => CurrentSubcategory.RootQuestions;
        public QuestionViewModel CurrentRootQuestion 
            => CurrentSubcategory.CurrentRootQuestion;    
        public bool HasNextQuestion 
            => CurrentSubcategory.HasNextQuestion;
        public bool HasPreviousQuestion 
            => CurrentSubcategory.HasPreviousQuestion;    

        #endregion

        public ReportQuestionnaireViewModel ReportQuestionnaire { get; set; }         

        private IDataService _service;
        private IMapper _mapper;

        public StateContainer(IDataService service, IMapper mapper)
        { 
            _service = service;  
            _mapper = mapper;                    
        }    

        public void Init(
            Guid userId, 
            Guid orgId = default(Guid), 
            Guid questionnaireId = default(Guid)) 
        { 
            UserId = userId; 
           
            OrgId = (orgId == Guid.Empty) ? 
                _service.GetOrganizationByUserId(userId).Id : orgId; 

            Questionnaire = (questionnaireId == Guid.Empty) ? 
                GetQuestionnaireViewModel(OrgId) : GetQuestionnaireViewModel(OrgId, questionnaireId); 

            QuestionnaireId = Questionnaire.Id;            
            ReportQuestionnaire = GetReportQuestionnaireViewModel(OrgId, QuestionnaireId);            
        }  

        #region Category functions

        public void NextCategory() 
        { 
            Questionnaire.NextCategory();
            NotifyStateChanged(); 
        }
        public void PreviousCategory() 
        { 
            Questionnaire.PreviousCategory(); 
            NotifyStateChanged(); 
        }

        public void SetCurrentCategory(Guid categoryId) 
        {
            Questionnaire.SetCurrentCategory(categoryId);  
            NotifyStateChanged(); 
        }

        #endregion 

        #region Subcategory functions    

        public void NextSubcategory() 
        { 
            CurrentCategory.NextSubcategory(); 
            NotifyStateChanged(); 
        }
        public void PreviousSubcategory() 
        { 
            CurrentCategory.PreviousSubcategory(); 
            NotifyStateChanged(); 
        }
        public void SetCurrentSubcategory(Guid subcategoryId) 
        {
            Questionnaire.CurrentCategory.SetCurrentSubategory(subcategoryId);
            NotifyStateChanged();
        }

        public List<NistMappingViewModel> GetCurrentNistMappings()
        {
            var nistMappings = 
                _service.GetCybersecurityStandards(CurrentSubcategory.Id, CybersecurityStandardsType.Nist800_53);

            return _mapper.Map<List<NistMappingViewModel>>(nistMappings);            
        }

        public List<NistMappingViewModel> GetCurrentIsoIecMappings()
        {
            var isoIecMappings = 
                _service.GetCybersecurityStandards(CurrentSubcategory.Id, CybersecurityStandardsType.IsoIec27001);

            return _mapper.Map<List<NistMappingViewModel>>(isoIecMappings);            
        }

        public List<ReportScoringRubricViewModel> GetScoringRubricsBySubcategory(Guid subcategoryId)
        {
            var scoringRubrics = _service.GetScoringRubrics(subcategoryId);

            return _mapper.Map<List<ReportScoringRubricViewModel>>(scoringRubrics);                      
        }

        #endregion  

        #region Question functions 

        public void NextQuestion() 
        { 
            if(!CurrentSubcategory.HasNextQuestion)
                return;

            var responses = CurrentRootQuestion.FlattenResponses();
            PersistResponses(responses); 

            CurrentSubcategory.NextQuestion();
            NotifyStateChanged(); 
        }

        public void PreviousQuestion() 
        { 
            if(!CurrentSubcategory.HasPreviousQuestion)
                return;

            CurrentSubcategory.PreviousQuestion();
            NotifyStateChanged(); 
        }  

        #endregion        

        public void PersistResponses(List<ResponseViewModel> responses)
        {
            foreach(var response in responses)
            {
                response.OrgId = OrgId;
                response.QuestionnaireId = QuestionnaireId;
                response.CreatedBy = UserId;
            }
            
            _service.PersistResponses(responses, UserId);
        }

        public ReportQuestionnaireViewModel GetReportQuestionnaireViewModel(Guid orgId, Guid questionnaireId) 
        { 
            var org = _service.GetOrganizationByOrgId(orgId);

            var report = _service.GetQuestionnaire(orgId, questionnaireId);  
            var reportViewModel = _mapper.Map<ReportQuestionnaireViewModel>(report);

            reportViewModel.OrgId = orgId;
            reportViewModel.OrgName = org.Name;
            reportViewModel.QuestionnaireId = questionnaireId;

            return reportViewModel;
        } 

        #region Helper functions 

        private QuestionnaireViewModel GetQuestionnaireViewModel(Guid orgId, Guid questionnaireId = default(Guid)) 
        { 
            var questionnaire = _service.GetQuestionnaire(orgId, questionnaireId);  
            var questionnaireViewModel = _mapper.Map<QuestionnaireViewModel>(questionnaire); 

            questionnaireViewModel.Categories
                .ForEach(x => x.Subcategories
                    .ForEach(y => y.Questions.Add(
                        new QuestionViewModel 
                        { 
                            IsLastQuestion = true
                        }
                    ))
                ); 

            return questionnaireViewModel;
        }        

        private void NotifyStateChanged() 
            => OnChange?.Invoke();   

        #endregion   
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.