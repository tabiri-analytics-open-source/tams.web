using System;
using System.Collections.Generic;
using Tams.Web.ViewModels.DisplayViewModels;
using Tams.Web.ViewModels.ReportViewModels;

namespace Tams.Web.BusinessLayer
{
    public interface IStateContainer
  {
    Guid UserId { get; set; }
    Guid QuestionnaireId { get; set; } 
    Guid OrgId { get; set; }
    QuestionnaireViewModel Questionnaire { get; set; } 
    event Action OnChange;

    #region Category properties

    List<CategoryViewModel> Categories { get; }
    CategoryViewModel CurrentCategory { get; }  
    bool HasNextCategory { get; } 
    bool HasPreviousCategory { get; } 

    #endregion

    #region Subcategory properties
    List<SubcategoryViewModel> Subcategories { get; }
    SubcategoryViewModel CurrentSubcategory { get; } 
    bool HasNextSubcategory { get; } 
    bool HasPreviousSubcategory { get; }    

    #endregion

    #region Question properties
    List<QuestionViewModel> RootQuestions { get; } 
    QuestionViewModel CurrentRootQuestion { get; }       
    bool HasNextQuestion { get; } 
    bool HasPreviousQuestion { get; } 

    #endregion    

    ReportQuestionnaireViewModel ReportQuestionnaire { get; set; } 

    void Init(Guid userId, Guid orgId = default(Guid), Guid questionnaireId = default(Guid));

    #region Category functions
    void NextCategory();
    void PreviousCategory();
    void SetCurrentCategory(Guid categoryId);

    #endregion

    #region Subcategory functions 
    void NextSubcategory();
    void PreviousSubcategory();
    void SetCurrentSubcategory(Guid subcategoryId);
    List<NistMappingViewModel> GetCurrentNistMappings();
    List<NistMappingViewModel> GetCurrentIsoIecMappings();
    List<ReportScoringRubricViewModel> GetScoringRubricsBySubcategory(Guid subcategoryId);

    #endregion

    #region Question functions
    void NextQuestion();
    void PreviousQuestion();

    #endregion
    void PersistResponses(List<ResponseViewModel> responses);
    ReportQuestionnaireViewModel GetReportQuestionnaireViewModel(Guid orgId, Guid questionnaireId);   
  }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.