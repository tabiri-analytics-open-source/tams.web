
using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;
using Tams.Web.ViewModels.DisplayViewModels;

namespace Tams.Web.BusinessLayer
{
    public interface IDataService
  { 
    Person GetPerson(Guid personId);
    Organization GetOrganizationByUserId(Guid userId);   
    Organization GetOrganizationByOrgId(Guid orgId);      
    List<Organization> GetOrganizationsWithQuestionnaires(OrganizationType orgType);   
    Questionnaire GetQuestionnaire(Guid orgId, Guid questionnaireId = default(Guid));   
    List<CybersecurityStandard> GetCybersecurityStandards(Guid subcategoryId, CybersecurityStandardsType standardsType);
    List<ScoringRubric> GetScoringRubrics(Guid subcategoryId);
    void PersistResponses(List<ResponseViewModel> responses, Guid userId);
  }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.