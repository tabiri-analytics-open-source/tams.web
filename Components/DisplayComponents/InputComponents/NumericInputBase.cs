using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;
using Tams.Web.ViewModels.DisplayViewModels;

namespace Tams.Web.Components.DisplayComponents.InputComponents
{
    public class NumericInputBase : ComponentBase
    {
        #region Cascading Parameters
        
        [CascadingParameter(Name = "UserId")]
        public Guid UserId { get; set; } 

        [CascadingParameter(Name = "OrgId")]
        public Guid OrgId { get; set; } 

        [CascadingParameter(Name = "QuestionnaireId")]
        public Guid QuestionnaireId { get; set; } 

        #endregion

        [Parameter]
        public QuestionViewModel Question { get; set; }           

        #region EventCallbacks       

        [Parameter]
        public EventCallback<QuestionViewModel> QuestionChanged { get; set; }

        [Parameter]
        public EventCallback<QuestionViewModel> OnInputChanged { get; set; }

        #endregion

        public Dictionary<string, object> InputAttributes { get; set; }                 

        protected override void OnInitialized()
        {            
            InputAttributes = new Dictionary<string, object>() 
                {   
                    { "type", "number" },                 
                    { "class", "form-control" },
                    { "name", Question.DisplayInputId },   
                    { "value", Question.DisplayResponse }, 
                    { "min", "0" },                    
                    { "required", true }
                };  
            
            if(Question.QuestionType == QuestionType.Integer)            
                InputAttributes.Add("step", "1");            

            if(Question.QuestionType == QuestionType.Currency)            
               InputAttributes.Add("step", "any");            

            if(Question.QuestionType == QuestionType.Percentage)            
                InputAttributes.Add("max","100");            

            base.OnInitialized();       
        } 

        protected async Task InputChanged(ChangeEventArgs args)
        {
            if(Question.Responses.Any())
            {
                var response = Question.Responses
                    .FirstOrDefault();
                    
                response.ResponseText = args.Value.ToString(); 
                response.LastUpdatedBy = UserId;  
                response.LastUpdatedTimestamp = DateTime.UtcNow;
            }
            else
            {
                Question.Responses.Add(
                    new ResponseViewModel
                    {
                        Id = Guid.NewGuid(),
                        OrgId = OrgId,
                        QuestionnaireId = QuestionnaireId,
                        QuestionId = Question.Id,
                        ResponseText = args.Value.ToString(),
                        CreatedBy = UserId,
                        CreatedTimestamp = DateTime.UtcNow
                    });
            }          
                   
            await QuestionChanged.InvokeAsync(Question); 

            await OnInputChanged.InvokeAsync(Question);
        }         
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.