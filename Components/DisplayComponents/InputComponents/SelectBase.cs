using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;
using Tams.Web.ViewModels.DisplayViewModels;

namespace Tams.Web.Components.DisplayComponents.InputComponents
{
    public class SelectBase : ComponentBase
    {
        #region Cascading Parameters
        
        [CascadingParameter(Name = "UserId")]
        public Guid UserId { get; set; } 

        [CascadingParameter(Name = "OrgId")]
        public Guid OrgId { get; set; } 

        [CascadingParameter(Name = "QuestionnaireId")]
        public Guid QuestionnaireId { get; set; } 

        #endregion

        #region Component Parameters

        [Parameter]
        public QuestionViewModel Question { get; set; }  

        [Parameter]
        public string Option { get; set; }  
        
        #endregion        

        #region EventCallbacks

        [Parameter]
        public EventCallback<QuestionViewModel> QuestionChanged { get; set; }

        [Parameter]
        public EventCallback<QuestionViewModel> OnInputChanged { get; set; }        

        #endregion
        
        #region HTML Tag Attributes
        public string InputDivClass { get; set; }   
        public Dictionary<string, object> InputAttributes { get; set; }       

        #endregion

        protected override void OnInitialized()
        {            
            InputAttributes = new Dictionary<string, object>() 
                {
                    { "type", "checkbox" },
                    { "class", "form-check-input" },
                    { "name", Question.DisplayInputId },
                    { "value", Option },
                    { "required", true }
                };   
            
            InputDivClass = Question.OptionsOrientation == OptionsOrientation.Vertical? 
                "form-check" : "form-check-inline";

            base.OnInitialized();       
        }

        public async Task InputChanged(ChangeEventArgs args)
        { 
            if((bool) args.Value)
            {
                Question.Responses.Add(
                    new ResponseViewModel
                    {
                        Id = Guid.NewGuid(),
                        OrgId = OrgId,
                        QuestionnaireId = QuestionnaireId,
                        QuestionId = Question.Id,
                        ResponseText = Option,
                        CreatedBy = UserId,
                        CreatedTimestamp = DateTime.UtcNow
                    });
            }
            else
            {
                var response = Question.Responses
                    .SingleOrDefault(x => x.ResponseText == Option);
                    
                response.ResponseText = null;
            }               

            await QuestionChanged.InvokeAsync(Question);  
               
            await OnInputChanged.InvokeAsync(Question);           
        }           
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.