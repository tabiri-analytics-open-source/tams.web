using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Tams.Web.BusinessLayer;
using Tams.Web.ViewModels.DisplayViewModels;

namespace Tams.Web.Components.DisplayComponents
{
    public class DisplaySubcategoryBase : ComponentBase
    { 
        [Parameter]
        public IStateContainer StateContainer { get; set; }   
        public List<NistMappingViewModel> NistMappingViewModels => 
            StateContainer.GetCurrentNistMappings();        
        public List<NistMappingViewModel> IsoIecMappingViewModels => 
            StateContainer.GetCurrentIsoIecMappings();
        public IEnumerable<INavigationViewModel> NavigationViewModels => 
            StateContainer.Subcategories.Cast<INavigationViewModel>();
        protected override void OnInitialized() => 
            StateContainer.OnChange += StateHasChanged;
        public void Dispose() => 
            StateContainer.OnChange -= StateHasChanged; 
        public void BreadcrumbClicked(MouseEventArgs args, Guid subcategoryId) => 
            StateContainer.SetCurrentSubcategory(subcategoryId);
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.