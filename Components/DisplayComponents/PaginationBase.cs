using Microsoft.AspNetCore.Components;
using Tams.Web.BusinessLayer;

namespace Tams.Web.Components.DisplayComponents
{
    public class PaginationBase : ComponentBase
    {       
        [Parameter]
        public IStateContainer StateContainer { get; set; } 
        public string QuestionnaireURL => 
            "/operational-assessment/questionnaire";
        protected override void OnInitialized() => StateContainer.OnChange += StateHasChanged;

        public void Dispose() => StateContainer.OnChange -= StateHasChanged;

        protected void NextQuestionButtonClicked() => StateContainer.NextQuestion();    

        protected void PreviousQuestionButtonClicked() => StateContainer.PreviousQuestion(); 
        
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.