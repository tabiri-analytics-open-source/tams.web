using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Tams.Web.BusinessLayer;
using Tams.Web.ViewModels.DisplayViewModels;

namespace Tams.Web.Components.DisplayComponents
{
    public class DisplayQuestionBase : ComponentBase
    {
        [Parameter]
        public IStateContainer StateContainer { get; set; }  
        
        [Parameter]
        public QuestionViewModel Question { get; set; }  

        #region EventCallbacks       

        [Parameter]
        public EventCallback<QuestionViewModel> QuestionChanged { get; set; }

        [Parameter]
        public EventCallback<QuestionViewModel> OnDisplayQuestionChanged { get; set; }        

        #endregion         
        
        protected override void OnInitialized() => StateContainer.OnChange += StateHasChanged;

        public void Dispose() => StateContainer.OnChange -= StateHasChanged;

        protected async Task DisplayQuestionChanged()
        {           
            await QuestionChanged.InvokeAsync(Question); 

            await OnDisplayQuestionChanged.InvokeAsync(Question);
        }    

        protected void NextSubcategoryButtonClicked() => StateContainer.NextSubcategory();       

        protected void NextCategoryButtonClicked() => StateContainer.NextCategory();             
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.