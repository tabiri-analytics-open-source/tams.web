
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Components;
using Tams.Web.BusinessLayer;
using Tams.Web.DataLayer.DomainModels;
using Tams.Web.ViewModels.ReportViewModels;

namespace Tams.Web.Components.AuditorComponents
{
    public class ReportCardsGalleryBase : ComponentBase
    {
        [Inject]
        public IDataService DataService { get; set; }         
        [Inject]
        public IStateContainer StateContainer { get; set; }        
        [Parameter]
        public OrganizationType OrganizationType { get; set; }        
        public List<ReportQuestionnaireViewModel> ReportsWithResponses { get; set; }
        public List<ReportQuestionnaireViewModel> ReportsWithoutResponses { get; set; }
        protected override void OnInitialized() 
        {
            var orgs = DataService.GetOrganizationsWithQuestionnaires(OrganizationType);

            var reports = orgs
                .Select(x => StateContainer.GetReportQuestionnaireViewModel(x.Id, x.Questionnaires.FirstOrDefault().Id))
                .ToList();

            ReportsWithResponses = reports
                .Where(x => x.HasResponses)
                .ToList();

            ReportsWithoutResponses = reports
                .Where(x => !x.HasResponses)
                .ToList();
        }                
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.