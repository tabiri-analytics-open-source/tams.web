using System;
using Microsoft.EntityFrameworkCore;
using Tams.Web.DataLayer.DomainModels;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;
using Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.CmuDatabaseEntities;
using Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.CmuDatabaseEntities.CmuCybersecurityStandards;
using Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.CmuDatabaseEntities.CmuQuestions;
using Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.TabiriDatabaseEntities;
using Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.TabiriDatabaseEntities.TabiriCybersecurityStandards;
using Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.TabiriDatabaseEntities.TabiriQuestions;

namespace Tams.Web.DataLayer.SeedDatabase
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            var currentTimestamp = DateTime.UtcNow;
            var createdBy = Guid.Parse("6118d3f5-2d54-4128-b9c1-5d4b888134e0");

            modelBuilder.Entity<Person>()
                .HasData(Users.Seed(createdBy, currentTimestamp));

            modelBuilder.Entity<Organization>()
                .HasData(Organizations.Seed(createdBy, currentTimestamp));

            modelBuilder.Entity<Questionnaire>()
                .HasData(Questionnaires.Seed(createdBy, currentTimestamp));

            modelBuilder.Entity<OrganizationQuestionnaire>()
                .HasData(OrganizationQuestionnaires.Seed(createdBy, currentTimestamp));            

            #region Seed CMU-specific database entities

            modelBuilder.Entity<Category>()
                .HasData(CmuCategories.Seed(createdBy, currentTimestamp));

            modelBuilder.Entity<Subcategory>()
                .HasData(CmuSubcategories.Seed(createdBy, currentTimestamp));

            modelBuilder.Entity<ScoringRubric>()
                .HasData(CmuScoringRubrics.Seed(createdBy, currentTimestamp));

            #region Seed CMU Questions

            modelBuilder.Entity<Question>()
                .HasData(CmuSecurityGovernanceQuestions.Seed(createdBy, currentTimestamp));

            modelBuilder.Entity<Question>()
                .HasData(CmuSecurityRiskManagementQuestions.Seed(createdBy, currentTimestamp));

            modelBuilder.Entity<Question>()
                .HasData(CmuDataProtectionQuestions.Seed(createdBy, currentTimestamp));

            modelBuilder.Entity<Question>()
                .HasData(CmuAccessManagementQuestions.Seed(createdBy, currentTimestamp));

            modelBuilder.Entity<Question>()
                .HasData(CmuSecurityArchitectureQuestions.Seed(createdBy, currentTimestamp));

            modelBuilder.Entity<Question>()
                .HasData(CmuIncidentResponseQuestions.Seed(createdBy, currentTimestamp));

            #endregion

            #region Seed CMU CybersecurityStandards

            modelBuilder.Entity<CybersecurityStandard>()
                .HasData(CmuNistCybersecurityStandards.Seed(createdBy, currentTimestamp));

            modelBuilder.Entity<CybersecurityStandard>()
                .HasData(CmuIsoIecCybersecurityStandards.Seed(createdBy, currentTimestamp));

            #endregion

            #endregion     
            
            #region Seed Tabiri-specific database entities

            modelBuilder.Entity<Category>()
                .HasData(TabiriCategories.Seed(createdBy, currentTimestamp));

            modelBuilder.Entity<Subcategory>()
                .HasData(TabiriSubcategories.Seed(createdBy, currentTimestamp));

            modelBuilder.Entity<ScoringRubric>()
                .HasData(TabiriScoringRubrics.Seed(createdBy, currentTimestamp));

            #region Seed Tabiri Questions

            modelBuilder.Entity<Question>()
                .HasData(TabiriSecurityGovernanceQuestions.Seed(createdBy, currentTimestamp));

            modelBuilder.Entity<Question>()
                .HasData(TabiriSecurityRiskManagementQuestions.Seed(createdBy, currentTimestamp));

            modelBuilder.Entity<Question>()
                .HasData(TabiriDataProtectionQuestions.Seed(createdBy, currentTimestamp));

            modelBuilder.Entity<Question>()
                .HasData(TabiriAccessManagementQuestions.Seed(createdBy, currentTimestamp));

            modelBuilder.Entity<Question>()
                .HasData(TabiriSecurityArchitectureQuestions.Seed(createdBy, currentTimestamp));

            modelBuilder.Entity<Question>()
                .HasData(TabiriIncidentResponseQuestions.Seed(createdBy, currentTimestamp));

            #endregion

            #region Seed Tabiri CybersecurityStandards

            modelBuilder.Entity<CybersecurityStandard>()
                .HasData(TabiriNistCybersecurityStandards.Seed(createdBy, currentTimestamp));

            modelBuilder.Entity<CybersecurityStandard>()
                .HasData(TabiriIsoIecCybersecurityStandards.Seed(createdBy, currentTimestamp));

            #endregion

            #endregion                   
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.