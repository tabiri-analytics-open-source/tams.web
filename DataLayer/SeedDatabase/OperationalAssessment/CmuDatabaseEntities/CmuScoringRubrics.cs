using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.CmuDatabaseEntities
{
    public static class CmuScoringRubrics
    {
        public static ScoringRubric[] Seed(Guid createdBy, DateTime currentTimestamp)
        {
            var scoringRubrics = new List<ScoringRubric>();  

            scoringRubrics.AddRange(StrategicPlanningScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(SecurityPolicyFrameworkScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(OrganizationalStructureScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(PerformanceMetricsScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(WorkforceManagementScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(RiskManagementFrameworkScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(ThreatManagementScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(SecurityAwarenessScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(DataProtectionFrameworkScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(DataClassificationScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(DataProtectionPoliciesScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(DataRetentionScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(DataLossScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(DataRecoveryScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(IdentityManagementScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(AccessControlsScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(SeparationOfDutiesScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(PrivilegedAccessManagementScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(RemoteAccessScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(ThirdPartyAccessScoringRubrics(createdBy, currentTimestamp));    
            scoringRubrics.AddRange(NetworkProtectionScoringRubrics(createdBy, currentTimestamp));    
            scoringRubrics.AddRange(EndpointProtectionScoringRubrics(createdBy, currentTimestamp));   
            scoringRubrics.AddRange(ApplicationProtectionScoringRubrics(createdBy, currentTimestamp));   
            scoringRubrics.AddRange(IncidentReadinessScoringRubrics(createdBy, currentTimestamp));   
            scoringRubrics.AddRange(IncidentDetectionScoringRubrics(createdBy, currentTimestamp)); 
            scoringRubrics.AddRange(IncidentRemediationScoringRubrics(createdBy, currentTimestamp));

            return scoringRubrics.ToArray();
        }

        private static List<ScoringRubric> StrategicPlanningScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("dcec4917-2291-43cb-8d2a-9e0945656b1c");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("c6f4d083-09e4-4ca1-9226-544804bcd9f2"),
                    Title = "Security department role and mission is not defined nor understood; organization does not recognize a need for dedicated security funding",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("7a4bb333-37dd-4967-a2d7-16056d518dd5"),
                    Title = "Security department role and mission are defined but not well communicated; limited security budget exists but with no clear accountability",
                    InstructionText = "01, 05.a.i",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("5a99aacc-7df6-475e-8968-11b4759e4d6c"),
                    Title = "Members of the security department clearly understand their role and mission; adequate security budget with limited accountability",
                    InstructionText = "01, 02, (05.a.ii or 05.a.iii), 05.b",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("0816bdcf-962c-4156-acd2-0072e65eaee1"),
                    Title = "Other departments understand the role and mission of the security department; goals are tracked and measured",
                    InstructionText = "01, 02.a, (05.a.ii or 05.a.iii), 05.b, 05.c",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("6d2ae894-ad55-40e9-acd8-9dd9e3c82cb6"),
                    Title = "Management is continuously briefed on security department activities; goal outcomes drive future security planning",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };               
        }

        private static List<ScoringRubric> SecurityPolicyFrameworkScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("96e4580d-05cb-4b7d-97b4-cba0a477bb1c");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("27b0614f-8a1b-4e6c-b9ce-2154fd64e3ac"),
                    Title = "No documented policies, standards or procedures",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("256c0a3e-82ac-405a-bcb2-0763f63b1f02"),
                    Title = "Some documented policies, but few if any standards and procedures",
                    InstructionText = "01.a",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("ac5014be-1270-4b36-98ae-577faab68498"),
                    Title = "Organization has documented policies, standards and procedures; benchmarked to industry frameworks",
                    InstructionText = "01.a, 01.b, 01.c, 02",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("9ce92749-240a-425b-bd19-9125440b867f"),
                    Title = "Regular reviews and revisions are carried out",
                    InstructionText = "01.a, 01.b, 01.c, 02, 01.f",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("d54cb976-b15e-415f-8abb-ae7277d82287"),
                    Title = "Clear accountability by linkage to individual employee performance",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }; 
        }

        private static List<ScoringRubric> OrganizationalStructureScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("b9912cf5-6e62-43ae-a0cd-36d7fbb52944");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("e01d5b41-5641-4980-9c29-c14a302d5655"),
                    Title = "No formal IT Security team structure",
                    InstructionText = "01, 02, 05",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("621e744a-e6bd-48b0-8d84-ead479da3575"),
                    Title = "IT Security team roles are not distinct nor well defined; insufficient staffing levels",
                    InstructionText = "(02.a or 02.d), 03<2",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("b2a1d26b-8ad0-4771-b92d-7384b8c61732"),
                    Title = "IT Security team roles are separate from other IT functions; sufficient staffing levels",
                    InstructionText = "01, 03>2, 05",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("8cb5a745-a7e4-4860-ad76-e000f063a0b0"),
                    Title = "IT Security department has an independent reporting structure",
                    InstructionText = "01, (02.b or 02.c), 03>=3, 04, 05",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("1c598a8d-42cf-45a1-910b-cef239dbedf9"),
                    Title = "Excellent coordination between IT security, other business units and third parties, both locally and across geographic regions",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };   
        }

        private static List<ScoringRubric> PerformanceMetricsScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("cd09235e-4fd1-4e8c-b751-f2bf6b449b27");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("3f008af2-1248-4bdf-9bc8-8bd12e77b64d"),
                    Title = "Organization does not collect security metrics",
                    InstructionText = "01",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("5e13110f-57ae-402c-87b6-ba600b0a9cbc"),
                    Title = "Organization collects some metrics, but with no clear criteria; no management reporting",
                    InstructionText = "01.a",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("6e59c86e-c094-49f9-bb48-dc039f194dc9"),
                    Title = "Metrics reported to management, but no defined KPIs; Collection and reporting are periodic",
                    InstructionText = "01.a, 01.c, 01.d.i(annual or quarterly)",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("5c35cbc7-ed3a-4e06-b64d-6ed5451d08a9"),
                    Title = "Defined KPIs and real-time collection and reporting",
                    InstructionText = "01.a, 01.b, 01.c.ii, 01.d.i(>=monthly), 01.d.ii",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("5e1a5b05-4f3e-42fc-805c-cc94e83734c7"),
                    Title = "Metrics are independently audited and have a well defined role in process improvement and strategic planning",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };   
        }

        private static List<ScoringRubric> WorkforceManagementScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("a34bdc05-1dc9-4f6e-a3ef-a96d5ff90ed3");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("212988c4-df9d-47e4-bd74-60fbb8062856"),
                    Title = "Organization cannot determine if security team staff are adequately qualified",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("f2233e9d-2147-49af-9b0b-bff85bd0f508"),
                    Title = "Security team staff are qualified based on measured skills and knowledge",
                    InstructionText = "01, 06",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("3c84be5a-884b-4231-87cc-f6989b928509"),
                    Title = "A formal process exists to provide continuous professional training",
                    InstructionText = "01, 02, 06",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("4853863c-0769-46bf-a0df-9860b4743919"),
                    Title = "A clear career path exists with opportunities for job rotation",
                    InstructionText = "01, 02, 03, 05.c, 06",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("ee8e5237-736f-4f02-b8fc-87e0741807a7"),
                    Title = "Well defined job retention program with industry benchmarked incentive packages",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };  
        }

        private static List<ScoringRubric> RiskManagementFrameworkScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("9452f450-6b56-437a-a6c0-9f7b03bfde8c");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("b467b2e2-48d7-44c8-966e-9409e0b84df8"),
                    Title = "Organization has no formal risk management team, risk assessment framework, nor understanding of compliance requirements",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("b24808dd-3627-480d-b896-c1635bbe3262"),
                    Title = "Risk management team exists, there is a documented risk assessment of critical systems and production systems, and an understanding of compliance requirements",
                    InstructionText = "01, 02.d[critical only], 02.k, 03",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("719d0e03-84ce-42ea-9a0e-d074ac613deb"),
                    Title = "IT Security team has representation on the Risk Management team, risk assessments conducted semi-annually or better, covers all systems, integrate with other IT activities, and  remeditian actively tracked",
                    InstructionText = "01, 01.b, 01.c, 02.b[>=semi-annual], 02.d[all systems], 02.e, 02.h, 02.k, 03",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("8c0175df-c20a-4652-8050-cea93c10cd2a"),
                    Title = "Risk assessment conducted quarterly or better, formal documentation of risks in a regularly updated risk register, clear ownership, progress tracking, immediate validation, third-party audits, and mitigating controls for identified risks",
                    InstructionText = "01, 01.b, 01.c, 02.b[>=quarterly], 02.c,  02.d[all systems], 02.e, 02.f, 02.g, 02.h, 02.i, 02.j, 02.k, 03",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("784ac414-1ca5-40d5-a3a5-1376ccfbacbf"),
                    Title = "IT security is a joint responsibility of all teams, real-time communication of IT security risks, integration with the overall Strategic Plan, use of automated risk assessment tools, use of automated compliance review tools",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }; 
        }

        private static List<ScoringRubric> ThreatManagementScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("bcbce862-3628-4f03-bd6b-4da0a08ce9c1");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("9c8304eb-2741-40ee-bf2b-054458ade665"),
                    Title = "Organization has no structured threat management process",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("873f2a36-bbfc-418d-8be5-fa5f402b100f"),
                    Title = "Organization conducts limited scope vulnerability scans once or twice a year",
                    InstructionText = "01, 02.a[>=semiannual], 02.b[unauthenticated only]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("3cfce13a-8bb2-47ad-a610-7e671352de1e"),
                    Title = "Vulnerability scans cover all assets, automated scans run on a scheduled basis, annual penetration tests, and basic SIEM deployment",
                    InstructionText = "01, 02.a[>=monthly], 02.b[all], 03.a[annual], 06.a[network+endpoint logs]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("864d60a6-7a2b-45bb-ba54-9827142afe60"),
                    Title = "Technical findings translated to business risk; remediation efforts tracked for completion; threat intel integration with SIEM",
                    InstructionText = "01, 02.a[>=weekly], 02.b[all], 03.a[semiannual], 05.b, 05.c, 06.a[all], 06.b",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("74888089-139f-4571-a33b-e8edd5708bc4"),
                    Title = "Emerging threats actively tracked and communicated, active collaboration with peers and authorities, and automated threat intel sharing",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };  
        }

        private static List<ScoringRubric> SecurityAwarenessScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("169c5d65-75fb-41b9-b607-fb43343ba884");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("a444c23c-e442-4861-aabe-9fe012b57b9f"),
                    Title = "Organization has no formal IT security awareness program",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("14a567f5-c2eb-47a1-99ad-93b94d00d334"),
                    Title = "Program exists, program budget allocated, limited training audience and scope, and conducted annually",
                    InstructionText = "01.b[some], 01.f.i[newsletters], 01.f.ii[annual], 01.h.i[some topics]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("f51e3ae2-5e3d-4a40-a5b4-a9a3862c72cb"),
                    Title = "Wider training audience and scope, clear progress tracking, and conducted semiannually",
                    InstructionText = "01.b[most], 01.f.i[workshops/courses], 01.f.ii[>=semiannual], 01.h.i[more topics], 01.i",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("ad0c7486-747f-4ef8-9ee0-fda8ed5cd926"),
                    Title = "Mandatory training for all employees and third-parties, role-specific training for employees in sensitive roles, and regular revisions to the program",
                    InstructionText = "01.b[all], 01.d, 01.e,  01.f.i[workshops/courses], 01.f.ii[>=quarterly], 01.g, 01.h.i[all topics], 01.i, 01.j.i[annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("3fba9478-7e6e-4f42-80f9-b610ab12e1fc"),
                    Title = "IT security awareness incorporated into employee performance, training content tracks emerging threats, and metrics have demonstrated improvement over time",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }; 
        }

        private static List<ScoringRubric> DataProtectionFrameworkScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("15d6d4cb-e910-42c5-bb47-b4d03103edb0");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("0957d3a8-3333-4931-90c4-538fb22d0555"),
                    Title = "Organization has no formal data protection framework",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("6f432771-db72-4b51-a751-d4330e13ed3e"),
                    Title = "Data protection framework exists, organization understands the types of data it processes, and framework covers some topics",
                    InstructionText = "01, 01.c[some topics], 02",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("e7124827-edef-486f-8a22-c1fab25e01a1"),
                    Title = "Organization has a process to track different types of data, framework covers all topics, based on industry standards, and reviewed yearly",
                    InstructionText = "01, 01.c[all topics], 01.d, 01.e[annual], 02, 02.b",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("ef05e0f0-e429-473a-b6f4-ad55d7983a9b"),
                    Title = "Organization formally defines and tracks data owners, and conducted reviews at least twice a year",
                    InstructionText = "01, 01.c[all topics], 01.d, 01.e[>=semiannual], 02, 02.b, 03",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("f71190e0-8a4c-4d11-8b66-df347c8a1250"),
                    Title = "Organization can track data in real-time and makes continuous adjustments to the framework based on business risk",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }; 
        }

        private static List<ScoringRubric> DataClassificationScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("7a697f1e-464c-4342-b369-33955d46663d");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("e6e5e268-2be2-408c-bdf6-f4b4e50f9612"),
                    Title = "Organization has no formal data classification policy and data handling guidelines do not exist",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("1eb59a30-f381-4ad3-918e-36eea996deeb"),
                    Title = "Organization has a data classification policy and data handling guidelines exist",
                    InstructionText = "01, 02",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("3139d450-d8e5-46ca-8b36-522cc502a770"),
                    Title = "Data owners are aware of and follow data handling guidelines, enforcement mapped to access control policies and annual reviews",
                    InstructionText = "01, 02, 02.a, 02.b, 02.b.ii, 03, 03.b[annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("f6d6b983-425e-4ba8-9a67-946ac34cebfd"),
                    Title = "Data marking and handling is consistent across the organization and frequent reviews",
                    InstructionText = "01, 02, 02.a, 02.b, 02.b.ii, 02.b.iii, 03, 03.b[>=quarterly]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("10696505-0865-44c0-8e2a-56d43a914f81"),
                    Title = "Data handling enforced via automated DLP solution, automated notifications for data marking changes and real-time reviews",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };  
        }

        private static List<ScoringRubric> DataProtectionPoliciesScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("32b2f015-8e14-4cfa-936a-6c58e18442cc");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("90c4a55a-70c3-4b23-b7c0-1f67e4d72696"),
                    Title = "Organization has no formal data protection policy",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("e5ce4afc-2416-4460-bde7-119a2951866a"),
                    Title = "Data protection policies exist with defined standards",
                    InstructionText = "01.a, 01.b, 02.a, 02.b",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("58b73574-28e5-43e3-9ef2-c9a9d4b3496d"),
                    Title = "Data owners understand how to protect data in some states, documented key management procedure, and annual reviews are conducted",
                    InstructionText = "01.a, 01.b, 01.f, 01.g.i[annual], 02.a, 02.b, 02.f.i[annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("ccb2f984-774a-434c-ad7c-7e8a2f9b011d"),
                    Title = "Data owners understand how to protect data in all states, and frequent reviews are conducted",
                    InstructionText = "01.a.ii, 01.b.ii, 01.c.ii, 01.d.ii, 01.e.ii, 01.f.ii, 01.g.i[>=quarterly], 02.a.ii, 02.b.ii, 02.c.ii, 02.d.ii, 02.e.ii, 02.f.i[>=quarterly]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("0d6e45b9-1888-4b19-adbe-b0cf8eb7dbd9"),
                    Title = "Process in place to conduct automated reviews with DLP enforcement",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<ScoringRubric> DataRetentionScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId =  Guid.Parse("82083045-11e8-4f48-a91c-eb615c34bffa");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("032a5bff-4c2d-4f8d-83f0-cd5d13093621"),
                    Title = "Organization has no defined data retention policy or disposal guidelines",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("e05d337b-d89c-4d04-b5f4-2213286e1d70"),
                    Title = "Data retention policy and disposal guidelines are defined",
                    InstructionText = "01, 03",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("d11a7315-1ce9-4c0f-a36a-6cb697170d9d"),
                    Title = "Data owners are aware, training is provided, and annual reviews are conducted",
                    InstructionText = "01, 01.c, 01.d[annual], 03, 03.b, 03.e[annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("b046b56d-5cd2-47c9-b59c-96e4d92af921"),
                    Title = "Organization has a procedure for securing retained data, securely erasing data, and frequent reviews are conducted",
                    InstructionText = "01, 01.c, 01.d[>=semiannual], 02, 03, 03.b, 03.c, 03.d, 03.e[>=semiannual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("f8614e68-0afd-49a7-8bd5-edf03dca4f87"),
                    Title = "Organization has automated systems to retain, secure and dispose data",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }; 
        }

        private static List<ScoringRubric> DataLossScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("952d593d-6f25-4418-bfe4-9f1dc59a4221");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("caf25f31-5ae6-4d44-b349-f8afe5144b3a"),
                    Title = "Organization has no formal data loss policy",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("764f318a-151a-4aa7-8fe0-e823dd503046"),
                    Title = "Data loss policy exists and includes a defined breach notification procedure",
                    InstructionText = "01.a",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("f0a90d4c-d0ce-4980-87ec-61bbadef8f1a"),
                    Title = "Data owners are aware of this policy, organization relies on self-reporting, and conducts annual reviews",
                    InstructionText = "01.a, 01.b, 01.c[annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("af8ee67d-afa6-4495-b6c5-25315e28047e"),
                    Title = "Organization has technology solutions in place to detect data loss and critical systems, conducts frequent reviews, and has cyber insurance coverage",
                    InstructionText = "01.a, 01.b, 01.c[>=semiannual], 02.a.ii[critical systems]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("66c42d1d-2610-4c18-9184-1c58b7451d1e"),
                    Title = "Technology solutions cover all systems with automated loss notification",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };  
        }

        private static List<ScoringRubric> DataRecoveryScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("fa4e445e-c6dc-4c8c-b386-09ac54cbe0e2");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("eda3e964-05df-49ed-82bf-e32134e52260"),
                    Title = "Organization has no formal data recovery policy",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("07df2360-f69e-4f91-9c95-b9ba3554b508"),
                    Title = "Data recovery policy exists and includes a disaster recovery plan",
                    InstructionText = "01.a",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("d31a1cad-c9d9-4bcd-a078-8fa9ac696e06"),
                    Title = "Data owners are aware, delegates are trained, backups conducted monthly, annual policy reviews, and monthly data recovery tests",
                    InstructionText = "01.a, 01.a.ii, 01.b[annual], 02.a[monthly], 04.b[annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("af46afca-0ff5-4829-bd4c-e20d00bf1536"),
                    Title = "Sensitive backups are encrypted, backups conducted weekly, off-site backup storage, frequent policy reviews, and frequent recovery tests",
                    InstructionText = "01.a, 01.a.ii, 01.b[>=semannual], 02.a[weekly], 04.b[annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("28755dad-9c01-41a2-a9ed-6548ff84ce17"),
                    Title = "Backups conducted daily, daily data recovery tests, and real-time failover for critical systems",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }; 
        }

        private static List<ScoringRubric> IdentityManagementScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId =  Guid.Parse("1f0fd461-a017-421b-a6fc-9e5ffebd91ae");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("8a67d684-9dbd-4adb-89a0-4a04e74cc10e"),
                    Title = "Organization has no identity management policies, and ad-hoc user provisioning/deprovisioning procedures",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("fb4eb0fd-08c6-4091-be73-9e1e1961c80a"),
                    Title = "Policies and procedures are documented, multiple identity sources, and no IAM solution",
                    InstructionText = "01",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("74c7111b-0c92-4b6e-a370-2ce28235d04f"),
                    Title = "Single authoritative identity source, background checks performed, IAM solution implemented, manual account provision/deprovision procedures, annual account reviews, and annual policy reviews",
                    InstructionText = "01.a, 01.e, 01.f[annual], 02, 05.b[annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("0599a1ff-b9ee-4770-9595-d2393862188c"),
                    Title = "Common identities across systems, automated account provision/deprovision procedures, account deprovision validation,  frequent account reviews, and frequent policy reviews (YES to 01.a, 01.d, 01.e, 01.f[>=semiannual], 02, 03.c, 04.d, 04.f, 05.b[>=monthly])",
                    InstructionText = "01.a, 01.d, 01.e, 01.f[>=semiannual], 02, 03.c, 04.d, 04.f, 05.b[>=monthly]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("2092ff22-9789-4ee3-8a4f-fef447792e16"),
                    Title = "Automated account reviews and notification of discrepancies",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }; 
        }

        private static List<ScoringRubric> AccessControlsScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("1a8e9b19-ec14-49fb-a2ce-ac0b89a92ed5");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("3aa59293-bd02-4f4f-a439-1c26f9396504"),
                    Title = "Organization has no access control policies",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("a4cb91b1-17be-4d5f-a35a-c0fbf7272bdd"),
                    Title = "Policy defined, and some standards defined",
                    InstructionText = "01.a[password complexity & account lockout], 01.c[annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("5784e92f-16cc-47d5-a7a2-7ed3571d4c07"),
                    Title = "All password standards defined, standards applied uniformly, least privilege standard implemented, two-factor authentication implemented, access control monitoring procedures, and annual policy reviews",
                    InstructionText = "01.a[all], 01.a.i, 01.b, 01.c[annual], 02, 03",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("f219dcad-f001-4ac1-8772-acfa4a58cbd2"),
                    Title = "Centralized Log Management solution implemented, with defined log retention schedule, log review procedures, and frequent policy reviews",
                    InstructionText = "01.a[all], 01.a.i, 01.b, 01.c[>=semiannual], 02, 03.a",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("f66a7832-9ef0-48e1-a1e1-2e0f2f7edcdc"),
                    Title = "Strict least privilege implementation on all systems, two-factor authentication on all critical systems/applications, automated access control violation notification, CLM integrated with SIEM",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }; 
        }

        private static List<ScoringRubric> SeparationOfDutiesScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("4e802dfe-3143-4a5a-b8ab-ecdcc9106bd8");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("53d04982-ab31-497b-b9c3-2d5566ef123c"),
                    Title = "Organization has no separation of duties policy",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("5efdcb0c-5e9f-4802-b62e-0b6cce5ae5d1"),
                    Title = "Policy defined and separation between production and dev/test environments",
                    InstructionText = "01.b",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("4de9f1ac-befd-4ff3-a672-4723e92a4347"),
                    Title = "User restriction on administrator level changes, similar controls across production and dev/test environments, developer access restrictions, and annual policy reviews (YES to 01.a, 01.b.i, 01.b.ii, 01.b.iii, 01.c[annual])",
                    InstructionText = "01.a, 01.b.i, 01.b.ii, 01.b.iii, 01.c[annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("bc2caecc-4295-4251-837c-68e36f9a25fe"),
                    Title = "RBAC implemented, enforced reviews prior to code approvals, RBAC role reviews, frequent policy reviews",
                    InstructionText = "01.a, 01.b.i, 01.b.ii, 01.b.iii, 01.b.iii.3, 01.c[>=semiannual], 02, 02.e[>=annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("6ecb6aef-659a-4c56-8abb-d038b9c646f6"),
                    Title = "RBAC integrated with IAM solution, automated role reviews and notification of discrepancies",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };  
        }

        private static List<ScoringRubric> PrivilegedAccessManagementScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("c062770f-2cc3-4f56-b87a-5d51fcabc9b7");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("737c51f8-c6d1-4c0a-9e17-37bfd21f9799"),
                    Title = "Organization has no privileged access management policy",
                    InstructionText = "01, 03, 04, 05",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("e00f14c1-5836-4f8a-a072-be314726daeb"),
                    Title = "Policies are documented and an inventory of privileged accounts exists",
                    InstructionText = "01.a, 03",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("a7a919fe-ed14-4c95-b4c0-fef4c7c38e18"),
                    Title = "Separate privileged/standard accounts, annual policy reviews, and regular privileged account activity reviews",
                    InstructionText = "01.a, 01.b.i, 01.c[annual], 03, 05.b[monthly]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("211a4fce-5a18-4625-ba43-06ab3bde1e5c"),
                    Title = "PAM solution implemented, frequent privileged account activity reviews, and semiannual policy reviews",
                    InstructionText = "01.a, 01.b.i, 01.c[semiannual], 03, 04, 05.b[>=weekly]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("a5f77739-2f69-4473-92c7-19dbbed5e4fe"),
                    Title = "PAM integration with IAM, automated validation against job roles, privileged credentials automatically reset, and real-time activity review with defined rules",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };  
        }

        private static List<ScoringRubric> RemoteAccessScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("61543e20-477b-4c07-a57a-cc856cbbf562");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("6589ff86-973f-41f8-9840-0f61fe227a75"),
                    Title = "Organization has no remote access policy",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("3f91ab0f-ca14-4997-9f2c-03c73154dc5d"),
                    Title = "Policies are documented and defined criteria for granting remote access",
                    InstructionText = "01, 01.c",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("588fa4f4-51aa-415d-af90-f630982bb230"),
                    Title = "Two-factor authentication implemented, annual policy reviews, and regular activity reviews",
                    InstructionText = "01, 01.c, 01.d[annual], 02.d, 03.b[monthly]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("d40154a5-339b-44f9-a391-b833181321cf"),
                    Title = "All remote traffic monitored (no split tunneling), remote users segmented, remote systems authenticated, remote system security checks, frequent activity reviews, and semiannual policy reviews",
                    InstructionText = "01, 01.c, 01.d[>=semiannual], 02.b, 02.c, 02.d, 02.e, 02.f, 03.b[>=weekly]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("8c87876a-215d-4761-afd6-41e2d16d2f40"),
                    Title = "Integration with IAM, automated validation against job roles, and real time activity reviews",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };   
        }

        private static List<ScoringRubric> ThirdPartyAccessScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("c71b6378-ca1c-4f87-94e5-bb79867ed456");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("ebe909e9-b4c8-4e0b-92aa-1144ff33e2d2"),
                    Title = "Organization has no third party access policies",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("04dcd4a1-d19d-4f4d-abf6-d5606e675c4b"),
                    Title = "Policies are documented, contractual language included, and third-parties defined",
                    InstructionText = "01, 01.a, 01.b",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("da419405-8113-46d7-9950-96bba7bca685"),
                    Title = "Secure API access, partial compliance monitoring, regular audits, regular activity reviews, and annual policy reviews",
                    InstructionText = "01, 01.a, 01.b, 01.c.iii[monthly], 01.d[annual], 02.a, 03[some], 02.b, 04.b[weekly]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("89a70611-582b-4682-8282-4c212268d87f"),
                    Title = "Full compliance monitoring, frequent audits, frequent activity reviews, and semiannual policy reviews",
                    InstructionText = "01, 01.a, 01.b, 01.b.iii.1, 01.c.iii[>=weekly], 01.d[>=semiannual], 02.a, 03[all], 02.b, 04.b[daily]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("065abe1b-dc24-4ec7-a556-c3d1b93eae74"),
                    Title = "Integration with IAM, automated validation against job roles, real time audits, and real time activity reviews",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };  
        }

        private static List<ScoringRubric> NetworkProtectionScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("1e676fbe-3330-43ab-9da4-b3ccb42f5068");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("4ea0f905-f323-4d03-84a1-78a7517454cb"),
                    Title = "Organization has no Network Protection policies",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("107d9614-a4fd-4bd7-8af3-336c6ba0b594"),
                    Title = "Policies are documented, external network firewall deployed, and external network IDS/IPS deployed",
                    InstructionText = "01, 02.a[DMZ], 03.a[DMZ]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("703a1d89-15b0-4020-afcb-f772a3b2a7f4"),
                    Title = "DMZ+internal network firewall and IDS/IPS deployed, silent network firewall block, IDS/IPS configured in block mode, forward network web proxy deployed, similar network protections on virtual/cloud infrastructure, forwarding of network logs to SIEM, regular log review, and annual policy reviews (YES to 01, 01.c[annual], 02.a[both], 02.c, 02.d, 02.g.i, 02.h.i, 02.i.ii[weekly], 03.a[both], 03.b[block], 03.g.i, 03.h.i, 03.i.ii[weekly], 04, 04.e.i, 04.f.i, 04.g.ii[weekly])",
                    InstructionText = "01, 01.c[annual], 02.a[both], 02.c, 02.d, 02.g.i, 02.h.i, 02.i.ii[weekly], 03.a[both], 03.b[block], 03.g.i, 03.h.i, 03.i.ii[weekly], 04, 04.e.i, 04.f.i, 04.g.ii[weekly]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("8cf57578-0b43-4371-abac-e803737bf1d2"),
                    Title = "WAF or reverse web proxies deployed, full packet capture capability, SSL/TLS traffic inspection, malware sandbox analysis, transparent web proxies deployed, frequent log review, and semiannual policy review (YES to 01, 01.c[>=semiannual], 02.a[both], (02.b[Application] or 04.c), 02.c, 02.d, 02.g.i, 02.h.i, 02.i.ii[daily], 03.a[both], 03.b[block], 03.d, 03.e, 03.f, 03.g.i, 03.h.i, 03.i.ii[daily], 04, 04.b[transparent], 04.e.i, 04.f.i, 04.g.ii[daily])",
                    InstructionText = "01, 01.c[>=semiannual], 02.a[both], (02.b[Application] or 04.c), 02.c, 02.d, 02.g.i, 02.h.i, 02.i.ii[daily], 03.a[both], 03.b[block], 03.d, 03.e, 03.f, 03.g.i, 03.h.i, 03.i.ii[daily], 04, 04.b[transparent], 04.e.i, 04.f.i, 04.g.ii[daily]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("82dd03ee-2d49-42ed-92c0-7ef2257746b5"),
                    Title = "Automated threat intelligence integration with rules/signatures, real time network log review",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }; 
        }

        private static List<ScoringRubric> EndpointProtectionScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("ffcdde59-8844-433a-92ea-714bf1fc8abc");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("2777eeb5-c290-420b-a951-53e70f1effc4"),
                    Title = "Organization has no Endpoint Protection policies or technologies",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("888b104a-f24c-45b3-bc61-b181ff03f05b"),
                    Title = "Policies are documented, signature-based malware protection deployed on Windows systems, endpoint firewall deployed",
                    InstructionText = "01, 02.c[Windows], 03.a[Windows]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("d7ab9069-e849-484e-a7fb-07f99c249dcc"),
                    Title = "Malware protection deployed on all systems, centralized management on Windows, similar endpoint protection on virtual/cloud infrastructure, forwarding of logs to SIEM, regular log review, and annual policy reviews (YES to 01, 01.b[annual], 02.c[Windows], 02.d.i[Windows], 03.a[Windows], 03.b, 04.a[Windows], 04.b, 05.a[Windows], 05.b, 06.a, 07.b[weekly])",
                    InstructionText = "01, 01.b[annual], 02.c[Windows], 02.d.i[Windows], 03.a[Windows], 03.b, 04.a[Windows], 04.b, 05.a[Windows], 05.b, 06.a, 07.b[weekly]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("f7b3c296-5803-48a8-8ac7-1d01d060157a"),
                    Title = "Centralized management on all operating systems, EDR capabilities, malware sandbox analysis, frequent log reviews, semiannual policy reviews (YES to 01, 01.b[>=semiannual], 02.a, 02.b, 02.c[all], 02.d.i[all], 03.a[all], 03.b, 04.a[all], 04.b, 05.a[all], 05.b, 06.a, 07.b[daily])",
                    InstructionText = "01, 01.b[>=semiannual], 02.a, 02.b, 02.c[all], 02.d.i[all], 03.a[all], 03.b, 04.a[all], 04.b, 05.a[all], 05.b, 06.a, 07.b[daily]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("69ccf9f4-6aa4-4d68-be4a-8ac18a1c196a"),
                    Title = "Automated threat intelligence integration with rules/signatures, real time endpoint log review",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };   
        }

        private static List<ScoringRubric> ApplicationProtectionScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("44a2cc87-75ac-49a9-9275-1b470f6509b3");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("3c03ecb3-8876-48bc-a0f8-f7f374523776"),
                    Title = "Organization has no Application Security policies",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("cc8d3c14-e56f-48d3-b66a-5f07cb193a07"),
                    Title = "Policies are documented, organization has a software inventory with manual software inventory updates",
                    InstructionText = "01, 03",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("ed80fef8-df2d-4f1d-9ed6-84e3e18ac643"),
                    Title = "Developers are trained on secure SDLC, software is certified prior to acquisition, automated software inventory updates, application whitelisting on servers, database logging, email spam filtering, forwarding of logs to SIEM, regular log reviews, and annual policy reviews (YES to 01, 01.a, 01.b, 01.c, 01.e[annual], 03, 03.b, 03.c[daily], 04.b[servers], 05.c, 07, 08.a, 09.b[weekly])",
                    InstructionText = "01, 01.a, 01.b, 01.c, 01.e[annual], 03, 03.b, 03.c[daily], 04.b[servers], 05.c, 07, 08.a, 09.b[weekly]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("b6c1f883-ff5b-44ce-9a5f-dbd07db07827"),
                    Title = "Manual source code reviews, digital signing of code, digital signature capable application whitelisting, application whitelisting on all systems, centrally managed application whitelisting, logging on all critical applications, documented stored procedures and prepared statements, centrally managed email encryption, email authentication, frequent log reviews, and semiannual policy reviews (YES to 01, 01.a, 01.b, 01.c, 01.e[>=semiannual], 02.a.iii[>monthly], 02.b, 03, 03.b, 03.c[daily], 04.a[all], 04.b[all], 04.c, 05.a, 05.b, 05.c, 06.b, 06.c, 07, 07.b, 08.a, 09.b[daily])",
                    InstructionText = "01, 01.a, 01.b, 01.c, 01.e[>=semiannual], 02.a.iii[>monthly], 02.b, 03, 03.b, 03.c[daily], 04.a[all], 04.b[all], 04.c, 05.a, 05.b, 05.c, 06.b, 06.c, 07, 07.b, 08.a, 09.b[daily]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("fa1369c1-6f28-422d-90c6-d6c429746e06"),
                    Title = "Automated source code reviews and real time application log review",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };  
        }

        private static List<ScoringRubric> IncidentReadinessScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("66ea6203-8e60-4fd6-8f45-4768fe375a5b");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("555df80c-a55d-457d-b16f-79b4fe5091df"),
                    Title = "Organization has no Incident Response Plan",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("ff463e0f-5a61-44d3-9bc0-2741d6712ed6"),
                    Title = "IRP documented, IRP roles defined, incident reporting structure defined, incident escalation procedures defined, and team members aware of their assigned roles",
                    InstructionText = "01.a, 01.c, 01.d, 02.c",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("d7884589-6b2e-4fcc-a39a-7f2a6638fa54"),
                    Title = "Security analyst roles defined, external party requirements defined, service retainers in place, staff training, annual drills, and annual IRP reviews",
                    InstructionText = "01.a, 01.b.i[some], 01.b.ii, 01.c, 01.d, 01.f[annual], 02.a.i[all], 02.c, 02.c.i, 03.b[annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("c1fcadb9-adfa-417d-9d8f-c630cf73c464"),
                    Title = "24/7 staff coverage, mandatory leave, staff certification, staff performance reviews, ISAC participation, semiannual drills; semiannual IRP review",
                    InstructionText = "01.a, 02.a.i[all], 02.b, 01.b.i[all], 01.b.ii, 01.c, 01.d, 01.e, 01.f[>=semiannual], 02.c, 02.c.i, 02.c.ii, 02.d, 03.b[semiannual], 04",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("38eb69eb-2aee-4674-8267-16878ae2d93a"),
                    Title = "CERT/CIRT/SOC team spans multiple business units, certification of all team members, quarterly drills with real time tracking, and drills integrated into staff performance review, ISAC threat intelligence sharing",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };   
        }

        private static List<ScoringRubric> IncidentDetectionScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("a1e57c2b-6331-4023-9d84-aafe8233783f");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("03ca8c91-dff3-4a2e-b739-0c284c88251b"),
                    Title = "Organization has no SOC/provider, SIEM capability, and detection procedures",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("c01fec22-7efa-46f0-aa6b-2612ae076aa9"),
                    Title = "SOC/provider, SIEM capability with limited log collection, and defined detection procedures",
                    InstructionText = "01, 02, 02.a[some], 04",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("1ed9d0c7-0421-480f-a649-21e69f600202"),
                    Title = "SIEM capability with extensive log collection, 1 month log retention schedule, timestamp synchronization, CERT/CIRT/SOC staff training on detection procedures, and annual procedure reviews (YES to 01, 02, 02.a[all], 02.b[1 month], 02.e, 04, 04.b, 04.c[annual])",
                    InstructionText = "01, 02, 02.a[all], 02.b[1 month], 02.e, 04, 04.b, 04.c[annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("d637cfdd-f251-479e-a825-ce94b1327410"),
                    Title = "6 month log retention, threat intelligence feed integration, log tampering measures, partial automation of log correlation, and semiannual procedure reviews (YES to 01, 02, 02.a[all], 02.b[6 months], 02.c, 02.d.ii[partial], 02.e, 03, 04, 04.b, 04.c[semiannual])",
                    InstructionText = "01, 02, 02.a[all], 02.b[6 months], 02.c, 02.d.ii[partial], 02.e, 03, 04, 04.b, 04.c[semiannual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("f2538092-edfb-4884-b229-f45f063917c9"),
                    Title = "1+ year log retention, fully automated log correlation, and continuous procedure reviews",
                    InstructionText = "01, 02, 02.a[all], 02.b[1+ year], 02.c, 02.d.ii[full], 02.e, 03, 04, 04.b, 04.c[>quarterly]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }; 
        }

        private static List<ScoringRubric> IncidentRemediationScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("a85131ca-5999-47e6-ab90-22db0c1d1236");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("229729fc-be3d-430e-bbf7-79fd49e597c1"),
                    Title = "Organization has no incident remediation procedures or tools",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("35e505ba-2d3b-4c20-aef3-40e00ff32b26"),
                    Title = "Limited incident containment procedures documented with standalone containment tools",
                    InstructionText = "01.a[some]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("1598c080-9748-40fd-aedc-908a27cb4709"),
                    Title = "Comprehensive containment procedures with staff training, centralized containment tools, limited forensics procedures, limited forensics analysis capabilities, case management tool, limited incident metrics tracking, and annual procedure reviews (YES to 01.a[all], 01.b, 01.c.i, 01.d[annual], 02.a[some], 02.c.ii[some], 02.d.ii[some], 02.f[annual], 03, 04)",
                    InstructionText = "01.a[all], 01.b, 01.c.i, 01.d[annual], 02.a[some], 02.c.ii[some], 02.d.ii[some], 02.f[annual], 03, 04",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("ada1f6de-bbd9-47ac-aaf7-26498dbcdc2d"),
                    Title = "Comprehensive forensics procedures with staff training, comprehensive digital forensics tools, centralized digital forensics collection, comprehensive forensics analysis capabilities, dedicated forensics lab, comprehensive incident metrics tracking, metrics integrated into performance metrics + strategic planning, and semiannual procedure reviews (YES to 01.a[all], 01.b, 01.c.i, 01.d[semiannual], 02.a[all], 02.c.ii[all], 02.d.ii[all], 02.e, 02.f[semiannual], 03, 04.c, 04.d, 4.e, 4.f)",
                    InstructionText = "01.a[all], 01.b, 01.c.i, 01.d[semiannual], 02.a[all], 02.c.ii[all], 02.d.ii[all], 02.e, 02.f[semiannual], 03, 04.c, 04.d, 4.e, 4.f",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("fcf9c376-1d1d-49cc-b461-071d098aecc3"),
                    Title = "Automated incident containment, automated digital forensics collection, automated forensic analysis (except static code analysis)",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };  
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.