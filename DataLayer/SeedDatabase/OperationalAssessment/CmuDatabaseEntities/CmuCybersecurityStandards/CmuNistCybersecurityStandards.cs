using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.CmuDatabaseEntities.CmuCybersecurityStandards
{
    public static class CmuNistCybersecurityStandards
    {
        public static CybersecurityStandard[] Seed(Guid createdBy, DateTime currentTimestamp)
        {
            var standards = new List<CybersecurityStandard>();   

            standards.AddRange(StrategicPlanningStandards(createdBy, currentTimestamp));
            standards.AddRange(SecurityPolicyFrameworkStandards(createdBy, currentTimestamp));
            standards.AddRange(OrganizationalStructureStandards(createdBy, currentTimestamp));      
            standards.AddRange(PerformanceMetricsStandards(createdBy, currentTimestamp));
            standards.AddRange(WorkforceManagementStandards(createdBy, currentTimestamp));    
            standards.AddRange(RiskManagementFrameworkStandards(createdBy, currentTimestamp));  
            standards.AddRange(ThreatManagementStandards(createdBy, currentTimestamp));  
            standards.AddRange(SecurityAwarenessStandards(createdBy, currentTimestamp));    
            standards.AddRange(DataProtectionFrameworkStandards(createdBy, currentTimestamp));      
            standards.AddRange(DataClassificationStandards(createdBy, currentTimestamp));  
            standards.AddRange(DataProtectionPoliciesStandards(createdBy, currentTimestamp));      
            standards.AddRange(DataRetentionStandards(createdBy, currentTimestamp));    
            standards.AddRange(DataLossStandards(createdBy, currentTimestamp));
            standards.AddRange(DataRecoveryStandards(createdBy, currentTimestamp));  
            standards.AddRange(IdentityManagementStandards(createdBy, currentTimestamp));  
            standards.AddRange(AccessControlsStandards(createdBy, currentTimestamp));     
            standards.AddRange(SeparationOfDutiesStandards(createdBy, currentTimestamp));   
            standards.AddRange(PrivilegedAccessManagementStandards(createdBy, currentTimestamp)); 
            standards.AddRange(RemoteAccessStandards(createdBy, currentTimestamp));    
            standards.AddRange(ThirdPartyAccessStandards(createdBy, currentTimestamp));    
            standards.AddRange(NetworkProtectionStandards(createdBy, currentTimestamp));   
            standards.AddRange(EndpointProtectionStandards(createdBy, currentTimestamp));     
            standards.AddRange(ApplicationProtectionStandards(createdBy, currentTimestamp));  
            standards.AddRange(IncidentReadinessStandards(createdBy, currentTimestamp));    
            standards.AddRange(IncidentDetectionStandards(createdBy, currentTimestamp));     
            standards.AddRange(IncidentRemediationStandards(createdBy, currentTimestamp));     
            
            return standards.ToArray();
        } 

        private static List<CybersecurityStandard> StrategicPlanningStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("dcec4917-2291-43cb-8d2a-9e0945656b1c");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("3d8ac617-bd3a-434f-be9b-16714ba1bbc7"),
                    Title = "PM-1 Information Security Program Plan",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("1a77464c-e733-465a-9c7e-60bfcf57c373"),
                    Title = "PM-3 Information Security Resources",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("e6de2505-7d0b-4deb-b9eb-1ec1fb2cd2f2"),
                    Title = "PM-4 Plan of Action and Milestones Process",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("54ba67f3-4ae7-464b-b5a3-2d24e21568e5"),
                    Title = "PM-11 Mission/Business Process Definition",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };  
        }

        private static List<CybersecurityStandard> SecurityPolicyFrameworkStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("96e4580d-05cb-4b7d-97b4-cba0a477bb1c");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("f39ef078-5024-418e-a949-c817d61cdd88"),
                    Title = "PL-1 Security Planning Policy and Procedures",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }; 
        }

        private static List<CybersecurityStandard> OrganizationalStructureStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("b9912cf5-6e62-43ae-a0cd-36d7fbb52944");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("53c73a55-740a-40de-a656-7f1aeed7f0e5"),
                    Title = "PM-2 Information Security Program Roles",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("bd9bced4-f126-4ad6-a6c5-3efb40c24a14"),
                    Title = "PM-3 Information Security and Privacy Resources",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };    
        }

        private static List<CybersecurityStandard> PerformanceMetricsStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("cd09235e-4fd1-4e8c-b751-f2bf6b449b27");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("c9249b36-3d83-45f9-a0c6-bfdff8e93503"),
                    Title = "PM-6 Measures of Performance",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> WorkforceManagementStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("a34bdc05-1dc9-4f6e-a3ef-a96d5ff90ed3");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("20b69df7-2014-47af-8cd6-35424d281c68"),
                    Title = "PM-13 Security and Privacy Workforce",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("0e59ebde-9d4b-4664-87df-947f461a9439"),
                    Title = "PM-14 Testing, Training and Monitoring",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> RiskManagementFrameworkStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("9452f450-6b56-437a-a6c0-9f7b03bfde8c");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("81806bba-ff19-483b-8338-be210e34f84c"),
                    Title = "RA-1 Risk Assessment Policy and Procedures",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("c687c11e-efa6-414f-bc45-fb59bcf7a0a9"),
                    Title = "RA-3 Risk Assessment",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("dc4659fc-7efb-42a2-bc08-2f99bf22bb3d"),
                    Title = "RA-7 Risk Response",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("5ecbb1a7-db8a-4cca-ac26-41a896f764c5"),
                    Title = "RA-9 Criticality Analysis",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> ThreatManagementStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("bcbce862-3628-4f03-bd6b-4da0a08ce9c1");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("d425a82e-3c03-4258-80fd-b7c718af8176"),
                    Title = "CA-2 Assessments",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("3c09d114-45df-484e-9e48-ddbcca6f9711"),
                    Title = "RA-5 Vulnerability Scanning",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("e30cfd38-53c0-4817-84c4-9ec50f6141a2"),
                    Title = "CA-8 Penetration Testing ",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("9cfb40f6-a959-4967-8597-5d55df7500a7"),
                    Title = "PA-4 Information Sharing with External Parties",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("812e3153-09f0-46b9-99d3-1fbe11f9d8b2"),
                    Title = "PM-15 Contacts with Groups and Associations",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("7d596fb2-3761-42c8-80e0-a394c66be3c5"),
                    Title = "PM-16(1) Automated Means for Sharing Threat Intelligence",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> SecurityAwarenessStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("169c5d65-75fb-41b9-b607-fb43343ba884");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("62ad110d-f85b-4a13-9f77-18dd239f8dfa"),
                    Title = "AT-1 Awareness and Training Policy and Procedures",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("82890771-c9c1-421e-b790-ea61b9b904b7"),
                    Title = "PM-16 Threat Awareness Program",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> DataProtectionFrameworkStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("15d6d4cb-e910-42c5-bb47-b4d03103edb0");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("6754d550-33c0-449d-a644-cd2b7e386be7"),
                    Title = "SC-1 System and Communications Protection Policy and Procedures",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("cc4d0a5c-fc2c-438b-a4c1-0e790bb31e46"),
                    Title = "SI-1 System and Information Integrity Policy and Procedures",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("91214eab-1fe8-4297-9be5-30de5542a468"),
                    Title = "PM-2 Information Security Program Roles",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> DataClassificationStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("7a697f1e-464c-4342-b369-33955d46663d");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("a754de0a-c8a0-4e3c-be41-5db9a2d98a57"),
                    Title = "RA-2 Security Categorization",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("0692adef-99ed-444c-a2f1-f5704e3fdff0"),
                    Title = "PM-29 Inventory of Personally Identifiable Information",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("22c0ab11-b329-4c25-b67e-8fcc87914182"),
                    Title = "AC-16 Security and Privacy Attributes",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("767c4d4f-f656-4f7e-82ad-19962bbfc56b"),
                    Title = "MP-3 Media Marking",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("4ae31319-4f6d-4b72-87fa-b3e924215bc7"),
                    Title = "PE-22 Component Marking",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> DataProtectionPoliciesStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("32b2f015-8e14-4cfa-936a-6c58e18442cc");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("87aab31a-042d-4408-9761-2f4ccd7a1b1d"),
                    Title = "AC-4 Information Flow Enforcement",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("2db64f77-8d01-45ae-8887-cd3b44694148"),
                    Title = "CA-3 System Interconnections",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("c89a1fa8-8d7b-4a96-93ec-75aa73c46fbc"),
                    Title = "SC-7 Boundary Protection",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("e83d568b-97ee-4412-9d53-dce7f49ab847"),
                    Title = "SC-8 Transmission Confidentiality and Integrity",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("078f7c1d-ec9f-4b34-9412-7d8fb1d12e7f"),
                    Title = "SC-13 Cryptographic Protection",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("57bd0af4-859e-4248-9329-a7b1ba3eded8"),
                    Title = "SC-28 Protection of Information at Rest",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> DataRetentionStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("82083045-11e8-4f48-a91c-eb615c34bffa");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("31cffb3a-2b95-43fc-8431-e1cf7681ed74"),
                    Title = "MP-1 Media Protection Policy and Procedures",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("3f4cbe98-5e45-4334-8519-8924be539351"),
                    Title = "PE-16 Delivery and Removal",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("a8e5c632-d6ed-4989-9712-69833d07b0ca"),
                    Title = "PE-20 Asset Monitoring and Tracking",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("84bc60c7-7a32-4264-9cd2-57dee4e0cd58"),
                    Title = "SI-12 Information Management and Retention",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("8a2ea626-d01e-4b1d-a83d-b810a3d2d2ad"),
                    Title = "SC-28 Protection of Information at Rest",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> DataLossStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("952d593d-6f25-4418-bfe4-9f1dc59a4221");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("8680cf02-7988-42df-ac6c-5386273b74cc"),
                    Title = "IR-1 Incident Response Policy and Procedures",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("82ff3825-8870-4aec-be6f-0806815e33b2"),
                    Title = "IR-6 Incident Reporting",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> DataRecoveryStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("fa4e445e-c6dc-4c8c-b386-09ac54cbe0e2");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("6b2fe078-675e-457f-bfe7-a87aeea0ee39"),
                    Title = "CP-1 Contingency Planning Policy and Procedures",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("402652f8-d4fc-463c-ab17-d34fa2143cfe"),
                    Title = "CP-2 Contingency Plan",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("b6d82b68-504d-4316-9056-40cab99c78ec"),
                    Title = "CP-3 Contingency Training",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("17537199-4d3c-4d93-8d65-71a07dca8881"),
                    Title = "CP-4 Contingency Plan Testing",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("a55f9727-0946-480e-9380-f90c9bef3265"),
                    Title = "CP-7 Alternate Storage Site",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("ee372a98-64ee-46bb-990a-b9ed68b7351d"),
                    Title = "CP-8 Alternate Processing Site",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("133d1a7e-c41e-43d2-8b5a-c07b4a4f3214"),
                    Title = "CP-9 System Backup ",
                    Order = 7,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("72785a41-5204-4ca0-9c0f-6652413eb2c3"),
                    Title = "CP-10 System Recovery and Reconstitution",
                    Order = 8,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("b74a67ce-4faf-4593-938c-c05b8819331d"),
                    Title = "CP-11 Alternate Communications Protocols",
                    Order = 9,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("6d097c3a-c8d7-4763-aa0d-2506a96f5bf0"),
                    Title = "CP-13 Alternative Security Mechanisms",
                    Order = 10,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> IdentityManagementStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("1f0fd461-a017-421b-a6fc-9e5ffebd91ae");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("c826e4c8-0c83-40b8-a345-7ebf8a8d87c5"),
                    Title = "AC-2 Account Management",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("20fcdff9-4183-441d-9a8e-c2cd03e50b34"),
                    Title = "IA-1 Identification and Authentication Policy and Procedures",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("4749748a-f31d-4823-b517-1d2249b9c0a1"),
                    Title = "IA-2 Identification and Authentication (Organizational Users)",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("99bfdda7-8eb9-48b7-a573-5a3e0b3bf68f"),
                    Title = "IA-4 Identifier Management",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("7f9d48f6-866b-4105-9156-6e7246263312"),
                    Title = "IA-5 Authenticator Management",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("f5eacb8b-3377-4205-bbd7-5617104dbac3"),
                    Title = "IA-8 Identification and Authentication (Non-Organizational Users)",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> AccessControlsStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("1a8e9b19-ec14-49fb-a2ce-ac0b89a92ed5");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("1486be10-4322-4e66-ab24-26c07ecb28fb"),
                    Title = "AC-1 Access Control Policy and Procedures",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("3a3f8471-d3c7-44a6-aa8e-030ee1ea80b9"),
                    Title = "AC-3 Access Enforcement",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("ddf79623-7855-4f39-89e2-012b40852c9d"),
                    Title = "AC-6 Least Privilege",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("555078d6-6068-4a56-a313-be8652e0e0d0"),
                    Title = "AC-7 Unsuccessful Logon Attempts",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("402a910d-3a35-49bb-84d3-2de4c94f748c"),
                    Title = "AC-8 System Use Notification",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("4fd07d22-c566-4f5d-a9b8-1c7670e27888"),
                    Title = "AC-9 Previous Logon (Access) Notification",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("75aa3dd1-2442-47af-b76e-13c53c7ddbb7"),
                    Title = "AC-24 Access Control Decisions",
                    Order = 7,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("b7aafbcd-8c00-4823-aecd-21595fd09cd9"),
                    Title = "CM-5 Access Restrictions for Change",
                    Order = 8,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("82d05a5b-d4cc-4268-a4f9-9fcd8b1c63e1"),
                    Title = "IA-6 Authenticator Feedback",
                    Order = 9,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> SeparationOfDutiesStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("4e802dfe-3143-4a5a-b8ab-ecdcc9106bd8");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("097f6cf0-04bb-4836-83bc-2aa6b76ed5ca"),
                    Title = "AC-5 Separation of Duties",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("988be210-1819-49ca-a957-99710269c196"),
                    Title = "CM-5 Access Restrictions for Change",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> PrivilegedAccessManagementStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("c062770f-2cc3-4f56-b87a-5d51fcabc9b7");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("2f95b189-b723-4ba0-b204-5e33c05e3834"),
                    Title = "AC-2 Account Management",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("f1bb8c81-ca1c-41aa-b228-ccf213346d70"),
                    Title = "AC-3 Access Enforcement",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("e23e3202-33e0-47d4-84fe-e11580f8589f"),
                    Title = "AC-6 Least Privilege",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("4d150e5c-34df-4003-9ba2-ab351418cd47"),
                    Title = "CM-5 Access Restrictions for Change",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> RemoteAccessStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId =  Guid.Parse("61543e20-477b-4c07-a57a-cc856cbbf562");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("2955af0b-4437-4100-9d3a-25a148db0124"),
                    Title = "AC-3 Access Enforcement",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("a6911624-da45-48de-af7b-55560648f389"),
                    Title = "AC-17 Remote Access",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("64ca8268-7732-4ba6-94b0-73f57fe5f847"),
                    Title = "PE-17 Alternate Work Site",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> ThirdPartyAccessStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("c71b6378-ca1c-4f87-94e5-bb79867ed456");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("059dc7f6-8470-47c0-ad4a-84a62d5602ac"),
                    Title = "IA-8 Identification and Authentication (Non-Organizational Users)",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("5f85f28c-ea91-4858-a326-8b4ebb266156"),
                    Title = "SA-12 Supply Chain Risk Management",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> NetworkProtectionStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("1e676fbe-3330-43ab-9da4-b3ccb42f5068");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("048c2cf5-1b4a-4534-9b90-5fecb7bd9da4"),
                    Title = "AC-4 Information Flow Enforcement",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("7bcabbda-01fe-4b77-b780-10ef159a2a2c"),
                    Title = "AC-17 Remote Access",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("5ba954ae-2e7a-44fb-8d4b-491a477500a8"),
                    Title = "AC-18 Wireless Access",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("ff6c9cdb-ec20-477f-bac4-a71e69694d4b"),
                    Title = "AC-20 Use of External Systems",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("d2e74ea8-2bf6-4505-9ed3-184aa5b0483e"),
                    Title = "CA-3 System Interconnections",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("a797d271-66a6-4bd9-90dc-dafde865edeb"),
                    Title = "SA-9 External System Services",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("3a0d498f-8fde-46f6-939e-dbbefb1ccca0"),
                    Title = "SC-7 Boundary Protection",
                    Order = 7,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("bd8154ba-48ec-46ae-9a61-77bfd970ed1d"),
                    Title = "SC-8 Transmission Confidentiality and Integrity",
                    Order = 8,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("a57aa0f5-bb73-4b45-b676-0a0a6d850add"),
                    Title = "SC-10 Network Disconnect",
                    Order = 9,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> EndpointProtectionStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("ffcdde59-8844-433a-92ea-714bf1fc8abc");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("4f7379da-d14d-419c-a258-d3cce9651cd2"),
                    Title = "AC-19 Access Control for Mobile Devices",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("460e30f5-3e65-4708-a833-356a22bc63b2"),
                    Title = "CM-2 Baseline Configuration",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("9a3f87d7-05d7-427b-887d-ba72d0170be6"),
                    Title = "CM-3 Configuration Change Control",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("1ec97d17-91b4-4743-9d66-71c09b12e090"),
                    Title = "CM-11 User-Installed Software",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("190a8b64-c3a2-48fb-81e7-48aa72fa0cc6"),
                    Title = "SI-3 Malicious Code Protection",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("aeab4476-7f4a-4c01-b70a-2604947cf9e7"),
                    Title = "SI-4 System Monitoring",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> ApplicationProtectionStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("44a2cc87-75ac-49a9-9275-1b470f6509b3");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("d00a8f4f-f368-485f-9a87-b50de2cd549b"),
                    Title = "CA-2 Assessments",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("f838d637-2787-4fd2-b5e1-e94fd6cbe8d0"),
                    Title = "CM-4 Security and Privacy Impact Analyses",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("2c0a9183-fbe1-4b08-9da4-06b58e8bce77"),
                    Title = "CM-10 Software Usage Restrictions",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("150dd83b-9d2d-47c8-b30d-2019424bf5aa"),
                    Title = "SA-3 System Development Life Cycle",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("cd0fcbda-e9cc-4dbe-9079-fcc03a7bc301"),
                    Title = "SA-4 Acquisition Process",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("a0073b8d-4f05-4a76-a4bf-0a827008af43"),
                    Title = "SA-8 Security and Privacy Engineering Principles",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("de1c89f5-713d-4537-8a74-0430f44fbfcd"),
                    Title = "SA-10 Developer Configuration Management",
                    Order = 7,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("2665cb22-af20-4737-a8de-17a7c3763a29"),
                    Title = "SA-11 Developer Testing and Evaluation",
                    Order = 8,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("3d7af779-2eca-4ecc-864c-b2083f3b4994"),
                    Title = "SA-12 Supply Chain Risk Management",
                    Order = 9,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("8d432bac-41f4-4ea0-a08a-c3882c913012"),
                    Title = "SA-15 Development Process, Standards, and Tools",
                    Order = 10,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("283f1dd7-d5dc-4a75-ba14-8de678a43213"),
                    Title = "SA-17 Developer Security Architecture and Design",
                    Order = 11,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("be3efcdf-5fb0-42f2-a71e-e708ce4aa7db"),
                    Title = "SI-2 Flaw Remediation",
                    Order = 12,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> IncidentReadinessStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("66ea6203-8e60-4fd6-8f45-4768fe375a5b");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("ae70aa6e-af57-45bb-859c-d77dc7704a9f"),
                    Title = "AT-2 Awareness Training",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("fb269f9f-2ad7-4f28-a169-de3cfc70b326"),
                    Title = "AT-3 Role-Based Training",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("dca004c9-7e95-427a-a760-628d8f7749e4"),
                    Title = "CP-3 Contingency Training",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("e986fb44-3a00-493d-bb98-8c87db81bd4b"),
                    Title = "IR-2 Incident Response Training",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("e01015a5-0543-4a9b-9041-3efdd6713b39"),
                    Title = "IR-3 Incident Response Testing",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("6da34d4b-e772-4ac3-a365-396617dbd1f7"),
                    Title = "IR-6 Incident Reporting",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("1992775e-4ae2-44ae-b2a0-19f479a9c387"),
                    Title = "IR-7 Incident Response Assistance",
                    Order = 7,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("b46b12b1-915b-4e34-ac1d-1a031367e204"),
                    Title = "IR-8 Incident Response Plan",
                    Order = 8,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("506193d9-2e8b-4158-a453-72338cf1bea2"),
                    Title = "IR-10 Integrated Information Security Analysis Team",
                    Order = 9,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("0a4bcea5-ba58-4496-a600-e563704967fe"),
                    Title = "PM-15 Contacts with Groups and Associations",
                    Order = 10,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("1ec51850-de77-4012-9767-5eb7a1e505dc"),
                    Title = "SI-5 Security Alerts, Advisories, and Directives",
                    Order = 11,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> IncidentDetectionStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("a1e57c2b-6331-4023-9d84-aafe8233783f");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("0e7e370b-031f-4598-b1c8-90eefa444b37"),
                    Title = "AU-6 Audit Review, Analysis, and Reporting",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("209cced9-36db-4737-9331-6e6b32d11677"),
                    Title = "AU-3 Content of Audit Records",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("6914bf88-1976-4779-afda-7dce2cdafbba"),
                    Title = "AU-4 Audit Storage Capacity",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("087e78e1-82c2-4587-9236-965cd40e3b3e"),
                    Title = "AU-8 Time Stamps",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("3cb50d8b-e46d-4a2c-98cc-52ada79912b7"),
                    Title = "AU-9 Protection of Audit Information",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("5f1a5b5e-ce69-4d1f-9b50-66584b178774"),
                    Title = "AU-10 Non-repudiation",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("93e215bb-feca-42cc-9747-b9671289b60a"),
                    Title = "AU-11 Audit Record Retention",
                    Order = 7,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("1e8c9707-7be2-456f-8048-a3800bd293b2"),
                    Title = "AU-12 Audit Generation",
                    Order = 8,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("dbaf24a7-727f-4ad9-ba33-2b9352858bc9"),
                    Title = "AU-14 Session Audit",
                    Order = 9,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("143432fd-bcd2-419b-8d60-98f078f5f256"),
                    Title = "CM-7 Continuous Monitoring",
                    Order = 10,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("ae61a883-db9e-4c8f-b3e0-90b2d8ee4a2c"),
                    Title = "IR-4 Incident Handling",
                    Order = 11,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("e07a606f-2932-4c0f-9687-a88216d93cf4"),
                    Title = "IR-5 Incident Monitoring",
                    Order = 12,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("e66bb429-fd81-4098-9134-5fc41a074143"),
                    Title = "IR-6 Incident Reporting",
                    Order = 13,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("868b92ab-b0a3-4088-9f79-6cdac195896b"),
                    Title = "SI-4 System Monitoring",
                    Order = 14,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> IncidentRemediationStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("a85131ca-5999-47e6-ab90-22db0c1d1236");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("b14eb969-1852-4aa5-9b0e-41b5fac9e07c"),
                    Title = "PS-8 Personnel Sanctions",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("980c5255-2301-4501-be0a-41ed0e961840"),
                    Title = "SI-2 Flaw Remediation",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.