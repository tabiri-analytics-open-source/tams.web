using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.CmuDatabaseEntities.CmuCybersecurityStandards
{
    public static class CmuIsoIecCybersecurityStandards
    {
        public static CybersecurityStandard[] Seed(Guid createdBy, DateTime currentTimestamp)
        {
            var cybersecurityStandards = new List<CybersecurityStandard>();   

            cybersecurityStandards.AddRange(StrategicPlanningStandards(createdBy, currentTimestamp));
            cybersecurityStandards.AddRange(SecurityPolicyFrameworkStandards(createdBy, currentTimestamp));
            cybersecurityStandards.AddRange(OrganizationalStructureStandards(createdBy, currentTimestamp));
            cybersecurityStandards.AddRange(PerformanceMetricsStandards(createdBy, currentTimestamp));
            cybersecurityStandards.AddRange(WorkforceManagementStandards(createdBy, currentTimestamp));
            cybersecurityStandards.AddRange(RiskManagementFrameworkStandards(createdBy, currentTimestamp));
            cybersecurityStandards.AddRange(ThreatManagementStandards(createdBy, currentTimestamp));
            cybersecurityStandards.AddRange(SecurityAwarenessStandards(createdBy, currentTimestamp));
            cybersecurityStandards.AddRange(DataProtectionFrameworkStandards(createdBy, currentTimestamp));
            cybersecurityStandards.AddRange(DataClassificationStandards(createdBy, currentTimestamp));
            cybersecurityStandards.AddRange(DataProtectionPoliciesStandards(createdBy, currentTimestamp));
            cybersecurityStandards.AddRange(DataRetentionStandards(createdBy, currentTimestamp));
            cybersecurityStandards.AddRange(DataLossStandards(createdBy, currentTimestamp));
            cybersecurityStandards.AddRange(DataRecoveryStandards(createdBy, currentTimestamp));
            cybersecurityStandards.AddRange(IdentityManagementStandards(createdBy, currentTimestamp));
            cybersecurityStandards.AddRange(AccessControlsStandards(createdBy, currentTimestamp));
            cybersecurityStandards.AddRange(SeparationOfDutiesStandards(createdBy, currentTimestamp));
            cybersecurityStandards.AddRange(PrivilegedAccessManagementStandards(createdBy, currentTimestamp));
            cybersecurityStandards.AddRange(RemoteAccessStandards(createdBy, currentTimestamp));
            cybersecurityStandards.AddRange(ThirdPartyAccessStandards(createdBy, currentTimestamp));    
            cybersecurityStandards.AddRange(NetworkProtectionStandards(createdBy, currentTimestamp));    
            cybersecurityStandards.AddRange(EndpointProtectionStandards(createdBy, currentTimestamp));   
            cybersecurityStandards.AddRange(ApplicationProtectionStandards(createdBy, currentTimestamp));   
            cybersecurityStandards.AddRange(IncidentReadinessStandards(createdBy, currentTimestamp));   
            cybersecurityStandards.AddRange(IncidentDetectionStandards(createdBy, currentTimestamp)); 
            cybersecurityStandards.AddRange(IncidentRemediationStandards(createdBy, currentTimestamp));              

            return cybersecurityStandards.ToArray();
        }

        private static List<CybersecurityStandard> StrategicPlanningStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("dcec4917-2291-43cb-8d2a-9e0945656b1c");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("7ac86897-9156-46fe-9819-4fa493eb10a8"),
                    Title = "A.5.1 Management direction for information security ",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("3b8c1214-8d43-4b00-b2c3-338708eec09d"),
                    Title = "A.6.1 Internal organization",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };               
        }

        private static List<CybersecurityStandard> SecurityPolicyFrameworkStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("96e4580d-05cb-4b7d-97b4-cba0a477bb1c");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("e496235c-48fe-455e-9fab-b511eab28e18"),
                    Title = "A.5.1.1 Policies for information security",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("1bc04738-8a09-4afa-b9c6-75ff0be8b1b5"),
                    Title = "A.5.1.2 Review of the policies",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("1cb1b75e-bbae-4ced-bad0-693e452c8651"),
                    Title = "A.6.1.1 Information security roles and responsibilities",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("85994a48-bb94-4d52-8ccf-e07a31aeb37d"),
                    Title = "A.7.2.1 Management responsibilities",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("6cec5259-3642-4f01-b17b-0e2bd741478a"),
                    Title = "A.12.1.1 Documented operating procedures",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("439528f1-180b-4ae6-8f8a-dca73180da98"),
                    Title = "A.18.1 Compliance with legal and contractual requirements",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }; 
        }

        private static List<CybersecurityStandard> OrganizationalStructureStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("b9912cf5-6e62-43ae-a0cd-36d7fbb52944");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("5fc7f2bc-2d7f-4e9e-a7ca-8747dfa0496a"),
                    Title = "A.6.1.1 Information security roles and responsibilities",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("6c2fbceb-0a4b-42cf-b2bd-28fc2ad5a04f"),
                    Title = "A.6.1.2 Segregation of duties",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };    
        }

        private static List<CybersecurityStandard> PerformanceMetricsStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("cd09235e-4fd1-4e8c-b751-f2bf6b449b27");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("adbe6661-fa71-42a4-a663-66f7c880b1fb"),
                    Title = "A.5.1 Management direction for information security",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("ae643a73-51d6-4909-8052-15cc37ce59df"),
                    Title = "A.18.2 Information security reviews",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> WorkforceManagementStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("a34bdc05-1dc9-4f6e-a3ef-a96d5ff90ed3");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("49455f7b-d17e-470e-b4c5-19da62b237ac"),
                    Title = "A.7.2 During employment",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> RiskManagementFrameworkStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("9452f450-6b56-437a-a6c0-9f7b03bfde8c");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("52be9935-fcbc-4947-8623-2d87f1a4f827"),
                    Title = "6.1.2 Information security risk assessment",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("9854999d-c1ac-4dbb-9163-a7b49b65bc6c"),
                    Title = "6.1.3 Information security risk treatment",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("cfe2eff6-36a1-4446-bfa9-f61597adb7f9"),
                    Title = "A.18.2 Information security reviews",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> ThreatManagementStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("bcbce862-3628-4f03-bd6b-4da0a08ce9c1");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("8e5df11a-ec67-4cf4-9656-185facc5de8e"),
                    Title = "8.2 Information security risk assessment",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("8c02a9b5-67a9-47b2-8c77-777a8667f7a7"),
                    Title = "8.3 Information security risk treatment",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("08552e3d-d220-470f-95e3-33dee2b51a8d"),
                    Title = "9.2 Internal audit",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("d1057caf-d580-423c-9a1b-5fb2a0cf58ce"),
                    Title = "A.12.6 Technical vulnerability management",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> SecurityAwarenessStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("169c5d65-75fb-41b9-b607-fb43343ba884");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("d8e9e33d-7319-4106-ad74-b9af424e179d"),
                    Title = "A.7.2.2 Information security awareness education and training",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("3fb0eb12-90ae-4e7e-b023-4f61200f83db"),
                    Title = "A.7.3 Awareness",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> DataProtectionFrameworkStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("15d6d4cb-e910-42c5-bb47-b4d03103edb0");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("36047e49-aa69-4e82-a5de-81bac999d98d"),
                    Title = "A.5.1.1 Policies for information security",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("9568dec5-7bc9-4a32-98d3-a0e2b3c98915"),
                    Title = "A.5.1.2 Review of policies for information security",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("1c56059e-1892-4ae5-b94f-f092f91d6d50"),
                    Title = "A.6.1.1 Information security roles and responsibilities",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> DataClassificationStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("7a697f1e-464c-4342-b369-33955d46663d");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("b37859b1-e50a-40fa-90a6-63afbb2fdf46"),
                    Title = "A.8.2.1 Classification of Information",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("9935d180-ad4c-47ef-8f46-1882448c4281"),
                    Title = "A.8.2.2 Labelling of Information",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("fadf9a31-d43e-492f-bb29-fdf7c0d91047"),
                    Title = "A.8.2.3 Handling of Assets",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> DataProtectionPoliciesStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("32b2f015-8e14-4cfa-936a-6c58e18442cc");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("66a4e76e-3b47-4430-b1eb-4493c8edf4eb"),
                    Title = "A.10.1 Cryptographic controls",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("aec39e2d-a74d-48ad-8cca-7b5924811b20"),
                    Title = "A.13.1 Network security management",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("a6fa3a68-707b-4db2-bed7-407fd7fc8cd7"),
                    Title = "A.13.2 Information transfer",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("a3a0478b-e65e-48b3-b6e0-dc732c81a6a8"),
                    Title = "A.14.1 Security requirements of information systems",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("1ba2b6e3-3266-445e-8559-4d1f47869f93"),
                    Title = "A.14.2 Security in development and support processes",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> DataRetentionStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId =  Guid.Parse("82083045-11e8-4f48-a91c-eb615c34bffa");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("6c4fbe54-075c-44c1-a199-5d1d3db99995"),
                    Title = "A.8.2.3 Handling of Assets",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("8edf7495-9340-49a7-89fa-01a3d0ccc7d7"),
                    Title = "A.8.3 Media Handling",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("911d14bd-69cd-41d0-a022-ddfc09003d35"),
                    Title = "A.8.3.2 Disposal of media",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("853658f6-536f-479c-86ff-14dbc4ab8e74"),
                    Title = "A.8.3.3 Physical media transfer",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("c1eef4b0-3a2f-4b12-84de-acf9869d386d"),
                    Title = "A.11.2.7 Secure disposal or reuse of equipment",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> DataLossStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("952d593d-6f25-4418-bfe4-9f1dc59a4221");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("57a95d14-52d3-49cf-997c-a81b812c42b8"),
                    Title = "A.16.1.1 Responsibilities and procedures",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("be26e8da-f5d9-40f5-84d0-b717f3ba31b0"),
                    Title = "A.16.1.2 Reporting information security events",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("e0c352ed-e35e-4367-83c4-9ebbc70cbfd6"),
                    Title = "A.16.1.4 Assessment of and decision on information security events",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> DataRecoveryStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("fa4e445e-c6dc-4c8c-b386-09ac54cbe0e2");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("d6cc2167-5d22-4654-b151-ec05cb817129"),
                    Title = "A.12.3 Backup",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("a0eed5a8-214d-4457-bd52-7ce7d5030164"),
                    Title = "A.17.1 Information security continuity",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("7c905610-484c-45d4-88a3-413a692574fe"),
                    Title = "A.17.2 Redundancies",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> IdentityManagementStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId =  Guid.Parse("1f0fd461-a017-421b-a6fc-9e5ffebd91ae");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("1376b9bd-c05f-4d32-ac32-0145e1776598"),
                    Title = "A.7.1 Prior to employment",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("2182c750-a7cb-417b-a2a4-dbef67f63f18"),
                    Title = "A.7.2 During employment",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("2b461cdd-8778-4710-a4c2-b270599d3645"),
                    Title = "A.7.3 Termination and change of employment",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("ac442987-1fdd-4f56-b6be-fa7df032f275"),
                    Title = "A.9.2 User access management",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("0f141b5f-3401-460f-ae48-04d5e1306e07"),
                    Title = "A.9.3 User responsibilities",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> AccessControlsStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("1a8e9b19-ec14-49fb-a2ce-ac0b89a92ed5");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("04da7ec7-3cb6-49eb-9247-38a098c6eca1"),
                    Title = "A.9.1 Business requirements of access control",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("1ee4a812-5622-438e-af8b-ea21f0f9bf49"),
                    Title = "A.9.4 System and application access control",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId =subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }      
            };
        }

        private static List<CybersecurityStandard> SeparationOfDutiesStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("4e802dfe-3143-4a5a-b8ab-ecdcc9106bd8");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("1f435caf-653c-4f50-b4e9-e2680c07e729"),
                    Title = "A.6.1.2 Segregation of duties",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("2123ec40-60ec-444e-a2ba-9876c9528312"),
                    Title = "A.12.1.4 Separation of development testing and operational environments",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> PrivilegedAccessManagementStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("c062770f-2cc3-4f56-b87a-5d51fcabc9b7");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("51560e80-5481-4f23-853c-169198dde1d5"),
                    Title = "A.9.2.3 Management of privileged access rights",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("a3a74294-caf3-46f8-b9f7-e25b3df9be5a"),
                    Title = "A.9.4.1 Information access restriction",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("fd8fb024-faac-4b64-81b3-37880644a6c7"),
                    Title = "A.9.4.4 Use of privileged utility programs",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> RemoteAccessStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("61543e20-477b-4c07-a57a-cc856cbbf562");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("2a37498a-b0b9-47f9-9593-a8a24c03981a"),
                    Title = "A.6.2.2 Teleworking",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("3bf9f8f0-64a9-4195-8f24-260b83f6dcfe"),
                    Title = "A.13.1.1 Network controls",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("deb02009-5355-4161-b87d-a19c8d045ad5"),
                    Title = "A.13.2.1 Information transfer policies and procedures",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("978d84bb-eb49-4cb7-a997-d5ed58d3f9db"),
                    Title = "A.14.1.2 Securing application services on public networks",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> ThirdPartyAccessStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("c71b6378-ca1c-4f87-94e5-bb79867ed456");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("fac0a9ec-5950-4b7b-9404-bececa6e59ef"),
                    Title = "A.9.2.1 User registration and de-registration",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("4a027c42-6c6b-4cc3-9d5f-d5a37a7d28e2"),
                    Title = "A.9.2.2 User access provisioning",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("490a2ecc-e86f-4ffa-a23f-b42144a4ae03"),
                    Title = "A.9.2.5 Review of user access rights",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("470f3771-cc59-4aab-8faa-1844cfafa155"),
                    Title = "A.13.1.2 Security of network services",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("7f814c4b-4983-4584-8e74-d4dbbdd18986"),
                    Title = "A.13.2.2 Agreements on information transfer",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("ccf46525-592f-4cb6-a304-3519e4a7d12b"),
                    Title = "A.15.1.2 Addressing security within supplier agreements",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("ba52848f-3df3-4398-a1f0-81d2001dcc80"),
                    Title = "A.15.2.2 Managing changes to supplier services",
                    Order = 7,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> NetworkProtectionStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("1e676fbe-3330-43ab-9da4-b3ccb42f5068");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("3999a7f7-9143-42c1-bf63-e796d61748f9"),
                    Title = "A.9.1.2 Access to networks and network services",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("61d56c21-4c05-48da-86bb-2aab90db2eb5"),
                    Title = "A.13.1.1 Network controls",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("ae7e62d3-353f-4ed4-bde5-0363018d397e"),
                    Title = "A.13.1.2 Security of network services",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("ccaa1632-d609-44a5-944a-f99489038867"),
                    Title = "A.13.1.3 Segregation in networks",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("782d4230-7017-4463-8130-2ae5dc87ce1a"),
                    Title = "A.14.1.3 Protecting application services transactions",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }      
            };
        }

        private static List<CybersecurityStandard> EndpointProtectionStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("ffcdde59-8844-433a-92ea-714bf1fc8abc");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("22eb984e-4fc8-4b28-ad0c-c3777b37b7f8"),
                    Title = "A.6.2.1 Mobile device policy",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("0addb8cf-6ca2-49ed-89aa-fbed18d0e417"),
                    Title = "A.12.1.2 Change management",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("9b2621d5-b77b-4531-a048-32429c42f3d5"),
                    Title = "A.12.2 Protection from malware",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("4ddbf9a8-12a7-42fe-aa09-2c13c8686033"),
                    Title = "A.12.6.2 Restrictions on software installation",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> ApplicationProtectionStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("44a2cc87-75ac-49a9-9275-1b470f6509b3");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("6806b587-e0b0-4d74-b0a2-729c907a733d"),
                    Title = "A.12.5 Control of operational software",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("e4bc5ab8-0ebe-43c7-afe6-286ee75c24e8"),
                    Title = "A.14.1.2 Securing application services on public networks",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("93a37ce4-594b-4c01-98a0-cb36094aeb80"),
                    Title = "A.14.2.1 Secure development policy",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("59eb417b-2254-4346-93d1-5f2340665ee5"),
                    Title = "A.14.2.2 System change control procedures",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("7bbfef39-a184-4d5c-b706-4bae3b3bd059"),
                    Title = "A.14.2.3 Technical review of applications after operating platform changes",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("7fb8e715-abdb-4d57-a86e-f494dae1ca0e"),
                    Title = "A.14.2.4 Restrictions on changes to software packages",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("300b7f39-e9ac-4e6b-a26c-2dd3f05f8b25"),
                    Title = "A.14.2.5 Secure system engineering principles",
                    Order = 7,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("293ccd97-e18f-4cbf-af59-a478ce66cf79"),
                    Title = "A.14.2.6 Secure development environment",
                    Order = 8,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("1e14c8c0-f8dc-4f0a-b911-2de9d595c497"),
                    Title = "A.14.2.7 Outsourced development",
                    Order = 9,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("3c0222e0-0302-444e-905e-601eff94279b"),
                    Title = "A.14.2.8 System security testing",
                    Order = 10,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> IncidentReadinessStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("66ea6203-8e60-4fd6-8f45-4768fe375a5b");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("ecf20d49-d36e-4d31-9c34-26a2ee290d12"),
                    Title = "A.6.1.1 Information security roles and responsibilities",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("a6f84127-7330-48c0-a675-53c7dab8cdf5"),
                    Title = "A.6.1.3 Contact with authorities",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("6115d977-0880-47b9-ac5d-e727767613d5"),
                    Title = "A.6.1.4 Contact with special interest groups",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("14bd5704-e266-4916-9572-21985a002fcc"),
                    Title = "A.7.2.2 Information security awareness education and training",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("1032c5cc-8152-4754-9ecf-415a9d1cf688"),
                    Title = "A.16.1.1 Responsibilities and procedures",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }        
            };
        }

        private static List<CybersecurityStandard> IncidentDetectionStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("a1e57c2b-6331-4023-9d84-aafe8233783f");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("42f7a293-2583-45ab-aaca-5a4b26b2949a"),
                    Title = "A.12.4.1 Event logging",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("d1256c83-afb9-499c-a94a-fa64091f5f6a"),
                    Title = "A.12.4.2 Protection of log information",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("50a61cbc-27e8-4c5c-85a6-7bec95b3a33b"),
                    Title = "A.12.4.4 Clock synchronization",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("7ed69e63-49ad-4f3d-81c1-bebb07a88b97"),
                    Title = "A.16.1.2 Reporting information security events",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("3ac04f18-1ecf-4e98-8e9e-59b093bfebd7"),
                    Title = "A.16.1.3 Reporting information security weaknesses",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("5346da18-4f31-45f2-90a7-81645cc625f7"),
                    Title = "A.16.1.4 Assessment of and decision on information security events",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("4556e1ee-0611-44e7-85b9-ee577524eb28"),
                    Title = "A.16.1.7 Collection of evidence",
                    Order = 7,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> IncidentRemediationStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("a85131ca-5999-47e6-ab90-22db0c1d1236");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("afe2f3cf-3128-45dc-b06d-6af34a229083"),
                    Title = "A.7.2.3 Disciplinary process",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("3f24b940-6268-43c9-b3e2-8249ac524f07"),
                    Title = "A.16.1.5 Response to information security incidents",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("8667955a-6131-430f-9a1e-5ea9c23f8a79"),
                    Title = "A.16.1.6 Learning from information security incidents",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.