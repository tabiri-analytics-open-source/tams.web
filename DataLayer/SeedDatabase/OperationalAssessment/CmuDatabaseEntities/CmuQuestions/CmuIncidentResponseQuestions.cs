using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.CmuDatabaseEntities.CmuQuestions
{
    public static class CmuIncidentResponseQuestions
    {
        public static Question[] Seed(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Empty;

            var results = new List<Question>();

            #region Incident Readiness Questions

            subcategoryId = Guid.Parse("66ea6203-8e60-4fd6-8f45-4768fe375a5b");

            var incidentReadinessQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("aa0c450d-6dc5-4486-bde7-263ca2bd9592"),
                    MainText = "Does your organization have an Incident Response Plan (IRP)?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f682e8e0-e444-49fa-bf16-5a1026a907ad"),
                    MainText = "What CERT/CIRT/SOC roles are defined in the IRP?",                    
                    HintText = "Computer Emergency/Incident Response Team, Security Operations Center",
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("aa0c450d-6dc5-4486-bde7-263ca2bd9592"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }, 
                new Question
                {
                    Id = Guid.Parse("3d7e3b08-4460-45ff-b512-7bfc59d3d50b"),
                    MainText = "Does the IRP document requirements for dealing with external parties?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("aa0c450d-6dc5-4486-bde7-263ca2bd9592"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("36760b51-52b4-4e18-abec-31a0e2690de0"),
                    MainText = "Which requirements for dealing with external parties have been documented?",
                    OptionChoices = "Digital forensics services,Law enforcement notification,Legal services,Public/media relations",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("3d7e3b08-4460-45ff-b512-7bfc59d3d50b"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("99d30388-1ae7-4435-876f-3306529547ab"),
                    MainText = "Do you have existing retainers for these services?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("3d7e3b08-4460-45ff-b512-7bfc59d3d50b"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("3b265c68-259f-4a84-a118-7b6056502a50"),
                    MainText = "Does the IRP document an incident reporting structure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("aa0c450d-6dc5-4486-bde7-263ca2bd9592"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("a1b587e1-af7a-4448-8350-684d46c9b8f3"),
                    MainText = "Does the IRP document incident escalation procedures?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("aa0c450d-6dc5-4486-bde7-263ca2bd9592"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("860d3373-90a6-41d0-8e2d-e665aba181bb"),
                    MainText = "Does the IRP document a process for mandatory leave when investigating internal staff?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("aa0c450d-6dc5-4486-bde7-263ca2bd9592"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0375bbaf-4d25-49f0-8bf8-474c7af1ffcf"),
                    MainText = "How often is this policy reviewed?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("aa0c450d-6dc5-4486-bde7-263ca2bd9592"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("984eed47-d2e0-4ba8-9cbb-b839b19bd9ca"),
                    MainText = " Does your organization have a CERT/CIRT/SOC team?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("6f7f06c8-6bb7-4dca-9801-24e163a1556c"),
                    MainText = "Do you have tiered security analyst roles?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("984eed47-d2e0-4ba8-9cbb-b839b19bd9ca"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a68e78a5-bc84-477d-a7e1-f24dda3e334e"),
                    MainText = "Which tiered security analyst roles do have you defined?",
                    OptionChoices = "Level 1, Level 2, Level 3",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("6f7f06c8-6bb7-4dca-9801-24e163a1556c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f6ff2461-fd09-48ac-8d51-7867a3ab41d7"),
                    MainText = "Which tiered security analyst roles are internal?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("6f7f06c8-6bb7-4dca-9801-24e163a1556c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question {
                    Id = Guid.Parse("6eac0bb9-0f76-4269-96e4-27781b3fa198"),
                    MainText = "Which tiered security analyst roles are outsourced?",                    
                    Order = 3,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("6f7f06c8-6bb7-4dca-9801-24e163a1556c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("4dd51306-e667-478d-8b19-b0a57054bdd3"),
                    MainText = "Does the IRP have a 24/7 staffing plan for the CERT/CIRT/SOC team?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("984eed47-d2e0-4ba8-9cbb-b839b19bd9ca"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d7db6e28-7cf2-4415-86cb-ece52da77226"),
                    MainText = "How many CERT/CIRT/SOC team members are staffed on each shift?",                    
                    Order = 1,
                    QuestionType = QuestionType.Integer,
                    ParentId = Guid.Parse("4dd51306-e667-478d-8b19-b0a57054bdd3"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("74b4c500-9f74-49dd-8ace-a1b9255f7b4c"),
                    MainText = "How many shifts do you have per 24-hour period?",                    
                    Order = 2,
                    QuestionType = QuestionType.Integer,
                    ParentId = Guid.Parse("4dd51306-e667-478d-8b19-b0a57054bdd3"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("175c98f6-77e3-471b-8000-019145fb9060"),
                    MainText = "Are members of the CERT/CIRT/SOC team aware of their assigned roles?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("984eed47-d2e0-4ba8-9cbb-b839b19bd9ca"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("fb038b3f-f0bb-4ab4-acda-c2dc6e2399b8"),
                    MainText = "Have team members been trained in their respective roles?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("175c98f6-77e3-471b-8000-019145fb9060"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a0b4cc53-3a10-41fa-b137-83c34c19402d"),
                    MainText = "How often is training provided?",
                    OptionChoices = "Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("fb038b3f-f0bb-4ab4-acda-c2dc6e2399b8"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c17a4f00-7439-49d8-911d-0c2dc02048be"),
                    MainText = "Are any members of the CERT/CIRT/SOC team certified?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("175c98f6-77e3-471b-8000-019145fb9060"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("205d0be6-052d-4251-b317-75bd91f96ae6"),
                    MainText = "What industry certifications do members of the CERT/CIRT/SOC team possess?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("c17a4f00-7439-49d8-911d-0c2dc02048be"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("9b370007-d109-4a42-aae1-57d68c8f8eea"),
                    MainText = "Is there a process to review CERT/CIRT/SOC team members performance?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("984eed47-d2e0-4ba8-9cbb-b839b19bd9ca"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("62915318-c6d9-4d16-b27b-cae985cf3146"),
                    MainText = "What does this process look like?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("9b370007-d109-4a42-aae1-57d68c8f8eea"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question {
                    Id = Guid.Parse("9a7310cd-8bcb-4c61-b0d5-3d7240e6ede4"),
                    MainText = "Do you conduct periodic incident response drill exercises?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("709e342b-510b-45e5-8675-13ed25f49027"),
                    MainText = "What kind of incident response drills do you conduct?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("9a7310cd-8bcb-4c61-b0d5-3d7240e6ede4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("69af4468-9b38-4053-b94b-191b69faea71"),
                    MainText = "How often are these incident response drills conducted?",
                    OptionChoices = "Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("9a7310cd-8bcb-4c61-b0d5-3d7240e6ede4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("96af8623-6320-4f5b-b701-7ade457b3091"),
                    MainText = "Do you participate in any cyber threat information sharing groups or associations?",
                    HintText = "e.g. an industry specific Information Sharing and Analysis Center",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("047e2fe5-c918-429d-aad4-16dc22d57d73"),
                    MainText = "Which cyber threat information sharing groups or associations do you participate in?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("96af8623-6320-4f5b-b701-7ade457b3091"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("afe82361-0792-417c-ae0b-2256decca7c3"),
                    MainText = "Do the information sharing groups or associations provide actionable threat intelligence information?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("96af8623-6320-4f5b-b701-7ade457b3091"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(incidentReadinessQuestions);

            #endregion

            #region Incident Detection Questions

            subcategoryId = Guid.Parse("a1e57c2b-6331-4023-9d84-aafe8233783f");

            var incidentDetectionQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("948fcc78-b5d7-4489-98d7-70281a433743"),
                    MainText = "Does your organization have an internal Security Operations Center or Managed Security Service Provider?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("aeb35f80-17da-4e7c-9ada-cddfa8d9a851"),
                    MainText = "Has your organization handled any cyber security incidents in the last 12 months?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ef613490-64ae-4158-ac06-458ed3bf6381"),
                    MainText = "What kind of cyber security incidents?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("aeb35f80-17da-4e7c-9ada-cddfa8d9a851"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("52555887-c084-468a-b77e-bb5140cb13f3"),
                    MainText = "Do you (or your managed security service provider) have a SIEM capability?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("87ca50c8-abbb-4245-8dd4-fbcb6792955f"),
                    MainText = "What logs does the SIEM collect?",
                    OptionChoices = "Application,DHCP,DNS,Endpoint,Firewall,Netflow,NIDS,VPN,Web proxy",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("52555887-c084-468a-b77e-bb5140cb13f3"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("aeec0e25-6460-4a0f-a3cd-b15b531d0910"),
                    MainText = "What is your maximum log retention period?",
                    OptionChoices = "1 day,1 week,1 month,6 months,1+ year",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("52555887-c084-468a-b77e-bb5140cb13f3"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("444f9981-386a-44a4-b28d-0d8c05bc8206"),
                    MainText = "Are cyber threat intelligence feeds integrated into the SIEM?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("52555887-c084-468a-b77e-bb5140cb13f3"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("57ab7a62-8570-4bd4-963d-b881e95dc69e"),
                    MainText = "Are logs correlated across different event types?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("52555887-c084-468a-b77e-bb5140cb13f3"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("4a8fd35b-147d-43c8-9cee-f98bde764af6"),
                    MainText = "How are logs correlated across different event types?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("57ab7a62-8570-4bd4-963d-b881e95dc69e"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("bfd7ef80-2d4f-4e89-a1a2-4b1adf80db12"),
                    MainText = "To what extent is log correlation automated?",
                    OptionChoices = "None,Partial,Full",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("57ab7a62-8570-4bd4-963d-b881e95dc69e"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0d51a21d-70d8-4bdd-926c-9ce85fc942dd"),
                    MainText = "Are timestamps synchronized across logs?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("52555887-c084-468a-b77e-bb5140cb13f3"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("bc578907-f203-4a52-9d67-5b1358553070"),
                    MainText = "What timezone are logs synchronized to?",
                    OptionChoices = "Coordinated Universal Time (UTC),Local time",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("0d51a21d-70d8-4bdd-926c-9ce85fc942dd"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("455ee45c-1008-443e-94fc-79dc2cdc33d9"),
                    MainText = "Do you (or your managed security service provider) have measures in place to prevent tampering of logs?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("754a1bf8-1f14-4179-9381-e924dc214476"),
                    MainText = "What measures are in place to prevent tampering of logs?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("455ee45c-1008-443e-94fc-79dc2cdc33d9"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("79c183d6-0338-47a8-bd8a-b150f7cb6dde"),
                    MainText = "Do you (or your managed security service provider) have incident detection procedures?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c13fe746-92bd-42f5-9097-2953381c8d08"),
                    MainText = "What types of incident detection procedures?",
                    OptionChoices = "Data exfiltration,Denial of service,Policy violation,System compromise,Unauthorized access",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("79c183d6-0338-47a8-bd8a-b150f7cb6dde"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b2e1471e-3111-4328-ba68-43dc53714aa0"),
                    MainText = "Are CERT/CIRT/SOC staff trained on these procedures?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("79c183d6-0338-47a8-bd8a-b150f7cb6dde"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("cc26586b-9c0d-48c7-9e1c-b20855625d18"),
                    MainText = "How often are these procedures reviewed?",
                    OptionChoices = "Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("79c183d6-0338-47a8-bd8a-b150f7cb6dde"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(incidentDetectionQuestions);

            #endregion

            #region Incident Remediation Questions
            
            subcategoryId = Guid.Parse("a85131ca-5999-47e6-ab90-22db0c1d1236");

            var incidentRemediationQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("11be1d35-d90a-4841-b646-d253dfdb5f70"),
                    MainText = "Does your organization have incident containment procedures?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("fdf29127-3748-4fad-aaac-7bc0d77a26b1"),
                    MainText = "Which incident containment procedures have you documented?",
                    OptionChoices = "Incident scoping,Network isolation,System isolation",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("11be1d35-d90a-4841-b646-d253dfdb5f70"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("416b8692-406f-486f-a68e-1425f0e0b4e5"),
                    MainText = "Are CERT/CIRT/SOC staff trained on these incident containment procedures?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("11be1d35-d90a-4841-b646-d253dfdb5f70"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ca8335c1-dc73-4b77-a82b-0ccc88bb2271"),
                    MainText = "Does your organization have tools to contain incidents?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("11be1d35-d90a-4841-b646-d253dfdb5f70"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2f043117-9c35-4206-afe3-5933744216a0"),
                    MainText = "What tools are available to contain incidents?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("ca8335c1-dc73-4b77-a82b-0ccc88bb2271"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question {
                    Id = Guid.Parse("0cd3c1c3-a9cb-4901-8d31-2b5e05dc6492"),
                    MainText = "Are these containment tools centrally managed?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("ca8335c1-dc73-4b77-a82b-0ccc88bb2271"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ebcf24b4-9402-45e9-b38b-21141f776d76"),
                    MainText = "How often are these procedures reviewed?",
                    OptionChoices = "Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("11be1d35-d90a-4841-b646-d253dfdb5f70"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("4810fa8d-a990-4a86-97f6-6296753a77a4"),
                    MainText = "Does your organization have digital forensics procedures?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("2875b91b-e6d9-4aa7-8496-843a6ce65318"),
                    MainText = "Which digital forensics procedures have you documented?",
                    OptionChoices = "Evidence analysis,Evidence collection,Evidence handling",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("4810fa8d-a990-4a86-97f6-6296753a77a4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("9df2d222-01bc-4ba8-a295-5386ebec42a9"),
                    MainText = "Are CERT/CIRT/SOC staff trained on these digital forensics procedures?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("4810fa8d-a990-4a86-97f6-6296753a77a4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("4aff459f-3218-423d-a2ef-fced158d14a0"),
                    MainText = "Does your organization have tools to collect digital forensics?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("4810fa8d-a990-4a86-97f6-6296753a77a4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("df4f610a-950f-4b33-9e8e-29da4b499468"),
                    MainText = "What tools are available to collect digital forensics?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("4aff459f-3218-423d-a2ef-fced158d14a0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("31797d98-d035-408a-9e9e-215fe136a617"),
                    MainText = "What capabilities do these tools have?",
                    OptionChoices = "Disk forensics,Memory forensics,Network forensics",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("4aff459f-3218-423d-a2ef-fced158d14a0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("3b8c7b2d-14c3-48ea-a3ec-df1cdb08b460"),
                    MainText = "Are these digital forensics collection tools centrally managed?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("4aff459f-3218-423d-a2ef-fced158d14a0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a3237640-8269-477a-b177-293acde1c731"),
                    MainText = "Does your organization have tools to analyze digital forensics?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("4810fa8d-a990-4a86-97f6-6296753a77a4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("78a6bcc1-270f-4e98-a85c-d30038ebbad8"),
                    MainText = "What tools are available to analyze digital forensics?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("a3237640-8269-477a-b177-293acde1c731"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("daf6e301-38eb-4c23-9962-644778bbd250"),
                    MainText = "What capabilities do these tools have?",
                    OptionChoices = "Content analysis,Dynamic code analysis,Feature extraction,Static code analysis",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("a3237640-8269-477a-b177-293acde1c731"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f2ed4a35-32e5-40e7-a76b-58eec8ee53a1"),
                    MainText = "Do you have a dedicated digital forensics lab?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("4810fa8d-a990-4a86-97f6-6296753a77a4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("24eb1870-dd20-49bf-835f-5d4b60cf7003"),
                    MainText = "How often are these procedures reviewed?",
                    OptionChoices = "Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("4810fa8d-a990-4a86-97f6-6296753a77a4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("aa06e871-d4eb-41d1-8a4b-c19258c824ca"),
                    MainText = "Does your organization have a case management tool?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("3396ff26-a9d8-409b-b8aa-269611e57d4a"),
                    MainText = "What type of case management tool do you have?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("aa06e871-d4eb-41d1-8a4b-c19258c824ca"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("4225ebb3-cdcf-4049-b8c4-faeb9718e86a"),
                    MainText = "Does your organization track incident metrics?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e64aa3a3-726d-469a-a2e1-b2eda4b9ad7b"),
                    MainText = "What incident metrics do you track?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("4225ebb3-cdcf-4049-b8c4-faeb9718e86a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("87fcd5c6-d4e3-4628-8c4e-1709e327eb7b"),
                    MainText = "How do you collect these incident metrics?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("4225ebb3-cdcf-4049-b8c4-faeb9718e86a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("cb574e27-4123-41e8-a0d0-9585ae34241c"),
                    MainText = "Do you track the average time to detect incidents?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("4225ebb3-cdcf-4049-b8c4-faeb9718e86a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a8ba2fd4-dfc3-4367-9e9c-991f62e92fad"),
                    MainText = "What incident categories does this metric track?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("cb574e27-4123-41e8-a0d0-9585ae34241c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("5f057a49-7c7b-4551-bf0f-e633f34defd9"),
                    MainText = "Do you track the average time to remediate incidents?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("4225ebb3-cdcf-4049-b8c4-faeb9718e86a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c5042043-862e-473c-be5c-972b4ee5611b"),
                    MainText = "What incident categories does this metric track?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("5f057a49-7c7b-4551-bf0f-e633f34defd9"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("bab93073-4d5f-4339-9aa8-4b85179dddb5"),
                    MainText = "Do these metrics factor into staff performance?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("4225ebb3-cdcf-4049-b8c4-faeb9718e86a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("13f074aa-6990-407b-b5c0-e9243a9e76d2"),
                    MainText = "How do they factor into staff performance?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("bab93073-4d5f-4339-9aa8-4b85179dddb5"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f1d19bcd-808a-4a62-83a9-8334a294b107"),
                    MainText = "Do these metrics factor into strategic planning?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("4225ebb3-cdcf-4049-b8c4-faeb9718e86a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("9ce11240-14ce-4120-b623-874706ec3191"),
                    MainText = "How do they factor into strategic planning?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("f1d19bcd-808a-4a62-83a9-8334a294b107"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(incidentRemediationQuestions);

            #endregion

            return results.ToArray();            
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.