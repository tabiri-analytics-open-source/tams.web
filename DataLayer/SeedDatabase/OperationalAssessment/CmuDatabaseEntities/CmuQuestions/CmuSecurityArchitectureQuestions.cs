using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.CmuDatabaseEntities.CmuQuestions
{
    public static class CmuSecurityArchitectureQuestions
    {
        public static Question[] Seed(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Empty;

            var results = new List<Question>();

            #region Network Protection Questions

            subcategoryId = Guid.Parse("1e676fbe-3330-43ab-9da4-b3ccb42f5068");

            var networkProtectionQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("54627f03-781a-48ee-b174-87f6f3820956"),
                    MainText = "Does your organization have a Network Protection policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("77d35621-9f8d-4ede-89e7-a5ec29fdd398"),
                    MainText = "Have you defined processes and procedures for evaluating, deploying and managing network protection solutions?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("54627f03-781a-48ee-b174-87f6f3820956"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("fb52340a-38da-43cf-9dee-9a4d6b2d2843"),
                    MainText = "Have you defined processes and procedures for segmenting your network?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("54627f03-781a-48ee-b174-87f6f3820956"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("47e45a58-015e-461b-837f-8ae53ad16eb7"),
                    MainText = "How often is this policy reviewed?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("54627f03-781a-48ee-b174-87f6f3820956"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a88bde1c-66a0-4640-97c9-f20a4d783073"),
                    MainText = "Do you have network firewalls deployed at your organization?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d928a926-d3fb-4b64-a957-5db25e176e0f"),
                    MainText = "What is the placement of the network firewalls?",
                    OptionChoices = "DMZ,Internal,Both",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("a88bde1c-66a0-4640-97c9-f20a4d783073"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("1069c4ef-7a6c-4d6f-9929-12960324e29c"),
                    MainText = "What Open Systems Interconnection (OSI) layers do your network firewalls support?",
                    OptionChoices = "Application layer,Network layer,Transport layer,Other",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("a88bde1c-66a0-4640-97c9-f20a4d783073"),
                    SubcategoryId = subcategoryId,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f4e6b26c-9d35-4855-9dce-813678a851d7"),
                    MainText = "Are your network firewalls configured to block traffic that does not match an explicit allow rule?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("a88bde1c-66a0-4640-97c9-f20a4d783073"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2f30dcdf-951f-46f5-8f60-d20090a1bd41"),
                    MainText = "Are your network firewalls configured to silently drop blocked traffic?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("a88bde1c-66a0-4640-97c9-f20a4d783073"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e9dd25bd-48aa-4ce4-b4bc-468be9e8274e"),
                    MainText = "Are your network firewalls configured to send TCP Reset, ICMP Destination Unreachable, or other error codes on blocked traffic?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("a88bde1c-66a0-4640-97c9-f20a4d783073"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("07d1ed34-b29a-45d9-a1b7-086f8777bdd3"),
                    MainText = "Do you have a process for managing the firewall rules?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("a88bde1c-66a0-4640-97c9-f20a4d783073"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0b1cf804-56d1-4726-87a9-a334f89e789a"),
                    MainText = "What is the process for managing the firewall rules?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("07d1ed34-b29a-45d9-a1b7-086f8777bdd3"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("79ff9454-e68f-4efe-a0a0-3bf130de1188"),
                    MainText = "Do you have a virtual/cloud computing infrastructure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 7,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("a88bde1c-66a0-4640-97c9-f20a4d783073"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1261669a-f636-4726-a721-3eb1eb022d08"),
                    MainText = "Do you have a similar network firewall configuration on your virtual/cloud computing infrastructure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("79ff9454-e68f-4efe-a0a0-3bf130de1188"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("531d36ad-b9c5-4f8a-9587-1ef1b71a5cb0"),
                    MainText = "Do you (or your provider) have a SIEM capability?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 8,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("a88bde1c-66a0-4640-97c9-f20a4d783073"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("616d77b5-3d74-4b92-9fc1-de7b95efc721"),
                    MainText = "Do you forward the network firewall logs to the SIEM?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("531d36ad-b9c5-4f8a-9587-1ef1b71a5cb0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0b962441-3aeb-4430-bfb3-1bce0d2da92f"),
                    MainText = "Do you have a process to review network firewall logs?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 9,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("a88bde1c-66a0-4640-97c9-f20a4d783073"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d14a98d4-e415-45aa-b313-7129f1bc1cca"),
                    MainText = "What is the process for reviewing network firewall logs?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("0b962441-3aeb-4430-bfb3-1bce0d2da92f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("dcee74ca-7d89-424d-b0ae-bd8f73c3fc4c"),
                    MainText = "How often do you review network firewall logs?",
                    OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("0b962441-3aeb-4430-bfb3-1bce0d2da92f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("4027d162-5187-4618-936c-2fb973379c45"),
                    MainText = "Do you have network Intrusion Detection Systems (IDS) or Intrusion Prevention Systems (IPS) deployed at your organization?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("9bb97556-bd38-44a6-919e-f6d90b51069a"),
                    MainText = "What is the placement of the network IDS/IPS?",
                    OptionChoices = "DMZ,Internal,Both",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("4027d162-5187-4618-936c-2fb973379c45"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d8ed1fd6-8d9d-4811-8bc0-3251140b63ec"),
                    MainText = "What mode is the network IDS/IPS deployed in?",
                    OptionChoices = "Block mode,Monitor only",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("4027d162-5187-4618-936c-2fb973379c45"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("cc0b6487-c100-4c6d-81ab-da5b18011e7e"),
                    MainText = "Do you have a process for managing the network IDS/IPS signatures?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("4027d162-5187-4618-936c-2fb973379c45"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2c2a897a-ca62-4ab8-898a-16529f00f5f1"),
                    MainText = "What is the process for managing the network IDS/IPS signatures?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("cc0b6487-c100-4c6d-81ab-da5b18011e7e"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("9d143f49-a1f0-401e-9397-95975f2137cb"),
                    MainText = "Does your network IDS/IPS have the capability to obtain full packet captures?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("4027d162-5187-4618-936c-2fb973379c45"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("733c4b9f-bae6-4b13-babb-6b39641f5d90"),
                    MainText = "On average, how many days of network traffic are available from the full packet captures at any given time?",                    
                    Order = 1,
                    QuestionType = QuestionType.Integer,
                    ParentId = Guid.Parse("9d143f49-a1f0-401e-9397-95975f2137cb"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("98e6cb41-b310-4dd6-ab9d-d8ced0843cc2"),
                    MainText = "Does your network IDS/IPS have the capability to decrypt SSL/TLS traffic prior to inspection?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("4027d162-5187-4618-936c-2fb973379c45"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("6d28fdf4-c9bc-45d7-8322-c3abd4c08330"),
                    MainText = "Does your network IDS/IPS have the capability to extract and analyze executable code in a malware sandbox?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("4027d162-5187-4618-936c-2fb973379c45"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a31a3fe6-2429-41b7-b0b1-ee71ab8a68dd"),
                    MainText = "Do you have a virtual/cloud computing infrastructure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 7,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("4027d162-5187-4618-936c-2fb973379c45"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b0547173-545d-4128-9c1c-e31538993a03"),
                    MainText = "Do you have a similar network IDS/IPS configuration on your virtual/cloud computing infrastructure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("a31a3fe6-2429-41b7-b0b1-ee71ab8a68dd"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7d63bb56-467f-447f-9da1-422d94b1b52f"),
                    MainText = "Do you have a SIEM?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 8,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("4027d162-5187-4618-936c-2fb973379c45"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("fb89dd93-af14-4533-8c78-eac9899650dd"),
                    MainText = "Do you forward the network IDS/IPS logs to the SIEM?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("7d63bb56-467f-447f-9da1-422d94b1b52f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("44fe115d-4e22-4215-96e3-822fb7fbd918"),
                    MainText = "Do you have a process to review network IDS/IPS logs?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 9,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("4027d162-5187-4618-936c-2fb973379c45"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d33112d1-7c2f-436e-a4fd-4129beeaacbf"),
                    MainText = "What is the process for reviewing network IDS/IPS logs?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("44fe115d-4e22-4215-96e3-822fb7fbd918"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c3873d23-9189-4fde-b7a9-f54ffc5e7d11"),
                    MainText = "How often do you review network IDS/IPS logs?",
                    OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("44fe115d-4e22-4215-96e3-822fb7fbd918"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("aa46fd52-47dd-4f56-9239-37f53c8ecf13"),
                    MainText = "Do you have network web proxies at your organization?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("06a96f5a-ff0d-4031-8235-441810d10ff9"),
                    MainText = "What mode are the network web proxies deployed in?",
                    OptionChoices = "Block mode,Monitor only",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("aa46fd52-47dd-4f56-9239-37f53c8ecf13"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("69031625-6c17-42ce-8924-18cf1f88e6c9"),
                    MainText = "What type of network web proxies are deployed?",
                    OptionChoices = "Transparent (inline),Non-transparent",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("aa46fd52-47dd-4f56-9239-37f53c8ecf13"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("8f0d12ad-1143-4232-aa1b-7e0938d4e959"),
                    MainText = "Do you have any reverse web proxies deployed?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("aa46fd52-47dd-4f56-9239-37f53c8ecf13"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("9e02b5a2-98b1-45d4-8460-7fe87d0716da"),
                    MainText = "What mode are the reverse web proxies deployed in?",
                    OptionChoices = "Block mode,Monitor only",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("8f0d12ad-1143-4232-aa1b-7e0938d4e959"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e404822e-8978-41c7-9ffe-cfeaf42ad4e5"),
                    MainText = "Are these reverse web proxies deployed on all publicly-facing web servers?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("8f0d12ad-1143-4232-aa1b-7e0938d4e959"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("b36306b9-a20f-4c79-8f38-96fee6e64ab4"),
                    MainText = "Do you have a process for managing the network web proxies rules?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("aa46fd52-47dd-4f56-9239-37f53c8ecf13"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("6dfc3acc-238c-424c-8324-433282152676"),
                    MainText = "What is the process for managing the network web proxies rules?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("b36306b9-a20f-4c79-8f38-96fee6e64ab4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0fbb2c4e-fd84-4a9b-a249-d624499581fc"),
                    MainText = "Do you have a virtual/cloud computing infrastructure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("aa46fd52-47dd-4f56-9239-37f53c8ecf13"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c3ae7d5e-672a-4e29-bda2-402aaff86b53"),
                    MainText = "Do you have a similar network web proxies configuration on your virtual/cloud computing infrastructure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("0fbb2c4e-fd84-4a9b-a249-d624499581fc"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d2f05d76-08c9-4e64-84a3-25e8c60e1a32"),
                    MainText = "Do you have a SIEM?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("aa46fd52-47dd-4f56-9239-37f53c8ecf13"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b75fdff0-7a6c-43ac-b5d8-ea62a2675f49"),
                    MainText = "Do you forward the network web proxies logs to the SIEM?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("d2f05d76-08c9-4e64-84a3-25e8c60e1a32"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("43bc0d9e-05d6-4ace-adea-f9eb89b5d295"),
                    MainText = "Do you have a process to review network web proxy logs?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 7,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("aa46fd52-47dd-4f56-9239-37f53c8ecf13"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("382d02de-04b8-4942-b612-49e1a238abc3"),
                    MainText = "What is the process for reviewing network web proxy logs?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("43bc0d9e-05d6-4ace-adea-f9eb89b5d295"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("6273abbe-a8dd-4697-9fd4-58237204ac0f"),
                    MainText = "How often do you review network web proxy logs?",
                    OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("43bc0d9e-05d6-4ace-adea-f9eb89b5d295"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(networkProtectionQuestions);

            #endregion

            #region Endpoint Protection Questions

            subcategoryId = Guid.Parse("ffcdde59-8844-433a-92ea-714bf1fc8abc");

            var endpointProtectionQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("b138efaf-a256-4a1e-a2a5-00dca24acc49"),
                    MainText = "Does your organization have an Endpoint Protection policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("8bee1841-2edf-4532-ac5a-90e47ae4849d"),
                    MainText = "Have you defined processes and procedures for evaluating, deploying and managing endpoint protection solutions?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b138efaf-a256-4a1e-a2a5-00dca24acc49"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f534dc24-7348-4b4b-9dc7-959705471743"),
                    MainText = "How often is this policy reviewed?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b138efaf-a256-4a1e-a2a5-00dca24acc49"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("85bab797-c5f1-430d-94a2-8d2f529c55a0"),
                    MainText = "Do you have a malware protection (antivirus) solution deployed on endpoints?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c0c80075-0dc1-4c2c-bce2-9c1b27dd4383"),
                    MainText = "Does the malware protection solution have Endpoint Detection and Response capabilities?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("85bab797-c5f1-430d-94a2-8d2f529c55a0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("625a87c7-e51e-4ae6-ac7c-ae5c94001028"),
                    MainText = "What EDR capabilities are available?",
                    OptionChoices = "File forensics,Memory forensics,System containment",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("c0c80075-0dc1-4c2c-bce2-9c1b27dd4383"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7de576b1-dccd-4899-8e09-09d7d73133a6"),
                    MainText = "Does your malware protection solution have the capability to extract and analyze executable code in a malware sandbox?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("85bab797-c5f1-430d-94a2-8d2f529c55a0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("332141d9-ec53-470d-b597-051aea45715b"),
                    MainText = "What operating systems is the malware protection solution deployed on?",
                    OptionChoices = "Windows,macOS,Linux,Mobile (Android/iOS)",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 3,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("85bab797-c5f1-430d-94a2-8d2f529c55a0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2ed572f0-0a71-4310-a7f1-3b4bbca360d4"),
                    MainText = "Is the malware protection solution centrally managed?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("85bab797-c5f1-430d-94a2-8d2f529c55a0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("22febb35-a3d5-4c38-b052-73797bccaa8b"),
                    MainText = "What is the process for managing the malware protection solution?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("2ed572f0-0a71-4310-a7f1-3b4bbca360d4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("4099fe2c-f788-4ce9-8cac-c627e48051be"),
                    MainText = "What components are centrally managed?",
                    OptionChoices = "Real-time scanning,Scheduled scans,Signature updates",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("2ed572f0-0a71-4310-a7f1-3b4bbca360d4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e35c8473-2233-4502-93f4-84e21d6941d9"),
                    MainText = "Do you have a virtual/cloud computing infrastructure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("85bab797-c5f1-430d-94a2-8d2f529c55a0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("5b4d2044-ce79-423d-af6c-06c3a5735d4a"),
                    MainText = "Do you have a similar malware protection configuration on your virtual/cloud computing infrastructure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("e35c8473-2233-4502-93f4-84e21d6941d9"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("a78f16cf-90fe-49b7-a916-cfc17b6d6d84"),
                    MainText = "Do you have an endpoint firewall solution deployed on endpoints?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("bca05201-8ab8-43a1-bf81-836038774920"),
                    MainText = "What operating systems is the endpoint firewall deployed on?",
                    OptionChoices = "Windows,macOS,Linux,Mobile (Android/iOS)",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("a78f16cf-90fe-49b7-a916-cfc17b6d6d84"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("9a0fe26c-061c-419e-b19a-684ab2733e8f"),
                    MainText = "Is the endpoint firewall centrally managed?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("a78f16cf-90fe-49b7-a916-cfc17b6d6d84"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c6c90f6c-579e-4f14-a578-f3da69a8510a"),
                    MainText = "What is the process for managing the endpoint firewall?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("9a0fe26c-061c-419e-b19a-684ab2733e8f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("3c883e40-f48b-4111-a4fe-c178b14b33a5"),
                    MainText = "Do you have a tool to manage software patches on endpoints?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ffbfcbe7-6688-4f41-ae3e-6e959250989f"),
                    MainText = "What operating systems is the patch management solution deployed on?",
                    OptionChoices = "Windows,macOS,Linux,Mobile (Android/iOS)",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("3c883e40-f48b-4111-a4fe-c178b14b33a5"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("535e96c6-406e-4dba-b0ae-9028cb6891fa"),
                    MainText = "Is the patch management tool centrally managed?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("3c883e40-f48b-4111-a4fe-c178b14b33a5"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1c9d698b-a163-480c-92ac-6b675b4fccae"),
                    MainText = "What is the process for managing the patch management tool?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("535e96c6-406e-4dba-b0ae-9028cb6891fa"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("04378943-4bc9-49a0-811d-186c992b6235"),
                    MainText = "Do you have a tool to manage the system configuration on endpoints?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a0c0fcbb-c6d9-445f-8f81-640918550c49"),
                    MainText = "What operating systems is the system configuration solution compatible with?",
                    OptionChoices = "Windows,macOS,Linux,Mobile (Android/iOS)",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("04378943-4bc9-49a0-811d-186c992b6235"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("22497f3f-53f8-4031-a868-dfd3696feaf5"),
                    MainText = "Is the system configuration tool centrally managed?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("04378943-4bc9-49a0-811d-186c992b6235"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d2565223-39f0-4428-b6e3-ba095ecb6dfb"),
                    MainText = "What is the process for managing the system configuration tool?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("22497f3f-53f8-4031-a868-dfd3696feaf5"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("cd76c937-534a-4c8c-93a1-e51944ed48c5"),
                    MainText = "Do you (or your managed security service provider) have a SIEM capability?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2133f5cf-200b-4be9-8a72-918376df1414"),
                    MainText = "Do you forward endpoint logs to the SIEM?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("cd76c937-534a-4c8c-93a1-e51944ed48c5"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("48b1f710-567d-428b-bd96-7839173fb1c5"),
                    MainText = "What endpoint logs do you forward to the SIEM?",
                    OptionChoices = "Configuration changes,Endpoint firewall,Malware protection,Other",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("2133f5cf-200b-4be9-8a72-918376df1414"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("8bf8aaec-dd05-49a6-93c2-7fa6cc54522b"),
                    MainText = "Do you have a process to review endpoint logs?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 7,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("085838ff-326b-4e12-98c1-524c0bee64c0"),
                    MainText = "What is the process for reviewing endpoint logs?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("8bf8aaec-dd05-49a6-93c2-7fa6cc54522b"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id =Guid.Parse("c76e32a6-18c7-4218-8f95-c21ce24b1c66"),
                    MainText = "How often do you review endpoint activity?",
                    OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("8bf8aaec-dd05-49a6-93c2-7fa6cc54522b"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(endpointProtectionQuestions);

            #endregion

            #region Application Protection Questions

            subcategoryId = Guid.Parse("44a2cc87-75ac-49a9-9275-1b470f6509b3");

            var applicationProtectionQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("557827fc-442a-4f67-93ca-fbcea064d121"),
                    MainText = "Does your organization have an Application Protection policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("28c5f4b8-314a-4049-8c2d-e2d0a319f1e6"),
                    MainText = "Does the policy cover the Software Development Life Cycle (SDLC) for internally developed software applications?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("557827fc-442a-4f67-93ca-fbcea064d121"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("fdc71e07-5b59-40fe-8a05-d3ecc479594c"),
                    MainText = "Does the policy cover training for your developers on secure software development practices?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("557827fc-442a-4f67-93ca-fbcea064d121"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c33b1eaf-101f-4275-aae2-26e6a46e718b"),
                    MainText = "Have you defined processes and procedures for certifying third-party software prior to acquisition?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("557827fc-442a-4f67-93ca-fbcea064d121"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("61814463-af3d-4d0b-af2e-ac9ee0b410fb"),
                    MainText = "Have you defined processes and procedures for evaluating, deploying and managing application protection solutions?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("557827fc-442a-4f67-93ca-fbcea064d121"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("fcb31ace-47e7-4034-9d13-7d26ebdb06c8"),
                    MainText = "How often is this policy reviewed?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("557827fc-442a-4f67-93ca-fbcea064d121"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b42c3c07-5b05-48b6-bb4d-6cef28c0d35a"),
                    MainText = "Does your organization develop software applications for internal or external use?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("547bd1d2-7d5e-4468-9186-37c8364ec06a"),
                    MainText = "Do you have a process to review source code for security flaws?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b42c3c07-5b05-48b6-bb4d-6cef28c0d35a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d3a8a32e-e733-492f-8d62-82becd248f22"),
                    MainText = "What is the process for reviewing source code for security flaws?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("547bd1d2-7d5e-4468-9186-37c8364ec06a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ae6b63e6-859a-4279-a4ed-d2c5f55d32a5"),
                    MainText = "Do you use automated tools to review source code for security flaws?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("547bd1d2-7d5e-4468-9186-37c8364ec06a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("6168d665-54a1-4cc8-be89-11c440182d0c"),
                    MainText = "How often do you review source code for security flaws?",
                    OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("547bd1d2-7d5e-4468-9186-37c8364ec06a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("91c32021-bbd9-4405-9667-99e02c73f808"),
                    MainText = "Do you have a process to digitally sign software applications developed by your organization?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b42c3c07-5b05-48b6-bb4d-6cef28c0d35a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("27504c96-871a-47bf-bd7f-eeab1eb2bae6"),
                    MainText = "What is the process to digitally sign software applications developed by your organization?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("91c32021-bbd9-4405-9667-99e02c73f808"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7ef9de5b-73f7-4ab9-966f-dc4da6661f32"),
                    MainText = "Do you maintain an inventory of software applications at your organization?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0261b356-0982-4e9c-9a52-fae0eb18286e"),
                    MainText = "What is the process for managing this software inventory?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("7ef9de5b-73f7-4ab9-966f-dc4da6661f32"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1be1907b-9e22-4c78-8942-0d93b427d040"),
                    MainText = "Is the software inventory automatically updated?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("7ef9de5b-73f7-4ab9-966f-dc4da6661f32"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("eafdfda8-04f0-481b-8916-ebba06bc1e1e"),
                    MainText = "How often is this software inventory updated?",
                    OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("7ef9de5b-73f7-4ab9-966f-dc4da6661f32"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f6a6262b-7e95-4e46-a8f0-6c40ce107b22"),
                    MainText = "Do you have a tool to manage what software applications users can run (application whitelisting)?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("39aedce0-7119-48eb-980f-03b89aad3bca"),
                    MainText = "What mechanism does this tool use to whitelist applications?",
                    OptionChoices = "File names/paths,File hashes,Digital signatures",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f6a6262b-7e95-4e46-a8f0-6c40ce107b22"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("8c2ce998-a59f-4b24-94fc-cea661ed9044"),
                    MainText = "What systems is this application whitelisting tool deployed on?",
                    OptionChoices = "Workstations/laptops,Servers,Mobile devices",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f6a6262b-7e95-4e46-a8f0-6c40ce107b22"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("beea5a01-9235-46d6-acc0-65087949a98d"),
                    MainText = "Is the application whitelisting tool centrally managed?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f6a6262b-7e95-4e46-a8f0-6c40ce107b22"),
                    SubcategoryId = subcategoryId,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c8572851-abed-4f62-ac0d-765fe6d265f9"),
                    MainText = "What is the process for managing the application whitelisting tool?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("beea5a01-9235-46d6-acc0-65087949a98d"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("12f8248e-64fc-49aa-9d07-cec62abd645a"),
                    MainText = "Have you enabled logging on critical applications?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("87b911e0-089b-4ede-9d08-30ace4516aec"),
                    MainText = "Do you log activity on internal web applications?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("12f8248e-64fc-49aa-9d07-cec62abd645a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("dd7446a7-136e-4363-b405-b338d4ecd474"),
                    MainText = "What logging level is enabled on web applications?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("87b911e0-089b-4ede-9d08-30ace4516aec"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("bff6a446-2db1-47d8-a87a-58527a496aa7"),
                    MainText = "Do you log activity on email applications?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("12f8248e-64fc-49aa-9d07-cec62abd645a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b94a79ef-80ac-41c8-99cf-16bdf39af9de"),
                    MainText = "What logging level is enabled on email applications?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("bff6a446-2db1-47d8-a87a-58527a496aa7"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ed23e87e-e178-49ae-8b36-e4dd81c6928c"),
                    MainText = "Do you log activity on database applications?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("12f8248e-64fc-49aa-9d07-cec62abd645a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("fe4cc3ab-91b0-4f31-95b6-e869c1728e22"),
                    MainText = "What logging level is enabled on database applications?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("ed23e87e-e178-49ae-8b36-e4dd81c6928c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e48f6e6c-e41b-4235-a9d7-60911713e96f"),
                    MainText = "Have stored procedures and prepared statements been documented?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("ed23e87e-e178-49ae-8b36-e4dd81c6928c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("754fbd4d-4759-483a-91a9-64c9234b68b0"),
                    MainText = "Do you have a tool to manage email encryption?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c008a124-d210-41d0-970c-49860fd9f8cb"),
                    MainText = "What is the process for managing email encryption?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("754fbd4d-4759-483a-91a9-64c9234b68b0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("3382dee5-3536-47c3-bae7-58d7970f23e0"),
                    MainText = "Are staff trained on how to use this email encryption tool?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("754fbd4d-4759-483a-91a9-64c9234b68b0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7b8e58a7-d146-43c5-a925-f955e5bbd114"),
                    MainText = "Is the email encryption tool centrally managed?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("754fbd4d-4759-483a-91a9-64c9234b68b0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("b18c38ce-0524-4f30-8779-3a3b395c42fe"),
                    MainText = "Do you have a tool to manage email spam?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 7,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("77d73cfc-1956-4c79-beda-1431a5bbec92"),
                    MainText = "What is the process for managing email spam?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("b18c38ce-0524-4f30-8779-3a3b395c42fe"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("32656ba7-ca85-4c4b-a80b-a823f1225558"),
                    MainText = "Have you implemented an email authentication solution?",
                    HintText = "e.g. Sender Policy Framework (SPF), DomainKeys Identified Mail (DKIM)",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b18c38ce-0524-4f30-8779-3a3b395c42fe"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("ffa68705-3408-45b5-9cfd-a95bd29de528"),
                    MainText = "Do you (or your managed security service provider) have a SIEM capability?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 8,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("4d7e00f6-dcc0-4835-911c-de9bd17a54fc"),
                    MainText = "Do you forward application logs to the SIEM?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("ffa68705-3408-45b5-9cfd-a95bd29de528"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("bc9c1c6c-b382-44b5-bccc-1d8da1cdc43f"),
                    MainText = "What application logs do you forward to the SIEM?",
                    OptionChoices = "Web logs, Email logs, Database logs, Other critical application logs",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("4d7e00f6-dcc0-4835-911c-de9bd17a54fc"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("30e6915f-2712-4d1b-9994-b490296c92a9"),
                    MainText = "Do you have a process to review application logs?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 9,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("ce685312-b991-4129-8e3f-a981d751e4d4"),
                    MainText = "What is the process for reviewing application logs?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("30e6915f-2712-4d1b-9994-b490296c92a9"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("56b78a40-b31a-43ad-9325-2722f1944a09"),
                    MainText = "How often do you review application activity?",
                    OptionChoices = "Monthly,Quarterly,Semi-annually,Annually,Other",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("30e6915f-2712-4d1b-9994-b490296c92a9"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(applicationProtectionQuestions);

            #endregion

            return results.ToArray();
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.