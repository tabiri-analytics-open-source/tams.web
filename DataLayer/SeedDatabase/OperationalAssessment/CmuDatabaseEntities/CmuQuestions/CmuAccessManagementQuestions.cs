using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.CmuDatabaseEntities.CmuQuestions
{
    public static class CmuAccessManagementQuestions
    {
        public static Question[] Seed(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Empty;
            
            var results = new List<Question>();

            #region Identity Management Questions

            subcategoryId = Guid.Parse("1f0fd461-a017-421b-a6fc-9e5ffebd91ae");

            var identityManagementQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("07f5dec0-96a8-4b02-bc95-c01c6c24d4cf"),
                    MainText = "Does your organization have an Identity Management policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },                
                new Question 
                {
                    Id = Guid.Parse("5304b168-012e-4978-bbae-5b1f135cd0be"),
                    MainText = "Do you have a single source of identity information?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("07f5dec0-96a8-4b02-bc95-c01c6c24d4cf"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("8856fb02-06b7-4de6-b3ce-4300c1ad8951"),
                    MainText = "What is your primary source of identity information?",
                    HintText = "e.g. Enterprise Resource Planning (ERP), Active Directory, LDAP",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("5304b168-012e-4978-bbae-5b1f135cd0be"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("998f2eb7-aa64-4c75-a409-14d349c508fb"),
                    MainText = "Which business unit is tasked with managing the primary source of identity information?",
                    HintText = "e.g. Human Resources department, Information Technology department",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("07f5dec0-96a8-4b02-bc95-c01c6c24d4cf"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("5dcb7b92-1499-4e9f-bb10-31c236bc0039"),
                    MainText = "How are user accounts established?",                    
                    Order = 3,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("07f5dec0-96a8-4b02-bc95-c01c6c24d4cf"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("5756017e-48b2-48c6-b2f4-25e5770e576f"),
                    MainText = "Do users have the same accounts (usernames) across systems?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 1,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("07f5dec0-96a8-4b02-bc95-c01c6c24d4cf"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2985db4d-fe42-4409-aa75-ffc965e52a66"),
                    MainText = "How do you track the same user across multiple systems?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("5756017e-48b2-48c6-b2f4-25e5770e576f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("8e3a9696-3328-4e62-82b1-401fca41fb6b"),
                    MainText = "Are background checks conducted for users with sensitive roles?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("07f5dec0-96a8-4b02-bc95-c01c6c24d4cf"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c618a2a1-6ab1-4bc6-9125-933287232455"),
                    MainText = "How often is this policy reviewed?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("07f5dec0-96a8-4b02-bc95-c01c6c24d4cf"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("09f099d5-5371-4ba5-8681-b33da1a520ca"),
                    MainText = "Does your organization utilize an Identity and Access Management (IAM) solution for user provisioning?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e959a509-8854-4420-99db-9c02367cf10b"),
                    MainText = "How does the IAM solution integrate with your primary source of identity data?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("09f099d5-5371-4ba5-8681-b33da1a520ca"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question {
                    Id = Guid.Parse("214cbd73-198e-459f-9f58-6da23987ffff"),
                    MainText = "Do you have a procedure for provisioning new users?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("5e806504-842c-4556-936a-4a02ce24e28d"),
                    MainText = "How are access requests tracked?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("214cbd73-198e-459f-9f58-6da23987ffff"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f6c07d7a-5ee9-4ffe-a753-48fede96b153"),
                    MainText = "Who approves access requests?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("214cbd73-198e-459f-9f58-6da23987ffff"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("170ffe08-1504-47bf-8e75-dd637f8a17e6"),
                    MainText = "Is the process of creating approved user accounts automated?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("214cbd73-198e-459f-9f58-6da23987ffff"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ff5026dc-606f-4ae9-81e8-26c4eb5ce79d"),
                    MainText = "Who manually creates the approved user accounts?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("170ffe08-1504-47bf-8e75-dd637f8a17e6"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("afe5332f-c7a9-4c82-a3ae-6b5021165d86"),
                    MainText = "Do you have a procedure for revoking/disabling existing user accounts?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("65b652ef-da40-4684-9195-85a0df0004be"),
                    MainText = "How are revocation requests initiated?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("afe5332f-c7a9-4c82-a3ae-6b5021165d86"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("647bb819-b156-4fdc-893e-8e62ec5d69d1"),
                    MainText = "How are revocation requests tracked?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("afe5332f-c7a9-4c82-a3ae-6b5021165d86"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ec06549d-c460-4ba3-bf91-aee7b8e80514"),
                    MainText = "Who approves the revocation requests?",                    
                    Order = 3,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("afe5332f-c7a9-4c82-a3ae-6b5021165d86"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("146ee991-5356-4101-85ec-cbbe5524adda"),
                    MainText = "Is the process of revoking/disabling user accounts automated?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 1,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("afe5332f-c7a9-4c82-a3ae-6b5021165d86"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2d1af693-bb6f-43de-ad52-38f2e2ca8b9b"),
                    MainText = "Who manually revokes/disables the user accounts?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("146ee991-5356-4101-85ec-cbbe5524adda"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1b1b4df9-2d06-460c-8ea3-720f336f3357"),
                    MainText = "Is there a procedure to validate accounts have been successfully revoked/disabled?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("afe5332f-c7a9-4c82-a3ae-6b5021165d86"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("84daae21-6681-4e90-a7bf-87861a8ecad3"),
                    MainText = "What is the procedure for validating?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("1b1b4df9-2d06-460c-8ea3-720f336f3357"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("73fc6147-d8c6-4025-829d-5045344c3351"),
                    MainText = "Do you review existing user accounts for appropriateness?",
                    HintText = "e.g. identify stale accounts, active accounts on legacy systems",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ffe494dc-b0d9-4e97-9479-136fe2314f6d"),
                    MainText = "How often?",
                    OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("73fc6147-d8c6-4025-829d-5045344c3351"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("294a5c3b-6b14-4084-a00a-58306c57417f"),
                    MainText = "What is the procedure for reviewing existing accounts for appropriateness?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("73fc6147-d8c6-4025-829d-5045344c3351"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }        
            };
            results.AddRange(identityManagementQuestions);

            #endregion

            #region Access Controls Questions

            subcategoryId = Guid.Parse("1a8e9b19-ec14-49fb-a2ce-ac0b89a92ed5");

            var accessControlQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("73ac21ab-67ad-4030-a71f-dacc7692a0d2"),
                    MainText = "Does your organization have an Access Control policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("5581433c-53cf-4f6f-bfc7-f1d2ece7bfa2"),
                    MainText = "Which of the following access control standards has your organization defined?",
                    OptionChoices = "Account lockout,Password complexity,Password expiry,Password history",
                    OptionsOrientation = OptionsOrientation.Vertical,                    
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("73ac21ab-67ad-4030-a71f-dacc7692a0d2"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("781a66ad-e509-4809-b64f-543f78ecb118"),
                    MainText = "Have these standards been implemented across all systems?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("5581433c-53cf-4f6f-bfc7-f1d2ece7bfa2"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("337a262a-3c74-4aa8-a93e-7db0616b925f"),
                    MainText = "What procedures do you have in place to implement these standards?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("781a66ad-e509-4809-b64f-543f78ecb118"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("57b6df69-93a5-4faa-8c03-fd8989f8ce8a"),
                    MainText = "Who is tasked with implementing these standards?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("781a66ad-e509-4809-b64f-543f78ecb118"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e17dda5f-9c26-4ff5-ba86-ae68975d0250"),
                    MainText = "Have you defined a 'least privilege' standard?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("73ac21ab-67ad-4030-a71f-dacc7692a0d2"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("cca07cd3-c239-4e22-92a7-ffe2fa51a40a"),
                    MainText = "Are administrator/root privileges limited to specific individuals?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("e17dda5f-9c26-4ff5-ba86-ae68975d0250"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("518ce8c8-76f4-41c7-8518-aeb6d5cade5b"),
                    MainText = "How are user, administrator/root, and guest account privileges managed?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("e17dda5f-9c26-4ff5-ba86-ae68975d0250"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("6bfb7e1c-4d1a-4bca-88e9-de11b4fc6e7b"),
                    MainText = "How are applications that run with administrator/root privileges managed?",                    
                    Order = 3,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("e17dda5f-9c26-4ff5-ba86-ae68975d0250"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("4e13ed85-fe22-4a88-9cf6-2a1d18f5ac82"),
                    MainText = "How are service accounts managed?",                    
                    Order = 4,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("e17dda5f-9c26-4ff5-ba86-ae68975d0250"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f354ef57-f9b3-45b7-8f4f-92c507b3c66b"),
                    MainText = "Who is tasked with implementing this standard?",                    
                    Order = 5,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("e17dda5f-9c26-4ff5-ba86-ae68975d0250"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("fa9a7259-f5a6-4957-bb22-eed89c0c55aa"),
                    MainText = "How often is this policy and standards reviewed?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 3,                    
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("73ac21ab-67ad-4030-a71f-dacc7692a0d2"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c01f5a52-cae7-4f82-ac4a-6d685145d184"),
                    MainText = "Has your organization defined a two-factor authentication standard?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id =Guid.Parse("57bd3423-8378-4542-a8f6-d66d738d7f3e"),
                    MainText = "Which systems/applications require two-factor authentication?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("c01f5a52-cae7-4f82-ac4a-6d685145d184"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7bb74c1f-909a-46d4-9fe6-dd132d08940b"),
                    MainText = "How is two-factor authentication implemented on these systems/applications?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("c01f5a52-cae7-4f82-ac4a-6d685145d184"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("46b8c402-0284-4ca0-ac1e-62557cb60883"),
                    MainText = "Do you have a procedure for monitoring access control?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7a014302-b52c-496b-a036-193da113289d"),
                    MainText = "Have you implemented a Centralized Log Management solution?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("46b8c402-0284-4ca0-ac1e-62557cb60883"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("16e85e78-2aca-44be-8892-e3284dc7f699"),
                    MainText = "Which systems/applications do you collect access logs from?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("7a014302-b52c-496b-a036-193da113289d"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ee7182b3-c672-4d68-9583-c142e64c0e82"),
                    MainText = "How do you collect these access logs?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("7a014302-b52c-496b-a036-193da113289d"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b1570cf0-4f92-4a89-8678-6138c8c26370"),
                    MainText = "Where are these access logs stored?",                    
                    Order = 3,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("7a014302-b52c-496b-a036-193da113289d"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("359fb7fb-2da7-4545-8c54-9db34a2dcfe0"),
                    MainText = "What is your log retention schedule?",                    
                    Order = 4,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("7a014302-b52c-496b-a036-193da113289d"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("6b9e670b-472b-48b9-833a-a9d5ff814a15"),
                    MainText = "What is your procedure for reviewing access logs?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("46b8c402-0284-4ca0-ac1e-62557cb60883"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
            };
            results.AddRange(accessControlQuestions);

            #endregion

            #region Separation Of Duties Questions

            subcategoryId = Guid.Parse("4e802dfe-3143-4a5a-b8ab-ecdcc9106bd8");

            var seperationOfDutiesQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("11f1e783-dc65-41d2-9086-0bb34d549f6c"),
                    MainText = "Does your organization have a Separation of Duties policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ccee966b-2990-4393-b113-d41e71b67e19"),
                    MainText = "Are regular users restricted from making system or application configuration changes?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("11f1e783-dc65-41d2-9086-0bb34d549f6c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("795a9e1c-b68b-437d-83a3-7ad0da8040f5"),
                    MainText = "How is this restriction enforced?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("ccee966b-2990-4393-b113-d41e71b67e19"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f93f1e27-d6d7-4151-bccc-f1d75b64304c"),
                    MainText = "Does a separation exist between production and development/test environments?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("11f1e783-dc65-41d2-9086-0bb34d549f6c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("73ab5bf3-1f8a-4e11-978f-43f50a9c40b8"),
                    MainText = "Are access controls similar across both environments?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f93f1e27-d6d7-4151-bccc-f1d75b64304c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0354b206-bfe1-4cc2-b6cc-6630f2e08274"),
                    MainText = "Are developers restricted to the development/test environment only?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f93f1e27-d6d7-4151-bccc-f1d75b64304c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("91ada30e-eb4b-4943-b66c-4d96c289c6d6"),
                    MainText = "Are there controls restricting developers from pushing new code directly into production?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f93f1e27-d6d7-4151-bccc-f1d75b64304c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ff9ccaf4-1e87-4bfa-995c-0494c96fb7e6"),
                    MainText = "What types of controls are implemented?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("91ada30e-eb4b-4943-b66c-4d96c289c6d6"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a75b8367-79ac-4636-aab8-75aacf6192d6"),
                    MainText = "What is the procedure for getting code approved prior to pushing it into to production?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("91ada30e-eb4b-4943-b66c-4d96c289c6d6"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e0116691-929a-4a97-bcdf-4390a196e6c2"),
                    MainText = "Are security reviews part of the approval procedure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("91ada30e-eb4b-4943-b66c-4d96c289c6d6"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("32beb371-32ff-49c3-af6f-87ad8c7a0965"),
                    MainText = "How often is this policy reviewed?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("11f1e783-dc65-41d2-9086-0bb34d549f6c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("91a38974-4959-4002-8457-92207f58bc18"),
                    MainText = "Does your organization have standards for Role Based Access Control (RBAC)?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f5bbeb34-97c8-4598-be83-aed2305a5236"),
                    MainText = "On what systems are RBAC implemented?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("91a38974-4959-4002-8457-92207f58bc18"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("68b2a896-cddb-4427-9a4b-3af89a9e7547"),
                    MainText = "What types of roles are defined on those systems?",
                    HintText = "e.g. user, approver/manager, auditor/reviewer, admin/root",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("91a38974-4959-4002-8457-92207f58bc18"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1a63c75e-a115-436c-bc33-30f1a3255d3e"),
                    MainText = "How are these roles managed?",                    
                    Order = 3,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("91a38974-4959-4002-8457-92207f58bc18"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0fdc0412-a429-46f9-a456-04968ba347bc"),
                    MainText = "Are these roles integrated with the IAM solution mentioned in Access Management > Identity Management > Question (2)?",                    
                    Order = 4,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("91a38974-4959-4002-8457-92207f58bc18"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c2585ee9-dead-4dd1-a6f2-596026948b60"),
                    MainText = "Is there a procedure for reviewing roles for appropriateness?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("91a38974-4959-4002-8457-92207f58bc18"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("4eeb91d4-5b8f-4378-ae58-82857808f15b"),
                    MainText = "What is the procedure for reviewing roles for appropriateness?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("c2585ee9-dead-4dd1-a6f2-596026948b60"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1b211ec4-40f3-4134-a900-4cc297f467b9"),
                    MainText = "How often is this review conducted?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("c2585ee9-dead-4dd1-a6f2-596026948b60"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(seperationOfDutiesQuestions);

            #endregion

            #region Privileged Access Management Questions

                subcategoryId = Guid.Parse("c062770f-2cc3-4f56-b87a-5d51fcabc9b7");

                var priviledgedAccessManagementQuestions = new List<Question>
                {
                    new Question 
                    {
                        Id = Guid.Parse("d6b3257c-f05f-441d-8b20-a96eed071c3e"),
                        MainText = "Does your organization have a Privileged Access Management policy?",
                        OptionChoices = "Yes,No",
                        OptionsOrientation = OptionsOrientation.Horizontal,
                        ActiveOptionIndex = 0,
                        Order = 1,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = null,
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("0b8be412-8295-454f-94df-3d724b4bb152"),
                        MainText = "Does this policy define under which conditions privileged access should be granted?",
                        OptionChoices = "Yes,No",
                        OptionsOrientation = OptionsOrientation.Horizontal,
                        Order = 1,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = Guid.Parse("d6b3257c-f05f-441d-8b20-a96eed071c3e"),
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("ff851d34-4f67-4274-81b2-b48f2e2a753a"),
                        MainText = "Are privileged accounts for system administration use only?",
                        OptionChoices = "Yes,No",
                        OptionsOrientation = OptionsOrientation.Horizontal,
                        ActiveOptionIndex = 0,
                        Order = 2,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = Guid.Parse("d6b3257c-f05f-441d-8b20-a96eed071c3e"),
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("d4d47bad-86fb-42db-94b3-01b1c21d6c9d"),
                        MainText = "Do system administrators have separate standard user accounts?",
                        OptionChoices = "Yes,No",
                        OptionsOrientation = OptionsOrientation.Horizontal,
                        Order = 1,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = Guid.Parse("ff851d34-4f67-4274-81b2-b48f2e2a753a"),
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("93fd6ec1-6e80-4220-8f22-a70eca3702df"),
                        MainText = "How often is this policy reviewed?",
                        OptionChoices = "Quarterly,Semi-annually,Annually",
                        OptionsOrientation = OptionsOrientation.Vertical,
                        Order = 3,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = Guid.Parse("d6b3257c-f05f-441d-8b20-a96eed071c3e"),
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("6a7d35ce-8c55-4c41-a8b7-8b2e63a51bbd"),
                        MainText = "Do you have any administrative accounts that are shared by more than one individual?",
                        OptionChoices = "Yes,No",
                        OptionsOrientation = OptionsOrientation.Horizontal,
                        ActiveOptionIndex = 0,
                        Order = 2,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = null,
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("450fa4a7-73d1-4d32-9725-37e3915e00cb"),
                        MainText = "How many administrative accounts are shared?",                        
                        Order = 1,
                        QuestionType = QuestionType.Integer,
                        ParentId = Guid.Parse("6a7d35ce-8c55-4c41-a8b7-8b2e63a51bbd"),
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("513e1257-4a58-4341-aa84-ac5252a54571"),
                        MainText = "Are these shared administrative accounts service accounts?",
                        OptionChoices = "Yes,No",
                        OptionsOrientation = OptionsOrientation.Horizontal,
                        ActiveOptionIndex = 0,
                        Order = 2,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = Guid.Parse("6a7d35ce-8c55-4c41-a8b7-8b2e63a51bbd"),
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("baa4e88e-371c-4f21-bb38-1cc9768cd8c2"),
                        MainText = "Do these service accounts allow for interactive login?",
                        OptionChoices = "Yes,No",
                        OptionsOrientation = OptionsOrientation.Horizontal,
                        Order = 3,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = Guid.Parse("513e1257-4a58-4341-aa84-ac5252a54571"),
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("4d7fb639-c416-45b2-b6b2-9f010967abd3"),
                        MainText = "Is there an inventory of privileged accounts?",
                        OptionChoices = "Yes,No",
                        OptionsOrientation = OptionsOrientation.Horizontal,
                        ActiveOptionIndex = 0,
                        Order = 3,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = null,
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("490f5458-0224-4123-9c18-330f38115433"),
                        MainText = "What was the most recent count of privileged accounts?",                        
                        Order = 1,
                        QuestionType = QuestionType.Integer,
                        ParentId = Guid.Parse("4d7fb639-c416-45b2-b6b2-9f010967abd3"),
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("29bfeb2c-db0b-4e2b-82a4-681fd68c67e5"),
                        MainText = "Does your organization have a privileged access management tool?",
                        OptionChoices = "Yes,No",
                        OptionsOrientation = OptionsOrientation.Horizontal,
                        ActiveOptionIndex = 0,
                        Order = 4,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = null,
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("5ddd5992-dfda-4f17-a1b9-a1304d940679"),
                        MainText = "How are privileged access requests made?",                        
                        Order = 1,
                        QuestionType = QuestionType.Text,
                        ParentId = Guid.Parse("29bfeb2c-db0b-4e2b-82a4-681fd68c67e5"),
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("08cb3b8f-ace0-4b1d-8879-04e3d189e6cf"),
                        MainText = "How are privileged access requests approved?",                        
                        Order = 2,
                        QuestionType = QuestionType.Text,
                        ParentId = Guid.Parse("29bfeb2c-db0b-4e2b-82a4-681fd68c67e5"),
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("7c77326e-3ca4-447e-ae81-58c6360feecb"),
                        MainText = "Does your organization have a process for reviewing administrative account activity?",
                        OptionChoices = "Yes,No",
                        OptionsOrientation = OptionsOrientation.Horizontal,
                        ActiveOptionIndex = 0,
                        Order = 5,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = null,
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("230dfcc3-16b3-4f3f-bb2f-6c3e3c005d4c"),
                        MainText = "What is the process for reviewing administrative account activity?",                        
                        Order = 1,
                        QuestionType = QuestionType.Text,
                        ParentId = Guid.Parse("7c77326e-3ca4-447e-ae81-58c6360feecb"),
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("f1d9244a-49dd-445b-83d7-3429e92f0e51"),
                        MainText = "How often is this review conducted?",
                        OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                        OptionsOrientation = OptionsOrientation.Vertical,
                        Order = 2,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = Guid.Parse("7c77326e-3ca4-447e-ae81-58c6360feecb"),
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("581a4949-0a44-4d49-8c7e-f1334b5a0a54"),
                        MainText = "Have you implemented any automated detection rules for privileged access?",
                        OptionChoices = "Yes,No",
                        OptionsOrientation = OptionsOrientation.Horizontal,
                        Order = 3,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = Guid.Parse("7c77326e-3ca4-447e-ae81-58c6360feecb"),
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    }
                };
                results.AddRange(priviledgedAccessManagementQuestions);

            #endregion

            #region Remote Access Questions

            subcategoryId = Guid.Parse("61543e20-477b-4c07-a57a-cc856cbbf562");

            var remoteAccessQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("64710f27-836f-4206-ba13-0cbcabeb1e7f"),
                    MainText = "Does your organization have a Remote Access policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d9165577-a15a-4dac-88d7-6f3d8a019c59"),
                    MainText = "Can employees access the corporate network remotely?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("64710f27-836f-4206-ba13-0cbcabeb1e7f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0fe08d93-9c02-4d0c-9d95-2fdb641767b6"),
                    MainText = "Is remote access limited to specific employee roles?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("64710f27-836f-4206-ba13-0cbcabeb1e7f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ce2ac066-112d-4a69-a379-c4ed20b0619a"),
                    MainText = "Which employee roles is remote access limited to?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("0fe08d93-9c02-4d0c-9d95-2fdb641767b6"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("283175f1-f9da-4400-a66c-95c4cad7cf3c"),
                    MainText = "Do you have an approval criteria for granting employees remote access privileges?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("64710f27-836f-4206-ba13-0cbcabeb1e7f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("4245f783-3534-4e64-9f88-5e05644ab630"),
                    MainText = "What are the approval criteria?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("283175f1-f9da-4400-a66c-95c4cad7cf3c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("310945d2-bb3a-4387-b4d2-a57e37babdb9"),
                    MainText = "How often is this policy reviewed?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("64710f27-836f-4206-ba13-0cbcabeb1e7f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("9e234890-a53d-42d6-a5e1-4fa7261e075a"),
                    MainText = "Do you have a technology in place to provide remote access?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("621a407f-6611-44b4-a2d1-529d283b780d"),
                    MainText = "What technology do you use to provide remote access?",
                    HintText = "e.g. Virtual Private Network (VPN), Virtual Desktop Infrastructure (VDI)",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("9e234890-a53d-42d6-a5e1-4fa7261e075a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("29e80678-a25a-4c94-bd1f-2a5fd754154a"),
                    MainText = "Do you monitor all traffic on the VPN, VDI or other remote access solution?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("9e234890-a53d-42d6-a5e1-4fa7261e075a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c8b00236-702c-444c-a296-184cf58303ce"),
                    MainText = "If using a corporate VPN, is it configured for split tunnel traffic?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("29e80678-a25a-4c94-bd1f-2a5fd754154a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b4ad4e8a-c6fc-4c74-bc0e-6c200d79ee02"),
                    MainText = "Are remote users placed on a segmented VLAN upon connecting?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("9e234890-a53d-42d6-a5e1-4fa7261e075a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e26aa501-dceb-4029-ad36-6e9ae772fa7f"),
                    MainText = "How is this network segmentation implemented?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("b4ad4e8a-c6fc-4c74-bc0e-6c200d79ee02"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("9c540964-9caf-4478-91e9-3b5dbd89cde8"),
                    MainText = "Have you implemented two-factor authentication authentication for remote access?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("9e234890-a53d-42d6-a5e1-4fa7261e075a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("cb3f1891-3479-4f33-a86b-af209b2f2392"),
                    MainText = "What kind of two-factor authentication have you implemented for remote access?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("9c540964-9caf-4478-91e9-3b5dbd89cde8"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("be65926a-c138-4f8c-be0d-77d873feb1c2"),
                    MainText = "Do you authenticate the remote user's system?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("9e234890-a53d-42d6-a5e1-4fa7261e075a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("88cc10a1-0752-4312-ad4f-41cf3f48a766"),
                    MainText = "What kind of system authentication have you implemented?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("be65926a-c138-4f8c-be0d-77d873feb1c2"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7cb786d6-4891-4994-9699-ccfda5fa47a1"),
                    MainText = "Do you conduct security checks of the remote user's system?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("9e234890-a53d-42d6-a5e1-4fa7261e075a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a70b3122-c93f-438a-9923-c334ec32a746"),
                    MainText = "What kind of security checks do you conduct of the remote user's system?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("7cb786d6-4891-4994-9699-ccfda5fa47a1"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c4e6fdb9-5f12-4093-87fb-feb968d19bbe"),
                    MainText = "What happens if the remote user's system fails the security checks?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("7cb786d6-4891-4994-9699-ccfda5fa47a1"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("04440bcb-93e3-429f-8dde-f32a80c86ab4"),
                    MainText = "Do you have a process to review remote access activity?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("88b7fe44-f707-4648-a241-3271f021e3c3"),
                    MainText = "What is the process for reviewing remote access activity?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("04440bcb-93e3-429f-8dde-f32a80c86ab4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a0ee8f83-d97c-479c-9908-64a254d22907"),
                    MainText = "How often do you review remote access activity?",
                    OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("04440bcb-93e3-429f-8dde-f32a80c86ab4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(remoteAccessQuestions);

            #endregion

            #region Third Party Access Questions

            subcategoryId = Guid.Parse("c71b6378-ca1c-4f87-94e5-bb79867ed456");

            var thirdPartyAccessQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("389ae400-b8ac-418d-a47a-419f4a28d624"),
                    MainText = "Does your organization have a Third-party Access policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c7b59b56-e9ea-42be-af4f-72442f9f71de"),
                    MainText = "Does this policy include contractual language on the third-party's compliance with your organization's policies?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("389ae400-b8ac-418d-a47a-419f4a28d624"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("31370565-d450-4802-8b52-6205cf423eca"),
                    MainText = "Have you defined which third-parties can access your corporate network?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("389ae400-b8ac-418d-a47a-419f4a28d624"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("6a92b1bd-7a27-4870-936e-a289a1beb140"),
                    MainText = "Which third-parties can access the organization’s network?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("31370565-d450-4802-8b52-6205cf423eca"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("95f38c40-2cfe-4663-be56-c4a654b6ac1f"),
                    MainText = "Are third-parties granted local access?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("31370565-d450-4802-8b52-6205cf423eca"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d3c25c3b-be35-4434-be98-4ef0a51dc3e9"),
                    MainText = "Are third-parties granted remote access?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("31370565-d450-4802-8b52-6205cf423eca"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("3f912680-64cd-4b52-9ff1-b351f944ed1b"),
                    MainText = "Does this remote access comply with the Remote Access policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("d3c25c3b-be35-4434-be98-4ef0a51dc3e9"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("54c1c1c1-ea74-408c-ac45-e8b47a5caa64"),
                    MainText = "Are audits of the third-party's systems conducted prior to access being granted?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("389ae400-b8ac-418d-a47a-419f4a28d624"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ecaca141-efa2-49b1-bf8c-6ce5a3987236"),
                    MainText = "What is the process for conducting audits of the third-party's systems?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("54c1c1c1-ea74-408c-ac45-e8b47a5caa64"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("403c98da-f5f3-489e-af1a-06a9f8902225"),
                    MainText = "Are these audits aligned with a specific industry framework?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("54c1c1c1-ea74-408c-ac45-e8b47a5caa64"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("310836f8-5ab4-418f-b890-60eef54788df"),
                    MainText = "What specific industry frameworks are these audits aligned with?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("403c98da-f5f3-489e-af1a-06a9f8902225"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("5e01d1a8-7d40-4e7c-89b4-9a0133095af4"),
                    MainText = "How often are these audits conducted?",
                    OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("54c1c1c1-ea74-408c-ac45-e8b47a5caa64"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("974865a2-081b-4ded-999a-bd523729f6dc"),
                    MainText = "How often is this policy reviewed?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("389ae400-b8ac-418d-a47a-419f4a28d624"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("341a0d02-99bc-448f-9695-d6a2611ece01"),
                    MainText = "Do third-parties have remote API access?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("8b7379ce-fcae-417d-aa42-e615e5336118"),
                    MainText = "Are these API calls authenticated?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("341a0d02-99bc-448f-9695-d6a2611ece01"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("60870b7c-ac1e-4bd0-9b0f-9d89bf7c101a"),
                    MainText = "How are the API calls authenticated?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("8b7379ce-fcae-417d-aa42-e615e5336118"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("69d90bcd-e544-498f-b3c3-4c5c0969ba81"),
                    MainText = "Are these API calls encrypted?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("341a0d02-99bc-448f-9695-d6a2611ece01"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2426e9e1-f5d8-4a65-97a0-20de1daa1910"),
                    MainText = "How are the API calls encrypted?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("69d90bcd-e544-498f-b3c3-4c5c0969ba81"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("25ebc263-ecca-4289-a95e-b72545326a4c"),
                    MainText = "Do you monitor the third-party's compliance with your organization's policies?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("5c6754a2-bb85-432d-92df-a0adc775051c"),
                    MainText = "How do you monitor for Data Protection policy violations?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("25ebc263-ecca-4289-a95e-b72545326a4c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0dd9faab-411f-4175-b84b-fa3550d5b9d0"),
                    MainText = "How do you monitor for Access Management policy violations?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("25ebc263-ecca-4289-a95e-b72545326a4c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("01043dda-40c7-4fdb-979d-b7b3d0a4d35d"),
                    MainText = "How do you monitor for Security Architecture policy violations?",                    
                    Order = 3,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("25ebc263-ecca-4289-a95e-b72545326a4c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7c8f5559-fcf0-489d-b6a6-e2beb8fe998f"),
                    MainText = "How do you monitor for Incident Response policy violations?",                    
                    Order = 4,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("25ebc263-ecca-4289-a95e-b72545326a4c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0376bd85-34b4-4b85-86dd-12fde8babeba"),
                    MainText = "Do you have a process to review third-party access activity?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c5e0c403-0bf2-43b9-ba19-0193c9ca5e3f"),
                    MainText = "What is the process for reviewing third-party access activity?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("0376bd85-34b4-4b85-86dd-12fde8babeba"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7d8f7fb2-0659-43e9-923c-8e87a1450aa8"),
                    MainText = "How often do you review remote access activity?",
                    OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("0376bd85-34b4-4b85-86dd-12fde8babeba"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(thirdPartyAccessQuestions);

            #endregion

            return results.ToArray();
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.