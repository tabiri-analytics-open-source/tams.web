using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.CmuDatabaseEntities.CmuQuestions
{
    public static class CmuSecurityGovernanceQuestions
    {
        public static Question[] Seed(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Empty;

            var results = new List<Question>();

            #region Strategic Planning Questions

            subcategoryId = Guid.Parse("dcec4917-2291-43cb-8d2a-9e0945656b1c");

            var strategicPlanningQuestions = new List<Question>
            {
              new Question
              {
                Id = Guid.Parse("10be36df-ce78-49f2-99b6-22a51aa414c5"),
                MainText = "Does your organization have a documented Strategic Plan for your IT Security department?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("182351eb-5930-474e-b583-7ebccb49208f"),
                MainText = "What is the timeline for your strategic plan outlining the goals and objectives for the security department?",
                OptionChoices = "1-year,3-year,5-years,>5-years",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("10be36df-ce78-49f2-99b6-22a51aa414c5"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("de06b2c8-8681-4895-9285-dcd693e9f243"),
                MainText = "Are members of the IT Security department aware of their assigned goals?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 2,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("d93e8aa4-480d-48e5-a89f-52a4b86cc47d"),
                MainText = "Are these goals and objectives tracked and reported?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("de06b2c8-8681-4895-9285-dcd693e9f243"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("da283278-a58a-4452-a93e-5353bd1e2208"),
                MainText = "Are other departments aware of the IT Security department’s mission?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                Order = 3,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("adb3a9f5-cb44-4920-9b38-a1ea1b892898"),
                MainText = "Is Management updated on the progress of the IT Security department?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 4,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("de4dc8b6-2760-4271-a8f0-520ebbd6f160"),
                MainText = "How often?",
                OptionChoices = "Weekly,Monthly,Quarterly,Annually",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("adb3a9f5-cb44-4920-9b38-a1ea1b892898"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("4c471373-6dfe-4883-846e-5e3919252846"),
                MainText = "In what format?",
                OptionChoices = "Email notes, Team meeting, Online portal",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 2,
                QuestionType = QuestionType.SelectMultiple,
                ParentId = Guid.Parse("adb3a9f5-cb44-4920-9b38-a1ea1b892898"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("aaef5237-9e3b-4f3f-8007-4d8a735c98f3"),
                MainText = "Does the organization have a dedicated budget for its IT Security department?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 5,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("0676afb5-550e-4647-aede-26725ec78673"),
                MainText = "How was the IT Security budget initially determined?",
                OptionChoices = "Ad hoc, Consulting study, Fixed percentage of IT budget",
                OptionsOrientation = OptionsOrientation.Vertical,
                ActiveOptionIndex = 2,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("aaef5237-9e3b-4f3f-8007-4d8a735c98f3"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("6ac2a0b7-8f69-451d-a26c-8792fc6f5c0e"),
                MainText = "If fixed percentage of IT budget, what percentage?",
                Order = 1,
                QuestionType = QuestionType.Percentage,
                ParentId = Guid.Parse("0676afb5-550e-4647-aede-26725ec78673"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("7082aff6-4236-4038-9bab-0b2cb8304ff2"),
                MainText = "Is the IT Security budget broken down by specific need?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 2,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("aaef5237-9e3b-4f3f-8007-4d8a735c98f3"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("41764f63-8be3-4f45-b8f4-1ef3eb94a645"),
                MainText = "Percentage allocated for personnel?",
                Order = 1,
                QuestionType = QuestionType.Percentage,
                ParentId = Guid.Parse("7082aff6-4236-4038-9bab-0b2cb8304ff2"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("3ae2cee3-10fa-4cd5-8c69-57ce35c779e3"),
                MainText = "Percentage allocated for tools and equipment?",
                Order = 2,
                QuestionType = QuestionType.Percentage,
                ParentId = Guid.Parse("7082aff6-4236-4038-9bab-0b2cb8304ff2"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("262fa68a-66b2-4a2d-92f8-1ae4e1c06e04"),
                MainText = "Percentage allocated for training?",
                Order = 3,
                QuestionType = QuestionType.Percentage,
                ParentId = Guid.Parse("7082aff6-4236-4038-9bab-0b2cb8304ff2"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("d6f89eb1-0079-4fbd-b9c5-aee4fb0a91ad"),
                MainText = "Is this budget revised regularly?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 3,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("aaef5237-9e3b-4f3f-8007-4d8a735c98f3"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("56f98a1a-8bb8-43a4-8138-8c0de7d24223"),
                MainText = "How often?",
                OptionChoices = "Quarterly, Semi-annually, Annually",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("d6f89eb1-0079-4fbd-b9c5-aee4fb0a91ad"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("56e6d243-ae61-42ca-9d0b-6be55578b83e"),
                MainText = "What criteria is used to justify increases or decreases in the budget?",
                Order = 2,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("d6f89eb1-0079-4fbd-b9c5-aee4fb0a91ad"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("6633048e-e633-452e-812d-9fc1cbb25df9"),
                MainText = "Do the outcome of the goals set by the IT security department drive revisions to the Strategic Plan for your IT Security department?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 6,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("2f49e657-1752-4ea2-a4e6-af9858401ec7"),
                MainText = "How often are revisions made?",
                OptionChoices = "Weekly,Monthly,Quarterly,Semi-annually,Annually",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("6633048e-e633-452e-812d-9fc1cbb25df9"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              }
            };
            results.AddRange(strategicPlanningQuestions);

            #endregion

            #region Security Policy Framework Questions

            subcategoryId = Guid.Parse("96e4580d-05cb-4b7d-97b4-cba0a477bb1c");

            var securityPolicyFrameworkQuestions = new List<Question>
            {
              new Question
              {
                Id = Guid.Parse("7d26502f-b3ad-4425-a061-fca35e04e803"),
                MainText = "Does your organization have documented security policies, standards and procedures?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },

              new Question
              {
                Id = Guid.Parse("5e696c3c-00c7-4620-9bd5-d09dd2940658"),
                MainText = "Are the policies, standards and procedures aligned with any industry framework?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("7d26502f-b3ad-4425-a061-fca35e04e803"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("27e230fb-851f-4d9e-a7e5-b5dc024963c8"),
                MainText = "Which industry frameworks are they aligned?",
                OptionChoices = "NIST,ISO/IEC,COBIT,Other",
                OptionsOrientation = OptionsOrientation.Vertical,
                ActiveOptionIndex = 3,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("5e696c3c-00c7-4620-9bd5-d09dd2940658"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("996fdba2-9bb3-4330-93f3-5b58614b41bb"),
                MainText = "Which other industry framework are they aligned to?",
                Order = 1,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("27e230fb-851f-4d9e-a7e5-b5dc024963c8"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("16d1f348-6726-47d9-87a5-ace16c33c736"),
                MainText = "List the policies that your organization has documented.",
                HintText = "A policy explains why you should do something, e.g. Acceptable Use, Disaster Recovery, Change Management",
                Order = 2,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("7d26502f-b3ad-4425-a061-fca35e04e803"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("18a4be76-8510-4161-8c62-5f6bfb4c98a8"),
                MainText = "List the standards that your organization has documented.",
                HintText = "A standard documents the system specific rules, e.g. Windows Configuration, Enterprise Logging",
                Order = 3,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("7d26502f-b3ad-4425-a061-fca35e04e803"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("7eb85a37-43ae-45c6-8732-8f4750494991"),
                MainText = "List the procedures that your organization has documented.",
                HintText = "A procedure describes how you should do something, e.g. IRP, System Backup",
                Order = 4,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("7d26502f-b3ad-4425-a061-fca35e04e803"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("a24516a3-be28-4ef7-8fb9-a06e1a4f2f92"),
                MainText = "Which of these policies do you currently have?",
                OptionChoices = "Acceptable Use Policy,Application Security Policy,Cloud Security Policy,Configuration Policy,Database Security Policy,Encryption Policy,Endpoint Security Policy,Log Management Policy,Media Disposal Policy,Mobile Device Policy,Network Security Policy,Password Policy,Patch Management Policy,Remote Access Policy,Vulnerability Management",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 5,
                QuestionType = QuestionType.SelectMultiple,
                ParentId = Guid.Parse("7d26502f-b3ad-4425-a061-fca35e04e803"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("38fbae79-a828-4020-b36d-8fbbcdfa5db0"),
                MainText = "Are there specific individuals accountable for these policies, standards and procedures?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 6,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("7d26502f-b3ad-4425-a061-fca35e04e803"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("beb660d3-62c6-4300-a6c3-529720a1cedb"),
                MainText = "What is the functional role of these individuals?",
                Order = 1,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("38fbae79-a828-4020-b36d-8fbbcdfa5db0"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("fc151659-73d1-455b-a552-90787e9e7a2e"),
                MainText = "Is there a process to review and revise these security policies, standards and procedures?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 7,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("7d26502f-b3ad-4425-a061-fca35e04e803"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("d7ade438-dfb8-481a-a70b-a47926366401"),
                MainText = "What does this process look like?",
                Order = 1,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("fc151659-73d1-455b-a552-90787e9e7a2e"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              }
            };
            results.AddRange(securityPolicyFrameworkQuestions);

            #endregion

            #region Organizational Structure Questions

            subcategoryId = Guid.Parse("b9912cf5-6e62-43ae-a0cd-36d7fbb52944");

            var organizationalStructureQuestions = new List<Question>
            {
              new Question
              {
                Id = Guid.Parse("d5cdd942-9f2e-45e0-a741-d5e00c827e11"),
                MainText = "Is the IT Security department its own business unit, separate from the general IT department?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("704413a3-2206-4473-be67-48394c4da2b6"),
                MainText = "Does your organization have a formal reporting structure for IT Security department?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 2,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question 
              {
                Id = Guid.Parse("03b4ec6b-75ae-420b-9516-ab01c834bb43"),
                MainText = "If yes, select one.",
                OptionChoices = "IT Security Manager → CISO → CIO → CEO/President,IT Security Manager → CISO → CEO/President,IT Security Manager → CEO/President/Board/Other",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("704413a3-2206-4473-be67-48394c4da2b6"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("ad999bd9-c3ed-4fdc-9092-451f59a3a342"),
                MainText = "How many people work in the IT Security department?",
                Order = 3,
                QuestionType = QuestionType.Integer,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("70100929-079f-4317-971d-9c47b53a5757"),
                MainText = "What fraction of the IT Security department are female?",
                Order = 1,
                QuestionType = QuestionType.Percentage,
                ParentId = Guid.Parse("ad999bd9-c3ed-4fdc-9092-451f59a3a342"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("72746b62-b45b-4460-83a7-c84a13780260"),
                MainText = "How many women are in management roles in your IT Security department?",
                Order = 2,
                QuestionType = QuestionType.Integer,
                ParentId = Guid.Parse("ad999bd9-c3ed-4fdc-9092-451f59a3a342"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("8477f67e-a768-4f5b-9fe3-6a5cc14efd9a"),
                MainText = "How many total employees do you have in your organization?",
                Order = 3,
                QuestionType = QuestionType.Integer,
                ParentId = Guid.Parse("ad999bd9-c3ed-4fdc-9092-451f59a3a342"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("a740dcfe-18f6-4c4e-8dd5-42549b48899d"),
                MainText = "What industry sector is your organization classified under?",
                OptionChoices = "Agriculture, Communications, Construction, Education, Energy, Entertainment, Finance, Healthcare, Hospitality, Information Technology, Insurance, Government, Manufacturing, Mining, Non-profit, Professional Services, Real Estate, Retail, Transportation, Other",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 4,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("ad999bd9-c3ed-4fdc-9092-451f59a3a342"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("0168613f-3d13-4889-8d4c-35d4567ab94e"),
                MainText = "Does the IT Security staff focus on just network, system or application security?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 4,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("e8b8f590-9d5c-49f6-a4cc-b60b2d158c25"),
                MainText = "Does the IT Security staff also perform general (non-security) IT functions?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("0168613f-3d13-4889-8d4c-35d4567ab94e"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("f1cfcb70-4070-4911-9be4-8d3343dc2667"),
                MainText = "Is there at least one IT Security staff member working after-hours, weekends or holidays?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                Order = 5,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("0a696ce0-74c6-4952-b53b-4eae10c1e072"),
                MainText = "Does your organization outsource any IT Security functions to third parties?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 6,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("c3ca4c7c-d5d5-47c6-aad8-bc0b3939c5a5"),
                MainText = "Which functions?",
                Order = 1,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("0a696ce0-74c6-4952-b53b-4eae10c1e072"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("67968e91-14c9-42b7-953f-dd91ca62de01"),
                MainText = "Are these functions incorporated into the same reporting structure identified in question (2)?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                Order = 2,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("0a696ce0-74c6-4952-b53b-4eae10c1e072"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              }
            };
            results.AddRange(organizationalStructureQuestions);

            #endregion

            #region Performance Metrics Questions

            subcategoryId = Guid.Parse("cd09235e-4fd1-4e8c-b751-f2bf6b449b27");

            var performanceMetricsQuestions = new List<Question>
            {
              new Question
              {
                Id = Guid.Parse("132e6da7-adbb-4d85-9d86-3b3225a89669"),
                MainText = "Does your organization collect any IT Security metrics?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("72460f8e-9eb6-4452-a905-a8061a0a47f0"),
                MainText = "Which metrics do you collect?",
                Order = 1,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("132e6da7-adbb-4d85-9d86-3b3225a89669"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("17ced44c-5b68-4538-b749-2eda3101ab50"),
                MainText = "Is the collection of these metrics based on a policy requirement?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 2,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("132e6da7-adbb-4d85-9d86-3b3225a89669"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("807ac3fe-c490-4bd1-99a6-2507d082505c"),
                MainText = "If yes, which policy requirements?",
                OptionChoices = "Business risk based,Regulatory compliance based,Return-on-Investment (ROI) based",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 1,
                QuestionType = QuestionType.SelectMultiple,
                ParentId = Guid.Parse("17ced44c-5b68-4538-b749-2eda3101ab50"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("def27465-76dc-42fd-bf9b-20809905c3f7"),
                MainText = "Does your organization use any specific metrics collection processes or tools?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 3,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("132e6da7-adbb-4d85-9d86-3b3225a89669"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("2c1f6d21-4693-4570-a871-44905390105f"),
                MainText = "Which ones?",
                Order = 1,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("def27465-76dc-42fd-bf9b-20809905c3f7"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("cbf02a36-01f8-453e-929b-401a281a7334"),
                MainText = "Is metrics collection automated?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                Order = 2,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("def27465-76dc-42fd-bf9b-20809905c3f7"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("f227de30-2ae5-4db3-a820-27618d309c00"),
                MainText = "Are these metrics reported to Management?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 4,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("132e6da7-adbb-4d85-9d86-3b3225a89669"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("e0588263-f6a9-4305-a497-a7421926547a"),
                MainText = "How often?",
                OptionChoices = "Weekly,Monthly,Quarterly,Annually",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("f227de30-2ae5-4db3-a820-27618d309c00"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("f653290a-ec6e-469f-b058-4729aa9e7e6f"),
                MainText = "Has management designated any Key Performance Indicators (KPIs)?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 2,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("f227de30-2ae5-4db3-a820-27618d309c00"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("9feb804b-69b9-4461-a354-81d0299858bb"),
                MainText = "Which ones?",
                Order = 1,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("f653290a-ec6e-469f-b058-4729aa9e7e6f"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("f7eb62a4-a53c-4a58-a2ff-93e7c2e371a5"),
                MainText = "Do these metrics play a role in revising the Strategic Plan?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                Order = 5,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("132e6da7-adbb-4d85-9d86-3b3225a89669"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("2666ddc3-b03e-4314-b3f8-643869814263"),
                MainText = "Are these metrics audited by an independent third-party?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                Order = 6,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("132e6da7-adbb-4d85-9d86-3b3225a89669"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              }
            };
            results.AddRange(performanceMetricsQuestions);

            #endregion

            #region Workforce Management Questions

            subcategoryId = Guid.Parse("a34bdc05-1dc9-4f6e-a3ef-a96d5ff90ed3");

            var workforceManagementQuestions = new List<Question>
            {
              new Question
              {
                Id = Guid.Parse("eec92a76-7e89-4e88-87b0-e5b99a97940e"),
                MainText = "Does your organization measure the level of skills and knowledge of the IT Security staff?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("d9b0081f-2e2d-4784-912d-f187a5079aa3"),
                MainText = "How often?",
                OptionChoices = "Monthly,Quarterly,Annually",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("eec92a76-7e89-4e88-87b0-e5b99a97940e"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("05a6c5d1-f704-40f2-8b2b-7739102f121a"),
                MainText = "What metrics does your organization use to measure the level of skills and knowledge?",
                Order = 2,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("eec92a76-7e89-4e88-87b0-e5b99a97940e"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("3e9ce9c3-1193-4071-bf3d-1cd60312e470"),
                MainText = "Does the organization provide training for IT Security staff?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 2,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("37e73aa8-314f-4fb6-9636-d5c5aae631e9"),
                MainText = "How often?",
                OptionChoices = "Monthly,Quarterly,Annually",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("3e9ce9c3-1193-4071-bf3d-1cd60312e470"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("baa6bc16-ffcf-451a-821d-74046d47e67d"),
                MainText = "What kind of training is provided?",
                Order = 2,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("3e9ce9c3-1193-4071-bf3d-1cd60312e470"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("615e1065-425f-4bd7-bd06-5409e1a657d2"),
                MainText = "What percentage of the IT budget is allocated to security training?",
                Order = 3,
                QuestionType = QuestionType.Percentage,
                ParentId = Guid.Parse("3e9ce9c3-1193-4071-bf3d-1cd60312e470"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("70b95a97-e3ca-458d-aac3-89791d1611ed"),
                MainText = "Is there a clear career path for IT Security staff?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                Order = 3,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("3f98ef86-5db7-4c50-a654-99c0a74b9e77"),
                MainText = "Is there a process to review security team staff performance?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 4,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("e78194e7-a7d1-40f6-b9a0-d47973b2b243"),
                MainText = "What does this process look like?",
                Order = 1,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("3f98ef86-5db7-4c50-a654-99c0a74b9e77"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("eb380e53-3b19-4edb-a37a-0ef3d0ae4609"),
                MainText = "Does your organization provide incentives to retain the best staff?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 5,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("fc9c3269-873c-4f4a-971c-edd4f1cb68da"),
                MainText = "If yes, specify the incentives.",
                OptionChoices = "Promotions,Bonuses,Job rotation",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 1,
                QuestionType = QuestionType.SelectMultiple,
                ParentId = Guid.Parse("eb380e53-3b19-4edb-a37a-0ef3d0ae4609"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("cfe0ca5d-815b-4fef-b724-194a4baf1941"),
                MainText = "Does your organization have a recruiting strategy for IT Security staff?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 6,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("c33dcbe1-5586-4f63-8309-ab7aa6fa9cf2"),
                MainText = "How does your organization determine what specific skills are needed?",
                Order = 1,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("cfe0ca5d-815b-4fef-b724-194a4baf1941"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("7e1e1ded-d462-4d59-b927-e4c878ac52be"),
                MainText = "How does your organization measure the competency of potential candidates?",
                Order = 2,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("cfe0ca5d-815b-4fef-b724-194a4baf1941"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("14247d45-512b-4803-88f2-d9d2c9178ef7"),
                MainText = "What tools or venues does your organization use to find prospective employees?",
                OptionChoices = "Universities,Job fairs,Online,Recruitment agencies,We do not explicitly recruit",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 3,
                QuestionType = QuestionType.SelectMultiple,
                ParentId = Guid.Parse("cfe0ca5d-815b-4fef-b724-194a4baf1941"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("b7d06faf-a0ba-4ada-ba8e-8813b97e597c"),
                MainText = "What fraction of applicants to IT Security staff roles are female?",
                Order = 4,
                QuestionType = QuestionType.Percentage,
                ParentId = Guid.Parse("cfe0ca5d-815b-4fef-b724-194a4baf1941"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              }
            };
            results.AddRange(workforceManagementQuestions);

            #endregion

            return results.ToArray();
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.