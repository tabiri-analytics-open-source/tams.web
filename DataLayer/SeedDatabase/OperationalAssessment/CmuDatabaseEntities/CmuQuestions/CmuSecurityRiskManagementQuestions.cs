using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.CmuDatabaseEntities.CmuQuestions
{
    public static class CmuSecurityRiskManagementQuestions
    {
        public static Question[] Seed(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("9452f450-6b56-437a-a6c0-9f7b03bfde8c");

            var results = new List<Question>();

            #region Risk Management Framework Questions

            subcategoryId = Guid.Parse("9452f450-6b56-437a-a6c0-9f7b03bfde8c");

            var riskManagementFrameworkQuestions = new List<Question>
            {
                new Question
                {
                    Id = Guid.Parse("c7660e97-88e7-48c7-8680-b7fb1de1efd7"),
                    MainText = "Does your organization have a Risk Management team?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("5daa50e7-7122-40c9-aa8e-98ccc3eb3dcb"),
                    MainText = "Is this team lead by a Chief Risk Officer (CRO) or equivalent role?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("c7660e97-88e7-48c7-8680-b7fb1de1efd7"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("72b666d1-f1a1-42f5-bb90-722a1d2fa991"),
                    MainText = "Is the IT security department represented within the risk team?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("c7660e97-88e7-48c7-8680-b7fb1de1efd7"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("2dcfc6d3-edaf-47a4-bce0-ce9682680216"),
                    MainText = "How is the IT Security department represented?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("72b666d1-f1a1-42f5-bb90-722a1d2fa991"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question {
                    Id = Guid.Parse("63ceb123-854e-42b1-9770-ec1e7fec0849"),
                    MainText = "Does the Risk Management team interact with the IT Security team?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("c7660e97-88e7-48c7-8680-b7fb1de1efd7"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("9fc7b35d-dd15-4c0f-9d32-ac29d8ac7c9b"),
                    MainText = "How often?",
                    OptionChoices = "Weekly,Monthly,Quarterly,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("63ceb123-854e-42b1-9770-ec1e7fec0849"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("f3d2f472-9ac0-434e-94e5-4a6d1e68cd3f"),
                    MainText = "In what format?",
                    OptionChoices = "Email notes, Team meeting, Online portal",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("63ceb123-854e-42b1-9770-ec1e7fec0849"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("53435414-1094-4628-ad67-70dbe595738c"),
                    MainText = "Is there a procedure for identifying IT security risks within the organization?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("333d6f05-c6a4-4b27-8dd0-856f16628e93"),
                    MainText = "Who is tasked with this responsibility internally?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("53435414-1094-4628-ad67-70dbe595738c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("ba01490f-9b8d-43ae-a041-de4c67ce489e"),
                    MainText = "How often is this task carried out?",
                    OptionChoices = "Weekly,Quarterly,Monthly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("53435414-1094-4628-ad67-70dbe595738c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("30a22855-1204-4d08-80ad-fc2131516b7c"),
                    MainText = "Is this procedure aligned with a specific industry framework?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("53435414-1094-4628-ad67-70dbe595738c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("e29c7ce3-a5e9-41e9-90e0-042616acddeb"),
                    MainText = "Which industry framework is it aligned with?",
                    HintText = "e.g. ISO/IEC 31000",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("30a22855-1204-4d08-80ad-fc2131516b7c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("d970a91f-9d47-41c6-88d7-84b83ed52b24"),
                    MainText = "What is the scope of the information security risk assessment?",
                    OptionChoices = "Critical systems only, Critical + Production systems, All systems",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("53435414-1094-4628-ad67-70dbe595738c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("6af552e8-aa77-4414-86b0-4d5f33e58dc0"),
                    MainText = "How are those systems identified?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("d970a91f-9d47-41c6-88d7-84b83ed52b24"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("4f00bac6-335d-4ce1-b361-f5fc79f4071e"),
                    MainText = "Are IT security risk assessments integrated with other IT activities like SDLC and vendor selection?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("53435414-1094-4628-ad67-70dbe595738c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("f0cdc20a-f840-4b33-baa5-8c20189dfac3"),
                    MainText = "Which IT activities integrate IT security risk assessments?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("4f00bac6-335d-4ce1-b361-f5fc79f4071e"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("7206ebbf-c184-458d-8407-f9db4953262b"),
                    MainText = "Is there a procedure for prioritizing IT security risk?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 7,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("53435414-1094-4628-ad67-70dbe595738c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("d02afc3f-0ef0-4747-bed1-b7155bc99f0b"),
                    MainText = "What does this procedure look like?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("7206ebbf-c184-458d-8407-f9db4953262b"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("0eb73819-229a-4095-ada2-724af8bb592f"),
                    MainText = "Are identified risks communicated to other business units?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 8,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("53435414-1094-4628-ad67-70dbe595738c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("8ea687a8-9169-4985-bdeb-de3423ff2245"),
                    MainText = "In what format?",
                    OptionChoices = "Email notes,Team meeting,Online portal",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("0eb73819-229a-4095-ada2-724af8bb592f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("6a9c142a-8ac7-4eda-887d-2d342b3dcc5b"),
                    MainText = "Is there a procedure for addressing identified risks?",
                    HintText = "Risk can either be avoided, mitigated, accepted, or transfered",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 9,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("53435414-1094-4628-ad67-70dbe595738c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("baa748ce-cc9d-4ee6-8a8d-41f9fbad5724"),
                    MainText = "What does this procedure look like?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("6a9c142a-8ac7-4eda-887d-2d342b3dcc5b"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("29382f91-0291-468e-ae0e-6849b486d641"),
                    MainText = "Who is assigned ownership of addressing these risks?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("6a9c142a-8ac7-4eda-887d-2d342b3dcc5b"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("e530fff3-0694-477c-958e-a05d94ab9b74"),
                    MainText = "How is progress on addressing these risks tracked?",                    
                    Order = 3,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("6a9c142a-8ac7-4eda-887d-2d342b3dcc5b"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("b0edaab9-e75a-4455-9034-5a5a44bdd0a2"),
                    MainText = "Is there a follow-up after the risks are addressed?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 10,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("53435414-1094-4628-ad67-70dbe595738c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("e3cf01f7-74fd-4636-a4e0-08c096f039a3"),
                    MainText = "What does this follow-up look like?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("b0edaab9-e75a-4455-9034-5a5a44bdd0a2"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("01eb66d0-e8c9-4872-9884-43924be4872c"),
                    MainText = "Are the IT risk assessments audited by a third-party?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 11,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("53435414-1094-4628-ad67-70dbe595738c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("8fff52c2-ab48-4456-8d3a-6a1cfad3ecc6"),
                    MainText = "How are discrepancies between the internal assessment and third-party assessment addressed?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("01eb66d0-e8c9-4872-9884-43924be4872c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("e0ec2359-a143-4638-8bb2-bd25b5631f1d"),
                    MainText = "Are key IT security risks identified at this time?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("53435414-1094-4628-ad67-70dbe595738c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("8af9189e-5827-4140-97f5-9d0b7261ae27"),
                    MainText = "Which ones?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("e0ec2359-a143-4638-8bb2-bd25b5631f1d"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("84367b57-8093-4a82-bf42-1f306ebcf84a"),
                    MainText = "Are you aware if your organization is subject to any external regulations?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("798cdbee-14e3-4c1e-a5fc-15409016084e"),
                    MainText = "Does a specific regulatory body have oversight authority over your organization?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("84367b57-8093-4a82-bf42-1f306ebcf84a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("089c4df8-2532-4a55-8fde-fc59e000ba09"),
                    MainText = "Which regulatory body?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("798cdbee-14e3-4c1e-a5fc-15409016084e"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("1786ff50-feac-4497-a9b7-7fa7f83cf605"),
                    MainText = "Is IT security risk in scope for those regulations?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("84367b57-8093-4a82-bf42-1f306ebcf84a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("65047972-8b5b-4cd5-80d5-0b320d397d5b"),
                    MainText = "How often is the organization reviewed for compliance with these regulations?",
                    OptionChoices = "Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1786ff50-feac-4497-a9b7-7fa7f83cf605"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("b25c53c8-4842-44ec-b250-e2bfff1e3cbe"),
                    MainText = "Is this a separate procedure from the one defined in Question 2 above?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1786ff50-feac-4497-a9b7-7fa7f83cf605"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("2987dee4-ba98-4aad-bab0-bd3ea8f0d178"),
                    MainText = "In what format are the results of these reviews communicated?",
                    OptionChoices = "Email notes,Team meeting,Online portal",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("b25c53c8-4842-44ec-b250-e2bfff1e3cbe"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("8de3b5ff-a207-4522-b0d3-c79806836476"),
                    MainText = "How are identified risks addressed?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("b25c53c8-4842-44ec-b250-e2bfff1e3cbe"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("9bd60fb1-6cf0-4b6e-b8cb-ac88f542acdf"),
                    MainText = "Are the results of these reviews audited by a third-party?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b25c53c8-4842-44ec-b250-e2bfff1e3cbe"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(riskManagementFrameworkQuestions);

            #endregion

            #region Threat Management Questions

            subcategoryId = Guid.Parse("bcbce862-3628-4f03-bd6b-4da0a08ce9c1");

            var threatManagementQuestions = new List<Question>
            {
                new Question
                {
                    Id = Guid.Parse("5ec9c586-51dd-41a0-b874-74a686df4678"),
                    MainText = "Does your organization have a threat management policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("3f492ccd-9b0e-435c-8926-bd33f3007595"),
                    MainText = "Who is tasked with this responsibility?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("5ec9c586-51dd-41a0-b874-74a686df4678"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("360e04d2-a730-42d6-9a54-8475c4288d60"),
                    MainText = "What technologies are used to identify new and existing threats?",
                    HintText = "e.g. vulnerability scanner, security information event management (SIEM), threat intelligence feeds",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("5ec9c586-51dd-41a0-b874-74a686df4678"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("55296dda-eef9-4bb0-b628-f6ec6407848f"),
                    MainText = "Does your organization conduct vulnerability assessments?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("5c5b640a-ebf8-413a-bdfc-3fd759003554"),
                    MainText = "How often?",
                    OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("55296dda-eef9-4bb0-b628-f6ec6407848f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("864173c2-e54a-4f01-8692-d69e3767212e"),
                    MainText = "What is the scope of the vulnerability scans?",
                    HintText = "e.g. internal/external, authenticated/unauthenticated",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("55296dda-eef9-4bb0-b628-f6ec6407848f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("a93608a0-f147-4d3e-a6be-e8c9d8a85ab9"),
                    MainText = "Does your organization conduct penetration tests?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("06bab364-f966-46db-a641-5ce179d3047f"),
                    MainText = "How often?",
                    OptionChoices = "Bi-Weekly,Quarterly,Monthly,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("a93608a0-f147-4d3e-a6be-e8c9d8a85ab9"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("78dd9701-a199-4d63-9f7e-f44881879022"),
                    MainText = "What is the scope of the penetration tests?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("a93608a0-f147-4d3e-a6be-e8c9d8a85ab9"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("8981116c-5011-4d3f-8324-aca6bc4baafe"),
                    MainText = "Is there a procedure for translating technical findings into business risk?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("afbdba4f-ee99-463a-a610-77484ac9ae2c"),
                    MainText = "What is the procedure?",
                    HintText = "e.g. root cause analysis, CVSS score into business impact",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("8981116c-5011-4d3f-8324-aca6bc4baafe"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("b5ab0b35-1361-4e45-b7cd-9db04f74caf4"),
                    MainText = "Does your organization have a procedure for addressing identified vulnerabilities?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("2721f565-8b1b-4e34-92bf-88034d55a82d"),
                    MainText = "How are identified vulnerabilities managed and remediated?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("b5ab0b35-1361-4e45-b7cd-9db04f74caf4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("0e47c908-ecd7-432c-9008-fa873a6660ca"),
                    MainText = "Is there a procedure for validating patched vulnerabilities?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b5ab0b35-1361-4e45-b7cd-9db04f74caf4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("03bc7d9d-5ce1-473c-af30-64ad37e11a44"),
                    MainText = "What does this procedure look like?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("0e47c908-ecd7-432c-9008-fa873a6660ca"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("b75e601b-ed61-4a01-adb6-e85077e15637"),
                    MainText = "Is there a procedure for remediating vulnerabilities that cannot be patched?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b5ab0b35-1361-4e45-b7cd-9db04f74caf4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("b47577e1-f1f9-414b-85c5-c63254710676"),
                    MainText = "What does this procedure look like?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("b75e601b-ed61-4a01-adb6-e85077e15637"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("8a895e91-1921-4301-b2b4-5120b6e92008"),
                    MainText = "Does your organization have a Security Information Event Management (SIEM) solution?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("412e49e7-78f0-478f-8eab-fefc271b3714"),
                    MainText = "What data sources are integrated into the SIEM?",
                    HintText = "e.g. network, endpoint or application logs",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("8a895e91-1921-4301-b2b4-5120b6e92008"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("318a643b-f0fa-42f6-9476-9196b743ca64"),
                    MainText = "Are threat intelligence feeds integrated into the SIEM?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("8a895e91-1921-4301-b2b4-5120b6e92008"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("6a4bc62a-d2e6-41c8-ab2e-50a4b038139b"),
                    MainText = "What kind of feeds are they?",
                    HintText = "e.g. open-source, commercial",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("318a643b-f0fa-42f6-9476-9196b743ca64"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("6cfb9f5a-0f84-4937-a6f8-4a05db7d8c58"),
                    MainText = "Does your organization subscribe to any cyber threat information sharing forums?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 7,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("c531f0e7-7854-41ba-bd63-e61847bf11c2"),
                    MainText = "Are these forums specific to your industry?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("6cfb9f5a-0f84-4937-a6f8-4a05db7d8c58"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("6c53de96-f9d5-441b-bab3-c0dfbb5999ff"),
                    MainText = "Which Information Sharing and Analysis Center (ISAC) or equivalent organization is your organization a member of?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("c531f0e7-7854-41ba-bd63-e61847bf11c2"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("70449363-dbee-4e54-8434-3bb951077a48"),
                    MainText = "How are emerging threats tracked and communicated?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("6cfb9f5a-0f84-4937-a6f8-4a05db7d8c58"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };

            results.AddRange(threatManagementQuestions);

            #endregion

            #region Security Awareness Questions     

            subcategoryId = Guid.Parse("169c5d65-75fb-41b9-b607-fb43343ba884");     

            var securityAwarenessQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("5d9a18b1-2f6f-4b5d-9133-66a381a081fc"),
                    MainText = "Does your organization have a Security Awareness training program?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0d8046a2-c45b-435c-a26d-403f4c5bb526"),
                    MainText = "What percentage of the IT budget is allocated to this program?",                    
                    Order = 1,
                    QuestionType = QuestionType.Percentage,
                    ParentId = Guid.Parse("5d9a18b1-2f6f-4b5d-9133-66a381a081fc"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a153d192-c6dc-497b-ae7d-7654d1302d15"),
                    MainText = "Who is the target audience for this training?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("5d9a18b1-2f6f-4b5d-9133-66a381a081fc"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("48b4f1ed-dc05-41f3-bdc5-9d667152c157"),
                    MainText = "Who is tasked with developing the training content?",                    
                    Order = 3,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("5d9a18b1-2f6f-4b5d-9133-66a381a081fc"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("23e83c48-105d-4fbb-94ab-a0b409bda575"),
                    MainText = "Is there role specific training for employees in sensitive roles?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("5d9a18b1-2f6f-4b5d-9133-66a381a081fc"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("73e19c6a-69aa-4fc4-9e1c-98d946ab7bf2"),
                    MainText = "Which employee roles fall under this category?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("23e83c48-105d-4fbb-94ab-a0b409bda575"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e89f830e-d7f5-4f17-bebf-22e62ce9a162"),
                    MainText = "What specific training is provided?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("23e83c48-105d-4fbb-94ab-a0b409bda575"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("03e9298b-645c-4bcc-a78d-629f9588996e"),
                    MainText = "Is the training mandatory?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("5d9a18b1-2f6f-4b5d-9133-66a381a081fc"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d7d65ebe-3275-41cf-8ca4-71ba361cccf4"),
                    MainText = "How is compliance enforced?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("03e9298b-645c-4bcc-a78d-629f9588996e"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f62efa67-642a-4c10-8b60-ea4f558d01e7"),
                    MainText = "Does your organization have a formal delivery method for the IT security training program?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("5d9a18b1-2f6f-4b5d-9133-66a381a081fc"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("8bbceb28-b268-4308-8a84-54db5216a2c8"),
                    MainText = "How is the training delivered?",
                    HintText = "e.g. email newsletters, training workshops, online courses",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("f62efa67-642a-4c10-8b60-ea4f558d01e7"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0fb967c5-d80b-448a-8583-cdaeadca04e4"),
                    MainText = "How often is the training conducted?",
                    OptionChoices = "Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f62efa67-642a-4c10-8b60-ea4f558d01e7"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a497e4e4-9997-4f73-b018-aded4018e412"),
                    MainText = "When is the IT security training conducted?",
                    OptionChoices = "During new employee onboarding,After employee job change",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 7,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("5d9a18b1-2f6f-4b5d-9133-66a381a081fc"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e1e2a459-804b-46dc-a73f-59f977edf4b1"),
                    MainText = "Does your organization have a formal list of IT security training topics?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 8,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("5d9a18b1-2f6f-4b5d-9133-66a381a081fc"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("8e871453-b15d-4d5e-9703-9bca4f834ea6"),
                    MainText = "What specific topics are covered?",
                    HintText = "e.g. data protection, web hygiene, email hygiene/phishing, incident reporting",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("e1e2a459-804b-46dc-a73f-59f977edf4b1"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("17378991-3a0d-4de4-93ac-a8b77acb6f42"),
                    MainText = "Is the IT security training progress tracked?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 9,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("5d9a18b1-2f6f-4b5d-9133-66a381a081fc"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c8286f43-7df0-4844-879c-09ba4bc19ec4"),
                    MainText = "What completion metrics are collected?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("17378991-3a0d-4de4-93ac-a8b77acb6f42"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("821f091d-b0b4-4ba2-a459-fead3c67fbc4"),
                    MainText = "In what format are metrics communicated to management?",
                    OptionChoices = "Email notes,Team meeting,Online portal",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("17378991-3a0d-4de4-93ac-a8b77acb6f42"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ec160691-055c-479a-bd8c-f40e70e4089b"),
                    MainText = "Is there a procedure for reviewing and revising the IT security awareness program?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 10,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("5d9a18b1-2f6f-4b5d-9133-66a381a081fc"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("50db4405-6d96-476f-ab3a-314f63bc474f"),
                    MainText = "How often is the program reviewed?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("ec160691-055c-479a-bd8c-f40e70e4089b"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0fe01b73-e67f-4085-9afa-4d07a7045d2d"),
                    MainText = "What is the procedure for revising the program?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("ec160691-055c-479a-bd8c-f40e70e4089b"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(securityAwarenessQuestions);

            #endregion
            
            return results.ToArray();
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.