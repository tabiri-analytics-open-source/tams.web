using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.CmuDatabaseEntities.CmuQuestions
{
    public static class CmuDataProtectionQuestions
    {
        public static Question[] Seed(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Empty;

            var results = new List<Question>();

            #region Data Protection Framework Questions

            subcategoryId = Guid.Parse("15d6d4cb-e910-42c5-bb47-b4d03103edb0");

            var dataProtectionFrameworkQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("053e4f0d-95ed-452c-a592-08bbead5eb35"),
                    MainText = "Does your organization have a Data Protection framework?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d3e4df64-410a-45b6-9b6d-5f7ac38f3385"),
                    MainText = "Which department is tasked with oversight of this framework?",
                    HintText = "e.g. Legal, Risk, IT, a joint team from these departments",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("053e4f0d-95ed-452c-a592-08bbead5eb35"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("43c5956a-b8b2-44c6-ad54-410ebab66e29"),
                    MainText = "What are the specific roles and responsibilities of the oversight members?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("053e4f0d-95ed-452c-a592-08bbead5eb35"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a5559e4a-61e4-4e72-836d-80cf1839686d"),
                    MainText = "Which data protection topics does this framework cover?	",
                    OptionChoices = "Data Classification,Data Loss,Data Protection,Data Recovery,Data Retention",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 3,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("053e4f0d-95ed-452c-a592-08bbead5eb35"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("466ac49f-41c5-4700-bfd7-65229a503182"),
                    MainText = "Is this framework based on a specific industry standard?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("053e4f0d-95ed-452c-a592-08bbead5eb35"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("62b47b29-85df-4fde-8eb7-54c1f75e79ae"),
                    MainText = "Which industry standard is it based on? {e.g. NIST 800-53, ISO/IEC 27001}",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("466ac49f-41c5-4700-bfd7-65229a503182"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c71373c9-ae93-4487-9b42-4bd7fb721b56"),
                    MainText = "Is this framework regularly reviewed and updated?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("053e4f0d-95ed-452c-a592-08bbead5eb35"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("21a8af30-47b1-4fce-819e-9d05fd417d93"),
                    MainText = "How often?",
                    OptionChoices = "Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("c71373c9-ae93-4487-9b42-4bd7fb721b56"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("014972e9-c66f-4ed0-8e80-ab25ad6842b0"),
                    MainText = "When was the most recent review?",                    
                    Order = 2,
                    QuestionType = QuestionType.Date,
                    ParentId = Guid.Parse("c71373c9-ae93-4487-9b42-4bd7fb721b56"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c5f6efd2-36f4-4747-b40b-30422641662a"),
                    MainText = "Has your organization documented the types of data it processes?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("10618c26-20f9-416b-ad35-d39901ada3f0"),
                    MainText = "What types of data does your organization process?",
                    HintText = "e.g. financial, PCI, PHI, PII, SCADA/ICS, intellectual property",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("c5f6efd2-36f4-4747-b40b-30422641662a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("df8978f8-e1cf-4922-aa85-719b1e335451"),
                    MainText = "Is this data tracked across the organization?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("c5f6efd2-36f4-4747-b40b-30422641662a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("86da6bcd-084a-425a-914f-8c912512aef2"),
                    MainText = "How is this data tracked across the organization?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("df8978f8-e1cf-4922-aa85-719b1e335451"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("57e3bbdf-8e2b-48e0-8ad7-6fa3dc31f0dd"),
                    MainText = "Has your organization defined data owners for each data type?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("242e691a-472b-4a04-9bf3-0225dafb80a5"),
                    MainText = "Who are the data owners for each data type?",
                    HintText = "e.g. specific departments, individuals or multiple owners",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("57e3bbdf-8e2b-48e0-8ad7-6fa3dc31f0dd"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(dataProtectionFrameworkQuestions);

            #endregion

            #region Data Classification Questions

            subcategoryId = Guid.Parse("7a697f1e-464c-4342-b369-33955d46663d");

            var dataClassificationQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("70c1a16e-29bc-439a-9ce2-9eb1510e6a6d"),
                    MainText = "Does your organization have a Data Classification policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b48eea4b-fdcb-4a96-a856-67dc6fe77076"),
                    MainText = "What are the existing classification levels?",
                    HintText = "e.g. Secret, Confidential, Internal Use Only, Public",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("70c1a16e-29bc-439a-9ce2-9eb1510e6a6d"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f1cbaff1-d553-4c50-bad1-57676ff8e191"),
                    MainText = "What is the procedure for classifying data across business units?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("70c1a16e-29bc-439a-9ce2-9eb1510e6a6d"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("31a8dbd8-a123-473d-abb2-6dad937a0152"),
                    MainText = "Who is tasked with this procedure across business units?",                    
                    Order = 3,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("70c1a16e-29bc-439a-9ce2-9eb1510e6a6d"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ee21dfcd-c5b8-41d1-a58c-0d791a286102"),
                    MainText = "Does your organization have data handling guidelines?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b6b59101-6f01-4279-9441-01c6bbb36016"),
                    MainText = "Are the respective data owners aware of these guidelines?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("ee21dfcd-c5b8-41d1-a58c-0d791a286102"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("179619c1-eb33-42a8-ac3e-4a67aa3025b8"),
                    MainText = "In what format are these guidelines communicated to data owners?",
                    OptionChoices = "Email notes,Team meeting,Online portal",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("b6b59101-6f01-4279-9441-01c6bbb36016"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0f51dc62-e772-4658-b6b9-842e83fe4670"),
                    MainText = "Are these data handling guidelines enforced?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("ee21dfcd-c5b8-41d1-a58c-0d791a286102"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("439bc428-d5ec-4884-886b-0a11be36688b"),
                    MainText = "How are these guidelines enforced?",
                    HintText = "e.g. manual data marking, automated DLP tool",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("0f51dc62-e772-4658-b6b9-842e83fe4670"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("522e7967-c22e-42db-b36e-1263652764b9"),
                    MainText = "Is enforcement mapped to existing access control policies?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("0f51dc62-e772-4658-b6b9-842e83fe4670"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("15bd493d-1256-472f-bfb8-c87ffedfdd82"),
                    MainText = "Is enforcement consistent across all business units?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("0f51dc62-e772-4658-b6b9-842e83fe4670"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("45fec52d-ad19-45d7-bde9-52ae7cf87b79"),
                    MainText = "Are data classification reviews conducted?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("84335b59-3f69-4b77-b442-aabb8fb94c3a"),
                    MainText = "How often?",
                    OptionChoices = "Monthly,Quarterly,Semi-annually,Annually,Other",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("45fec52d-ad19-45d7-bde9-52ae7cf87b79"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2cbd9db4-a536-4bfb-8cf8-69c1c769a690"),
                    MainText = "How are these reviews carried out?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("45fec52d-ad19-45d7-bde9-52ae7cf87b79"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(dataClassificationQuestions);

            #endregion

            #region Data Protection Questions

            subcategoryId = Guid.Parse("32b2f015-8e14-4cfa-936a-6c58e18442cc");

            var dataProtectionPoliciesQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("f15a89b3-5ec1-443c-ac6e-4421089d0ebe"),
                    MainText = "Does your organization have procedures in place to protect data-at-rest?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("4ec3dc83-c04f-4e02-81e3-2c18bc4cd5c6"),
                    MainText = "Are there encryption requirements for data-at-rest?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f15a89b3-5ec1-443c-ac6e-4421089d0ebe"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("dfd35734-ec06-4988-9066-b2c288854a49"),
                    MainText = "What types of data do these requirements apply to?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("4ec3dc83-c04f-4e02-81e3-2c18bc4cd5c6"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("fb4f526d-b126-4e28-8fc8-28ea513bebdf"),
                    MainText = "Are data owners aware of these requirements? ",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("4ec3dc83-c04f-4e02-81e3-2c18bc4cd5c6"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1791d99f-b274-4962-9fd6-1c531570dfb7"),
                    MainText = "Are there encryption standards for data-at-rest?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f15a89b3-5ec1-443c-ac6e-4421089d0ebe"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("aaa89fb3-7376-4c9d-8451-3c514d69a876"),
                    MainText = "What cryptographic algorithms are used for encrypting data-at-rest?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("1791d99f-b274-4962-9fd6-1c531570dfb7"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("062ea495-6cd2-4e51-a7ab-1cb9343e5149"),
                    MainText = "Are data owners aware of these standards?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1791d99f-b274-4962-9fd6-1c531570dfb7"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("cb47c1ee-7da8-47a4-a836-ee4515bd9c19"),
                    MainText = "Is there a procedure for encrypting files on disk?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f15a89b3-5ec1-443c-ac6e-4421089d0ebe"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("07736aad-5c2f-45be-8024-f9f85237b7a3"),
                    MainText = "What specific solutions are used for encrypting files on disk?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("cb47c1ee-7da8-47a4-a836-ee4515bd9c19"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2a02d2b1-a4f2-41a5-a74e-6c5fa5855dc4"),
                    MainText = "Are data owners aware of this procedure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("cb47c1ee-7da8-47a4-a836-ee4515bd9c19"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("41768381-29fc-4fc2-9338-836b03dc1cce"),
                    MainText = "Is full disk encryption required for mobile devices and removable media?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f15a89b3-5ec1-443c-ac6e-4421089d0ebe"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("9625807d-071a-4ba0-ac68-474bf5805fed"),
                    MainText = "What specific full disk encryption solutions are used?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("41768381-29fc-4fc2-9338-836b03dc1cce"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("266cc26c-f248-4f1f-a01a-5a2ad670a211"),
                    MainText = "Are data owners aware of these solutions?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("41768381-29fc-4fc2-9338-836b03dc1cce"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("00156e8c-c2dc-4519-a71c-55104b053466"),
                    MainText = "Is there a procedure for encrypting databases?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f15a89b3-5ec1-443c-ac6e-4421089d0ebe"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("5d750637-ee67-4b0f-845c-293f897637fb"),
                    MainText = "What specific solutions are used for encrypting databases?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("00156e8c-c2dc-4519-a71c-55104b053466"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("3b63a61c-2a67-437d-a167-398a5267ec1f"),
                    MainText = "Are data owners aware of these solutions?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("00156e8c-c2dc-4519-a71c-55104b053466"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("edb10302-686b-483c-b314-2e308618c108"),
                    MainText = "Is there a procedure for managing encryption recovery keys?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f15a89b3-5ec1-443c-ac6e-4421089d0ebe"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("94546c8d-706a-4570-8ae0-7db4be47569d"),
                    MainText = "What is the procedure for managing encryption recovery keys?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("edb10302-686b-483c-b314-2e308618c108"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("8e18621a-f73e-4a0e-9e76-9ca02f282802"),
                    MainText = "Are data owners aware of this procedure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("edb10302-686b-483c-b314-2e308618c108"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("38b8aca5-71c9-431b-baf0-ce45e54c0343"),
                    MainText = "Are reviews of data-at-rest procedures conducted?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 7,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f15a89b3-5ec1-443c-ac6e-4421089d0ebe"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("80ca18ef-29dc-43a6-acbd-1dd96fba5d83"),
                    MainText = "How often?",
                    OptionChoices = "Monthly,Quarterly,Semi-annually,Annually,Other",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("38b8aca5-71c9-431b-baf0-ce45e54c0343"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },       
                new Question 
                {
                    Id = Guid.Parse("fe8840bf-a8b6-4507-bd60-ef54579c741e"),
                    MainText = "In what format?",
                    OptionChoices = "Email notes, Team meeting, Online portal",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2, 
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("38b8aca5-71c9-431b-baf0-ce45e54c0343"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d80d8050-9b3e-4a3f-acd3-c48725d02c49"),
                    MainText = "Does your organization have procedures in place to protect data-in-motion or use?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("cc43eca7-d640-410b-93c9-0797826ad8eb"),
                    MainText = "Are there encryption requirements for data-in-motion?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("d80d8050-9b3e-4a3f-acd3-c48725d02c49"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("dc30347b-6116-4187-9e19-222766733644"),
                    MainText = "What types of data do these requirements apply to?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("cc43eca7-d640-410b-93c9-0797826ad8eb"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f2359547-a0d7-40d4-9e05-493e55cdd3d5"),
                    MainText = "Are data owners aware of these requirements?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("cc43eca7-d640-410b-93c9-0797826ad8eb"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("63561d41-6c82-4758-99c3-13c307bcc320"),
                    MainText = "Are there encryption standards for data-in-motion?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("d80d8050-9b3e-4a3f-acd3-c48725d02c49"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("eaa8c3cf-2cb0-4e58-bd0c-1b4524f73e60"),
                    MainText = "What cryptographic algorithms are used for encrypting data-in-motion?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("63561d41-6c82-4758-99c3-13c307bcc320"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d4f1956a-85af-44bd-9b78-b3a278d240b9"),
                    MainText = "Are data owners aware of these standards?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("63561d41-6c82-4758-99c3-13c307bcc320"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("4e70a8fe-c5a3-4493-afd2-edee8e01355b"),
                    MainText = "Is there a procedure for encrypting email communications?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("d80d8050-9b3e-4a3f-acd3-c48725d02c49"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("8dc1ce51-a693-46c0-846c-c0da8d7bd6d9"),
                    MainText = "What specific solutions are used for encrypting email communications?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId =  Guid.Parse("4e70a8fe-c5a3-4493-afd2-edee8e01355b"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b5539dd1-d041-4c1e-9933-732421defe6a"),
                    MainText = "Are data owners aware of this procedure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId =  Guid.Parse("4e70a8fe-c5a3-4493-afd2-edee8e01355b"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("733650ec-e979-4708-938c-4a4e74119526"),
                    MainText = "Is there a procedure for encrypting external network communications?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("d80d8050-9b3e-4a3f-acd3-c48725d02c49"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f45adfef-0f6d-4351-ba38-23173fe06920"),
                    MainText = "What specific solutions are used for encrypting external network communications?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("733650ec-e979-4708-938c-4a4e74119526"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("35a665ac-e6ac-4420-831b-f85b2a281498"),
                    MainText = "Are data owners aware of this procedure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("733650ec-e979-4708-938c-4a4e74119526"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("aea06ecb-ef6f-4673-b736-d3496d3ef507"),
                    MainText = "Is there a procedure for encrypting internal network communications?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("d80d8050-9b3e-4a3f-acd3-c48725d02c49"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("66ea0420-8c03-477f-b907-e18ea877e2e7"),
                    MainText = "What specific solutions are used for encrypting internal network communications?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("aea06ecb-ef6f-4673-b736-d3496d3ef507"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("33b74538-7af4-43cd-b195-13ebe3f5cb01"),
                    MainText = "Are data owners aware of this procedure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("aea06ecb-ef6f-4673-b736-d3496d3ef507"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("721cd7fa-6916-446f-bd95-d1bbb8d82613"),
                    MainText = "Are reviews of data-in-motion procedures conducted?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("d80d8050-9b3e-4a3f-acd3-c48725d02c49"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d1badc4d-a972-4135-96a3-3135acbb47f3"),
                    MainText = "How often?",
                    OptionChoices = "Monthly,Quarterly,Semi-annually,Annually,Other",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("721cd7fa-6916-446f-bd95-d1bbb8d82613"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ab6079c3-4ec9-4f12-bf58-11509d8c72ea"),
                    MainText = "How are these reviews carried out?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("721cd7fa-6916-446f-bd95-d1bbb8d82613"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(dataProtectionPoliciesQuestions);

            #endregion

            #region Data Retention Questions

            subcategoryId = Guid.Parse("82083045-11e8-4f48-a91c-eb615c34bffa");

            var dataRetentionQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("7b4480d4-cdc2-4613-9df0-d8826f7a94d7"),
                    MainText = "Does your organization have a Data Retention policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("ccde4992-7271-414e-8e23-bf1e1e99415b"),
                    MainText = "Is this policy based on specific regulatory requirements?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("7b4480d4-cdc2-4613-9df0-d8826f7a94d7"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("eb193871-a71a-41a9-83e7-f7525739376f"),
                    MainText = "What specific regulatory requirements?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("ccde4992-7271-414e-8e23-bf1e1e99415b"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id =Guid.Parse("b3c69006-d6ac-418c-9266-bcb6c7651f58"),
                    MainText = "What types of data are retained?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("7b4480d4-cdc2-4613-9df0-d8826f7a94d7"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a5548362-05da-4bac-9c7e-028925b427c8"),
                    MainText = "Are data owners aware of the retention requirements?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("7b4480d4-cdc2-4613-9df0-d8826f7a94d7"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("3b81cd2d-f220-4f3a-bd0f-3d886d08f7d1"),
                    MainText = "What training is provided to those tasked with data retention?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("a5548362-05da-4bac-9c7e-028925b427c8"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("912f986c-18bf-4986-84dc-61a82a85f6aa"),
                    MainText = "Is this policy reviewed?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("7b4480d4-cdc2-4613-9df0-d8826f7a94d7"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7ae1b03c-6f6c-4677-80bd-0cf6575a3204"),
                    MainText = "How often?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("912f986c-18bf-4986-84dc-61a82a85f6aa"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f6e20335-3fbc-43d7-be67-5ce3fce7bd3b"),
                    MainText = "Does your organization have a procedure for securing retained data?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("19370c31-b792-47f3-8ca2-869a6e468857"),
                    MainText = "Is data retained on the same systems that produced it?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f6e20335-3fbc-43d7-be67-5ce3fce7bd3b"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("546edcde-6364-4e78-b481-3ebb3b9c9a7a"),
                    MainText = "Are the data protection controls on the data retention system similar to the ones on the data production system?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("19370c31-b792-47f3-8ca2-869a6e468857"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1a17f7bb-ecbc-44c4-a2a3-996b2e9c7c1b"),
                    MainText = "Does your organization have guidelines on data/media disposal?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("cbaebb82-1f63-4570-9b7a-6b9a3d29ebe6"),
                    MainText = "How is data that should be disposed of identified?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("1a17f7bb-ecbc-44c4-a2a3-996b2e9c7c1b"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("48ae6067-c78e-4fbc-b76b-839a80832626"),
                    MainText = "Are data owners aware of the data/media disposal guidelines?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1a17f7bb-ecbc-44c4-a2a3-996b2e9c7c1b"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7e71f1cc-fb6d-45fe-a010-f3ee66befe29"),
                    MainText = "What training is provided to those tasked with data/media disposal?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("48ae6067-c78e-4fbc-b76b-839a80832626"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("9c2036c8-a582-4123-b6c5-e976b5da8929"),
                    MainText = "Is there a procedure for securely erasing data?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1a17f7bb-ecbc-44c4-a2a3-996b2e9c7c1b"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("69560830-822f-4be7-9635-5975e39a1911"),
                    MainText = "What is the procedure for securely erasing data on internal hard drives?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("9c2036c8-a582-4123-b6c5-e976b5da8929"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("90fc5091-5b5d-4458-933a-0bf9df074e42"),
                    MainText = "What is the procedure for securely erasing data on removable media?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("9c2036c8-a582-4123-b6c5-e976b5da8929"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a370116c-d4d8-4708-a45e-60d40245b4a2"),
                    MainText = "What is the procedure for securely erasing data on mobile devices?",                    
                    Order = 3,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("9c2036c8-a582-4123-b6c5-e976b5da8929"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("583fa97b-fc6c-4d40-ade9-77c9acfdd3dc"),
                    MainText = "Is there a post-disposal certification process?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1a17f7bb-ecbc-44c4-a2a3-996b2e9c7c1b"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f042a38c-5d9f-431d-93e1-48fe7bd69907"),
                    MainText = "What is the process for each media type?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("583fa97b-fc6c-4d40-ade9-77c9acfdd3dc"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("213efaea-df54-4b8c-8a7c-6f6210772476"),
                    MainText = "Are these guidelines reviewed?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1a17f7bb-ecbc-44c4-a2a3-996b2e9c7c1b"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a497a990-560c-4485-8f88-90b35f83d3e5"),
                    MainText = "How often?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("213efaea-df54-4b8c-8a7c-6f6210772476"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(dataRetentionQuestions);

            #endregion

            #region Data Loss Questions

            subcategoryId = Guid.Parse("952d593d-6f25-4418-bfe4-9f1dc59a4221");

            var dataLossQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("63acfbf7-2113-493a-a6cc-33e5235f1c40"),
                    MainText = "Does your organization have a Data Loss policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c8035816-a078-49a9-810d-d89ff7c736ca"),
                    MainText = "Does this policy include a breach notification procedure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("63acfbf7-2113-493a-a6cc-33e5235f1c40"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0343d8ad-eb8b-4a6a-881c-bc4fa5d21401"),
                    MainText = "Who is responsible for notifying the relevant parties?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("c8035816-a078-49a9-810d-d89ff7c736ca"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("17a34799-43db-48a8-9704-4e3094c42270"),
                    MainText = "Who are the notified parties?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("c8035816-a078-49a9-810d-d89ff7c736ca"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("87cb2af8-b87d-43dd-8b4b-c1945e61c508"),
                    MainText = "What is the timeframe for notification in days?",                    
                    Order = 3,
                    QuestionType = QuestionType.Integer,
                    ParentId = Guid.Parse("c8035816-a078-49a9-810d-d89ff7c736ca"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("3d475c2f-489e-4568-9373-6368db5e3da9"),
                    MainText = "Is your organization required to notify specific regulators?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("c8035816-a078-49a9-810d-d89ff7c736ca"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("bf96a7cd-3140-4baf-b461-c4065fc37484"),
                    MainText = "Which specific regulators is your organization required to notify?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("3d475c2f-489e-4568-9373-6368db5e3da9"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("9a8bbbdb-bf00-4fa4-a2e9-b05cd4dedc52"),
                    MainText = "Are data owners aware of this data loss policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("63acfbf7-2113-493a-a6cc-33e5235f1c40"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("bc0056e3-8373-4f42-ad42-0d4645041cd1"),
                    MainText = "What training is provided to data owners?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("9a8bbbdb-bf00-4fa4-a2e9-b05cd4dedc52"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e7f718ed-fd1b-4824-80c7-5db976618448"),
                    MainText = "How often is this policy reviewed?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("63acfbf7-2113-493a-a6cc-33e5235f1c40"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("cec22ca3-da1e-42ee-b005-40190217d39e"),
                    MainText = "Is there a procedure for identifying intentional or unintentional data loss?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("72a899d8-4956-428d-be00-865c3910e1a7"),
                    MainText = "Do you have any technologies in place to identify intentional or unintentional data loss?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("cec22ca3-da1e-42ee-b005-40190217d39e"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("3d0121fa-917b-4bef-906d-496a3886ab1e"),
                    MainText = "What kinds of technologies do you have in place?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("72a899d8-4956-428d-be00-865c3910e1a7"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d1472fd1-7783-4b1c-b790-2cd23baa80c8"),
                    MainText = "What is the coverage of these technologies?",
                    OptionChoices = "Critical systems,Critical & all production systems,Critical production & test systems",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("72a899d8-4956-428d-be00-865c3910e1a7"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("4eaf35e4-9153-4c80-9445-24ad997362ac"),
                    MainText = "Does your organization have cyber insurance?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },     
                new Question 
                {
                    Id = Guid.Parse("f06ef684-8e7f-4416-ac8e-47d7ca77a647"),
                    MainText = "What is this insurance policy’s coverage limit (in USD)?",                    
                    Order = 1,
                    QuestionType = QuestionType.Currency,
                    ParentId = Guid.Parse("4eaf35e4-9153-4c80-9445-24ad997362ac"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },  
                new Question 
                {
                    Id = Guid.Parse("c9538b7a-363b-4934-b616-dbc7ab519a64"),
                    MainText = "What types of First-Party Coverages are included in this insurance policy?",
                    OptionChoices = "Business interruption (i.e. loss of income and extra expenses),Computer systems replacement costs,Crisis services costs (e.g. notification; credit and monitoring services; breach coach etc),Cyber extortion (e.g. ransomware),Damage to your organization’s reputation, Loss or damage to electronic data, None, Other",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    ActiveOptionIndex = 7,
                    Order = 2,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("4eaf35e4-9153-4c80-9445-24ad997362ac"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },               
                new Question 
                {
                    Id = Guid.Parse("b0f5b5a4-7579-422a-aa2b-09ba3540b773"),
                    MainText = "Enter the other type of First-Party Coverage.",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId =Guid.Parse("c9538b7a-363b-4934-b616-dbc7ab519a64"),
                    SubcategoryId = Guid.Parse("fa4e445e-c6dc-4c8c-b386-09ac54cbe0e2"),
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d38b5573-8b9a-4621-8460-6ba82b900bdd"),
                    MainText = "What types of Third-Party Coverages are included in this insurance policy?",
                    OptionChoices = "Electronic media liability,Network security liability,Privacy liability,Regulatory proceedings and penalties, None, Other",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    ActiveOptionIndex = 5,
                    Order = 3,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("4eaf35e4-9153-4c80-9445-24ad997362ac"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },    
                new Question 
                {
                    Id = Guid.Parse("e9cf2cc8-d22b-4c01-9692-28ec67407738"),
                    MainText = "Enter the other type of Third-Party Coverage.",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("d38b5573-8b9a-4621-8460-6ba82b900bdd"),
                    SubcategoryId = Guid.Parse("fa4e445e-c6dc-4c8c-b386-09ac54cbe0e2"),
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },                    
                new Question 
                {
                    Id = Guid.Parse("be5be664-81e1-41fb-af80-80bb59ef2b14"),
                    MainText = "Does this insurance policy have sub-limits?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("4eaf35e4-9153-4c80-9445-24ad997362ac"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("9fd1cd1a-b0ab-4e49-ac7e-ef005d7ce425"),
                    MainText = "Which types of coverage have sub-limits?",
                    OptionChoices = "Business interruption (i.e. loss of income and extra expenses),Crisis services costs (e.g. notification; credit and monitoring services; breach coach etc),Cyber extortion (e.g. ransomware),Computer systems replacement costs,Damage to your organization’s reputation,Electronic media liability,Loss or damage to electronic data,Network security liability,Privacy liability,Regulatory proceedings and penalties, Other",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    ActiveOptionIndex = 10,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("be5be664-81e1-41fb-af80-80bb59ef2b14"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("6c658c6c-773b-4894-8038-0fbbf1f51e3b"),
                    MainText = "Enter the other type of coverage that has sub-limits.",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("9fd1cd1a-b0ab-4e49-ac7e-ef005d7ce425"),
                    SubcategoryId = Guid.Parse("fa4e445e-c6dc-4c8c-b386-09ac54cbe0e2"),
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(dataLossQuestions);

            #endregion

            #region Data Recovery

            subcategoryId = Guid.Parse("fa4e445e-c6dc-4c8c-b386-09ac54cbe0e2");

            var dataRecoveryQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("bca75158-a8d6-4ed5-8438-71d491548683"),
                    MainText = "Does your organization have a Data Recovery policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e4bac838-2495-4112-9fcd-a7c3970a6c68"),
                    MainText = "Does the policy include a Disaster Recovery Plan (DRP)?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("bca75158-a8d6-4ed5-8438-71d491548683"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("6773842e-f96c-4f41-b84a-ce4c59f5ab16"),
                    MainText = "What data types are covered by the data recovery procedure in the DRP?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("e4bac838-2495-4112-9fcd-a7c3970a6c68"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7baddcdb-e88d-4aac-a1a3-cd5e7bd9abda"),
                    MainText = "Are data owners aware of the data recovery procedure in the DRP?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("e4bac838-2495-4112-9fcd-a7c3970a6c68"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("bcb793d0-cdb8-493d-a18b-0f087e2dfa32"),
                    MainText = "What training is provided to those tasked with data recovery?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("7baddcdb-e88d-4aac-a1a3-cd5e7bd9abda"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("922a2f1b-dbc3-4518-b02c-1ed8d4bb1dd6"),
                    MainText = "How often is this policy and DRP reviewed?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("bca75158-a8d6-4ed5-8438-71d491548683"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("11095913-dcff-46c1-b36f-d857de3a4fb9"),
                    MainText = "Are data backups performed on a regular basis?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("da660077-26e9-4408-8951-f55d8fc214fd"),
                    MainText = "What is the backup schedule?",
                    OptionChoices = "Monthly,Weekly,Daily",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("11095913-dcff-46c1-b36f-d857de3a4fb9"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("68fd65bd-44ac-4343-a9e8-4647ceec597a"),
                    MainText = "What is the backup type?",
                    OptionChoices = "Incremental,Full",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("11095913-dcff-46c1-b36f-d857de3a4fb9"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("96ab38d7-4c66-4432-8a94-9bc9ea6a5a7a"),
                    MainText = "Are backups of sensitive data encrypted?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("11095913-dcff-46c1-b36f-d857de3a4fb9"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("3cc74232-7f41-4f0c-b471-dfb4c4d6b27a"),
                    MainText = "How is the sensitivity of the data determined?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("96ab38d7-4c66-4432-8a94-9bc9ea6a5a7a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("66944343-069a-4958-b2bc-958b67569897"),
                    MainText = "How is this sensitive data encrypted?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("96ab38d7-4c66-4432-8a94-9bc9ea6a5a7a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("bec2c850-66bc-441a-9e0f-59e740e26b0b"),
                    MainText = "Where are data backups stored?",
                    OptionChoices = "Local backup,Remote site,Local & remote",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("11095913-dcff-46c1-b36f-d857de3a4fb9"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e152cadb-4492-4697-9223-8f57dd4c5127"),
                    MainText = "Has your organization identified any critical systems requiring redundancy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7d84270e-021a-45b2-9fe5-0fa2f275af1f"),
                    MainText = "What critical systems currently have redundant systems?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("e152cadb-4492-4697-9223-8f57dd4c5127"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7ffe24dd-3a56-4968-9bf3-ea41bce9a297"),
                    MainText = "How were those systems identified?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("e152cadb-4492-4697-9223-8f57dd4c5127"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("84aa629b-c9b7-4e32-8343-2dad5726a2e7"),
                    MainText = "Have you implemented real-time failover for the redundant systems?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("e152cadb-4492-4697-9223-8f57dd4c5127"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("995364e2-e82a-471c-a173-61be59d018a3"),
                    MainText = "Does your organization test data backups to ensure you can recover data properly?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f5a35202-980f-422c-beac-fa6c7c8901dd"),
                    MainText = "What were the results of the last test?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("995364e2-e82a-471c-a173-61be59d018a3"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0bac265b-b916-445b-8dda-93b95be45509"),
                    MainText = "How often are these tests conducted?",
                    OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,          
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("995364e2-e82a-471c-a173-61be59d018a3"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(dataRecoveryQuestions);

            #endregion

            return results.ToArray();
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.