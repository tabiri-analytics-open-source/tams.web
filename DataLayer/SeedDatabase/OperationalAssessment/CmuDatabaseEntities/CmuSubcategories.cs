using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.CmuDatabaseEntities
{
    public static class CmuSubcategories
    {
        public static Subcategory[] Seed(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategories = new List<Subcategory>();

            subcategories.AddRange(SecurityGovernance(createdBy, currentTimestamp));
            subcategories.AddRange(SecurityRiskManagement(createdBy, currentTimestamp));
            subcategories.AddRange(DataProtection(createdBy, currentTimestamp));
            subcategories.AddRange(AccessManagement(createdBy, currentTimestamp));
            subcategories.AddRange(SecurityArchitecture(createdBy, currentTimestamp));
            subcategories.AddRange(IncidentResponse(createdBy, currentTimestamp));

            return subcategories.ToArray();
        }
    
        private static List<Subcategory> SecurityGovernance(Guid createdBy, DateTime currentTimestamp)
        {
            var categoryId = Guid.Parse("a1206ba9-5e9b-44b1-97aa-5c10864c1def");

            return new List<Subcategory>
            {
                new Subcategory
                {
                    Id = Guid.Parse("dcec4917-2291-43cb-8d2a-9e0945656b1c"),
                    Title = "Strategic Planning",
                    Objective = "Determine the breadth and depth of the security department’s goals and their role within the organization.",                  
                    Order = 1,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("96e4580d-05cb-4b7d-97b4-cba0a477bb1c"),
                    Title = "Security Policy Framework",
                    Objective = "Establish the existence of formal guidelines for security practices within the organization, and their alignment with industry standards.",          
                    Order = 2,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("b9912cf5-6e62-43ae-a0cd-36d7fbb52944"),
                    Title = "Organizational Structure",
                    Objective = "Determine the hierarchical and reporting structure of the IT Security department and its position within the organization.",          
                    Order = 3,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("cd09235e-4fd1-4e8c-b751-f2bf6b449b27"),
                    Title = "Performance Metrics",
                    Objective = "Evaluate how well the security department documents and communicates its performance over time against set objectives.",           
                    Order = 4,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("a34bdc05-1dc9-4f6e-a3ef-a96d5ff90ed3"),
                    Title = "Workforce Management",
                    Objective = "Determine the organization’s commitment and/or ability to obtain and maintain skilled security personnel.",         
                    Order = 5,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
            };
        }

        private static List<Subcategory> SecurityRiskManagement(Guid createdBy, DateTime currentTimestamp)
        {
            var categoryId = Guid.Parse("ff52619c-ef19-4596-999c-24e4f87fda1b");

            return new List<Subcategory>
            {
                new Subcategory
                {
                    Id = Guid.Parse("9452f450-6b56-437a-a6c0-9f7b03bfde8c"),
                    Title = "Risk Management Framework",
                    Objective = "Evaluate the involvement of the security department in creating processes for identifying, communicating and mitigating risk within the organization. Evaluate the process of reviewing compliance of the organization with information security regulations.",         
                    Order = 1,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("bcbce862-3628-4f03-bd6b-4da0a08ce9c1"),
                    Title = "Threat Management",
                    Objective = "Evaluate the process used to identify, remediate and track security threats and determine how these threats are translated into business risk.",         
                    Order = 2,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("169c5d65-75fb-41b9-b607-fb43343ba884"),
                    Title = "Security Awareness",
                    Objective = "Evaluate the effectiveness and scope of formal security training within the organization",  
                    Order = 3,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }
    
        private static List<Subcategory> DataProtection(Guid createdBy, DateTime currentTimestamp)
        {
            var categoryId = Guid.Parse("c210c295-6e06-4805-9f98-4dcd358da07a");

            return new List<Subcategory>
            {
                new Subcategory
                {
                    Id = Guid.Parse("15d6d4cb-e910-42c5-bb47-b4d03103edb0"),
                    Title = "Data Protection Framework",
                    Objective = "Establish the existence of a formal data protection framework and determine its scope in contrast to industry standards.",     
                    Order = 1,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("7a697f1e-464c-4342-b369-33955d46663d"),
                    Title = "Data Classification",
                    Objective = "Evaluate an organization’s ability to properly classify and handle data based on its sensitivity.",      
                    Order = 2,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("32b2f015-8e14-4cfa-936a-6c58e18442cc"),
                    Title = "Data Protection Policies",
                    Objective = "Evaluate the processes involved in protecting an organization's data, both at rest and in motion.",   
                    Order = 3,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("82083045-11e8-4f48-a91c-eb615c34bffa"),
                    Title = "Data Retention",
                    Objective = "Evaluate the criteria an organization follows when retaining or disposing data, and the process followed.",         
                    Order = 4,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("952d593d-6f25-4418-bfe4-9f1dc59a4221"),
                    Title = "Data Loss",
                    Objective = "Evaluate an organization's ability to mitigate effects of a data loss event.",         
                    Order = 5,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("fa4e445e-c6dc-4c8c-b386-09ac54cbe0e2"),
                    Title = "Data Recovery",
                    Objective = "Evaluate an organization's preparedness and ability to recover from a data loss event.",          
                    Order = 6,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },  
            };
        }
    
        private static List<Subcategory> AccessManagement(Guid createdBy, DateTime currentTimestamp)
        {
            var categoryId = Guid.Parse("5d5a0457-98bb-4b11-90f5-698dc8bc2495");

            return new List<Subcategory>
            {
                new Subcategory
                {
                    Id = Guid.Parse("1f0fd461-a017-421b-a6fc-9e5ffebd91ae"),
                    Title = "Identity Management",
                    Objective = "Evaluate the process of acquiring identity information within an organization and managing it to ensure appropriateness.",      
                    Order = 1,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("1a8e9b19-ec14-49fb-a2ce-ac0b89a92ed5"),
                    Title = "Access Controls",
                    Objective = "Evaluate the techniques adopted by an organization to ensure appropriate access to systems.",        
                    Order = 2,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("4e802dfe-3143-4a5a-b8ab-ecdcc9106bd8"),
                    Title = "Separation of Duties",
                    Objective = "Determine the existence of access controls based on roles established within the organization.",  
                    Order = 3,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("c062770f-2cc3-4f56-b87a-5d51fcabc9b7"),
                    Title = "Privileged Access Management",
                    Objective = "Evaluate the process and criteria used when assigning privileged access to account.",    
                    Order = 4,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("61543e20-477b-4c07-a57a-cc856cbbf562"),
                    Title = "Remote Access",
                    Objective = "Evaluate the process of granting secure remote access to an organization’s staff.",  
                    Order = 5,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("c71b6378-ca1c-4f87-94e5-bb79867ed456"),
                    Title = "Third Party Access",
                    Objective = "Evaluate the process granting external entities secure access to internal resources.",       
                    Order = 6,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
            };
        }
    
        private static List<Subcategory> SecurityArchitecture(Guid createdBy, DateTime currentTimestamp)
        {
            var categoryId = Guid.Parse("d2b7180a-f9ec-4366-9bc8-aac6b1d6fb02");

            return new List<Subcategory>
            {
                new Subcategory
                {
                    Id = Guid.Parse("1e676fbe-3330-43ab-9da4-b3ccb42f5068"),
                    Title = "Network Protection",
                    Objective = "Evaluate an organization’s ability to monitor and control activities over the network.",        
                    Order = 1,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("ffcdde59-8844-433a-92ea-714bf1fc8abc"),
                    Title = "Endpoint Protection",
                    Objective = "Evaluate an organization’s ability to monitor and control activities at endpoint devices.",      
                    Order = 2,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("44a2cc87-75ac-49a9-9275-1b470f6509b3"),
                    Title = "Application Protection",
                    Objective = "Evaluate the organization’s use of tools and procedures for secure application development, deployment and management.",      
                    Order = 3,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<Subcategory> IncidentResponse(Guid createdBy, DateTime currentTimestamp)
        {
            var categoryId = Guid.Parse("34d6dfef-688e-4a98-a47e-1affaf844728");

            return new List<Subcategory>
            {
                new Subcategory
                {
                    Id = Guid.Parse("66ea6203-8e60-4fd6-8f45-4768fe375a5b"),
                    Title = "Incident Readiness",
                    Objective = "Evaluate an organization’s readiness to respond to cyber security incidents.",          
                    Order = 1,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("a1e57c2b-6331-4023-9d84-aafe8233783f"),
                    Title = "Incident Detection",
                    Objective = "Evaluate an organization’s ability to identify cyber security incidents.",         
                    Order = 2,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("a85131ca-5999-47e6-ab90-22db0c1d1236"),
                    Title = "Incident Remediation",
                    Objective = "Evaluate an organization’s ability to recover from cyber security incidents.",        
                    Order = 3,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.