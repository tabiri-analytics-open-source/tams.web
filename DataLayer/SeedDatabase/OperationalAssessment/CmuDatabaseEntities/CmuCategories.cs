using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.CmuDatabaseEntities
{
    public static class CmuCategories
    {
        public static Category[] Seed(Guid createdBy, DateTime currentTimestamp)
        {
            var questionnaireId = Guid.Parse("ca6bfb11-6c10-4351-8a96-a5809f5e5dae");

            return new List<Category>
            {
                new Category
                {
                    Id = Guid.Parse("a1206ba9-5e9b-44b1-97aa-5c10864c1def"),
                    Title = "Security Governance",
                    Objective = "Evaluate the alignment of the organization’s current information security program with business objectives.",
                    ExecutiveSummary = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut est augue, ullamcorper in pulvinar non, volutpat porta lectus.    Etiam sed bibendum eros. Aenean non libero eros. Vivamus pulvinar lacinia tortor.",
                    Order = 1,
                    QuestionnaireId = questionnaireId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Category
                {
                    Id = Guid.Parse("ff52619c-ef19-4596-999c-24e4f87fda1b"),
                    Title = "Security Risk Management",
                    Objective = "valuates the organization’s risk management framework and processes.",
                    ExecutiveSummary = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut est augue, ullamcorper in pulvinar non, volutpat porta lectus. Etiam sed bibendum eros. Aenean non libero eros. Vivamus pulvinar lacinia tortor.",
                    Order = 2,
                    QuestionnaireId = questionnaireId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Category
                {
                    Id = Guid.Parse("c210c295-6e06-4805-9f98-4dcd358da07a"),
                    Title = "Data Protection",
                    Objective = "Evaluates the organizations data protection framework and underlying data protection capabilities.",
                    ExecutiveSummary = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut est augue, ullamcorper in pulvinar non, volutpat porta lectus. Etiam sed bibendum eros. Aenean non libero eros. Vivamus pulvinar lacinia tortor.",
                    Order = 3,
                    QuestionnaireId = questionnaireId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Category
                {
                    Id = Guid.Parse("5d5a0457-98bb-4b11-90f5-698dc8bc2495"),
                    Title = "Access Management",
                    Objective = "Evaluates the organization’s access management policies and procedures to determine if they reduce the risk of inappropriate access to sensitive data.",
                    ExecutiveSummary = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut est augue, ullamcorper in pulvinar non, volutpat porta lectus. Etiam sed bibendum eros. Aenean non libero eros. Vivamus pulvinar lacinia tortor.",
                    Order = 4,
                    QuestionnaireId = questionnaireId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Category
                {
                    Id = Guid.Parse("d2b7180a-f9ec-4366-9bc8-aac6b1d6fb02"),
                    Title = "Security Architecture",
                    Objective = "Evaluates the organization’s use of various tools/technologies to determine their effectiveness in providing visibility into network, host and application-based activities.",
                    ExecutiveSummary = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut est augue, ullamcorper in pulvinar non, volutpat porta lectus. Etiam sed bibendum eros. Aenean non libero eros. Vivamus pulvinar lacinia tortor.",
                    Order = 5,
                    QuestionnaireId = questionnaireId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Category
                {
                    Id = Guid.Parse("34d6dfef-688e-4a98-a47e-1affaf844728"),
                    Title = "Incident Response",
                    Objective = "Evaluates the organization’s existing processes and technologies that are deployed to detect, analyze and contain cyber attacks.",
                    ExecutiveSummary = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut est augue, ullamcorper in pulvinar non, volutpat porta lectus. Etiam sed bibendum eros. Aenean non libero eros. Vivamus pulvinar lacinia tortor.",
                    Order = 6,
                    QuestionnaireId = questionnaireId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }.ToArray();
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.