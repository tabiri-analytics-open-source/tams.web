using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.DataLayer.SeedDatabase
{
    public static class OrganizationQuestionnaires
    {
        public static OrganizationQuestionnaire[] Seed(Guid createdBy, DateTime currentTimestamp)
        {
            var cmuQuestionnaireId = Guid.Parse("ca6bfb11-6c10-4351-8a96-a5809f5e5dae");
            var tabiriQuestionnaireId = Guid.Parse("17c8eb52-f859-4230-919a-ace015ed55b2");

            return new List<OrganizationQuestionnaire>
            {              
                new OrganizationQuestionnaire
                { 
                    OrgId = Guid.Parse("759344af-0e24-4673-852f-9b3e92742314"), 
                    QuestionnaireId = tabiriQuestionnaireId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new OrganizationQuestionnaire
                { 
                    OrgId = Guid.Parse("d482e97e-83bd-4ae3-bcfc-09bd16289b98"), 
                    QuestionnaireId = cmuQuestionnaireId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp 
                },
                new OrganizationQuestionnaire 
                { 
                    OrgId = Guid.Parse("d81018d9-e441-4f98-b5c5-38ee1434fb46"),
                    QuestionnaireId = cmuQuestionnaireId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp 
                },
                new OrganizationQuestionnaire 
                { 
                    OrgId = Guid.Parse("2e9a9635-16ba-41bd-aa87-3c9dc212e008"), 
                    QuestionnaireId = cmuQuestionnaireId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp 
                }
            }.ToArray();
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.