using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.TabiriDatabaseEntities
{
    public static class TabiriCategories
    {
        public static Category[] Seed(Guid createdBy, DateTime currentTimestamp)
        {
            var questionnaireId = Guid.Parse("17c8eb52-f859-4230-919a-ace015ed55b2");

            return new List<Category>
            {
                new Category
                {
                    Id = Guid.Parse("5d393c0a-e5cd-49cb-aa8d-4a5171d3fce4"),
                    Title = "Security Governance",
                    Objective = "Evaluate the alignment of the organization’s current information security program with business objectives.",
                    ExecutiveSummary = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut est augue, ullamcorper in pulvinar non, volutpat porta lectus.    Etiam sed bibendum eros. Aenean non libero eros. Vivamus pulvinar lacinia tortor.",
                    Order = 1,
                    QuestionnaireId = questionnaireId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Category
                {
                    Id = Guid.Parse("39ac5390-03fe-4f4a-b7af-235c181b8068"),
                    Title = "Security Risk Management",
                    Objective = "valuates the organization’s risk management framework and processes.",
                    ExecutiveSummary = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut est augue, ullamcorper in pulvinar non, volutpat porta lectus. Etiam sed bibendum eros. Aenean non libero eros. Vivamus pulvinar lacinia tortor.",
                    Order = 2,
                    QuestionnaireId = questionnaireId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Category
                {
                    Id = Guid.Parse("d127465d-23e6-4390-a625-1c3780b1abfc"),
                    Title = "Data Protection",
                    Objective = "Evaluates the organizations data protection framework and underlying data protection capabilities.",
                    ExecutiveSummary = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut est augue, ullamcorper in pulvinar non, volutpat porta lectus. Etiam sed bibendum eros. Aenean non libero eros. Vivamus pulvinar lacinia tortor.",
                    Order = 3,
                    QuestionnaireId = questionnaireId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Category
                {
                    Id = Guid.Parse("ad44985c-d73f-4d85-aa57-0e3f6605c49e"),
                    Title = "Access Management",
                    Objective = "Evaluates the organization’s access management policies and procedures to determine if they reduce the risk of inappropriate access to sensitive data.",
                    ExecutiveSummary = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut est augue, ullamcorper in pulvinar non, volutpat porta lectus. Etiam sed bibendum eros. Aenean non libero eros. Vivamus pulvinar lacinia tortor.",
                    Order = 4,
                    QuestionnaireId = questionnaireId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Category
                {
                    Id = Guid.Parse("36ad31b6-0018-4126-8c48-12f5550db97d"),
                    Title = "Security Architecture",
                    Objective = "Evaluates the organization’s use of various tools/technologies to determine their effectiveness in providing visibility into network, host and application-based activities.",
                    ExecutiveSummary = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut est augue, ullamcorper in pulvinar non, volutpat porta lectus. Etiam sed bibendum eros. Aenean non libero eros. Vivamus pulvinar lacinia tortor.",
                    Order = 5,
                    QuestionnaireId = questionnaireId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Category
                {
                    Id = Guid.Parse("bc02a4a4-aaec-436a-bc41-53dde368a6d3"),
                    Title = "Incident Response",
                    Objective = "Evaluates the organization’s existing processes and technologies that are deployed to detect, analyze and contain cyber attacks.",
                    ExecutiveSummary = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut est augue, ullamcorper in pulvinar non, volutpat porta lectus. Etiam sed bibendum eros. Aenean non libero eros. Vivamus pulvinar lacinia tortor.",
                    Order = 6,
                    QuestionnaireId = questionnaireId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }.ToArray();
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.