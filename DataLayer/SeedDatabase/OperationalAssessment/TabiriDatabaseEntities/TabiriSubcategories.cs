using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.TabiriDatabaseEntities
{
    public static class TabiriSubcategories
    {
        public static Subcategory[] Seed(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategories = new List<Subcategory>();

            subcategories.AddRange(SecurityGovernance(createdBy, currentTimestamp));
            subcategories.AddRange(SecurityRiskManagement(createdBy, currentTimestamp));
            subcategories.AddRange(DataProtection(createdBy, currentTimestamp));
            subcategories.AddRange(AccessManagement(createdBy, currentTimestamp));
            subcategories.AddRange(SecurityArchitecture(createdBy, currentTimestamp));
            subcategories.AddRange(IncidentResponse(createdBy, currentTimestamp));

            return subcategories.ToArray();
        }
    
        private static List<Subcategory> SecurityGovernance(Guid createdBy, DateTime currentTimestamp)
        {
            var categoryId = Guid.Parse("5d393c0a-e5cd-49cb-aa8d-4a5171d3fce4");

            return new List<Subcategory>
            {
                new Subcategory
                {
                    Id = Guid.Parse("5e0fdb74-9606-4776-ab23-24bf5898160d"),
                    Title = "Strategic Planning",
                    Objective = "Determine the breadth and depth of the security department’s goals and their role within the organization.",                  
                    Order = 1,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("3df232a9-b172-4cd0-9d91-d3d025e5f36f"),
                    Title = "Security Policy Framework",
                    Objective = "Establish the existence of formal guidelines for security practices within the organization, and their alignment with industry standards.",          
                    Order = 2,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("14d74f5a-aae4-4958-aaf5-c10af094515f"),
                    Title = "Organizational Structure",
                    Objective = "Determine the hierarchical and reporting structure of the IT Security department and its position within the organization.",          
                    Order = 3,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("dcbecea7-7a7e-4378-bd4c-acc6ac24afc1"),
                    Title = "Performance Metrics",
                    Objective = "Evaluate how well the security department documents and communicates its performance over time against set objectives.",           
                    Order = 4,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("7969918f-98f9-4436-8a07-ae7a9cccf9b5"),
                    Title = "Workforce Management",
                    Objective = "Determine the organization’s commitment and/or ability to obtain and maintain skilled security personnel.",         
                    Order = 5,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
            };
        }

        private static List<Subcategory> SecurityRiskManagement(Guid createdBy, DateTime currentTimestamp)
        {
            var categoryId = Guid.Parse("39ac5390-03fe-4f4a-b7af-235c181b8068");

            return new List<Subcategory>
            {
                new Subcategory
                {
                    Id = Guid.Parse("38a12386-d26f-4921-9e4a-08abe7d8e4cb"),
                    Title = "Risk Management Framework",
                    Objective = "Evaluate the involvement of the security department in creating processes for identifying, communicating and mitigating risk within the organization. Evaluate the process of reviewing compliance of the organization with information security regulations.",         
                    Order = 1,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("7c0fa42c-e5e3-4d16-9599-963f9041ed9f"),
                    Title = "Threat Management",
                    Objective = "Evaluate the process used to identify, remediate and track security threats and determine how these threats are translated into business risk.",         
                    Order = 2,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("b9ec8ca6-7e6e-4fe6-8e8d-7b768eba2648"),
                    Title = "Security Awareness",
                    Objective = "Evaluate the effectiveness and scope of formal security training within the organization",  
                    Order = 3,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }
    
        private static List<Subcategory> DataProtection(Guid createdBy, DateTime currentTimestamp)
        {
            var categoryId = Guid.Parse("d127465d-23e6-4390-a625-1c3780b1abfc");

            return new List<Subcategory>
            {
                new Subcategory
                {
                    Id = Guid.Parse("ec0ed13d-e887-41e2-a0cd-62fd1ca2de25"),
                    Title = "Data Protection Framework",
                    Objective = "Establish the existence of a formal data protection framework and determine its scope in contrast to industry standards.",     
                    Order = 1,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("8859dd17-94e3-433d-90d8-2fcf3cffaac9"),
                    Title = "Data Classification",
                    Objective = "Evaluate an organization’s ability to properly classify and handle data based on its sensitivity.",      
                    Order = 2,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("ed613e1b-6a1a-4d6b-8efc-216f9520226a"),
                    Title = "Data Protection Policies",
                    Objective = "Evaluate the processes involved in protecting an organization's data, both at rest and in motion.",   
                    Order = 3,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("b679ff1a-2121-45de-a695-9517c3b5319e"),
                    Title = "Data Retention",
                    Objective = "Evaluate the criteria an organization follows when retaining or disposing data, and the process followed.",         
                    Order = 4,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("95bfad59-1224-414a-8c51-d688abac70ed"),
                    Title = "Data Loss",
                    Objective = "Evaluate an organization's ability to mitigate effects of a data loss event.",         
                    Order = 5,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("673d41d1-e017-4b8a-ad8e-a0992882f385"),
                    Title = "Data Recovery",
                    Objective = "Evaluate an organization's preparedness and ability to recover from a data loss event.",          
                    Order = 6,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },  
            };
        }
    
        private static List<Subcategory> AccessManagement(Guid createdBy, DateTime currentTimestamp)
        {
            var categoryId = Guid.Parse("ad44985c-d73f-4d85-aa57-0e3f6605c49e");

            return new List<Subcategory>
            {
                new Subcategory
                {
                    Id = Guid.Parse("a4c329d6-193e-441f-b0cb-97b9770c7265"),
                    Title = "Identity Management",
                    Objective = "Evaluate the process of acquiring identity information within an organization and managing it to ensure appropriateness.",      
                    Order = 1,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("eb909bf7-654c-4314-b1c8-6356169a2c6f"),
                    Title = "Access Controls",
                    Objective = "Evaluate the techniques adopted by an organization to ensure appropriate access to systems.",        
                    Order = 2,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("5e1e6a79-046c-4fbd-89c0-3b41fc69e555"),
                    Title = "Separation of Duties",
                    Objective = "Determine the existence of access controls based on roles established within the organization.",  
                    Order = 3,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("4b17e07a-b608-4406-bdeb-32e2653a26cb"),
                    Title = "Privileged Access Management",
                    Objective = "Evaluate the process and criteria used when assigning privileged access to account.",    
                    Order = 4,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("9734a725-6f43-4a49-8b7c-fd008db3562d"),
                    Title = "Remote Access",
                    Objective = "Evaluate the process of granting secure remote access to an organization’s staff.",  
                    Order = 5,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("754ed961-0edd-45c5-87f6-2ff75e409fdd"),
                    Title = "Third Party Access",
                    Objective = "Evaluate the process granting external entities secure access to internal resources.",       
                    Order = 6,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
            };
        }
    
        private static List<Subcategory> SecurityArchitecture(Guid createdBy, DateTime currentTimestamp)
        {
            var categoryId = Guid.Parse("36ad31b6-0018-4126-8c48-12f5550db97d");

            return new List<Subcategory>
            {
                new Subcategory
                {
                    Id = Guid.Parse("7ebe6f6c-001f-4022-bb69-6ebd6111338b"),
                    Title = "Network Protection",
                    Objective = "Evaluate an organization’s ability to monitor and control activities over the network.",        
                    Order = 1,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("72e5fa4d-5c0f-4d37-943c-d972393efdcb"),
                    Title = "Endpoint Protection",
                    Objective = "Evaluate an organization’s ability to monitor and control activities at endpoint devices.",      
                    Order = 2,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("0cf39325-3c91-453f-a254-a482d86575e6"),
                    Title = "Application Protection",
                    Objective = "Evaluate the organization’s use of tools and procedures for secure application development, deployment and management.",      
                    Order = 3,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<Subcategory> IncidentResponse(Guid createdBy, DateTime currentTimestamp)
        {
            var categoryId = Guid.Parse("bc02a4a4-aaec-436a-bc41-53dde368a6d3");

            return new List<Subcategory>
            {
                new Subcategory
                {
                    Id = Guid.Parse("02a82cba-a900-4ca0-aa4e-810065e4c33c"),
                    Title = "Incident Readiness",
                    Objective = "Evaluate an organization’s readiness to respond to cyber security incidents.",          
                    Order = 1,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("3b500510-0aa2-44bc-9600-f49ad7208113"),
                    Title = "Incident Detection",
                    Objective = "Evaluate an organization’s ability to identify cyber security incidents.",         
                    Order = 2,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Subcategory
                {
                    Id = Guid.Parse("6b4a7bed-c626-4369-87ab-1245d5560a38"),
                    Title = "Incident Remediation",
                    Objective = "Evaluate an organization’s ability to recover from cyber security incidents.",        
                    Order = 3,
                    CategoryId = categoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.