using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.TabiriDatabaseEntities.TabiriCybersecurityStandards
{
    public static class TabiriIsoIecCybersecurityStandards
    {
        public static CybersecurityStandard[] Seed(Guid createdBy, DateTime currentTimestamp)
        {
            var standards = new List<CybersecurityStandard>();   

            standards.AddRange(StrategicPlanningStandards(createdBy, currentTimestamp));
            standards.AddRange(SecurityPolicyFrameworkStandards(createdBy, currentTimestamp));
            standards.AddRange(OrganizationalStructureStandards(createdBy, currentTimestamp));
            standards.AddRange(PerformanceMetricsStandards(createdBy, currentTimestamp));
            standards.AddRange(WorkforceManagementStandards(createdBy, currentTimestamp));
            standards.AddRange(RiskManagementFrameworkStandards(createdBy, currentTimestamp));
            standards.AddRange(ThreatManagementStandards(createdBy, currentTimestamp));
            standards.AddRange(SecurityAwarenessStandards(createdBy, currentTimestamp));
            standards.AddRange(DataProtectionFrameworkStandards(createdBy, currentTimestamp));
            standards.AddRange(DataClassificationStandards(createdBy, currentTimestamp));
            standards.AddRange(DataProtectionPoliciesStandards(createdBy, currentTimestamp));
            standards.AddRange(DataRetentionStandards(createdBy, currentTimestamp));
            standards.AddRange(DataLossStandards(createdBy, currentTimestamp));
            standards.AddRange(DataRecoveryStandards(createdBy, currentTimestamp));
            standards.AddRange(IdentityManagementStandards(createdBy, currentTimestamp));
            standards.AddRange(AccessControlsStandards(createdBy, currentTimestamp));
            standards.AddRange(SeparationOfDutiesStandards(createdBy, currentTimestamp));
            standards.AddRange(PrivilegedAccessManagementStandards(createdBy, currentTimestamp));
            standards.AddRange(RemoteAccessStandards(createdBy, currentTimestamp));
            standards.AddRange(ThirdPartyAccessStandards(createdBy, currentTimestamp));    
            standards.AddRange(NetworkProtectionStandards(createdBy, currentTimestamp));    
            standards.AddRange(EndpointProtectionStandards(createdBy, currentTimestamp));   
            standards.AddRange(ApplicationProtectionStandards(createdBy, currentTimestamp));   
            standards.AddRange(IncidentReadinessStandards(createdBy, currentTimestamp));   
            standards.AddRange(IncidentDetectionStandards(createdBy, currentTimestamp)); 
            standards.AddRange(IncidentRemediationStandards(createdBy, currentTimestamp));              

            return standards.ToArray();
        }

        private static List<CybersecurityStandard> StrategicPlanningStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("5e0fdb74-9606-4776-ab23-24bf5898160d");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("7b23d8a0-b6f2-4ce7-9987-12e0b93664c6"),
                    Title = "A.5.1 Management direction for information security ",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("cbe0f994-e61a-44a3-9aef-5384f3b58039"),
                    Title = "A.6.1 Internal organization",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };               
        }

        private static List<CybersecurityStandard> SecurityPolicyFrameworkStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("3df232a9-b172-4cd0-9d91-d3d025e5f36f");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("30786ea2-043c-4c10-882f-a01508d03da7"),
                    Title = "A.5.1.1 Policies for information security",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("f72dd9e5-41cc-498e-b884-d3622519a74d"),
                    Title = "A.5.1.2 Review of the policies",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("e36747be-ac46-41ca-befe-32c11a5edbd9"),
                    Title = "A.6.1.1 Information security roles and responsibilities",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("1d7cc9f1-9466-4b11-80e1-06e5f36386a7"),
                    Title = "A.7.2.1 Management responsibilities",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("c5fdf104-e1f0-4c0e-9bdf-6d565069b498"),
                    Title = "A.12.1.1 Documented operating procedures",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("6aa344f3-7c7d-456c-983c-606534203c6f"),
                    Title = "A.18.1 Compliance with legal and contractual requirements",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }; 
        }

        private static List<CybersecurityStandard> OrganizationalStructureStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("14d74f5a-aae4-4958-aaf5-c10af094515f");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("be6705dc-f67d-4d51-b826-025be9196692"),
                    Title = "A.6.1.1 Information security roles and responsibilities",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("92328e13-93a5-4021-890a-222b68cf7464"),
                    Title = "A.6.1.2 Segregation of duties",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };    
        }

        private static List<CybersecurityStandard> PerformanceMetricsStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("dcbecea7-7a7e-4378-bd4c-acc6ac24afc1");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("b4d060f3-2fed-4831-afba-9fee0229f2a3"),
                    Title = "A.5.1 Management direction for information security",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("5026c5e5-dd72-4935-8bca-d238043b5040"),
                    Title = "A.18.2 Information security reviews",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> WorkforceManagementStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("7969918f-98f9-4436-8a07-ae7a9cccf9b5");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("06b79ef3-f15a-45c1-983b-a6ad2256fa14"),
                    Title = "A.7.2 During employment",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> RiskManagementFrameworkStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("38a12386-d26f-4921-9e4a-08abe7d8e4cb");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("232e81dc-4812-4770-8c0e-7c0d6da75365"),
                    Title = "6.1.2 Information security risk assessment",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("fb36ec34-ea3f-474c-89d0-5c0a4b15ee20"),
                    Title = "6.1.3 Information security risk treatment",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("f5917665-8fc9-4437-8531-08df7aebecc9"),
                    Title = "A.18.2 Information security reviews",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> ThreatManagementStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("7c0fa42c-e5e3-4d16-9599-963f9041ed9f");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("b6e08ddb-1faa-4216-ba38-4530d3e89d49"),
                    Title = "8.2 Information security risk assessment",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("5e929743-d12b-44a5-baf9-acbc69a2211e"),
                    Title = "8.3 Information security risk treatment",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("adffbd72-9475-4a37-8c6d-0ea854bbd77e"),
                    Title = "9.2 Internal audit",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("8a18228d-b78d-493c-8253-a2b398bc928f"),
                    Title = "A.12.6 Technical vulnerability management",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> SecurityAwarenessStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("b9ec8ca6-7e6e-4fe6-8e8d-7b768eba2648");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("f9326e76-b475-401e-8606-b9319b873c33"),
                    Title = "A.7.2.2 Information security awareness education and training",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("9859587e-bd68-4fbe-bb0c-c9758a12f94d"),
                    Title = "A.7.3 Awareness",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> DataProtectionFrameworkStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("ec0ed13d-e887-41e2-a0cd-62fd1ca2de25");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("2c5dece9-34a3-4f3c-9bad-8b480c11adad"),
                    Title = "A.5.1.1 Policies for information security",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("c79ebf2e-e049-467d-a35c-d706f81f3efa"),
                    Title = "A.5.1.2 Review of policies for information security",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("a85c68f5-05f0-4e06-9010-e0fb0a4ad234"),
                    Title = "A.6.1.1 Information security roles and responsibilities",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> DataClassificationStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("8859dd17-94e3-433d-90d8-2fcf3cffaac9");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("e331328e-6c24-408f-bc04-1c580bf13621"),
                    Title = "A.8.2.1 Classification of Information",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("5014f12b-424c-4bf9-8df2-87a0b47421a7"),
                    Title = "A.8.2.2 Labelling of Information",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("1ce50997-cb6c-4595-937a-621f05185249"),
                    Title = "A.8.2.3 Handling of Assets",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> DataProtectionPoliciesStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("ed613e1b-6a1a-4d6b-8efc-216f9520226a");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("c060a750-9fbe-49b1-a5b3-79f80f165200"),
                    Title = "A.10.1 Cryptographic controls",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("5a17fbc3-de77-463e-8513-66d013632b88"),
                    Title = "A.13.1 Network security management",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("af82bc80-80c4-4dfe-8cdc-3cac85adefa5"),
                    Title = "A.13.2 Information transfer",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("e7ab6ddf-101c-4b63-b095-c8346faf5f78"),
                    Title = "A.14.1 Security requirements of information systems",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("711cc7c2-ecae-4485-b174-6865ec789b95"),
                    Title = "A.14.2 Security in development and support processes",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> DataRetentionStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("b679ff1a-2121-45de-a695-9517c3b5319e");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("aeb7f10b-57a0-46c1-9b90-3c52b245308c"),
                    Title = "A.8.2.3 Handling of Assets",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("021787bd-b5ed-4f11-8f55-febdfe7a6568"),
                    Title = "A.8.3 Media Handling",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("6527706e-a2fd-4175-ab6c-790ab2234450"),
                    Title = "A.8.3.2 Disposal of media",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("1ec549fb-c273-4c91-b2ba-fa3ca1c085a7"),
                    Title = "A.8.3.3 Physical media transfer",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("27990585-61a7-4a22-9d80-b72e60c71b03"),
                    Title = "A.11.2.7 Secure disposal or reuse of equipment",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> DataLossStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("95bfad59-1224-414a-8c51-d688abac70ed");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("f33c3a2b-5c9c-454d-bb05-53d7a89e7928"),
                    Title = "A.16.1.1 Responsibilities and procedures",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("30c30835-7bd8-4e78-ad23-cce9167ad78e"),
                    Title = "A.16.1.2 Reporting information security events",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("75a20281-caaa-4272-86ed-c4fdeaf60432"),
                    Title = "A.16.1.4 Assessment of and decision on information security events",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> DataRecoveryStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("673d41d1-e017-4b8a-ad8e-a0992882f385");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("234cb2f2-8a3c-4a24-8feb-69b3579c8be9"),
                    Title = "A.12.3 Backup",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("1ab64504-2ff3-4356-a804-9ea8bdd46868"),
                    Title = "A.17.1 Information security continuity",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("28862c94-4bb1-48aa-b0b0-fb78cfe4e7a0"),
                    Title = "A.17.2 Redundancies",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> IdentityManagementStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("a4c329d6-193e-441f-b0cb-97b9770c7265");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("e1538147-4570-4b7f-88b3-73b59ddd0cae"),
                    Title = "A.7.1 Prior to employment",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("f2a6c4dc-c5d5-4b46-b0d1-9f5971ef1cc1"),
                    Title = "A.7.2 During employment",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("08a560e2-b96d-4c40-bee1-e7124f892b61"),
                    Title = "A.7.3 Termination and change of employment",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("a9b321cc-eb07-403e-90a9-d0a6b667f249"),
                    Title = "A.9.2 User access management",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("99bbc58d-66ae-4df5-867a-dfe211a0fc9d"),
                    Title = "A.9.3 User responsibilities",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> AccessControlsStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("eb909bf7-654c-4314-b1c8-6356169a2c6f");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("8314d38d-0090-4142-a657-008fa542afff"),
                    Title = "A.9.1 Business requirements of access control",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("64c49b65-0e1a-4130-91f9-2496ce698215"),
                    Title = "A.9.4 System and application access control",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId =subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }      
            };
        }

        private static List<CybersecurityStandard> SeparationOfDutiesStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("5e1e6a79-046c-4fbd-89c0-3b41fc69e555");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("f7248419-9abb-40e4-9a3f-f43b41f0db3b"),
                    Title = "A.6.1.2 Segregation of duties",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("49c0a2e9-27aa-4336-a7a0-48cbb766eef9"),
                    Title = "A.12.1.4 Separation of development testing and operational environments",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> PrivilegedAccessManagementStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("4b17e07a-b608-4406-bdeb-32e2653a26cb");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("97cc360f-9cce-48e2-b34a-cc00c78c447e"),
                    Title = "A.9.2.3 Management of privileged access rights",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("516bb06d-4c31-4787-aa27-6ffc6d6f0527"),
                    Title = "A.9.4.1 Information access restriction",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("e6862058-395c-4dd7-96c0-152e5bdac9ab"),
                    Title = "A.9.4.4 Use of privileged utility programs",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> RemoteAccessStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("9734a725-6f43-4a49-8b7c-fd008db3562d");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("fe76f81e-2f67-47bf-8034-8649970379b8"),
                    Title = "A.6.2.2 Teleworking",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("58064a4f-0f58-47da-ace2-e8b9055dd4f8"),
                    Title = "A.13.1.1 Network controls",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("b31aac45-fc07-40ee-9aad-6d77390b33d8"),
                    Title = "A.13.2.1 Information transfer policies and procedures",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("a75f2f6b-b7d2-4a4f-8b45-408157e85f86"),
                    Title = "A.14.1.2 Securing application services on public networks",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> ThirdPartyAccessStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("754ed961-0edd-45c5-87f6-2ff75e409fdd");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("84ef50f8-9d19-448b-bc09-4122661788e9"),
                    Title = "A.9.2.1 User registration and de-registration",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("071e83f8-206e-4360-b023-22883ac3ff17"),
                    Title = "A.9.2.2 User access provisioning",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("59279445-8f52-4c0d-aedc-ca20c5d47bba"),
                    Title = "A.9.2.5 Review of user access rights",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("a76f1b2c-def7-4f5d-91d5-e060e68359d0"),
                    Title = "A.13.1.2 Security of network services",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("a78ae736-50fc-48cc-b121-1f9873d851e0"),
                    Title = "A.13.2.2 Agreements on information transfer",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("92d6850d-a40a-43e3-951c-2f18d167eaa9"),
                    Title = "A.15.1.2 Addressing security within supplier agreements",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("908ec4a5-adec-4719-8c7f-c98c9c242db9"),
                    Title = "A.15.2.2 Managing changes to supplier services",
                    Order = 7,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> NetworkProtectionStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("7ebe6f6c-001f-4022-bb69-6ebd6111338b");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("77723efd-6366-4954-ad2d-4e73a2bfdbf8"),
                    Title = "A.9.1.2 Access to networks and network services",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("9f672d8e-1c3c-481d-b2a0-3519b7b512c8"),
                    Title = "A.13.1.1 Network controls",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("e0995aac-9d77-4302-9a7a-4ab21b578bdc"),
                    Title = "A.13.1.2 Security of network services",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("ef749d66-6580-4b98-9bb3-1eba01035599"),
                    Title = "A.13.1.3 Segregation in networks",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("1c79385f-88f5-4938-a538-c966d8d520c3"),
                    Title = "A.14.1.3 Protecting application services transactions",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }      
            };
        }

        private static List<CybersecurityStandard> EndpointProtectionStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("72e5fa4d-5c0f-4d37-943c-d972393efdcb");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("49ea82d7-0e78-4ba8-af28-0427e10c023a"),
                    Title = "A.6.2.1 Mobile device policy",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("4916328a-f294-445e-8ca6-074cd6e8686c"),
                    Title = "A.12.1.2 Change management",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("0dc5cc6d-da07-4f26-aa95-ebd165fd46af"),
                    Title = "A.12.2 Protection from malware",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("72802180-70e1-4166-a3df-401294a5a1a3"),
                    Title = "A.12.6.2 Restrictions on software installation",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> ApplicationProtectionStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("0cf39325-3c91-453f-a254-a482d86575e6");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("4ac8a862-07f3-4aed-9367-28d980f93c8c"),
                    Title = "A.12.5 Control of operational software",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("a030b75d-579f-4226-9a4f-de6e1045b0a5"),
                    Title = "A.14.1.2 Securing application services on public networks",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("d2193a1c-6f21-4855-a5cf-84e3bfbb9de8"),
                    Title = "A.14.2.1 Secure development policy",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("40c7dc5e-ae9d-4ffc-aaf5-202dbc3d190f"),
                    Title = "A.14.2.2 System change control procedures",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("a9beeaef-2ca6-43f3-8f7a-18b5264709cb"),
                    Title = "A.14.2.3 Technical review of applications after operating platform changes",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("9cefb084-d4bd-4814-8399-5b687ee34538"),
                    Title = "A.14.2.4 Restrictions on changes to software packages",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("5b8024ab-71b0-4a7c-bf4b-3db9804d25f9"),
                    Title = "A.14.2.5 Secure system engineering principles",
                    Order = 7,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("c9c5cbe4-ce2d-4d65-8797-287c1e524d19"),
                    Title = "A.14.2.6 Secure development environment",
                    Order = 8,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("295d2c94-a36b-4f2c-920a-1f67aeff543e"),
                    Title = "A.14.2.7 Outsourced development",
                    Order = 9,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("1f0c3bac-5f9f-4ca1-9bc9-5de740fb23db"),
                    Title = "A.14.2.8 System security testing",
                    Order = 10,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> IncidentReadinessStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("02a82cba-a900-4ca0-aa4e-810065e4c33c");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("246176c5-618b-47c1-a7a2-bd99b5c690dd"),
                    Title = "A.6.1.1 Information security roles and responsibilities",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("7e76ce33-0674-4297-874d-8f8a72c97893"),
                    Title = "A.6.1.3 Contact with authorities",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("22f01a40-6931-4a12-8bad-5c92591b2dd1"),
                    Title = "A.6.1.4 Contact with special interest groups",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("2f55d51a-9f89-4713-b8d1-3aaf85942bd7"),
                    Title = "A.7.2.2 Information security awareness education and training",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("9046c2ec-cb3d-4670-ae83-fbaca48787b5"),
                    Title = "A.16.1.1 Responsibilities and procedures",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }        
            };
        }

        private static List<CybersecurityStandard> IncidentDetectionStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("3b500510-0aa2-44bc-9600-f49ad7208113");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("64f25e52-254a-484c-9a70-163ad4003b24"),
                    Title = "A.12.4.1 Event logging",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("579c5e72-f320-42a9-88fa-524e0ca01598"),
                    Title = "A.12.4.2 Protection of log information",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("e764a4f4-cc0b-4a48-b79d-7a412d686c8b"),
                    Title = "A.12.4.4 Clock synchronization",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("b38ef27a-d581-4695-b311-376c48054962"),
                    Title = "A.16.1.2 Reporting information security events",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("4465bef8-b01c-48f4-ba5b-034cf0e7e768"),
                    Title = "A.16.1.3 Reporting information security weaknesses",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("f78b945c-45bd-491c-850c-cfd7682aaf86"),
                    Title = "A.16.1.4 Assessment of and decision on information security events",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("a05761bd-7698-4013-80ad-8fddcdd7fbde"),
                    Title = "A.16.1.7 Collection of evidence",
                    Order = 7,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> IncidentRemediationStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("6b4a7bed-c626-4369-87ab-1245d5560a38");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("308897dc-31ff-4875-8848-ff1e7f0fe2f1"),
                    Title = "A.7.2.3 Disciplinary process",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("7a8d2e8b-0131-47cd-92d4-63474a1e1eea"),
                    Title = "A.16.1.5 Response to information security incidents",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("33324c6f-ea93-4ba1-9eef-7fed60930f21"),
                    Title = "A.16.1.6 Learning from information security incidents",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.IsoIec27001,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.