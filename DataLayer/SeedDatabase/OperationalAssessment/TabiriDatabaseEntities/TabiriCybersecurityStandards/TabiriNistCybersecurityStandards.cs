using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.TabiriDatabaseEntities.TabiriCybersecurityStandards
{
    public static class TabiriNistCybersecurityStandards
    {
        public static CybersecurityStandard[] Seed(Guid createdBy, DateTime currentTimestamp)
        {
            var standards = new List<CybersecurityStandard>();   

            standards.AddRange(StrategicPlanningStandards(createdBy, currentTimestamp));
            standards.AddRange(SecurityPolicyFrameworkStandards(createdBy, currentTimestamp));
            standards.AddRange(OrganizationalStructureStandards(createdBy, currentTimestamp));      
            standards.AddRange(PerformanceMetricsStandards(createdBy, currentTimestamp));
            standards.AddRange(WorkforceManagementStandards(createdBy, currentTimestamp));    
            standards.AddRange(RiskManagementFrameworkStandards(createdBy, currentTimestamp));  
            standards.AddRange(ThreatManagementStandards(createdBy, currentTimestamp));  
            standards.AddRange(SecurityAwarenessStandards(createdBy, currentTimestamp));    
            standards.AddRange(DataProtectionFrameworkStandards(createdBy, currentTimestamp));      
            standards.AddRange(DataClassificationStandards(createdBy, currentTimestamp));  
            standards.AddRange(DataProtectionPoliciesStandards(createdBy, currentTimestamp));      
            standards.AddRange(DataRetentionStandards(createdBy, currentTimestamp));    
            standards.AddRange(DataLossStandards(createdBy, currentTimestamp));
            standards.AddRange(DataRecoveryStandards(createdBy, currentTimestamp));  
            standards.AddRange(IdentityManagementStandards(createdBy, currentTimestamp));  
            standards.AddRange(AccessControlsStandards(createdBy, currentTimestamp));     
            standards.AddRange(SeparationOfDutiesStandards(createdBy, currentTimestamp));   
            standards.AddRange(PrivilegedAccessManagementStandards(createdBy, currentTimestamp)); 
            standards.AddRange(RemoteAccessStandards(createdBy, currentTimestamp));    
            standards.AddRange(ThirdPartyAccessStandards(createdBy, currentTimestamp));    
            standards.AddRange(NetworkProtectionStandards(createdBy, currentTimestamp));   
            standards.AddRange(EndpointProtectionStandards(createdBy, currentTimestamp));     
            standards.AddRange(ApplicationProtectionStandards(createdBy, currentTimestamp));  
            standards.AddRange(IncidentReadinessStandards(createdBy, currentTimestamp));    
            standards.AddRange(IncidentDetectionStandards(createdBy, currentTimestamp));     
            standards.AddRange(IncidentRemediationStandards(createdBy, currentTimestamp));     
            
            return standards.ToArray();
        } 

        private static List<CybersecurityStandard> StrategicPlanningStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("5e0fdb74-9606-4776-ab23-24bf5898160d");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("32f315f3-c220-42eb-a705-cac318c9ca92"),
                    Title = "PM-1 Information Security Program Plan",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("eb08c8f0-d35c-4999-b34f-47e5064feb40"),
                    Title = "PM-3 Information Security Resources",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("bbb9e7bf-abd1-4c6c-80b2-bf44f67c37cc"),
                    Title = "PM-4 Plan of Action and Milestones Process",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("7397f4a3-b925-4710-97f1-292939286ab2"),
                    Title = "PM-11 Mission/Business Process Definition",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };  
        }

        private static List<CybersecurityStandard> SecurityPolicyFrameworkStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("3df232a9-b172-4cd0-9d91-d3d025e5f36f");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("fa01d072-f453-4d0b-9be6-d26f4edca07f"),
                    Title = "PL-1 Security Planning Policy and Procedures",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }; 
        }

        private static List<CybersecurityStandard> OrganizationalStructureStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("14d74f5a-aae4-4958-aaf5-c10af094515f");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("eb574df3-c51c-41d6-b2d1-62121ea621a3"),
                    Title = "PM-2 Information Security Program Roles",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("356dca7e-ff82-477c-88e1-bb7124c7ecf5"),
                    Title = "PM-3 Information Security and Privacy Resources",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };    
        }

        private static List<CybersecurityStandard> PerformanceMetricsStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("dcbecea7-7a7e-4378-bd4c-acc6ac24afc1");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("b740795c-6ed6-415b-a97e-1e5a1dbc84a6"),
                    Title = "PM-6 Measures of Performance",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> WorkforceManagementStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("7969918f-98f9-4436-8a07-ae7a9cccf9b5");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("ef5c908f-5c44-4571-8f3f-6fcee2068f34"),
                    Title = "PM-13 Security and Privacy Workforce",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("48bebb28-01b3-4c11-afb5-93c386bbdd69"),
                    Title = "PM-14 Testing, Training and Monitoring",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> RiskManagementFrameworkStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("38a12386-d26f-4921-9e4a-08abe7d8e4cb");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("87b98bf9-c989-4701-af59-6fae559ea8f7"),
                    Title = "RA-1 Risk Assessment Policy and Procedures",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("5b640335-c5cc-40ea-ac34-4d4ef88a3e13"),
                    Title = "RA-3 Risk Assessment",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("402eb8ed-9c5c-4d41-8a22-b5dc423aa09b"),
                    Title = "RA-7 Risk Response",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("47cae3e9-2622-4ae2-a579-57633a9a5e33"),
                    Title = "RA-9 Criticality Analysis",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> ThreatManagementStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("7c0fa42c-e5e3-4d16-9599-963f9041ed9f");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("33956e83-07d5-4e12-95f6-ddeec657e859"),
                    Title = "CA-2 Assessments",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("b4509eaf-2e3b-493a-9c69-29b16ec3a48c"),
                    Title = "RA-5 Vulnerability Scanning",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("8979d05c-ee31-4236-ab08-9b7ec15f6717"),
                    Title = "CA-8 Penetration Testing ",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("56a67ac2-db05-4aef-a683-c539a217fb67"),
                    Title = "PA-4 Information Sharing with External Parties",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("3dbfe122-108d-467b-9143-dffe7c8d61ae"),
                    Title = "PM-15 Contacts with Groups and Associations",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("96bdbd6e-3196-4c87-a3e9-70eb1a0b8e18"),
                    Title = "PM-16(1) Automated Means for Sharing Threat Intelligence",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> SecurityAwarenessStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("b9ec8ca6-7e6e-4fe6-8e8d-7b768eba2648");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("6ae11476-9f8e-4c7a-9b64-8c41dcb9d082"),
                    Title = "AT-1 Awareness and Training Policy and Procedures",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("316c9e63-f95f-401b-b641-4ba1ff377b7a"),
                    Title = "PM-16 Threat Awareness Program",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> DataProtectionFrameworkStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("ec0ed13d-e887-41e2-a0cd-62fd1ca2de25");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("a21679fa-f3d1-4563-aeed-0a66c35dbe85"),
                    Title = "SC-1 System and Communications Protection Policy and Procedures",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("b5319bbb-5741-45c1-9220-4e77550e674c"),
                    Title = "SI-1 System and Information Integrity Policy and Procedures",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("c2ba37b8-a889-42de-8b2e-8d96f9bc437f"),
                    Title = "PM-2 Information Security Program Roles",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> DataClassificationStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("8859dd17-94e3-433d-90d8-2fcf3cffaac9");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("12bce385-53c6-41b7-af5e-c5db5ad290ca"),
                    Title = "RA-2 Security Categorization",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("dfa24c70-4bd5-40b3-b8f1-4ad853211e10"),
                    Title = "PM-29 Inventory of Personally Identifiable Information",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("6da66ba2-8da2-45ec-99a7-738acac5d050"),
                    Title = "AC-16 Security and Privacy Attributes",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("891fa3a3-2c60-4bad-ad7e-6e2513401223"),
                    Title = "MP-3 Media Marking",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("3e1b324f-d5f1-4c8c-afab-77f4cf2d254c"),
                    Title = "PE-22 Component Marking",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> DataProtectionPoliciesStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("ed613e1b-6a1a-4d6b-8efc-216f9520226a");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("79038f81-075d-4b02-9661-9741ea5e2237"),
                    Title = "AC-4 Information Flow Enforcement",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("52419bfe-cc76-44e6-9894-91314467a869"),
                    Title = "CA-3 System Interconnections",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("03661d4d-518e-4bdd-ae44-33be6258624c"),
                    Title = "SC-7 Boundary Protection",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("4fb3a970-546e-46db-a105-c709f625e8c0"),
                    Title = "SC-8 Transmission Confidentiality and Integrity",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("6b07bbe1-e224-4c48-93da-78320090bd17"),
                    Title = "SC-13 Cryptographic Protection",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("b371718b-7e1a-4c29-8ba4-907a43af624a"),
                    Title = "SC-28 Protection of Information at Rest",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> DataRetentionStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("b679ff1a-2121-45de-a695-9517c3b5319e");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("51c74bdc-4c20-4f18-a656-a4b7a46ab5d5"),
                    Title = "MP-1 Media Protection Policy and Procedures",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("153740fe-43fb-4d8e-a32d-eb0407e12416"),
                    Title = "PE-16 Delivery and Removal",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("a84456c5-768c-465c-a381-2442930f16e7"),
                    Title = "PE-20 Asset Monitoring and Tracking",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("5c1db3dc-b57f-459d-9fad-eccbd6eaabc7"),
                    Title = "SI-12 Information Management and Retention",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("3c6982cf-e688-4268-bc2e-44fd043b76a5"),
                    Title = "SC-28 Protection of Information at Rest",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> DataLossStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("95bfad59-1224-414a-8c51-d688abac70ed");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("efc17733-e710-472b-9f94-f9c1fab943a7"),
                    Title = "IR-1 Incident Response Policy and Procedures",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("c8bfce8e-3a0a-4bf3-ab5c-9f7713f1634a"),
                    Title = "IR-6 Incident Reporting",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> DataRecoveryStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("673d41d1-e017-4b8a-ad8e-a0992882f385");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("1bc67971-3d32-4e0f-be07-8b2f6dfc18f5"),
                    Title = "CP-1 Contingency Planning Policy and Procedures",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("ca24a7a0-3e99-48bf-9ac5-64441fc6086a"),
                    Title = "CP-2 Contingency Plan",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("a5874ef6-003b-492a-a8dc-9ed87b9dc360"),
                    Title = "CP-3 Contingency Training",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("4a258d56-9dea-4a36-87d9-f222497cf062"),
                    Title = "CP-4 Contingency Plan Testing",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("47e1b432-5ffc-45b8-961a-bd3dc74684d0"),
                    Title = "CP-7 Alternate Storage Site",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("6420c744-d22d-4073-bdf5-e4d11ef2f059"),
                    Title = "CP-8 Alternate Processing Site",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("7b8d56fa-8f9e-436f-8e3e-4205b2929e41"),
                    Title = "CP-9 System Backup ",
                    Order = 7,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("6ef2783d-24d9-4ac2-95e6-2c6cdbb5909a"),
                    Title = "CP-10 System Recovery and Reconstitution",
                    Order = 8,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("8a736f5a-ea02-44a9-8153-641c87775358"),
                    Title = "CP-11 Alternate Communications Protocols",
                    Order = 9,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("2de4d37c-b62e-45cd-ba7d-38cd45489119"),
                    Title = "CP-13 Alternative Security Mechanisms",
                    Order = 10,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> IdentityManagementStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("a4c329d6-193e-441f-b0cb-97b9770c7265");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("97f103d9-021d-418c-8660-70d698473e36"),
                    Title = "AC-2 Account Management",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("eb953887-a6fe-4c20-a7be-f88ead61275e"),
                    Title = "IA-1 Identification and Authentication Policy and Procedures",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("431717c3-b81b-44bc-aed9-bb062ea2d714"),
                    Title = "IA-2 Identification and Authentication (Organizational Users)",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("4cef905b-297f-49e9-9de1-146aa5608902"),
                    Title = "IA-4 Identifier Management",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("f37d5bff-9992-4343-9b2d-521a9ef1174a"),
                    Title = "IA-5 Authenticator Management",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("29bcde4f-1c77-432b-b9dc-1fa443c57edb"),
                    Title = "IA-8 Identification and Authentication (Non-Organizational Users)",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> AccessControlsStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("eb909bf7-654c-4314-b1c8-6356169a2c6f");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("5a6aed80-3892-4ca4-8b52-93156a153d53"),
                    Title = "AC-1 Access Control Policy and Procedures",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("fec8d085-8bb5-4cd8-abb5-cf6d1dc94b84"),
                    Title = "AC-3 Access Enforcement",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("9919015d-5c6e-4221-a898-7eae493e7a2c"),
                    Title = "AC-6 Least Privilege",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("a28a13bc-449d-4b98-989a-f75c6ec732be"),
                    Title = "AC-7 Unsuccessful Logon Attempts",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("8461eb36-ff50-4044-a30f-3ce46b3fa15f"),
                    Title = "AC-8 System Use Notification",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("d9d3c94e-e860-4301-90e2-34271da7169c"),
                    Title = "AC-9 Previous Logon (Access) Notification",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("2e530d79-2259-44ad-b74d-d67f1222ad87"),
                    Title = "AC-24 Access Control Decisions",
                    Order = 7,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("6bedcda1-6ce0-43fe-816a-0983ecc6182e"),
                    Title = "CM-5 Access Restrictions for Change",
                    Order = 8,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("748b84c4-1fbf-494c-8a81-c4450519ae2b"),
                    Title = "IA-6 Authenticator Feedback",
                    Order = 9,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> SeparationOfDutiesStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("5e1e6a79-046c-4fbd-89c0-3b41fc69e555");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("bd2fe731-d496-4bdd-8893-d5ed9729b10a"),
                    Title = "AC-5 Separation of Duties",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("0086d55c-a692-48bf-bd1c-16efaaf5de87"),
                    Title = "CM-5 Access Restrictions for Change",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> PrivilegedAccessManagementStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("4b17e07a-b608-4406-bdeb-32e2653a26cb");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("6a015dd0-1923-48c3-879a-600d06b2f48e"),
                    Title = "AC-2 Account Management",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("82577a8f-788d-4acf-b0e3-9adcd3eaaf2c"),
                    Title = "AC-3 Access Enforcement",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("fd8935e2-46b6-495a-b786-b7f2e38abdab"),
                    Title = "AC-6 Least Privilege",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("47e4630b-75f9-4d0f-b30d-e45e6acaaa59"),
                    Title = "CM-5 Access Restrictions for Change",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> RemoteAccessStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("9734a725-6f43-4a49-8b7c-fd008db3562d");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("88c0d340-b9b3-4ed7-b716-7fde256f531f"),
                    Title = "AC-3 Access Enforcement",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("b4b1df42-045f-4455-b027-c4d96d3c14e1"),
                    Title = "AC-17 Remote Access",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("4836abe6-1f77-4db9-8a18-9d6b21e4f170"),
                    Title = "PE-17 Alternate Work Site",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> ThirdPartyAccessStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("754ed961-0edd-45c5-87f6-2ff75e409fdd");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("57ab8eef-873e-4cb8-a5c7-b6c7b774e43d"),
                    Title = "IA-8 Identification and Authentication (Non-Organizational Users)",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("b984487d-47f1-455a-ac4a-949c97a45366"),
                    Title = "SA-12 Supply Chain Risk Management",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> NetworkProtectionStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("7ebe6f6c-001f-4022-bb69-6ebd6111338b");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("449d9f4e-c117-42e3-82d6-d8ca3bcc857f"),
                    Title = "AC-4 Information Flow Enforcement",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("3fbaa1a4-edb6-4f18-9b54-100a94bef0dd"),
                    Title = "AC-17 Remote Access",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("7635da35-260b-4bff-bdc5-3ffa1e902fdc"),
                    Title = "AC-18 Wireless Access",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("26a780e7-74c6-4e3d-b6ab-ffbde7673f8a"),
                    Title = "AC-20 Use of External Systems",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("52da4b40-29d3-44b8-a754-7e7dcfbe5cb9"),
                    Title = "CA-3 System Interconnections",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("b46626f4-8b74-4326-8640-c5cc788ded78"),
                    Title = "SA-9 External System Services",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("9b9786e1-c07c-48be-a193-b957f2d6d8f2"),
                    Title = "SC-7 Boundary Protection",
                    Order = 7,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("25130729-8329-49b3-b89a-6ed19a2cfde4"),
                    Title = "SC-8 Transmission Confidentiality and Integrity",
                    Order = 8,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("2b0c0045-ee8e-4cda-87a5-7c2a2078bb89"),
                    Title = "SC-10 Network Disconnect",
                    Order = 9,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> EndpointProtectionStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("72e5fa4d-5c0f-4d37-943c-d972393efdcb");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("478cb551-cfd9-4ffb-b19b-42b4aab1d8c9"),
                    Title = "AC-19 Access Control for Mobile Devices",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("86837a55-fa59-498b-a05a-1615ce16263b"),
                    Title = "CM-2 Baseline Configuration",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("56bff0a9-d474-49d8-abea-e81035e9e99d"),
                    Title = "CM-3 Configuration Change Control",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("7c08f026-f1d6-4048-8dda-5ed8771332ae"),
                    Title = "CM-11 User-Installed Software",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("331c2969-6790-439d-8b70-d1d4527111cc"),
                    Title = "SI-3 Malicious Code Protection",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("3f1942d4-1387-49ac-9c10-94fd93b14efc"),
                    Title = "SI-4 System Monitoring",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> ApplicationProtectionStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("0cf39325-3c91-453f-a254-a482d86575e6");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("53df993c-da2c-4d34-91ad-e471a5e25439"),
                    Title = "CA-2 Assessments",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("aa5e489c-5a78-4e85-8cda-b088ed3f308f"),
                    Title = "CM-4 Security and Privacy Impact Analyses",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("63ec2ee1-871b-47ef-ae29-41a67792e80c"),
                    Title = "CM-10 Software Usage Restrictions",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("c890d573-e561-4cda-836c-873032f93253"),
                    Title = "SA-3 System Development Life Cycle",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("31fb9847-25da-4d3a-8e21-aaa81cb8efe1"),
                    Title = "SA-4 Acquisition Process",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("7c13821c-86a2-4beb-967f-1f201b6c5a5d"),
                    Title = "SA-8 Security and Privacy Engineering Principles",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("bf44f48d-ecc2-471b-bb8b-da5a67964fef"),
                    Title = "SA-10 Developer Configuration Management",
                    Order = 7,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("3f2277a7-dc94-4614-9f5d-cace0601bb55"),
                    Title = "SA-11 Developer Testing and Evaluation",
                    Order = 8,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("5806d915-2c5d-4ff0-a9f8-28709948f181"),
                    Title = "SA-12 Supply Chain Risk Management",
                    Order = 9,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("49ea96d6-7031-49d8-946e-ced96ee04574"),
                    Title = "SA-15 Development Process, Standards, and Tools",
                    Order = 10,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("9f62eff7-f2e5-43a9-a92c-7a87b4457a6a"),
                    Title = "SA-17 Developer Security Architecture and Design",
                    Order = 11,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("09199da0-3d8b-4f13-98f6-e56dbf5032f8"),
                    Title = "SI-2 Flaw Remediation",
                    Order = 12,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> IncidentReadinessStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("02a82cba-a900-4ca0-aa4e-810065e4c33c");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("5a5d5c42-e633-4dc6-a2f2-0c6d1ea9d099"),
                    Title = "AT-2 Awareness Training",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("c595baf3-3de0-43d9-bc29-e14ca66eb0b2"),
                    Title = "AT-3 Role-Based Training",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("b9325582-3601-43b0-8ac2-2942148c8565"),
                    Title = "CP-3 Contingency Training",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("8f130a3e-7a11-4a1b-bfa6-48c7b6b5dd2d"),
                    Title = "IR-2 Incident Response Training",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("603bc976-c6c6-4e6c-8277-228c080e50e9"),
                    Title = "IR-3 Incident Response Testing",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("9b5f34c4-8c10-4ac2-a09f-37eaeda0ac3b"),
                    Title = "IR-6 Incident Reporting",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("453bc1a6-518f-4b10-ac94-3ca99313efa3"),
                    Title = "IR-7 Incident Response Assistance",
                    Order = 7,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("82632e44-c422-457b-a31a-39bc31dd939d"),
                    Title = "IR-8 Incident Response Plan",
                    Order = 8,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("bc957ee3-0ded-4b0d-aaba-84f55b72c75d"),
                    Title = "IR-10 Integrated Information Security Analysis Team",
                    Order = 9,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("e4b64fe9-25b1-4320-839b-e314bc2f9022"),
                    Title = "PM-15 Contacts with Groups and Associations",
                    Order = 10,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("4cd3f3ea-e0f3-47f0-bab7-315af5a01813"),
                    Title = "SI-5 Security Alerts, Advisories, and Directives",
                    Order = 11,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> IncidentDetectionStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("3b500510-0aa2-44bc-9600-f49ad7208113");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("fcf93595-836b-4dd0-80ac-5ec9f989dbee"),
                    Title = "AU-6 Audit Review, Analysis, and Reporting",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("53ab0e0c-1b08-4717-be07-203dfe47556b"),
                    Title = "AU-3 Content of Audit Records",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("d38be767-38a4-4cda-903e-1cc76c32a024"),
                    Title = "AU-4 Audit Storage Capacity",
                    Order = 3,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("f5383db7-7bc3-4092-9c27-68337e15fe17"),
                    Title = "AU-8 Time Stamps",
                    Order = 4,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("d3b5e83b-e31c-40f0-8f72-c1de04112cca"),
                    Title = "AU-9 Protection of Audit Information",
                    Order = 5,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("1ba7311f-4ebd-4b4d-876e-d109a9d571f0"),
                    Title = "AU-10 Non-repudiation",
                    Order = 6,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("94f9cde9-dc4c-4f83-baba-5a43023c3307"),
                    Title = "AU-11 Audit Record Retention",
                    Order = 7,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("16877c1c-f00d-4972-9118-50b7c8466583"),
                    Title = "AU-12 Audit Generation",
                    Order = 8,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("ef53cb24-7049-4067-bf87-e64e67ce4859"),
                    Title = "AU-14 Session Audit",
                    Order = 9,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("9a2190be-3e0c-4db8-ab68-80027bb7be9b"),
                    Title = "CM-7 Continuous Monitoring",
                    Order = 10,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("d865093d-6dcb-44d4-a43f-016a7829e8f3"),
                    Title = "IR-4 Incident Handling",
                    Order = 11,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("0f72d024-7b9d-4131-9f32-7882a837234a"),
                    Title = "IR-5 Incident Monitoring",
                    Order = 12,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("2b86f15f-6059-40d3-973c-f320d8008f99"),
                    Title = "IR-6 Incident Reporting",
                    Order = 13,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("19669369-8c77-4af9-b84f-ddac6d6e2f9d"),
                    Title = "SI-4 System Monitoring",
                    Order = 14,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<CybersecurityStandard> IncidentRemediationStandards(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("6b4a7bed-c626-4369-87ab-1245d5560a38");

            return new List<CybersecurityStandard>
            {
                new CybersecurityStandard
                {
                    Id = Guid.Parse("d8eff340-263f-4bb8-965c-7ac49cf58107"),
                    Title = "PS-8 Personnel Sanctions",
                    Order = 1,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new CybersecurityStandard
                {
                    Id = Guid.Parse("3fc707c3-95cf-4a1d-b007-90b3c21f27e3"),
                    Title = "SI-2 Flaw Remediation",
                    Order = 2,
                    StandardsType = CybersecurityStandardsType.Nist800_53,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.