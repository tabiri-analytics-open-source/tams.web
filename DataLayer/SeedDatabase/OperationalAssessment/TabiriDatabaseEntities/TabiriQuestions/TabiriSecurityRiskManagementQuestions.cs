using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.TabiriDatabaseEntities.TabiriQuestions
{
    public static class TabiriSecurityRiskManagementQuestions
    {
        public static Question[] Seed(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Empty;

            var results = new List<Question>();

            #region Risk Management Framework Questions

            subcategoryId = Guid.Parse("38a12386-d26f-4921-9e4a-08abe7d8e4cb");

            var riskManagementFrameworkQuestions = new List<Question>
            {
                new Question
                {
                    Id = Guid.Parse("c4cae77c-7d90-416d-8cf7-49dc13033d95"),
                    MainText = "Does your organization have a Risk Management team?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("555d360a-88e1-4138-b26f-63a78a9a94e8"),
                    MainText = "Is this team lead by a Chief Risk Officer (CRO) or equivalent role?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("c4cae77c-7d90-416d-8cf7-49dc13033d95"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("3ad62b77-ff90-4d18-8c36-8359b70603d1"),
                    MainText = "Is the IT security department represented within the risk team?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("c4cae77c-7d90-416d-8cf7-49dc13033d95"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("6879309c-34b2-417c-b2e3-5de6fe773265"),
                    MainText = "How is the IT Security department represented?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("3ad62b77-ff90-4d18-8c36-8359b70603d1"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question {
                    Id = Guid.Parse("94b45bed-f0b9-43dd-8c48-75bc066863bf"),
                    MainText = "Does the Risk Management team interact with the IT Security team?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("c4cae77c-7d90-416d-8cf7-49dc13033d95"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("6b7f191a-1b52-405c-a396-102522994493"),
                    MainText = "How often?",
                    OptionChoices = "Weekly,Monthly,Quarterly,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("94b45bed-f0b9-43dd-8c48-75bc066863bf"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("e534406c-6207-4ccc-9d71-e2005fe42bdd"),
                    MainText = "In what format?",
                    OptionChoices = "Email notes, Team meeting, Online portal",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("94b45bed-f0b9-43dd-8c48-75bc066863bf"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("f85e2fc1-253f-409b-94dc-ff9af516e582"),
                    MainText = "Is there a procedure for identifying IT security risks within the organization?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("e0cc9833-7cd4-40f7-b0a6-28c9734156e1"),
                    MainText = "Who is tasked with this responsibility internally?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("f85e2fc1-253f-409b-94dc-ff9af516e582"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("3128daf9-43e9-4d35-9033-56af697cf1ef"),
                    MainText = "How often is this task carried out?",
                    OptionChoices = "Weekly,Quarterly,Monthly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f85e2fc1-253f-409b-94dc-ff9af516e582"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("ac92aac6-3a9b-4766-b88f-30589c063dc3"),
                    MainText = "Is this procedure aligned with a specific industry framework?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f85e2fc1-253f-409b-94dc-ff9af516e582"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("b2b7c4b2-cc95-44c5-9791-ed2e793112e8"),
                    MainText = "Which industry framework is it aligned with?",
                    HintText = "e.g. ISO/IEC 31000",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("ac92aac6-3a9b-4766-b88f-30589c063dc3"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("8fe20f12-588f-4e80-b12b-82d31e331264"),
                    MainText = "What is the scope of the information security risk assessment?",
                    OptionChoices = "Critical systems only, Critical + Production systems, All systems",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f85e2fc1-253f-409b-94dc-ff9af516e582"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("3e693b38-d8a6-4811-8c76-1d74a9239c8a"),
                    MainText = "How are those systems identified?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("8fe20f12-588f-4e80-b12b-82d31e331264"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("1d127aa2-89bc-4391-9553-8f84a644afa4"),
                    MainText = "Are IT security risk assessments integrated with other IT activities like SDLC and vendor selection?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f85e2fc1-253f-409b-94dc-ff9af516e582"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("d1fd5738-c60c-4bb7-9150-ea7647ea9a4c"),
                    MainText = "Which IT activities integrate IT security risk assessments?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("1d127aa2-89bc-4391-9553-8f84a644afa4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("e7cb49c1-118e-42cd-afcf-d17207862512"),
                    MainText = "Is there a procedure for prioritizing IT security risk?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 7,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f85e2fc1-253f-409b-94dc-ff9af516e582"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("53c091e3-1725-4519-ad70-891930fec44a"),
                    MainText = "What does this procedure look like?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("e7cb49c1-118e-42cd-afcf-d17207862512"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("022bb297-9e3b-4217-b90a-ab76ef2f9e02"),
                    MainText = "Are identified risks communicated to other business units?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 8,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f85e2fc1-253f-409b-94dc-ff9af516e582"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("6d674796-9309-4edc-a723-e9d2671a3184"),
                    MainText = "In what format?",
                    OptionChoices = "Email notes,Team meeting,Online portal",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("022bb297-9e3b-4217-b90a-ab76ef2f9e02"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("2bfc9496-cec6-437e-8f02-849eb4352602"),
                    MainText = "Is there a procedure for addressing identified risks?",
                    HintText = "Risk can either be avoided, mitigated, accepted, or transfered",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 9,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f85e2fc1-253f-409b-94dc-ff9af516e582"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("9c96a45e-6ae0-40f9-ae05-83e6de93e85d"),
                    MainText = "What does this procedure look like?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("2bfc9496-cec6-437e-8f02-849eb4352602"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("74d90e43-1471-4783-8182-c44e102d3a3d"),
                    MainText = "Who is assigned ownership of addressing these risks?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("2bfc9496-cec6-437e-8f02-849eb4352602"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("7d5e4bee-86f6-4382-8459-020554efdbfa"),
                    MainText = "How is progress on addressing these risks tracked?",                    
                    Order = 3,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("2bfc9496-cec6-437e-8f02-849eb4352602"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("3c83ee29-350b-4c55-a9e5-88c9f9d3ce82"),
                    MainText = "Is there a follow-up after the risks are addressed?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 10,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f85e2fc1-253f-409b-94dc-ff9af516e582"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("7a51517a-39e2-4d6f-abf0-54834a2efeeb"),
                    MainText = "What does this follow-up look like?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("3c83ee29-350b-4c55-a9e5-88c9f9d3ce82"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("b5dfdb6c-83a6-498e-baa3-5988ff0f23da"),
                    MainText = "Are the IT risk assessments audited by a third-party?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 11,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f85e2fc1-253f-409b-94dc-ff9af516e582"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("fc17128f-4ee1-4ba9-8501-bf00487fb9c6"),
                    MainText = "How are discrepancies between the internal assessment and third-party assessment addressed?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("b5dfdb6c-83a6-498e-baa3-5988ff0f23da"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("e40df83d-c1a8-4088-9296-30ffc693403b"),
                    MainText = "Are key IT security risks identified at this time?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f85e2fc1-253f-409b-94dc-ff9af516e582"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("ca057991-7da2-4287-aafe-f64210b757a1"),
                    MainText = "Which ones?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("e40df83d-c1a8-4088-9296-30ffc693403b"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("7beb4efe-4533-4eae-8f6b-55fdb668d1ab"),
                    MainText = "Are you aware if your organization is subject to any external regulations?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("8f2d20d8-3dbe-4af1-826c-f8737e001ba4"),
                    MainText = "Does a specific regulatory body have oversight authority over your organization?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("7beb4efe-4533-4eae-8f6b-55fdb668d1ab"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("0a9792df-a06d-4912-8b8f-1dbb5f9dc6c5"),
                    MainText = "Which regulatory body?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("8f2d20d8-3dbe-4af1-826c-f8737e001ba4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("197abf77-6232-4afd-ad39-7a8d95c3e0a4"),
                    MainText = "Is IT security risk in scope for those regulations?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("7beb4efe-4533-4eae-8f6b-55fdb668d1ab"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("48a21594-9f6d-4a18-99a4-340700090968"),
                    MainText = "How often is the organization reviewed for compliance with these regulations?",
                    OptionChoices = "Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("197abf77-6232-4afd-ad39-7a8d95c3e0a4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("1736337f-3239-432a-b8f7-203f8276264c"),
                    MainText = "Is this a separate procedure from the one defined in Question 2 above?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("197abf77-6232-4afd-ad39-7a8d95c3e0a4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("d8299b64-a8dd-4d12-a915-422f33f2631f"),
                    MainText = "In what format are the results of these reviews communicated?",
                    OptionChoices = "Email notes,Team meeting,Online portal",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("1736337f-3239-432a-b8f7-203f8276264c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("622c67bd-bae9-4dad-9e7e-a4fcd761b40a"),
                    MainText = "How are identified risks addressed?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("1736337f-3239-432a-b8f7-203f8276264c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("19af2dad-e207-4a9b-b6e0-ef5f2d28c347"),
                    MainText = "Are the results of these reviews audited by a third-party?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1736337f-3239-432a-b8f7-203f8276264c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(riskManagementFrameworkQuestions);

            #endregion

            #region Threat Management Questions

            subcategoryId = Guid.Parse("7c0fa42c-e5e3-4d16-9599-963f9041ed9f");

            var threatManagementQuestions = new List<Question>
            {
                new Question
                {
                    Id = Guid.Parse("e0c017df-959f-4512-acc8-670a850c3477"),
                    MainText = "Does your organization have a threat management policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("498b5ced-8203-4b27-a93a-43cdd5dd2097"),
                    MainText = "Who is tasked with this responsibility?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("e0c017df-959f-4512-acc8-670a850c3477"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("ee62c508-0800-47d3-83c3-702231ac7f8e"),
                    MainText = "What technologies are used to identify new and existing threats?",
                    HintText = "e.g. vulnerability scanner, security information event management (SIEM), threat intelligence feeds",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("e0c017df-959f-4512-acc8-670a850c3477"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("83ed221c-2424-4f25-a5ec-c8d148b9b369"),
                    MainText = "Does your organization conduct vulnerability assessments?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("b4f951ed-f0d6-4bba-b173-0534579ab493"),
                    MainText = "How often?",
                    OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("83ed221c-2424-4f25-a5ec-c8d148b9b369"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("db6a7189-46eb-4fc3-b946-311db80199c0"),
                    MainText = "What is the scope of the vulnerability scans?",
                    HintText = "e.g. internal/external, authenticated/unauthenticated",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("83ed221c-2424-4f25-a5ec-c8d148b9b369"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("bbf42afb-e8a8-455a-8381-ea2eeca39aee"),
                    MainText = "Does your organization conduct penetration tests?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("c18f8576-cc84-4aba-ac53-ce244712acef"),
                    MainText = "How often?",
                    OptionChoices = "Bi-Weekly,Quarterly,Monthly,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("bbf42afb-e8a8-455a-8381-ea2eeca39aee"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("2d6b9c8f-96f9-4a0b-a2c5-425935b06f8d"),
                    MainText = "What is the scope of the penetration tests?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("bbf42afb-e8a8-455a-8381-ea2eeca39aee"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("61c7bc67-bf2f-4e55-a388-747d0dfcf321"),
                    MainText = "Is there a procedure for translating technical findings into business risk?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("47325450-0e93-4e22-8038-134e34b4e91f"),
                    MainText = "What is the procedure?",
                    HintText = "e.g. root cause analysis, CVSS score into business impact",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("61c7bc67-bf2f-4e55-a388-747d0dfcf321"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("61390f77-fba5-447a-9bd3-7a200fd4018f"),
                    MainText = "Does your organization have a procedure for addressing identified vulnerabilities?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("e1956229-0711-43e5-ae6c-ffed0ce745b9"),
                    MainText = "How are identified vulnerabilities managed and remediated?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("61390f77-fba5-447a-9bd3-7a200fd4018f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("c082e5d6-c59c-4538-b02f-6e6976d41c72"),
                    MainText = "Is there a procedure for validating patched vulnerabilities?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("61390f77-fba5-447a-9bd3-7a200fd4018f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("cb78056c-42ab-4bb2-9af7-59f85d7c8d9c"),
                    MainText = "What does this procedure look like?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("c082e5d6-c59c-4538-b02f-6e6976d41c72"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("cfa47f58-84e3-48ef-b186-86dd7903d4fc"),
                    MainText = "Is there a procedure for remediating vulnerabilities that cannot be patched?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("61390f77-fba5-447a-9bd3-7a200fd4018f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("dc441c68-bd30-4a87-b8b5-dd62a231b761"),
                    MainText = "What does this procedure look like?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("cfa47f58-84e3-48ef-b186-86dd7903d4fc"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("d2c5bcd5-8147-41d8-8fa4-66f59cacb795"),
                    MainText = "Does your organization have a Security Information Event Management (SIEM) solution?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("8fb512ae-de75-44d8-a7bf-6bd375562538"),
                    MainText = "What data sources are integrated into the SIEM?",
                    HintText = "e.g. network, endpoint or application logs",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("d2c5bcd5-8147-41d8-8fa4-66f59cacb795"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("8e0decc8-ee8a-4f92-a877-deb2a106b8d3"),
                    MainText = "Are threat intelligence feeds integrated into the SIEM?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("d2c5bcd5-8147-41d8-8fa4-66f59cacb795"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("b772922e-9d0d-4dd9-958d-6facc83855ca"),
                    MainText = "What kind of feeds are they?",
                    HintText = "e.g. open-source, commercial",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("8e0decc8-ee8a-4f92-a877-deb2a106b8d3"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("7e3a62f8-3286-4069-a7df-9e5349d9bfa9"),
                    MainText = "Does your organization subscribe to any cyber threat information sharing forums?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 7,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("fbcc9b22-aee5-4f4f-9ddc-22b76ce454b8"),
                    MainText = "Are these forums specific to your industry?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("7e3a62f8-3286-4069-a7df-9e5349d9bfa9"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("0d98c7f3-d572-4e73-bdc3-8a5749d0df80"),
                    MainText = "Which Information Sharing and Analysis Center (ISAC) or equivalent organization is your organization a member of?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("fbcc9b22-aee5-4f4f-9ddc-22b76ce454b8"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("fbcfa0be-ef3d-45a8-b99a-dd33bf517093"),
                    MainText = "How are emerging threats tracked and communicated?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("7e3a62f8-3286-4069-a7df-9e5349d9bfa9"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };

            results.AddRange(threatManagementQuestions);

            #endregion

            #region Security Awareness Questions   

            subcategoryId = Guid.Parse("b9ec8ca6-7e6e-4fe6-8e8d-7b768eba2648");       

            var securityAwarenessQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("31ea4300-ee4c-4e55-afca-8141c48d9379"),
                    MainText = "Does your organization have a Security Awareness training program?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("829ea463-01c0-4d6f-a51e-7ee7b968a323"),
                    MainText = "What percentage of the IT budget is allocated to this program?",                    
                    Order = 1,
                    QuestionType = QuestionType.Percentage,
                    ParentId = Guid.Parse("31ea4300-ee4c-4e55-afca-8141c48d9379"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("68c75743-3468-4210-9239-4b9cee1c692e"),
                    MainText = "Who is the target audience for this training?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("31ea4300-ee4c-4e55-afca-8141c48d9379"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f53428ac-4d44-4f6e-99b9-81747a8216e9"),
                    MainText = "Who is tasked with developing the training content?",                    
                    Order = 3,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("31ea4300-ee4c-4e55-afca-8141c48d9379"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("65993e20-1bba-4496-831c-ea1edf6d062a"),
                    MainText = "Is there role specific training for employees in sensitive roles?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("31ea4300-ee4c-4e55-afca-8141c48d9379"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("bdc0e9d1-ecbd-4ec4-ad49-9496014db5ef"),
                    MainText = "Which employee roles fall under this category?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("65993e20-1bba-4496-831c-ea1edf6d062a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("4022e633-ab84-4531-91f5-b03f2da6cdf1"),
                    MainText = "What specific training is provided?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("65993e20-1bba-4496-831c-ea1edf6d062a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("19821265-7456-4e41-a7a5-c506270cf9c5"),
                    MainText = "Is the training mandatory?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("31ea4300-ee4c-4e55-afca-8141c48d9379"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("444278a6-e1fb-401c-8a69-e81399e67666"),
                    MainText = "How is compliance enforced?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("19821265-7456-4e41-a7a5-c506270cf9c5"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("31ce0c26-29db-421c-9027-b553da5b4a9b"),
                    MainText = "Does your organization have a formal delivery method for the IT security training program?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("31ea4300-ee4c-4e55-afca-8141c48d9379"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a1dbd869-a136-43c6-8021-d3777e169453"),
                    MainText = "How is the training delivered?",
                    HintText = "e.g. email newsletters, training workshops, online courses",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("31ce0c26-29db-421c-9027-b553da5b4a9b"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c23bdf73-7c67-41c8-9dc2-88332d248d4a"),
                    MainText = "How often is the training conducted?",
                    OptionChoices = "Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("31ce0c26-29db-421c-9027-b553da5b4a9b"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("9e5980ac-8126-4e3d-9423-ec3c804e6889"),
                    MainText = "When is the IT security training conducted?",
                    OptionChoices = "During new employee onboarding,After employee job change",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 7,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("31ea4300-ee4c-4e55-afca-8141c48d9379"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d525779f-ee4d-4a52-be14-bade64f6e70c"),
                    MainText = "Does your organization have a formal list of IT security training topics?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 8,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("31ea4300-ee4c-4e55-afca-8141c48d9379"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f2e1957f-0e2b-471d-9b64-e83d78420cc8"),
                    MainText = "What specific topics are covered?",
                    HintText = "e.g. data protection, web hygiene, email hygiene/phishing, incident reporting",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("d525779f-ee4d-4a52-be14-bade64f6e70c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c20fc35a-bf9e-4188-879d-2f88d2ec3956"),
                    MainText = "Is the IT security training progress tracked?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 9,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("31ea4300-ee4c-4e55-afca-8141c48d9379"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c242c8a3-01d0-499b-9b52-05594411145d"),
                    MainText = "What completion metrics are collected?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("c20fc35a-bf9e-4188-879d-2f88d2ec3956"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("8b43663f-8b4a-4265-b740-e6f2ecd9883e"),
                    MainText = "In what format are metrics communicated to management?",
                    OptionChoices = "Email notes,Team meeting,Online portal",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("c20fc35a-bf9e-4188-879d-2f88d2ec3956"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7bd51745-367e-4438-bd72-084a6d235119"),
                    MainText = "Is there a procedure for reviewing and revising the IT security awareness program?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 10,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("31ea4300-ee4c-4e55-afca-8141c48d9379"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d97cfd75-863f-420d-a85f-9d953d9cb5fb"),
                    MainText = "How often is the program reviewed?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("7bd51745-367e-4438-bd72-084a6d235119"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("774cbe0e-3347-4b32-b756-10cb4b6fb5b5"),
                    MainText = "What is the procedure for revising the program?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("7bd51745-367e-4438-bd72-084a6d235119"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(securityAwarenessQuestions);

            #endregion
            
            return results.ToArray();
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.