using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.TabiriDatabaseEntities.TabiriQuestions
{
    public static class TabiriSecurityGovernanceQuestions
    {
        public static Question[] Seed(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Empty;

            var results = new List<Question>();

            #region Strategic Planning Questions

            subcategoryId = Guid.Parse("5e0fdb74-9606-4776-ab23-24bf5898160d");

            var strategicPlanningQuestions = new List<Question>
            {
              new Question
              {
                Id = Guid.Parse("744c77b7-48af-4efb-bd29-db4a25dc9357"),
                MainText = "Does your organization have a documented Strategic Plan for your IT Security department?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("54bc4b8d-e12d-44ce-86a7-84f90e8f5a8f"),
                MainText = "What is the timeline for your strategic plan outlining the goals and objectives for the security department?",
                OptionChoices = "1-year,3-year,5-years,>5-years",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("744c77b7-48af-4efb-bd29-db4a25dc9357"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("1d78e3c8-2bf6-4b69-a541-a5d29b8d50da"),
                MainText = "Are members of the IT Security department aware of their assigned goals?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 2,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("3d4020c7-99c7-4b32-a750-bdd135f0077d"),
                MainText = "Are these goals and objectives tracked and reported?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("1d78e3c8-2bf6-4b69-a541-a5d29b8d50da"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("da4d856a-e888-4912-a196-25c178ca75d2"),
                MainText = "Are other departments aware of the IT Security department’s mission?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                Order = 3,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("b75d9a5a-ef51-45f1-86cf-ba8eb8aa61ef"),
                MainText = "Is Management updated on the progress of the IT Security department?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 4,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("f7cbd54a-9de9-48a6-b0ff-099e19494614"),
                MainText = "How often?",
                OptionChoices = "Weekly,Monthly,Quarterly,Annually",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("b75d9a5a-ef51-45f1-86cf-ba8eb8aa61ef"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("99aa6982-3665-4309-97e9-c853c0c347c6"),
                MainText = "In what format?",
                OptionChoices = "Email notes, Team meeting, Online portal",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 2,
                QuestionType = QuestionType.SelectMultiple,
                ParentId = Guid.Parse("b75d9a5a-ef51-45f1-86cf-ba8eb8aa61ef"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("4eaf9508-6bb9-4fa3-b6a9-9a9249213018"),
                MainText = "Does the organization have a dedicated budget for its IT Security department?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 5,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("be6f5a97-c43e-44bf-9e89-e7a763df0067"),
                MainText = "How was the IT Security budget initially determined?",
                OptionChoices = "Ad hoc, Consulting study, Fixed percentage of IT budget",
                OptionsOrientation = OptionsOrientation.Vertical,
                ActiveOptionIndex = 2,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("4eaf9508-6bb9-4fa3-b6a9-9a9249213018"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("e0125055-9095-42a6-aa15-9a617984f96c"),
                MainText = "If fixed percentage of IT budget, what percentage?",
                Order = 1,
                QuestionType = QuestionType.Percentage,
                ParentId = Guid.Parse("be6f5a97-c43e-44bf-9e89-e7a763df0067"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("410f93fd-42d1-4bf5-8f35-43f85efbd2d7"),
                MainText = "Is the IT Security budget broken down by specific need?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 2,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("4eaf9508-6bb9-4fa3-b6a9-9a9249213018"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("0bb360a9-c289-4da4-bd3e-86b3d6e09782"),
                MainText = "Percentage allocated for personnel?",
                Order = 1,
                QuestionType = QuestionType.Percentage,
                ParentId = Guid.Parse("410f93fd-42d1-4bf5-8f35-43f85efbd2d7"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("9602e341-182a-4956-96b5-feac1e802706"),
                MainText = "Percentage allocated for tools and equipment?",
                Order = 2,
                QuestionType = QuestionType.Percentage,
                ParentId = Guid.Parse("410f93fd-42d1-4bf5-8f35-43f85efbd2d7"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("6a503c67-6e7b-4cdc-8dce-d81dfbd17805"),
                MainText = "Percentage allocated for training?",
                Order = 3,
                QuestionType = QuestionType.Percentage,
                ParentId = Guid.Parse("410f93fd-42d1-4bf5-8f35-43f85efbd2d7"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("aad34a6c-3020-4f27-a5f9-0a8adf7e9af3"),
                MainText = "Is this budget revised regularly?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 3,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("4eaf9508-6bb9-4fa3-b6a9-9a9249213018"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("92ca8629-8b8b-46be-a0d2-540cb5f6f130"),
                MainText = "How often?",
                OptionChoices = "Quarterly, Semi-annually, Annually",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("aad34a6c-3020-4f27-a5f9-0a8adf7e9af3"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("d35f51e1-3829-45a9-a283-85b4e5ce3f05"),
                MainText = "What criteria is used to justify increases or decreases in the budget?",
                Order = 2,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("aad34a6c-3020-4f27-a5f9-0a8adf7e9af3"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("58a512cb-54af-4f21-9512-fecd655fab34"),
                MainText = "Do the outcome of the goals set by the IT security department drive revisions to the Strategic Plan for your IT Security department?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 6,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("a4eaa994-0b21-441d-840e-9c36cc4142e5"),
                MainText = "How often are revisions made?",
                OptionChoices = "Weekly,Monthly,Quarterly,Semi-annually,Annually",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("58a512cb-54af-4f21-9512-fecd655fab34"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              }
            };
            results.AddRange(strategicPlanningQuestions);

            #endregion

            #region Security Policy Framework Questions

            subcategoryId = Guid.Parse("3df232a9-b172-4cd0-9d91-d3d025e5f36f");

            var securityPolicyFrameworkQuestions = new List<Question>
            {
              new Question
              {
                Id = Guid.Parse("d7132735-9ab3-4862-adc2-5110e7310103"),
                MainText = "Does your organization have documented security policies, standards and procedures?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },

              new Question
              {
                Id = Guid.Parse("30dd8056-fbe2-4af6-b194-eb5601e944d2"),
                MainText = "Are the policies, standards and procedures aligned with any industry framework?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("d7132735-9ab3-4862-adc2-5110e7310103"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("d5a45a83-a6cf-405d-be34-f6becd1e8bb4"),
                MainText = "Which industry frameworks are they aligned?",
                OptionChoices = "NIST,ISO/IEC,COBIT,Other",
                OptionsOrientation = OptionsOrientation.Vertical,
                ActiveOptionIndex = 3,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("30dd8056-fbe2-4af6-b194-eb5601e944d2"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("fe413d4e-4903-457f-9bde-67fc9039160c"),
                MainText = "Which other industry framework are they aligned to?",
                Order = 1,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("d5a45a83-a6cf-405d-be34-f6becd1e8bb4"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("00706b22-4d9c-4523-a49a-f317ee6651f7"),
                MainText = "List the policies that your organization has documented.",
                HintText = "A policy explains why you should do something, e.g. Acceptable Use, Disaster Recovery, Change Management",
                Order = 2,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("d7132735-9ab3-4862-adc2-5110e7310103"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("160d28e0-0862-4a83-8a4a-1bceadcfa9f7"),
                MainText = "List the standards that your organization has documented.",
                HintText = "A standard documents the system specific rules, e.g. Windows Configuration, Enterprise Logging",
                Order = 3,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("d7132735-9ab3-4862-adc2-5110e7310103"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("15ab2b64-dc3c-4b7f-91a6-7babe928c679"),
                MainText = "List the procedures that your organization has documented.",
                HintText = "A procedure describes how you should do something, e.g. IRP, System Backup",
                Order = 4,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("d7132735-9ab3-4862-adc2-5110e7310103"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("ba7b303c-b3b9-44c4-8e3a-ff10d20582ea"),
                MainText = "Which of these policies do you currently have?",
                OptionChoices = "Acceptable Use Policy,Application Security Policy,Cloud Security Policy,Configuration Policy,Database Security Policy,Encryption Policy,Endpoint Security Policy,Log Management Policy,Media Disposal Policy,Mobile Device Policy,Network Security Policy,Password Policy,Patch Management Policy,Remote Access Policy,Vulnerability Management",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 5,
                QuestionType = QuestionType.SelectMultiple,
                ParentId = Guid.Parse("d7132735-9ab3-4862-adc2-5110e7310103"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("53d7156b-ff2b-4639-a2b9-d8cafd071753"),
                MainText = "Are there specific individuals accountable for these policies, standards and procedures?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 6,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("d7132735-9ab3-4862-adc2-5110e7310103"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("b14722ad-9e30-44d7-bc90-fc28a72abc4f"),
                MainText = "What is the functional role of these individuals?",
                Order = 1,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("53d7156b-ff2b-4639-a2b9-d8cafd071753"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("12579938-ae89-410f-a742-29eeb4a0e695"),
                MainText = "Is there a process to review and revise these security policies, standards and procedures?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 7,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("d7132735-9ab3-4862-adc2-5110e7310103"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("046fe7c3-0ccf-4997-8623-d44f0dea4c0e"),
                MainText = "What does this process look like?",
                Order = 1,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("12579938-ae89-410f-a742-29eeb4a0e695"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              }
            };
            results.AddRange(securityPolicyFrameworkQuestions);
            
            #endregion

            #region Organizational Structure Questions

            subcategoryId =  Guid.Parse("14d74f5a-aae4-4958-aaf5-c10af094515f");

            var organizationalStructureQuestions = new List<Question>
            {
              new Question
              {
                Id = Guid.Parse("df866785-7247-43c5-b3bc-941b16519466"),
                MainText = "Is the IT Security department its own business unit, separate from the general IT department?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
            },
              new Question
              {
                Id = Guid.Parse("968b685e-f113-4911-aa5c-92cb2b51e635"),
                MainText = "Does your organization have a formal reporting structure for IT Security department?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 2,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question {
                Id = Guid.Parse("c4939152-a4ed-4910-ab08-fef475b05a70"),
                MainText = "If yes, select one.",
                OptionChoices = "IT Security Manager → CISO → CIO → CEO/President,IT Security Manager → CISO → CEO/President,IT Security Manager → CEO/President/Board/Other",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("968b685e-f113-4911-aa5c-92cb2b51e635"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("d4177431-aa7e-44fc-980b-e1251b5d0256"),
                MainText = "How many people work in the IT Security department?",
                Order = 3,
                QuestionType = QuestionType.Integer,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("7c0d0b73-c062-40f9-a1b9-7b8bfeca70f8"),
                MainText = "How many total employees do you have in your organization?",
                Order = 1,
                QuestionType = QuestionType.Integer,
                ParentId = Guid.Parse("d4177431-aa7e-44fc-980b-e1251b5d0256"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("a04da182-9c49-4cc7-a6fc-2f07bb3e189c"),
                MainText = "What industry sector is your organization classified under?",
                OptionChoices = "Agriculture, Communications, Construction, Education, Energy, Entertainment, Finance, Healthcare, Hospitality, Information Technology, Insurance, Government, Manufacturing, Mining, Non-profit, Professional Services, Real Estate, Retail, Transportation, Other",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 2,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("d4177431-aa7e-44fc-980b-e1251b5d0256"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("eeba0369-00ec-4c52-9a7c-45b04b1b9983"),
                MainText = "Does the IT Security staff focus on just network, system or application security?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 4,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("90b6e547-7b52-489a-8def-80cef6804b7a"),
                MainText = "Does the IT Security staff also perform general (non-security) IT functions?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("eeba0369-00ec-4c52-9a7c-45b04b1b9983"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("59489c5e-bd31-4e1b-9a1d-abb4fb4ca63f"),
                MainText = "Is there at least one IT Security staff member working after-hours, weekends or holidays?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                Order = 5,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("7a34129a-bc8a-4d6c-92d2-65027211d96b"),
                MainText = "Does your organization outsource any IT Security functions to third parties?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 6,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("75747d88-6bb8-4e47-945d-496106d266e2"),
                MainText = "Which functions?",
                Order = 1,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("7a34129a-bc8a-4d6c-92d2-65027211d96b"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("552b8e2d-cfa4-46cd-a019-dcb7fc304e91"),
                MainText = "Are these functions incorporated into the same reporting structure identified in question (2)?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                Order = 2,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("7a34129a-bc8a-4d6c-92d2-65027211d96b"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("7771a060-a75e-4eed-8b2d-4e7045d0c475"),
                MainText = "Do the third parties provide 24/7 continuous monitoring and reporting of security events?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                Order = 3,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("7a34129a-bc8a-4d6c-92d2-65027211d96b"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              }
            };
            results.AddRange(organizationalStructureQuestions);

            #endregion

            #region Performance Metrics Questions

            subcategoryId = Guid.Parse("dcbecea7-7a7e-4378-bd4c-acc6ac24afc1");

            var performanceMetricsQuestions = new List<Question>
            {
              new Question
              {
                Id = Guid.Parse("1faa5c21-b33d-4f82-ae09-4d4e88d98520"),
                MainText = "Does your organization collect any IT Security metrics?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("811334e8-3253-46d5-9a31-abbbdc125c6b"),
                MainText = "Which metrics do you collect?",
                Order = 1,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("1faa5c21-b33d-4f82-ae09-4d4e88d98520"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("0b09fbe0-897a-4af3-b9c9-e9d5ed59fc4b"),
                MainText = "Is the collection of these metrics based on a policy requirement?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 2,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("1faa5c21-b33d-4f82-ae09-4d4e88d98520"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("29f74d0a-57bc-49b9-9e32-120365bcbcfd"),
                MainText = "If yes, which policy requirements?",
                OptionChoices = "Business risk based,Regulatory compliance based,Return-on-Investment (ROI) based",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 1,
                QuestionType = QuestionType.SelectMultiple,
                ParentId = Guid.Parse("0b09fbe0-897a-4af3-b9c9-e9d5ed59fc4b"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("14a6f513-0b6d-4bd2-8236-ee23abe5362d"),
                MainText = "Does your organization use any specific metrics collection processes or tools?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 3,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("1faa5c21-b33d-4f82-ae09-4d4e88d98520"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("889db628-5629-41f1-a9b8-52181b5449d9"),
                MainText = "Which ones?",
                Order = 1,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("14a6f513-0b6d-4bd2-8236-ee23abe5362d"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("a802e3fe-40e2-4655-af64-d743fb6a7a90"),
                MainText = "Is metrics collection automated?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                Order = 2,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("14a6f513-0b6d-4bd2-8236-ee23abe5362d"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("84898e10-a8ce-4631-9b3e-1c8c21cb0241"),
                MainText = "Are these metrics reported to Management?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 4,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("1faa5c21-b33d-4f82-ae09-4d4e88d98520"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("1ed92891-53a7-472d-8efe-aaeb4dea1d83"),
                MainText = "How often?",
                OptionChoices = "Weekly,Monthly,Quarterly,Annually",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("84898e10-a8ce-4631-9b3e-1c8c21cb0241"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("0e1c72ca-5c4a-48b3-b87b-3a35fee60848"),
                MainText = "Has management designated any Key Performance Indicators (KPIs)?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 2,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("84898e10-a8ce-4631-9b3e-1c8c21cb0241"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("096d2ca9-73a7-4f44-ac32-4ef7f55b237a"),
                MainText = "Which ones?",
                Order = 1,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("0e1c72ca-5c4a-48b3-b87b-3a35fee60848"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("b402e06c-fd44-439e-aaca-7b4b1c908bb2"),
                MainText = "Do these metrics play a role in revising the Strategic Plan?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                Order = 5,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("1faa5c21-b33d-4f82-ae09-4d4e88d98520"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("02e36305-d6e0-4f83-b015-cdc7ce811f68"),
                MainText = "Are these metrics audited by an independent third-party?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                Order = 6,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("1faa5c21-b33d-4f82-ae09-4d4e88d98520"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              }
            };
            results.AddRange(performanceMetricsQuestions);

            #endregion

            #region Workforce Management Questions

            subcategoryId = Guid.Parse("7969918f-98f9-4436-8a07-ae7a9cccf9b5");

            var workforceManagementQuestions = new List<Question>
            {
              new Question
              {
                Id = Guid.Parse("7334644a-6361-4013-8a68-66814385274e"),
                MainText = "Does your organization measure the level of skills and knowledge of the IT Security staff?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("ec464d69-9c4c-42d3-baab-27f3992d4965"),
                MainText = "How often?",
                OptionChoices = "Monthly,Quarterly,Annually",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("7334644a-6361-4013-8a68-66814385274e"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("db2298b7-003f-461c-802b-7ae917c66f7d"),
                MainText = "What metrics does your organization use to measure the level of skills and knowledge?",
                Order = 2,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("7334644a-6361-4013-8a68-66814385274e"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("ac59454a-9f48-43b1-89b3-a9854f9d69e4"),
                MainText = "Does the organization provide training for IT Security staff?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 2,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("9539fabe-2d66-4be4-a34c-725f902ff940"),
                MainText = "How often?",
                OptionChoices = "Monthly,Quarterly,Annually",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 1,
                QuestionType = QuestionType.SelectSingle,
                ParentId = Guid.Parse("ac59454a-9f48-43b1-89b3-a9854f9d69e4"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("4baa145b-5894-4994-8026-f35edb1e831c"),
                MainText = "What kind of training is provided?",
                Order = 2,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("ac59454a-9f48-43b1-89b3-a9854f9d69e4"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("28094ffe-45d6-4ce8-9a17-91fb0f6d2e1a"),
                MainText = "What percentage of the IT budget is allocated to security training?",
                Order = 3,
                QuestionType = QuestionType.Percentage,
                ParentId = Guid.Parse("ac59454a-9f48-43b1-89b3-a9854f9d69e4"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("44abe766-5d3b-4907-ae31-42a01f23c8bb"),
                MainText = "Is there a clear career path for IT Security staff?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                Order = 3,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("4fdaa1a2-abb1-40f6-8c49-04df754ab355"),
                MainText = "Is there a process to review security team staff performance?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 4,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("4ef0b4f9-6ad9-4dee-bc1c-bc786690274c"),
                MainText = "What does this process look like?",
                Order = 1,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("4fdaa1a2-abb1-40f6-8c49-04df754ab355"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("e23d9f49-2b88-42da-82ef-358f6fbf42c8"),
                MainText = "Does your organization provide incentives to retain the best staff?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 5,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("df62880b-eebe-4138-8884-054ac946f02a"),
                MainText = "If yes, specify the incentives.",
                OptionChoices = "Promotions,Bonuses,Job rotation",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 1,
                QuestionType = QuestionType.SelectMultiple,
                ParentId = Guid.Parse("e23d9f49-2b88-42da-82ef-358f6fbf42c8"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("572eceb1-c984-49e2-b3c9-c43098c3c32d"),
                MainText = "Does your organization have a recruiting strategy for IT Security staff?",
                OptionChoices = "Yes,No",
                OptionsOrientation = OptionsOrientation.Horizontal,
                ActiveOptionIndex = 0,
                Order = 6,
                QuestionType = QuestionType.SelectSingle,
                ParentId = null,
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("6f830b04-1afd-4be3-ae88-cd20349b3a04"),
                MainText = "How does your organization determine what specific skills are needed?",
                Order = 1,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("572eceb1-c984-49e2-b3c9-c43098c3c32d"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("897d472d-9fed-4300-8d94-0d17d8aabec9"),
                MainText = "How does your organization measure the competency of potential candidates?",
                Order = 2,
                QuestionType = QuestionType.Text,
                ParentId = Guid.Parse("572eceb1-c984-49e2-b3c9-c43098c3c32d"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("cf96d715-36ba-434a-92b2-8568ce09d148"),
                MainText = "What tools or venues does your organization use to find prospective employees?",
                OptionChoices = "Universities,Job fairs,Online,Recruitment agencies,We do not explicitly recruit",
                OptionsOrientation = OptionsOrientation.Vertical,
                Order = 3,
                QuestionType = QuestionType.SelectMultiple,
                ParentId = Guid.Parse("572eceb1-c984-49e2-b3c9-c43098c3c32d"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              },
              new Question
              {
                Id = Guid.Parse("17142a0c-d2ee-4767-ab1f-0cff0a8cb7bb"),
                MainText = "What fraction of applicants to IT Security staff roles are female?",
                Order = 4,
                QuestionType = QuestionType.Percentage,
                ParentId = Guid.Parse("572eceb1-c984-49e2-b3c9-c43098c3c32d"),
                SubcategoryId = subcategoryId,
                CreatedBy = createdBy,
                CreatedTimestamp = currentTimestamp
              }
            };
            results.AddRange(workforceManagementQuestions);

            #endregion

            return results.ToArray();
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.