using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.TabiriDatabaseEntities.TabiriQuestions
{
    public static class TabiriDataProtectionQuestions
    {
        public static Question[] Seed(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Empty;

            var results = new List<Question>();

            #region Data Protection Framework Questions

            subcategoryId = Guid.Parse("ec0ed13d-e887-41e2-a0cd-62fd1ca2de25");

            var dataProtectionFrameworkQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("124b9db2-bd87-4cf1-8dbd-9542d0aab308"),
                    MainText = "Does your organization have a Data Protection framework?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a3cb78e6-ed37-4c92-97a7-ab22c5016282"),
                    MainText = "Which department is tasked with oversight of this framework?",
                    HintText = "e.g. Legal, Risk, IT, a joint team from these departments",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("124b9db2-bd87-4cf1-8dbd-9542d0aab308"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("8d1a65cf-2296-4e6e-ac38-0695710daad2"),
                    MainText = "What are the specific roles and responsibilities of the oversight members?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("124b9db2-bd87-4cf1-8dbd-9542d0aab308"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ac4b7403-de1d-4ec8-af65-5bd3800dd39a"),
                    MainText = "Which data protection topics does this framework cover?	",
                    OptionChoices = "Data Classification,Data Loss,Data Protection,Data Recovery,Data Retention",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 3,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("124b9db2-bd87-4cf1-8dbd-9542d0aab308"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("306e550b-f38e-4054-b358-031d4d78b5ba"),
                    MainText = "Is this framework based on a specific industry standard?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("124b9db2-bd87-4cf1-8dbd-9542d0aab308"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("86e34d4a-a377-4c62-b452-97488ce117ca"),
                    MainText = "Which industry standard is it based on? {e.g. NIST 800-53, ISO/IEC 27001}",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("306e550b-f38e-4054-b358-031d4d78b5ba"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("5597df7f-f46f-47f2-9f24-ade275c2501d"),
                    MainText = "Is this framework regularly reviewed and updated?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("124b9db2-bd87-4cf1-8dbd-9542d0aab308"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("99ebb617-f7de-4264-bd67-3790c31d92ee"),
                    MainText = "How often?",
                    OptionChoices = "Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("5597df7f-f46f-47f2-9f24-ade275c2501d"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("63793017-fb92-4ed4-a326-184df425ee8b"),
                    MainText = "When was the most recent review?",                    
                    Order = 2,
                    QuestionType = QuestionType.Date,
                    ParentId = Guid.Parse("5597df7f-f46f-47f2-9f24-ade275c2501d"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("3362d185-e4c5-425c-a62b-f09431066de0"),
                    MainText = "Has your organization documented the types of data it processes?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("fbfa97cb-7f2c-417c-a043-73c9c45c167d"),
                    MainText = "What types of data does your organization process?",
                    HintText = "e.g. financial, PCI, PHI, PII, SCADA/ICS, intellectual property",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("3362d185-e4c5-425c-a62b-f09431066de0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("abebcc43-0774-4a6d-9a7d-0fb6794e49b6"),
                    MainText = "Is this data tracked across the organization?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("3362d185-e4c5-425c-a62b-f09431066de0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2d84eedc-fc83-42b9-8966-aa534c3f2617"),
                    MainText = "How is this data tracked across the organization?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("abebcc43-0774-4a6d-9a7d-0fb6794e49b6"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("40bcc217-fb01-4289-ab62-3cde082f92f6"),
                    MainText = "Has your organization defined data owners for each data type?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("06a08233-58f2-4735-8278-c5d1f81f5347"),
                    MainText = "Who are the data owners for each data type?",
                    HintText = "e.g. specific departments, individuals or multiple owners",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("40bcc217-fb01-4289-ab62-3cde082f92f6"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(dataProtectionFrameworkQuestions);

            #endregion

            #region Data Classification Questions

            subcategoryId = Guid.Parse("8859dd17-94e3-433d-90d8-2fcf3cffaac9");

            var dataClassificationQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("df3b05fd-3ebe-4c01-8d7f-ed82b3a79078"),
                    MainText = "Does your organization have a Data Classification policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f6ad64e7-3c92-465f-a342-d14ad525afbe"),
                    MainText = "What are the existing classification levels?",
                    HintText = "e.g. Secret, Confidential, Internal Use Only, Public",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("df3b05fd-3ebe-4c01-8d7f-ed82b3a79078"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2fc8d385-da5d-4897-8c36-92ef72d39fb4"),
                    MainText = "What is the procedure for classifying data across business units?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("df3b05fd-3ebe-4c01-8d7f-ed82b3a79078"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e63d8dea-17f0-4919-9706-60adcc2cd322"),
                    MainText = "Who is tasked with this procedure across business units?",                    
                    Order = 3,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("df3b05fd-3ebe-4c01-8d7f-ed82b3a79078"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1e44b051-2d11-4780-90db-f595aa653c7c"),
                    MainText = "Does your organization have data handling guidelines?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e868ccbd-9e0e-45ed-8ba1-ae399fc5364c"),
                    MainText = "Are the respective data owners aware of these guidelines?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1e44b051-2d11-4780-90db-f595aa653c7c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("cb898375-482d-4365-ab10-0abf137ece89"),
                    MainText = "In what format are these guidelines communicated to data owners?",
                    OptionChoices = "Email notes,Team meeting,Online portal",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("e868ccbd-9e0e-45ed-8ba1-ae399fc5364c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("3d8f8ec5-61b3-41f3-8f8b-164fb8a518bd"),
                    MainText = "Are these data handling guidelines enforced?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1e44b051-2d11-4780-90db-f595aa653c7c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("341c418d-2594-41d5-8122-90d65b8c9b60"),
                    MainText = "How are these guidelines enforced?",
                    HintText = "e.g. manual data marking, automated DLP tool",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("3d8f8ec5-61b3-41f3-8f8b-164fb8a518bd"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("236d1762-f238-435c-9196-7c589098eeb5"),
                    MainText = "Is enforcement mapped to existing access control policies?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("3d8f8ec5-61b3-41f3-8f8b-164fb8a518bd"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ca83b99a-ddbd-495f-b955-d5cccfa65ed3"),
                    MainText = "Is enforcement consistent across all business units?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("3d8f8ec5-61b3-41f3-8f8b-164fb8a518bd"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("38dd0026-e212-4a67-87e9-1b91af872ab5"),
                    MainText = "Are data classification reviews conducted?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0aaf13ce-fd00-4c66-8484-807806098e46"),
                    MainText = "How often?",
                    OptionChoices = "Monthly,Quarterly,Semi-annually,Annually,Other",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("38dd0026-e212-4a67-87e9-1b91af872ab5"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("59c98d83-99b6-47e2-91bb-b9f2a131af37"),
                    MainText = "How are these reviews carried out?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("38dd0026-e212-4a67-87e9-1b91af872ab5"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(dataClassificationQuestions);

            #endregion

            #region Data Protection Questions

            subcategoryId = Guid.Parse("ed613e1b-6a1a-4d6b-8efc-216f9520226a");

            var dataProtectionPoliciesQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("46753849-c6c1-41af-9517-db432c8ba05f"),
                    MainText = "Does your organization have procedures in place to protect data-at-rest?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2ae3701b-4895-4de2-8cfb-99547ae39be6"),
                    MainText = "Are there encryption requirements for data-at-rest?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("46753849-c6c1-41af-9517-db432c8ba05f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("cb974c82-229d-46a7-979c-413e0ecee129"),
                    MainText = "What types of data do these requirements apply to?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("2ae3701b-4895-4de2-8cfb-99547ae39be6"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("49357197-9dd5-407e-b07a-abf68f5cf70e"),
                    MainText = "Are data owners aware of these requirements? ",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("2ae3701b-4895-4de2-8cfb-99547ae39be6"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7a97b90b-954f-4801-84b4-6e3ef299cd89"),
                    MainText = "Are there encryption standards for data-at-rest?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("46753849-c6c1-41af-9517-db432c8ba05f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("80106f97-568b-448c-b3be-1aead6ec4d88"),
                    MainText = "What cryptographic algorithms are used for encrypting data-at-rest?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("7a97b90b-954f-4801-84b4-6e3ef299cd89"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("15e634f8-8cfa-48d2-90c4-18e71e5bee24"),
                    MainText = "Are data owners aware of these standards?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("7a97b90b-954f-4801-84b4-6e3ef299cd89"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7f71baf5-7246-4046-8166-eed2d2ef95fc"),
                    MainText = "Is there a procedure for encrypting files on disk?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("46753849-c6c1-41af-9517-db432c8ba05f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("9ad0700d-900c-4e6d-adf8-8c02ed00792e"),
                    MainText = "What specific solutions are used for encrypting files on disk?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("7f71baf5-7246-4046-8166-eed2d2ef95fc"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ab8b1037-54f0-4039-9cf9-3e29176d0742"),
                    MainText = "Are data owners aware of this procedure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("7f71baf5-7246-4046-8166-eed2d2ef95fc"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e536d7e3-47f1-45ac-add5-4ab4261af115"),
                    MainText = "Is full disk encryption required for mobile devices and removable media?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("46753849-c6c1-41af-9517-db432c8ba05f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7a142b48-2045-4dbd-9773-1d7f254c68c5"),
                    MainText = "What specific full disk encryption solutions are used?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("e536d7e3-47f1-45ac-add5-4ab4261af115"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("eaf9f081-0440-4850-802b-b4a52469c987"),
                    MainText = "Are data owners aware of these solutions?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("e536d7e3-47f1-45ac-add5-4ab4261af115"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("6c849b14-e5e4-47ee-bd74-052587a81390"),
                    MainText = "Is there a procedure for encrypting databases?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("46753849-c6c1-41af-9517-db432c8ba05f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("af031e99-a097-4627-8f42-37cbad0bd1eb"),
                    MainText = "What specific solutions are used for encrypting databases?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("6c849b14-e5e4-47ee-bd74-052587a81390"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c3ff2dbc-2c23-41ac-947b-8f0705f9ac98"),
                    MainText = "Are data owners aware of these solutions?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("6c849b14-e5e4-47ee-bd74-052587a81390"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("5090606d-0038-4efa-89f6-f7ea81bc5c40"),
                    MainText = "Is there a procedure for managing encryption recovery keys?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("46753849-c6c1-41af-9517-db432c8ba05f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("756ef5c0-c530-461b-9ee9-3665d183b2d5"),
                    MainText = "What is the procedure for managing encryption recovery keys?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("5090606d-0038-4efa-89f6-f7ea81bc5c40"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f5f5b15a-0c86-4320-8a8d-186cc9ab825d"),
                    MainText = "Are data owners aware of this procedure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("5090606d-0038-4efa-89f6-f7ea81bc5c40"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("875d7c1d-1e76-4a7c-95f3-8dbbdd674f45"),
                    MainText = "Are reviews of data-at-rest procedures conducted?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 7,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("46753849-c6c1-41af-9517-db432c8ba05f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("9224008a-081d-4c79-8b8a-9e5b25315d06"),
                    MainText = "How often?",
                    OptionChoices = "Monthly,Quarterly,Semi-annually,Annually,Other",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("875d7c1d-1e76-4a7c-95f3-8dbbdd674f45"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },       
                new Question 
                {
                    Id = Guid.Parse("5a6e0ba2-56a7-4434-813c-e7e1fa7ba746"),
                    MainText = "In what format?",
                    OptionChoices = "Email notes, Team meeting, Online portal",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2, 
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("875d7c1d-1e76-4a7c-95f3-8dbbdd674f45"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b282e277-4060-46b0-a6b9-8e492886f531"),
                    MainText = "Does your organization have procedures in place to protect data-in-motion or use?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0991d117-9612-4462-8e72-6ca12aac9cd5"),
                    MainText = "Are there encryption requirements for data-in-motion?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b282e277-4060-46b0-a6b9-8e492886f531"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("5963ac2a-6cee-452a-9954-2c574057e8f7"),
                    MainText = "What types of data do these requirements apply to?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("0991d117-9612-4462-8e72-6ca12aac9cd5"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("67ee7b00-9bdd-401e-a5a6-9b99706b844c"),
                    MainText = "Are data owners aware of these requirements?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("0991d117-9612-4462-8e72-6ca12aac9cd5"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7249ec46-2f8e-49b0-91a1-3636aa34eb6a"),
                    MainText = "Are there encryption standards for data-in-motion?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b282e277-4060-46b0-a6b9-8e492886f531"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f120f352-6544-4f20-b07c-922483ec2b7f"),
                    MainText = "What cryptographic algorithms are used for encrypting data-in-motion?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("7249ec46-2f8e-49b0-91a1-3636aa34eb6a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("077d6075-491c-4c07-bd9e-c833a895ff8c"),
                    MainText = "Are data owners aware of these standards?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("7249ec46-2f8e-49b0-91a1-3636aa34eb6a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2184ab10-fd9d-4319-aa05-c787aaa35d32"),
                    MainText = "Is there a procedure for encrypting email communications?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b282e277-4060-46b0-a6b9-8e492886f531"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e57815bc-7cf0-4780-a258-ea27bc0c69c1"),
                    MainText = "What specific solutions are used for encrypting email communications?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId =  Guid.Parse("2184ab10-fd9d-4319-aa05-c787aaa35d32"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1491f891-e525-4360-bcb6-174f85b117e7"),
                    MainText = "Are data owners aware of this procedure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId =  Guid.Parse("2184ab10-fd9d-4319-aa05-c787aaa35d32"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b464ff8f-51da-49fa-ae36-301aca22f4ea"),
                    MainText = "Is there a procedure for encrypting external network communications?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b282e277-4060-46b0-a6b9-8e492886f531"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("95879e40-54d9-4306-943e-ef3620cc26a2"),
                    MainText = "What specific solutions are used for encrypting external network communications?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("b464ff8f-51da-49fa-ae36-301aca22f4ea"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a626763f-a96e-4167-a39c-ec55ad5fded1"),
                    MainText = "Are data owners aware of this procedure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b464ff8f-51da-49fa-ae36-301aca22f4ea"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("5d5b4bc7-8335-4744-80da-dfb7959a8f93"),
                    MainText = "Is there a procedure for encrypting internal network communications?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b282e277-4060-46b0-a6b9-8e492886f531"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("5aae043c-84f3-4cf1-b1e5-4e238250415c"),
                    MainText = "What specific solutions are used for encrypting internal network communications?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("5d5b4bc7-8335-4744-80da-dfb7959a8f93"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("eb778c03-d5b6-4b1a-9929-a941ef125da2"),
                    MainText = "Are data owners aware of this procedure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("5d5b4bc7-8335-4744-80da-dfb7959a8f93"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("5c9976ee-474c-42d4-a872-890d64f9ebdb"),
                    MainText = "Are reviews of data-in-motion procedures conducted?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b282e277-4060-46b0-a6b9-8e492886f531"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a5d516db-1c99-4aec-b69f-b5ceb1af6622"),
                    MainText = "How often?",
                    OptionChoices = "Monthly,Quarterly,Semi-annually,Annually,Other",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("5c9976ee-474c-42d4-a872-890d64f9ebdb"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d4d28303-64f2-4103-9d22-8aed96d9da30"),
                    MainText = "How are these reviews carried out?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("5c9976ee-474c-42d4-a872-890d64f9ebdb"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(dataProtectionPoliciesQuestions);

            #endregion

            #region Data Retention Questions

            subcategoryId = Guid.Parse("b679ff1a-2121-45de-a695-9517c3b5319e");

            var dataRetentionQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("53ddada1-bd5a-4263-a43a-801bdcda5fcd"),
                    MainText = "Does your organization have a Data Retention policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("fa63a781-2cdf-4938-8df4-f98a0a096ec4"),
                    MainText = "Is this policy based on specific regulatory requirements?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("53ddada1-bd5a-4263-a43a-801bdcda5fcd"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("9d7ad020-9de7-4898-a770-6169c95d2ec7"),
                    MainText = "What specific regulatory requirements?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("fa63a781-2cdf-4938-8df4-f98a0a096ec4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id =Guid.Parse("b03d2d7b-a9c8-4f16-acdc-219ab9484166"),
                    MainText = "What types of data are retained?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("53ddada1-bd5a-4263-a43a-801bdcda5fcd"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7ece6cee-c470-44b9-a266-ed7bd21e9017"),
                    MainText = "Are data owners aware of the retention requirements?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("53ddada1-bd5a-4263-a43a-801bdcda5fcd"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("53cfa881-7ebc-40e5-9794-87aef33e51cf"),
                    MainText = "What training is provided to those tasked with data retention?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("7ece6cee-c470-44b9-a266-ed7bd21e9017"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("9614e115-3517-475b-9212-c942353a1390"),
                    MainText = "Is this policy reviewed?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("53ddada1-bd5a-4263-a43a-801bdcda5fcd"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("10b98244-1f29-4452-8ba6-5bce201d4815"),
                    MainText = "How often?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("9614e115-3517-475b-9212-c942353a1390"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2126fb28-65af-4128-9a7e-545a810b0476"),
                    MainText = "Does your organization have a procedure for securing retained data?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2340fcd6-d1fa-47fc-a8b1-1a09288ecc10"),
                    MainText = "Is data retained on the same systems that produced it?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("2126fb28-65af-4128-9a7e-545a810b0476"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("be462b8b-607d-47c7-9a81-71219ee47a3b"),
                    MainText = "Are the data protection controls on the data retention system similar to the ones on the data production system?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("2340fcd6-d1fa-47fc-a8b1-1a09288ecc10"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("afcb8cd4-652c-43c1-bd48-dd92f44cf9b4"),
                    MainText = "Does your organization have guidelines on data/media disposal?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("3bc7f375-694e-431d-8810-2c2c4fd368ce"),
                    MainText = "How is data that should be disposed of identified?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("afcb8cd4-652c-43c1-bd48-dd92f44cf9b4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b5f8590a-e7ff-4b4c-8fae-3741dca62856"),
                    MainText = "Are data owners aware of the data/media disposal guidelines?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("afcb8cd4-652c-43c1-bd48-dd92f44cf9b4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("8c3f8543-ea25-436a-9683-6407836fc77e"),
                    MainText = "What training is provided to those tasked with data/media disposal?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("b5f8590a-e7ff-4b4c-8fae-3741dca62856"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("21285455-f6a1-418a-b519-a982e44436c6"),
                    MainText = "Is there a procedure for securely erasing data?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("afcb8cd4-652c-43c1-bd48-dd92f44cf9b4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d6eb7c96-1059-4a62-94e1-4052fbb40c97"),
                    MainText = "What is the procedure for securely erasing data on internal hard drives?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("21285455-f6a1-418a-b519-a982e44436c6"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("795161e9-382b-4754-8d69-a0c00ca5f2e4"),
                    MainText = "What is the procedure for securely erasing data on removable media?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("21285455-f6a1-418a-b519-a982e44436c6"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b7dd75f7-bdd1-4551-854c-caa60513a85c"),
                    MainText = "What is the procedure for securely erasing data on mobile devices?",                    
                    Order = 3,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("21285455-f6a1-418a-b519-a982e44436c6"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b56bc038-f14c-4d38-99d3-493e1d6a35f3"),
                    MainText = "Is there a post-disposal certification process?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("afcb8cd4-652c-43c1-bd48-dd92f44cf9b4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2cbe826e-23b9-4095-988f-d3f9aad61c0e"),
                    MainText = "What is the process for each media type?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("b56bc038-f14c-4d38-99d3-493e1d6a35f3"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("701b360c-7de6-4ca9-bcf9-c655c981b633"),
                    MainText = "Are these guidelines reviewed?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("afcb8cd4-652c-43c1-bd48-dd92f44cf9b4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("164b6c52-8c99-4d92-b722-7e5f10b3bd1c"),
                    MainText = "How often?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("701b360c-7de6-4ca9-bcf9-c655c981b633"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(dataRetentionQuestions);

            #endregion

            #region Data Loss Questions

            subcategoryId = Guid.Parse("95bfad59-1224-414a-8c51-d688abac70ed");

            var dataLossQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("fc1dd8c4-5971-45a6-b2a2-e055119c6ac6"),
                    MainText = "Does your organization have a Data Loss policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("5ed54934-f7a5-4684-b433-0efbb9668ed0"),
                    MainText = "Does this policy include a breach notification procedure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("fc1dd8c4-5971-45a6-b2a2-e055119c6ac6"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1a447d95-4e41-419a-94fd-93b34461d88b"),
                    MainText = "Who is responsible for notifying the relevant parties?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("5ed54934-f7a5-4684-b433-0efbb9668ed0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("0984dbfd-5732-48c7-a0bb-580eadd78209"),
                    MainText = "Who are the notified parties?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("5ed54934-f7a5-4684-b433-0efbb9668ed0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d61b1849-d887-4432-a157-280f5acc6e62"),
                    MainText = "What is the timeframe for notification in days?",                    
                    Order = 3,
                    QuestionType = QuestionType.Integer,
                    ParentId = Guid.Parse("5ed54934-f7a5-4684-b433-0efbb9668ed0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("edbd17d2-9614-424d-a554-1c601d292152"),
                    MainText = "Is your organization required to notify specific regulators?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("5ed54934-f7a5-4684-b433-0efbb9668ed0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0f707840-0229-4638-a012-68f9b3b86248"),
                    MainText = "Which specific regulators is your organization required to notify?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("edbd17d2-9614-424d-a554-1c601d292152"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("008e6273-7ec1-49a0-a353-868e2c0ad65f"),
                    MainText = "Are data owners aware of this data loss policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("fc1dd8c4-5971-45a6-b2a2-e055119c6ac6"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("63d6a1f1-d7cb-49ef-84e1-46e53ed7ef35"),
                    MainText = "What training is provided to data owners?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("008e6273-7ec1-49a0-a353-868e2c0ad65f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0adb29d9-d98b-4db7-ab8d-75c777bc39eb"),
                    MainText = "How often is this policy reviewed?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("fc1dd8c4-5971-45a6-b2a2-e055119c6ac6"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("37fb9a68-b075-4ade-9556-54141cd45a9c"),
                    MainText = "Is there a procedure for identifying intentional or unintentional data loss?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f44bf989-a448-4407-b6a4-3bf02a8ba481"),
                    MainText = "Do you have any technologies in place to identify intentional or unintentional data loss?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("37fb9a68-b075-4ade-9556-54141cd45a9c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("820792ea-40d5-40cd-bddf-c269e7f17c48"),
                    MainText = "What kinds of technologies do you have in place?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("f44bf989-a448-4407-b6a4-3bf02a8ba481"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1e7a2c85-bbfa-4c99-8a5d-3c48fdf2f0d4"),
                    MainText = "What is the coverage of these technologies?",
                    OptionChoices = "Critical systems,Critical & all production systems,Critical production & test systems",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f44bf989-a448-4407-b6a4-3bf02a8ba481"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("3eaf0e0f-0584-4207-9139-3c4fb814833e"),
                    MainText = "Does your organization have cyber insurance?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },     
                new Question 
                {
                    Id = Guid.Parse("44301316-2a2e-498b-8428-92acc0b5f37c"),
                    MainText = "What is this insurance policy’s coverage limit (in USD)?",                    
                    Order = 1,
                    QuestionType = QuestionType.Currency,
                    ParentId = Guid.Parse("3eaf0e0f-0584-4207-9139-3c4fb814833e"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },  
                new Question 
                {
                    Id = Guid.Parse("4b774dd9-0e9e-487e-8945-d7d06ef4a374"),
                    MainText = "What types of First-Party Coverages are included in this insurance policy?",
                    OptionChoices = "Business interruption (i.e. loss of income and extra expenses),Computer systems replacement costs,Crisis services costs (e.g. notification; credit and monitoring services; breach coach etc),Cyber extortion (e.g. ransomware),Damage to your organization’s reputation, Loss or damage to electronic data, None, Other",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    ActiveOptionIndex = 7,
                    Order = 2,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("3eaf0e0f-0584-4207-9139-3c4fb814833e"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },               
                new Question 
                {
                    Id = Guid.Parse("a6f2798e-202f-4cca-ab50-64fbe80c51bb"),
                    MainText = "Enter the other type of First-Party Coverage.",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId =Guid.Parse("4b774dd9-0e9e-487e-8945-d7d06ef4a374"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0b814aec-daea-4003-8883-a32135e61682"),
                    MainText = "What types of Third-Party Coverages are included in this insurance policy?",
                    OptionChoices = "Electronic media liability,Network security liability,Privacy liability,Regulatory proceedings and penalties, None, Other",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    ActiveOptionIndex = 5,
                    Order = 3,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("3eaf0e0f-0584-4207-9139-3c4fb814833e"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },    
                new Question 
                {
                    Id = Guid.Parse("a547c6f8-1d7c-4515-b982-cf4e8f0c5dfb"),
                    MainText = "Enter the other type of Third-Party Coverage.",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("0b814aec-daea-4003-8883-a32135e61682"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },                    
                new Question 
                {
                    Id = Guid.Parse("cff374e4-5469-4f43-bfce-2c9b7e3e8423"),
                    MainText = "Does this insurance policy have sub-limits?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("3eaf0e0f-0584-4207-9139-3c4fb814833e"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("93732758-4299-4b5f-9897-01677326bfa6"),
                    MainText = "Which types of coverage have sub-limits?",
                    OptionChoices = "Business interruption (i.e. loss of income and extra expenses),Crisis services costs (e.g. notification; credit and monitoring services; breach coach etc),Cyber extortion (e.g. ransomware),Computer systems replacement costs,Damage to your organization’s reputation,Electronic media liability,Loss or damage to electronic data,Network security liability,Privacy liability,Regulatory proceedings and penalties, Other",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    ActiveOptionIndex = 10,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("cff374e4-5469-4f43-bfce-2c9b7e3e8423"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("69015861-6acc-48f2-8c2a-6b90de611a3b"),
                    MainText = "Enter the other type of coverage that has sub-limits.",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("93732758-4299-4b5f-9897-01677326bfa6"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(dataLossQuestions);

            #endregion

            #region Data Recovery

            subcategoryId = Guid.Parse("673d41d1-e017-4b8a-ad8e-a0992882f385");

            var dataRecoveryQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("1b4e1742-9ce4-4240-a864-994a514c4bff"),
                    MainText = "Does your organization have a Data Recovery policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("934c41a0-fb99-4338-8010-f07aabf095ec"),
                    MainText = "Does the policy include a Disaster Recovery Plan (DRP)?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1b4e1742-9ce4-4240-a864-994a514c4bff"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1432d3cf-51be-4e58-bddc-d14655dcdea2"),
                    MainText = "What data types are covered by the data recovery procedure in the DRP?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("934c41a0-fb99-4338-8010-f07aabf095ec"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f530bdc3-6a49-41f0-bb02-0a4f71d987ca"),
                    MainText = "Are data owners aware of the data recovery procedure in the DRP?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("934c41a0-fb99-4338-8010-f07aabf095ec"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("195f661e-3de5-438e-bd0a-4ecf7ea1d007"),
                    MainText = "What training is provided to those tasked with data recovery?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("f530bdc3-6a49-41f0-bb02-0a4f71d987ca"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("69f14321-a629-46c0-ac9e-d6c5fcd415b5"),
                    MainText = "How often is this policy and DRP reviewed?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1b4e1742-9ce4-4240-a864-994a514c4bff"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b01522f3-05f7-49c8-8f67-e764228fa707"),
                    MainText = "Are data backups performed on a regular basis?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("bcba7242-5e79-4190-844d-5bbbcbf73732"),
                    MainText = "What is the backup schedule?",
                    OptionChoices = "Monthly,Weekly,Daily",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b01522f3-05f7-49c8-8f67-e764228fa707"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7b03b9d5-ae6c-4bad-aa9c-919abd38af37"),
                    MainText = "What is the backup type?",
                    OptionChoices = "Incremental,Full",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b01522f3-05f7-49c8-8f67-e764228fa707"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("32c24e4f-2d0e-4d27-beb3-292ee8b55caf"),
                    MainText = "Are backups of sensitive data encrypted?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b01522f3-05f7-49c8-8f67-e764228fa707"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("4b7e926a-f42d-4244-b652-0f89dfabf76b"),
                    MainText = "How is the sensitivity of the data determined?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("32c24e4f-2d0e-4d27-beb3-292ee8b55caf"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("9bcf1c63-023c-46c8-b3d4-5bb70db8665c"),
                    MainText = "How is this sensitive data encrypted?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("32c24e4f-2d0e-4d27-beb3-292ee8b55caf"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("5cef1d46-d26d-428f-839a-6fcbb0e9ffc2"),
                    MainText = "Where are data backups stored?",
                    OptionChoices = "Local backup,Remote site,Local & remote",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b01522f3-05f7-49c8-8f67-e764228fa707"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f8d24563-a308-4447-9493-31df179fdf91"),
                    MainText = "Has your organization identified any critical systems requiring redundancy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("81d39ff4-3bd5-4f24-baf8-5b98e2ef93c0"),
                    MainText = "What critical systems currently have redundant systems?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("f8d24563-a308-4447-9493-31df179fdf91"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("33c5f881-d159-4bef-acfe-092d6afec404"),
                    MainText = "How were those systems identified?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("f8d24563-a308-4447-9493-31df179fdf91"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e15bf540-89d7-4829-a733-c450ce351e1e"),
                    MainText = "Have you implemented real-time failover for the redundant systems?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f8d24563-a308-4447-9493-31df179fdf91"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("45dd10ff-9ea4-49df-b83a-57c827c19533"),
                    MainText = "Does your organization test data backups to ensure you can recover data properly?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7ac2629f-c9f1-489d-8353-36fd4e0de904"),
                    MainText = "What were the results of the last test?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("45dd10ff-9ea4-49df-b83a-57c827c19533"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("abf591ca-12f2-4e14-a5e4-79a62294e3d3"),
                    MainText = "How often are these tests conducted?",
                    OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,          
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("45dd10ff-9ea4-49df-b83a-57c827c19533"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(dataRecoveryQuestions);

            #endregion

            return results.ToArray();
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.