using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.TabiriDatabaseEntities.TabiriQuestions
{
    public static class TabiriIncidentResponseQuestions
    {
        public static Question[] Seed(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Empty;

            var results = new List<Question>();

            #region Incident Readiness

            subcategoryId = Guid.Parse("02a82cba-a900-4ca0-aa4e-810065e4c33c");

            var incidentReadinessQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("3346fc21-56fd-44cd-94f4-1ab7d73d85fe"),
                    MainText = "Does your organization have an Incident Response Plan (IRP)?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("31d73a60-0b03-4c12-aaa9-7d6a16f66b11"),
                    MainText = "What CERT/CIRT/SOC roles are defined in the IRP?",                    
                    HintText = "Computer Emergency/Incident Response Team, Security Operations Center",
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("3346fc21-56fd-44cd-94f4-1ab7d73d85fe"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }, 
                new Question
                {
                    Id = Guid.Parse("18490970-1967-40dd-b3f2-222683c57a8a"),
                    MainText = "Does the IRP document requirements for dealing with external parties?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("3346fc21-56fd-44cd-94f4-1ab7d73d85fe"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("db5035c4-2f49-4b7f-b75d-7bced43e6980"),
                    MainText = "Which requirements for dealing with external parties have been documented?",
                    OptionChoices = "Digital forensics services,Law enforcement notification,Legal services,Public/media relations",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("18490970-1967-40dd-b3f2-222683c57a8a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("c5accaf9-b9ec-4907-aacf-c1deaa9469ff"),
                    MainText = "Do you have existing retainers for these services?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("18490970-1967-40dd-b3f2-222683c57a8a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("f09de035-862a-484e-aa38-be498e5b827b"),
                    MainText = "Does the IRP document an incident reporting structure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("3346fc21-56fd-44cd-94f4-1ab7d73d85fe"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("d36a24ab-b66a-486c-a27c-c3e4eee39243"),
                    MainText = "Does the IRP document incident escalation procedures?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("3346fc21-56fd-44cd-94f4-1ab7d73d85fe"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("6b9c7fbb-0349-40db-8a83-b2cbb6137eea"),
                    MainText = "Does the IRP document a process for mandatory leave when investigating internal staff?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("3346fc21-56fd-44cd-94f4-1ab7d73d85fe"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("6f28c58f-719f-48b6-a471-e25b06f04bb2"),
                    MainText = "How often is this policy reviewed?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("3346fc21-56fd-44cd-94f4-1ab7d73d85fe"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("92d533e7-12a7-4156-8b0f-ceb03a85207e"),
                    MainText = " Does your organization have a CERT/CIRT/SOC team?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("989e8a86-c037-46ea-860f-ddc0aa8f9158"),
                    MainText = "Do you have tiered security analyst roles?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("92d533e7-12a7-4156-8b0f-ceb03a85207e"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c982b8b8-9e70-451e-8fe9-a4ddb5e4a028"),
                    MainText = "Which tiered security analyst roles do have you defined?",
                    OptionChoices = "Level 1, Level 2, Level 3",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("989e8a86-c037-46ea-860f-ddc0aa8f9158"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("6ca6715a-e3b6-41e3-abb0-5979e11c4ba5"),
                    MainText = "Which tiered security analyst roles are internal?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("989e8a86-c037-46ea-860f-ddc0aa8f9158"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question {
                    Id = Guid.Parse("9ccbb867-8d09-4dd7-9461-2e14336fad65"),
                    MainText = "Which tiered security analyst roles are outsourced?",                    
                    Order = 3,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("989e8a86-c037-46ea-860f-ddc0aa8f9158"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("449504ca-d05a-45e9-8a94-bee27681f250"),
                    MainText = "Does the IRP have a 24/7 staffing plan for the CERT/CIRT/SOC team?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("92d533e7-12a7-4156-8b0f-ceb03a85207e"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("df5e6243-74e0-4e07-90c0-9a0bc28a9bbd"),
                    MainText = "How many CERT/CIRT/SOC team members are staffed on each shift?",                    
                    Order = 1,
                    QuestionType = QuestionType.Integer,
                    ParentId = Guid.Parse("449504ca-d05a-45e9-8a94-bee27681f250"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("fe74c0a5-0816-4651-b372-ef6c32f22fad"),
                    MainText = "How many shifts do you have per 24-hour period?",                    
                    Order = 2,
                    QuestionType = QuestionType.Integer,
                    ParentId = Guid.Parse("449504ca-d05a-45e9-8a94-bee27681f250"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ef7d251b-2f4c-4f4c-a329-66f2e5228904"),
                    MainText = "Are members of the CERT/CIRT/SOC team aware of their assigned roles?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("92d533e7-12a7-4156-8b0f-ceb03a85207e"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1682ea91-8a3f-4bb7-b72c-9eaf8aef1106"),
                    MainText = "Have team members been trained in their respective roles?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("ef7d251b-2f4c-4f4c-a329-66f2e5228904"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e72aed93-7a99-4648-b735-f0a0b47f3a9d"),
                    MainText = "How often is training provided?",
                    OptionChoices = "Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1682ea91-8a3f-4bb7-b72c-9eaf8aef1106"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("6eeeb6c4-aeda-4294-945c-a87db2ae9ed2"),
                    MainText = "Are any members of the CERT/CIRT/SOC team certified?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("ef7d251b-2f4c-4f4c-a329-66f2e5228904"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("bf120885-2554-47c8-a747-a8681f17cdb0"),
                    MainText = "What industry certifications do members of the CERT/CIRT/SOC team possess?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("6eeeb6c4-aeda-4294-945c-a87db2ae9ed2"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1ed3a713-d751-4213-9007-9f23f87d983d"),
                    MainText = "Is there a process to review CERT/CIRT/SOC team members performance?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("92d533e7-12a7-4156-8b0f-ceb03a85207e"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b2c8b265-e9f8-4c7c-8a0f-5857d460d78c"),
                    MainText = "What does this process look like?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("1ed3a713-d751-4213-9007-9f23f87d983d"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question {
                    Id = Guid.Parse("0c74d061-007c-4219-8a2c-df999a198b24"),
                    MainText = "Do you conduct periodic incident response drill exercises?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("16186a48-6203-42f8-88fb-5626a31f3868"),
                    MainText = "What kind of incident response drills do you conduct?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("0c74d061-007c-4219-8a2c-df999a198b24"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f440b58f-6aea-487c-9caf-c0121911c14e"),
                    MainText = "How often are these incident response drills conducted?",
                    OptionChoices = "Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("0c74d061-007c-4219-8a2c-df999a198b24"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("faef1b37-11a2-4824-a573-02dc973b644b"),
                    MainText = "Do you participate in any cyber threat information sharing groups or associations?",
                    HintText = "e.g. an industry specific Information Sharing and Analysis Center",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("09936fc8-55fd-4fec-b994-edfedf949166"),
                    MainText = "Which cyber threat information sharing groups or associations do you participate in?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("faef1b37-11a2-4824-a573-02dc973b644b"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("87399404-4df9-4c42-b60c-f281bad67e38"),
                    MainText = "Do the information sharing groups or associations provide actionable threat intelligence information?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("faef1b37-11a2-4824-a573-02dc973b644b"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(incidentReadinessQuestions);

            #endregion

            #region Incident Detection

            subcategoryId = Guid.Parse("3b500510-0aa2-44bc-9600-f49ad7208113");

            var incidentDetectionQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("5f936c16-7358-4e86-9cde-84cac2468df7"),
                    MainText = "Does your organization have an internal Security Operations Center or Managed Security Service Provider?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a325d1a3-b0d2-4ecc-8139-48fb0cd14ca3"),
                    MainText = "Has your organization handled any cyber security incidents in the last 12 months?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("83047ec8-2589-43ee-8934-b21cbb7e2f17"),
                    MainText = "What kind of cyber security incidents?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("a325d1a3-b0d2-4ecc-8139-48fb0cd14ca3"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("73ac391e-8726-4fd1-9e9c-f2e6ee8c0d55"),
                    MainText = "Do you (or your managed security service provider) have a SIEM capability?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e3447f51-f701-40b2-9e09-269847015a9a"),
                    MainText = "What logs does the SIEM collect?",
                    OptionChoices = "Application,DHCP,DNS,Endpoint,Firewall,Netflow,NIDS,VPN,Web proxy",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("73ac391e-8726-4fd1-9e9c-f2e6ee8c0d55"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("bedf0276-3d99-48ba-9ea7-d7fdb824e861"),
                    MainText = "What is your maximum log retention period?",
                    OptionChoices = "1 day,1 week,1 month,6 months,1+ year",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("73ac391e-8726-4fd1-9e9c-f2e6ee8c0d55"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0c44847b-b493-4456-a5cc-295936c9e916"),
                    MainText = "Are cyber threat intelligence feeds integrated into the SIEM?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("73ac391e-8726-4fd1-9e9c-f2e6ee8c0d55"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("5fddbdcd-904f-45bd-aec6-4b069b5b3dd5"),
                    MainText = "Are logs correlated across different event types?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("73ac391e-8726-4fd1-9e9c-f2e6ee8c0d55"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b266e52a-bb66-4dcb-a5db-ab9e503e4c14"),
                    MainText = "How are logs correlated across different event types?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("5fddbdcd-904f-45bd-aec6-4b069b5b3dd5"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("475991e3-1582-419c-9622-74659e60833b"),
                    MainText = "To what extent is log correlation automated?",
                    OptionChoices = "None,Partial,Full",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("5fddbdcd-904f-45bd-aec6-4b069b5b3dd5"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d7232833-84b1-43a5-b8a7-8241df79fa6d"),
                    MainText = "Are timestamps synchronized across logs?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("73ac391e-8726-4fd1-9e9c-f2e6ee8c0d55"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1edadea3-4296-4c5e-bc7a-0d9ac7012062"),
                    MainText = "What timezone are logs synchronized to?",
                    OptionChoices = "Coordinated Universal Time (UTC),Local time",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("d7232833-84b1-43a5-b8a7-8241df79fa6d"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("26c93143-81aa-44ca-b363-c6f3afa3baea"),
                    MainText = "Do you (or your managed security service provider) have measures in place to prevent tampering of logs?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("42ebb644-5a81-4dbf-a0cf-b972c3aaf801"),
                    MainText = "What measures are in place to prevent tampering of logs?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("26c93143-81aa-44ca-b363-c6f3afa3baea"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("45e28ff7-9b93-471d-b56c-4554b592e02e"),
                    MainText = "Do you (or your managed security service provider) have incident detection procedures?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e52a3d26-4c8d-4890-913c-87ed6287996e"),
                    MainText = "What types of incident detection procedures?",
                    OptionChoices = "Data exfiltration,Denial of service,Policy violation,System compromise,Unauthorized access",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("45e28ff7-9b93-471d-b56c-4554b592e02e"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2c0571b6-05c2-4d9c-a9da-3ce6ea72d367"),
                    MainText = "Are CERT/CIRT/SOC staff trained on these procedures?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("45e28ff7-9b93-471d-b56c-4554b592e02e"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("4ae82ee8-0b9b-4934-b333-b5d57595adbb"),
                    MainText = "How often are these procedures reviewed?",
                    OptionChoices = "Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("45e28ff7-9b93-471d-b56c-4554b592e02e"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(incidentDetectionQuestions);

            #endregion

            #region Incident Remediation

            subcategoryId = Guid.Parse("6b4a7bed-c626-4369-87ab-1245d5560a38");

            var incidentRemediationQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("58844a0c-d111-41ba-b80b-2b08cf50e2e4"),
                    MainText = "Does your organization have incident containment procedures?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("81a47c4e-3ece-4b9b-a511-8d8c01a9c9ec"),
                    MainText = "Which incident containment procedures have you documented?",
                    OptionChoices = "Incident scoping,Network isolation,System isolation",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("58844a0c-d111-41ba-b80b-2b08cf50e2e4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("acce82d5-d8dd-4091-bcf7-7698acb19fe0"),
                    MainText = "Are CERT/CIRT/SOC staff trained on these incident containment procedures?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("58844a0c-d111-41ba-b80b-2b08cf50e2e4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("848774c0-8efe-4909-a1b9-4d2924f97606"),
                    MainText = "Does your organization have tools to contain incidents?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("58844a0c-d111-41ba-b80b-2b08cf50e2e4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("63fe68c4-6ad2-4bc5-adee-98b2affcec7b"),
                    MainText = "What tools are available to contain incidents?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("848774c0-8efe-4909-a1b9-4d2924f97606"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question {
                    Id = Guid.Parse("d989d014-4c3c-4ae1-a5d7-dcd5b3a87316"),
                    MainText = "Are these containment tools centrally managed?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("848774c0-8efe-4909-a1b9-4d2924f97606"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f544a06a-797a-48b5-ac02-f281f5d97858"),
                    MainText = "How often are these procedures reviewed?",
                    OptionChoices = "Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("58844a0c-d111-41ba-b80b-2b08cf50e2e4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("6bd6984e-6bb4-417e-8113-4a422556aa49"),
                    MainText = "Does your organization have digital forensics procedures?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("4ea0684e-2a7e-445b-917a-95d8b7640aaa"),
                    MainText = "Which digital forensics procedures have you documented?",
                    OptionChoices = "Evidence analysis,Evidence collection,Evidence handling",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("6bd6984e-6bb4-417e-8113-4a422556aa49"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("712113ab-d203-4fc1-bf2f-46f3c43e43ff"),
                    MainText = "Are CERT/CIRT/SOC staff trained on these digital forensics procedures?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("6bd6984e-6bb4-417e-8113-4a422556aa49"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d6085457-0dcc-44ea-8f93-7cf712048778"),
                    MainText = "Does your organization have tools to collect digital forensics?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("6bd6984e-6bb4-417e-8113-4a422556aa49"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("50ca9b52-b2a8-4973-ac65-036979bab548"),
                    MainText = "What tools are available to collect digital forensics?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("d6085457-0dcc-44ea-8f93-7cf712048778"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("6f6fc957-a8ea-4a93-b5a7-21b4f5cf347c"),
                    MainText = "What capabilities do these tools have?",
                    OptionChoices = "Disk forensics,Memory forensics,Network forensics",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("d6085457-0dcc-44ea-8f93-7cf712048778"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a6037bfe-f9c1-4434-b5ba-2cef11bce939"),
                    MainText = "Are these digital forensics collection tools centrally managed?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("d6085457-0dcc-44ea-8f93-7cf712048778"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ab69ce25-4529-4c2a-a361-afe179d27c07"),
                    MainText = "Does your organization have tools to analyze digital forensics?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("6bd6984e-6bb4-417e-8113-4a422556aa49"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("13f86860-d650-4288-b765-ecca29aa756d"),
                    MainText = "What tools are available to analyze digital forensics?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("ab69ce25-4529-4c2a-a361-afe179d27c07"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1c087fe6-0c9d-429d-a33e-f49a72732c79"),
                    MainText = "What capabilities do these tools have?",
                    OptionChoices = "Content analysis,Dynamic code analysis,Feature extraction,Static code analysis",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("ab69ce25-4529-4c2a-a361-afe179d27c07"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("021295c2-ee72-4cdf-a00e-8323336a51a1"),
                    MainText = "Do you have a dedicated digital forensics lab?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("6bd6984e-6bb4-417e-8113-4a422556aa49"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("24b44775-708c-419f-8620-b432a2d83dc4"),
                    MainText = "How often are these procedures reviewed?",
                    OptionChoices = "Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("6bd6984e-6bb4-417e-8113-4a422556aa49"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("46404a55-0b67-4ff2-ab08-b7fbcc435bd8"),
                    MainText = "Does your organization have a case management tool?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("41ae7d22-b686-4677-9c6e-533a73ba218d"),
                    MainText = "What type of case management tool do you have?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("46404a55-0b67-4ff2-ab08-b7fbcc435bd8"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b2fcaccb-39d3-4caa-9f56-209a54ac0fda"),
                    MainText = "Does your organization track incident metrics?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c5c2bbe0-0cfc-4636-ba34-5ce01bf0b937"),
                    MainText = "What incident metrics do you track?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("b2fcaccb-39d3-4caa-9f56-209a54ac0fda"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("df48ac8f-210f-40f8-8e7a-110fa1ab682e"),
                    MainText = "How do you collect these incident metrics?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("b2fcaccb-39d3-4caa-9f56-209a54ac0fda"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("9bb0aee1-27da-4078-aa3b-b8d2587cdc2f"),
                    MainText = "Do you track the average time to detect incidents?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b2fcaccb-39d3-4caa-9f56-209a54ac0fda"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("06ed43d2-f8cb-4074-ac71-cc763b83e636"),
                    MainText = "What incident categories does this metric track?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("9bb0aee1-27da-4078-aa3b-b8d2587cdc2f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("dcc7ee7f-43a6-4354-9842-b3df7ef20995"),
                    MainText = "Do you track the average time to remediate incidents?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b2fcaccb-39d3-4caa-9f56-209a54ac0fda"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("43cc31ec-2389-4a42-806b-0666a7689aa6"),
                    MainText = "What incident categories does this metric track?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("dcc7ee7f-43a6-4354-9842-b3df7ef20995"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1e0b9540-9480-4b87-9451-7a05e1e0e814"),
                    MainText = "Do these metrics factor into staff performance?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b2fcaccb-39d3-4caa-9f56-209a54ac0fda"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("257c1aa3-5b09-40c2-bac8-7de6f061d898"),
                    MainText = "How do they factor into staff performance?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("1e0b9540-9480-4b87-9451-7a05e1e0e814"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b6e7efba-7ed3-4198-8f97-4513c05d4141"),
                    MainText = "Do these metrics factor into strategic planning?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b2fcaccb-39d3-4caa-9f56-209a54ac0fda"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("fbaef4c4-098a-4068-8822-3a6b4d122b64"),
                    MainText = "How do they factor into strategic planning?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("b6e7efba-7ed3-4198-8f97-4513c05d4141"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(incidentRemediationQuestions);

            #endregion

            return results.ToArray();            
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.