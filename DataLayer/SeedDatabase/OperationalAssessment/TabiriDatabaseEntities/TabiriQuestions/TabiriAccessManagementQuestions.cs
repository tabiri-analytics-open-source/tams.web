using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.TabiriDatabaseEntities.TabiriQuestions
{
    public static class TabiriAccessManagementQuestions
    {
        public static Question[] Seed(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Empty;
            
            var results = new List<Question>();

            #region Identity Management

            subcategoryId = Guid.Parse("a4c329d6-193e-441f-b0cb-97b9770c7265");

            var identityManagementQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("ad86df4c-83d3-494f-bfb4-be7ebde347ed"),
                    MainText = "Does your organization have an Identity Management policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },                
                new Question 
                {
                    Id = Guid.Parse("e9de8f48-2810-4e64-9ec9-ad924ac64298"),
                    MainText = "Do you have a single source of identity information?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("ad86df4c-83d3-494f-bfb4-be7ebde347ed"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d76460fe-d34e-40d2-a743-c24e1cbd7eb2"),
                    MainText = "What is your primary source of identity information?",
                    HintText = "e.g. Enterprise Resource Planning (ERP), Active Directory, LDAP",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("e9de8f48-2810-4e64-9ec9-ad924ac64298"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0d3f6947-c42d-4449-8e47-48df2e8f6d8f"),
                    MainText = "Which business unit is tasked with managing the primary source of identity information?",
                    HintText = "e.g. Human Resources department, Information Technology department",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("ad86df4c-83d3-494f-bfb4-be7ebde347ed"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b93a540a-fd51-497f-b176-01f8e84d20c8"),
                    MainText = "How are user accounts established?",                    
                    Order = 3,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("ad86df4c-83d3-494f-bfb4-be7ebde347ed"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("bf4c6ed9-f7db-4f5c-b746-f7f560267ed5"),
                    MainText = "Do users have the same accounts (usernames) across systems?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 1,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("ad86df4c-83d3-494f-bfb4-be7ebde347ed"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c47460a4-e5ae-4ee7-9778-ae840a7a577b"),
                    MainText = "How do you track the same user across multiple systems?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("bf4c6ed9-f7db-4f5c-b746-f7f560267ed5"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("302de3f8-5e61-4a66-b8ef-1e06ca53e3ad"),
                    MainText = "Are background checks conducted for users with sensitive roles?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("ad86df4c-83d3-494f-bfb4-be7ebde347ed"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("186dd4b8-72c6-4344-9a9b-784437d8a7de"),
                    MainText = "How often is this policy reviewed?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("ad86df4c-83d3-494f-bfb4-be7ebde347ed"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d9622ee7-bf1d-45ff-bf71-752fd0661b0c"),
                    MainText = "Does your organization utilize an Identity and Access Management (IAM) solution for user provisioning?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("41e8db89-4441-48eb-9c6d-d41023589705"),
                    MainText = "How does the IAM solution integrate with your primary source of identity data?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("d9622ee7-bf1d-45ff-bf71-752fd0661b0c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question {
                    Id = Guid.Parse("6c664389-f885-434e-a87d-4f813bd5ae91"),
                    MainText = "Do you have a procedure for provisioning new users?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("74818a6a-3f7a-4246-997e-6e5953ae6383"),
                    MainText = "How are access requests tracked?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("6c664389-f885-434e-a87d-4f813bd5ae91"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a9adef4c-cae0-4115-9602-acdf1ec40c41"),
                    MainText = "Who approves access requests?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("6c664389-f885-434e-a87d-4f813bd5ae91"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("243acf52-4d69-4666-87af-97757475f466"),
                    MainText = "Is the process of creating approved user accounts automated?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("6c664389-f885-434e-a87d-4f813bd5ae91"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("70e7e91c-5b0a-44bc-b2fb-6f84d4e34fcd"),
                    MainText = "Who manually creates the approved user accounts?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("243acf52-4d69-4666-87af-97757475f466"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("59619f61-f4d5-44f9-b725-b2dc279b624f"),
                    MainText = "Do you have a procedure for revoking/disabling existing user accounts?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("294552f0-1c1a-4ee4-b65e-a9738027bfed"),
                    MainText = "How are revocation requests initiated?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("59619f61-f4d5-44f9-b725-b2dc279b624f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("5d385bd8-51de-4668-8e3c-a8d277eb6500"),
                    MainText = "How are revocation requests tracked?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("59619f61-f4d5-44f9-b725-b2dc279b624f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ea2248c9-eda6-455e-a9af-dca7ad5b0493"),
                    MainText = "Who approves the revocation requests?",                    
                    Order = 3,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("59619f61-f4d5-44f9-b725-b2dc279b624f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("59bf0a6a-50c0-4c93-9ff6-9564389167d8"),
                    MainText = "Is the process of revoking/disabling user accounts automated?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 1,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("59619f61-f4d5-44f9-b725-b2dc279b624f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("5150e385-6236-48a2-841e-63970269c40d"),
                    MainText = "Who manually revokes/disables the user accounts?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("59bf0a6a-50c0-4c93-9ff6-9564389167d8"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d4a1a280-8325-4be1-842a-8a1f75ba109e"),
                    MainText = "Is there a procedure to validate accounts have been successfully revoked/disabled?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("59619f61-f4d5-44f9-b725-b2dc279b624f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("9c4c4b4b-d91c-4e14-99bf-85400cf73245"),
                    MainText = "What is the procedure for validating?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("d4a1a280-8325-4be1-842a-8a1f75ba109e"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("146a406e-6742-461f-9655-90ec1b5d32ac"),
                    MainText = "Do you review existing user accounts for appropriateness?",
                    HintText = "e.g. identify stale accounts, active accounts on legacy systems",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("6c16fbe3-8921-4574-8cfb-ac3a0091c2ba"),
                    MainText = "How often?",
                    OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("146a406e-6742-461f-9655-90ec1b5d32ac"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a1c0af56-645a-4c89-a623-88da0cde0170"),
                    MainText = "What is the procedure for reviewing existing accounts for appropriateness?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("146a406e-6742-461f-9655-90ec1b5d32ac"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }        
            };
            results.AddRange(identityManagementQuestions);

            #endregion

            #region Access Controls

            subcategoryId = Guid.Parse("eb909bf7-654c-4314-b1c8-6356169a2c6f");

            var accessControlQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("f083cbc5-2bdf-4bef-89c4-ace3df35cb18"),
                    MainText = "Does your organization have an Access Control policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("28c07d2a-005e-4da3-9f89-3c096ee93a9b"),
                    MainText = "Which of the following access control standards has your organization defined?",
                    OptionChoices = "Account lockout,Password complexity,Password expiry,Password history",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("f083cbc5-2bdf-4bef-89c4-ace3df35cb18"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e06e9076-b30c-41d1-a91c-ce83b3ca011f"),
                    MainText = "Have these standards been implemented across all systems?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,                    
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("28c07d2a-005e-4da3-9f89-3c096ee93a9b"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ec8a0723-0cd5-472c-89fa-6785e0185c3a"),
                    MainText = "What procedures do you have in place to implement these standards?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("e06e9076-b30c-41d1-a91c-ce83b3ca011f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("6b5c931d-db7e-478e-8905-e09e7ec309ad"),
                    MainText = "Who is tasked with implementing these standards?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("e06e9076-b30c-41d1-a91c-ce83b3ca011f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d9280486-1a38-4bf9-bcc9-9998dfec4aec"),
                    MainText = "Have you defined a 'least privilege' standard?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f083cbc5-2bdf-4bef-89c4-ace3df35cb18"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("795141e3-cd28-4e4e-81b6-83831dfb5a9c"),
                    MainText = "Are administrator/root privileges limited to specific individuals?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("d9280486-1a38-4bf9-bcc9-9998dfec4aec"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f36d56d1-1867-408d-89ae-da9732c8ac6a"),
                    MainText = "How are user, administrator/root, and guest account privileges managed?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("d9280486-1a38-4bf9-bcc9-9998dfec4aec"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2db85c06-ee27-48fb-a70c-8f34d0e34d15"),
                    MainText = "How are applications that run with administrator/root privileges managed?",                    
                    Order = 3,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("d9280486-1a38-4bf9-bcc9-9998dfec4aec"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("76ce0d3b-80c8-4704-adb4-a40219cff9eb"),
                    MainText = "How are service accounts managed?",                    
                    Order = 4,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("d9280486-1a38-4bf9-bcc9-9998dfec4aec"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2d394c22-4b5f-4a57-a380-16d64ae38de4"),
                    MainText = "Who is tasked with implementing this standard?",                    
                    Order = 5,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("d9280486-1a38-4bf9-bcc9-9998dfec4aec"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("57295672-98be-4e0e-8924-e0638ea7bad3"),
                    MainText = "How often is this policy and standards reviewed?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 3,                    
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f083cbc5-2bdf-4bef-89c4-ace3df35cb18"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f0463428-e89a-4146-bb16-0e67c01f16c7"),
                    MainText = "Has your organization defined a two-factor authentication standard?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id =Guid.Parse("c172add7-94d7-4b91-96b0-802fb150eab1"),
                    MainText = "Which systems/applications require two-factor authentication?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("f0463428-e89a-4146-bb16-0e67c01f16c7"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("4893d1d7-9fa3-4cc1-b6b0-f022b24030b5"),
                    MainText = "How is two-factor authentication implemented on these systems/applications?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("f0463428-e89a-4146-bb16-0e67c01f16c7"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("504fe625-4a35-42d6-915b-03098fdf63f9"),
                    MainText = "Do you have a procedure for monitoring access control?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("06cbdc5a-b0d3-4c2e-94d8-a89ce6fb205a"),
                    MainText = "Have you implemented a Centralized Log Management solution?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("504fe625-4a35-42d6-915b-03098fdf63f9"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("445eb8d3-b275-4f35-b43e-af8a5ba22060"),
                    MainText = "Which systems/applications do you collect access logs from?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("06cbdc5a-b0d3-4c2e-94d8-a89ce6fb205a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("bcac94a0-3202-4caf-8c01-c0295715a7ca"),
                    MainText = "How do you collect these access logs?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("06cbdc5a-b0d3-4c2e-94d8-a89ce6fb205a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d92bc0bd-bb91-49fa-a33b-7c5c64ec502c"),
                    MainText = "Where are these access logs stored?",                    
                    Order = 3,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("06cbdc5a-b0d3-4c2e-94d8-a89ce6fb205a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("80c8ed6b-20c5-4376-982c-bade3d69cefc"),
                    MainText = "What is your log retention schedule?",                    
                    Order = 4,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("06cbdc5a-b0d3-4c2e-94d8-a89ce6fb205a"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ac805b30-df02-4856-801e-becdc5d390e7"),
                    MainText = "What is your procedure for reviewing access logs?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("504fe625-4a35-42d6-915b-03098fdf63f9"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
            };
            results.AddRange(accessControlQuestions);

            #endregion

            #region Separation Of Duties Questions

            subcategoryId = Guid.Parse("5e1e6a79-046c-4fbd-89c0-3b41fc69e555");

            var seperationOfDutiesQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("04ecc7e8-fa34-4df7-a7ab-ebb4b0082bd0"),
                    MainText = "Does your organization have a Separation of Duties policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("4b4aa3c2-8bee-4757-a58b-13797971f884"),
                    MainText = "Are regular users restricted from making system or application configuration changes?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("04ecc7e8-fa34-4df7-a7ab-ebb4b0082bd0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("57048c2c-a9db-4122-ad9e-939429103167"),
                    MainText = "How is this restriction enforced?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("4b4aa3c2-8bee-4757-a58b-13797971f884"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b0566ada-2743-4d55-a7ed-b1cae451f1f5"),
                    MainText = "Does a separation exist between production and development/test environments?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("04ecc7e8-fa34-4df7-a7ab-ebb4b0082bd0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0ce3489d-ec9f-49d7-8a7c-12206d093ffa"),
                    MainText = "Are access controls similar across both environments?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b0566ada-2743-4d55-a7ed-b1cae451f1f5"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("3327ba74-f07f-4145-b311-97f8d03ff960"),
                    MainText = "Are developers restricted to the development/test environment only?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b0566ada-2743-4d55-a7ed-b1cae451f1f5"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("043a78b8-d019-43f8-af0c-63ffd0a18f96"),
                    MainText = "Are there controls restricting developers from pushing new code directly into production?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b0566ada-2743-4d55-a7ed-b1cae451f1f5"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("6fa83951-5e3c-400d-99a0-8558f2c94713"),
                    MainText = "What types of controls are implemented?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("043a78b8-d019-43f8-af0c-63ffd0a18f96"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("19e181fe-4d2c-44db-843e-1e8e828670e7"),
                    MainText = "What is the procedure for getting code approved prior to pushing it into to production?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("043a78b8-d019-43f8-af0c-63ffd0a18f96"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7fead837-0494-4402-b261-87b154207996"),
                    MainText = "Are security reviews part of the approval procedure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("043a78b8-d019-43f8-af0c-63ffd0a18f96"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("cbdd1f9f-dc75-45a2-95a2-f96912e29a60"),
                    MainText = "How often is this policy reviewed?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("04ecc7e8-fa34-4df7-a7ab-ebb4b0082bd0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a5b85713-ecb6-403f-ba45-4a208982bc7c"),
                    MainText = "Does your organization have standards for Role Based Access Control (RBAC)?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b0a89fbf-9805-43a9-9d76-06a32ea5c85f"),
                    MainText = "On what systems are RBAC implemented?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("a5b85713-ecb6-403f-ba45-4a208982bc7c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("3b4b7615-4bc1-4c11-a9a2-2ea16321dfd9"),
                    MainText = "What types of roles are defined on those systems?",
                    HintText = "e.g. user, approver/manager, auditor/reviewer, admin/root",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("a5b85713-ecb6-403f-ba45-4a208982bc7c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("276269e0-10b9-4b25-90e8-e8c60de437a8"),
                    MainText = "How are these roles managed?",                    
                    Order = 3,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("a5b85713-ecb6-403f-ba45-4a208982bc7c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("327cf1c3-79df-46dd-9c01-942692ff02a4"),
                    MainText = "Are these roles integrated with the IAM solution mentioned in Access Management > Identity Management > Question (2)?",                    
                    Order = 4,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("a5b85713-ecb6-403f-ba45-4a208982bc7c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("28fdf758-114b-4eb2-b067-edf0628aa8b5"),
                    MainText = "Is there a procedure for reviewing roles for appropriateness?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("a5b85713-ecb6-403f-ba45-4a208982bc7c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c72f2721-303f-437c-8c20-aa25797d8fe9"),
                    MainText = "What is the procedure for reviewing roles for appropriateness?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("28fdf758-114b-4eb2-b067-edf0628aa8b5"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("51aa85d2-a74a-40cc-a4a3-8fe4f1b2bfa5"),
                    MainText = "How often is this review conducted?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("28fdf758-114b-4eb2-b067-edf0628aa8b5"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(seperationOfDutiesQuestions);

            #endregion

            #region Privileged Access Management Questions

                subcategoryId = Guid.Parse("4b17e07a-b608-4406-bdeb-32e2653a26cb");

                var priviledgedAccessManagementQuestions = new List<Question>
                {
                    new Question 
                    {
                        Id = Guid.Parse("02289c0a-5174-4267-92a4-f6062ea671b3"),
                        MainText = "Does your organization have a Privileged Access Management policy?",
                        OptionChoices = "Yes,No",
                        OptionsOrientation = OptionsOrientation.Horizontal,
                        ActiveOptionIndex = 0,
                        Order = 1,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = null,
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("f5a68be2-2635-41a0-bcd9-bebc5cf3146c"),
                        MainText = "Does this policy define under which conditions privileged access should be granted?",
                        OptionChoices = "Yes,No",
                        OptionsOrientation = OptionsOrientation.Horizontal,
                        Order = 1,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = Guid.Parse("02289c0a-5174-4267-92a4-f6062ea671b3"),
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("f956a9d9-de00-492a-80f7-e7e8d89bdb7e"),
                        MainText = "Are privileged accounts for system administration use only?",
                        OptionChoices = "Yes,No",
                        OptionsOrientation = OptionsOrientation.Horizontal,
                        ActiveOptionIndex = 0,
                        Order = 2,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = Guid.Parse("02289c0a-5174-4267-92a4-f6062ea671b3"),
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("0878b4b4-d743-4bf4-8fcd-b96071d9b9e6"),
                        MainText = "Do system administrators have separate standard user accounts?",
                        OptionChoices = "Yes,No",
                        OptionsOrientation = OptionsOrientation.Horizontal,
                        Order = 1,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = Guid.Parse("f956a9d9-de00-492a-80f7-e7e8d89bdb7e"),
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("a73e3209-f5b0-4f07-8e58-f02843cb643d"),
                        MainText = "How often is this policy reviewed?",
                        OptionChoices = "Quarterly,Semi-annually,Annually",
                        OptionsOrientation = OptionsOrientation.Vertical,
                        Order = 3,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = Guid.Parse("02289c0a-5174-4267-92a4-f6062ea671b3"),
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("87c2d274-b131-44a6-b1c7-46c054de9772"),
                        MainText = "Do you have any administrative accounts that are shared by more than one individual?",
                        OptionChoices = "Yes,No",
                        OptionsOrientation = OptionsOrientation.Horizontal,
                        ActiveOptionIndex = 0,
                        Order = 2,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = null,
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("1f5c2baa-c354-4cd9-8c85-da57e0233613"),
                        MainText = "How many administrative accounts are shared?",                        
                        Order = 1,
                        QuestionType = QuestionType.Integer,
                        ParentId = Guid.Parse("87c2d274-b131-44a6-b1c7-46c054de9772"),
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("d3f57bbf-e003-4671-ad82-5b38bf67448d"),
                        MainText = "Are these shared administrative accounts service accounts?",
                        OptionChoices = "Yes,No",
                        OptionsOrientation = OptionsOrientation.Horizontal,
                        ActiveOptionIndex = 0,
                        Order = 2,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = Guid.Parse("87c2d274-b131-44a6-b1c7-46c054de9772"),                        
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("46bac824-11ad-4b9e-b031-57c0e3db5dd5"),
                        MainText = "Do these service accounts allow for interactive login?",
                        OptionChoices = "Yes,No",
                        OptionsOrientation = OptionsOrientation.Horizontal,
                        Order = 1,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = Guid.Parse("d3f57bbf-e003-4671-ad82-5b38bf67448d"),
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("4d91327f-858e-40a2-9a4f-16d7277592f0"),
                        MainText = "Is there an inventory of privileged accounts?",
                        OptionChoices = "Yes,No",
                        OptionsOrientation = OptionsOrientation.Horizontal,
                        ActiveOptionIndex = 0,
                        Order = 3,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = null,
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("1d0785e9-a19a-4d52-a374-8200e903cb8e"),
                        MainText = "What was the most recent count of privileged accounts?",                        
                        Order = 1,
                        QuestionType = QuestionType.Integer,
                        ParentId = Guid.Parse("4d91327f-858e-40a2-9a4f-16d7277592f0"),
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("605f9d97-bebf-4961-920a-0a8668c983d5"),
                        MainText = "Does your organization have a privileged access management tool?",
                        OptionChoices = "Yes,No",
                        OptionsOrientation = OptionsOrientation.Horizontal,
                        ActiveOptionIndex = 0,
                        Order = 4,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = null,
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("92caffe9-74f5-403f-aec0-3a9bff9ac71f"),
                        MainText = "How are privileged access requests made?",                        
                        Order = 1,
                        QuestionType = QuestionType.Text,
                        ParentId = Guid.Parse("605f9d97-bebf-4961-920a-0a8668c983d5"),
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("1fa1e161-ea6c-416c-a8b3-030c229ffe0a"),
                        MainText = "How are privileged access requests approved?",                        
                        Order = 2,
                        QuestionType = QuestionType.Text,
                        ParentId = Guid.Parse("605f9d97-bebf-4961-920a-0a8668c983d5"),
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("378bc3ff-ccfd-4865-9833-235393fba808"),
                        MainText = "Does your organization have a process for reviewing administrative account activity?",
                        OptionChoices = "Yes,No",
                        OptionsOrientation = OptionsOrientation.Horizontal,
                        ActiveOptionIndex = 0,
                        Order = 5,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = null,
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("0fdb5068-7b1f-46b8-9513-57f2504e135d"),
                        MainText = "What is the process for reviewing administrative account activity?",                        
                        Order = 1,
                        QuestionType = QuestionType.Text,
                        ParentId = Guid.Parse("378bc3ff-ccfd-4865-9833-235393fba808"),
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("7f4ad2c4-8dff-4244-8133-53acea5a1bca"),
                        MainText = "How often is this review conducted?",
                        OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                        OptionsOrientation = OptionsOrientation.Vertical,
                        Order = 2,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = Guid.Parse("378bc3ff-ccfd-4865-9833-235393fba808"),
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    },
                    new Question 
                    {
                        Id = Guid.Parse("d7dbe97a-4b71-4be9-949d-a95273750af3"),
                        MainText = "Have you implemented any automated detection rules for privileged access?",
                        OptionChoices = "Yes,No",
                        OptionsOrientation = OptionsOrientation.Horizontal,
                        Order = 3,
                        QuestionType = QuestionType.SelectSingle,
                        ParentId = Guid.Parse("378bc3ff-ccfd-4865-9833-235393fba808"),
                        SubcategoryId = subcategoryId,
                        CreatedBy = createdBy,
                        CreatedTimestamp = currentTimestamp
                    }
                };
                results.AddRange(priviledgedAccessManagementQuestions);

            #endregion

            #region Remote Access Questions

            subcategoryId = Guid.Parse("9734a725-6f43-4a49-8b7c-fd008db3562d");

            var remoteAccessQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("ba007216-21ed-4d8a-a568-ddd3776cdba6"),
                    MainText = "Does your organization have a Remote Access policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1cfb10df-1597-407b-8ca9-4ed7e972c7ff"),
                    MainText = "Can employees access the corporate network remotely?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("ba007216-21ed-4d8a-a568-ddd3776cdba6"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2be05a1a-f8ca-41dc-9821-1faf6b6f1070"),
                    MainText = "Is remote access limited to specific employee roles?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("ba007216-21ed-4d8a-a568-ddd3776cdba6"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b9c0de06-09c8-4f41-923d-3f1e3f4a89ef"),
                    MainText = "Which employee roles is remote access limited to?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("2be05a1a-f8ca-41dc-9821-1faf6b6f1070"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("434837b3-c0b1-4948-80c4-d06042846890"),
                    MainText = "Do you have an approval criteria for granting employees remote access privileges?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("ba007216-21ed-4d8a-a568-ddd3776cdba6"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d8b41b4f-46c7-4c15-8b43-3e7bba6805cf"),
                    MainText = "What are the approval criteria?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("434837b3-c0b1-4948-80c4-d06042846890"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1e61391c-57ec-4762-aa1b-8e6502cb43c3"),
                    MainText = "How often is this policy reviewed?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("ba007216-21ed-4d8a-a568-ddd3776cdba6"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1ca8029e-11d2-40ae-a0d3-86f8f91bc5d4"),
                    MainText = "Do you have a technology in place to provide remote access?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ecbd5e64-69ef-4a92-a0bd-a9012d3c9078"),
                    MainText = "What technology do you use to provide remote access?",
                    HintText = "e.g. Virtual Private Network (VPN), Virtual Desktop Infrastructure (VDI)",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("1ca8029e-11d2-40ae-a0d3-86f8f91bc5d4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("94e16a4c-b766-42d1-a4b2-5656389bd280"),
                    MainText = "Do you monitor all traffic on the VPN, VDI or other remote access solution?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1ca8029e-11d2-40ae-a0d3-86f8f91bc5d4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("39f6d1b8-90ea-4dfc-8207-82c95e3dfefd"),
                    MainText = "If using a corporate VPN, is it configured for split tunnel traffic?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("94e16a4c-b766-42d1-a4b2-5656389bd280"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ca5f0eed-586b-4fd6-a311-8b05647a517f"),
                    MainText = "Are remote users placed on a segmented VLAN upon connecting?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1ca8029e-11d2-40ae-a0d3-86f8f91bc5d4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ff3d3cf1-2fbf-428f-a8f9-46ecd9375e66"),
                    MainText = "How is this network segmentation implemented?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("ca5f0eed-586b-4fd6-a311-8b05647a517f"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("6d330fc6-0f68-4a48-91b7-7c2672b58423"),
                    MainText = "Have you implemented two-factor authentication authentication for remote access?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1ca8029e-11d2-40ae-a0d3-86f8f91bc5d4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("68a6078d-7fe9-47d2-95a6-2f0bf49b6d3a"),
                    MainText = "What kind of two-factor authentication have you implemented for remote access?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("6d330fc6-0f68-4a48-91b7-7c2672b58423"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("bf88bb83-8305-465f-899f-6e9e2a3533ae"),
                    MainText = "Do you authenticate the remote user's system?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1ca8029e-11d2-40ae-a0d3-86f8f91bc5d4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("8f80982c-e96d-4a37-bd93-0c7f7a690d97"),
                    MainText = "What kind of system authentication have you implemented?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("bf88bb83-8305-465f-899f-6e9e2a3533ae"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("9587d3f9-4a18-483b-9353-3a00f8548806"),
                    MainText = "Do you conduct security checks of the remote user's system?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1ca8029e-11d2-40ae-a0d3-86f8f91bc5d4"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("6e03987e-e89e-43d4-93c8-5b1b109435d1"),
                    MainText = "What kind of security checks do you conduct of the remote user's system?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("9587d3f9-4a18-483b-9353-3a00f8548806"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7ba45896-06c9-470c-bb4b-457bb6334cac"),
                    MainText = "What happens if the remote user's system fails the security checks?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("9587d3f9-4a18-483b-9353-3a00f8548806"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1e9eb2a1-f5d8-4541-b286-c424d9c30ba1"),
                    MainText = "Do you have a process to review remote access activity?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("3aa35e5c-ec89-4773-8b88-136c5f5d1d39"),
                    MainText = "What is the process for reviewing remote access activity?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("1e9eb2a1-f5d8-4541-b286-c424d9c30ba1"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2a30fd7c-a706-4fa9-9584-e36159e3e80e"),
                    MainText = "How often do you review remote access activity?",
                    OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1e9eb2a1-f5d8-4541-b286-c424d9c30ba1"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(remoteAccessQuestions);

            #endregion

            #region Third Party Access Questions

            subcategoryId = Guid.Parse("754ed961-0edd-45c5-87f6-2ff75e409fdd");

            var thirdPartyAccessQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("7c26354c-4927-407e-889b-76eba85bf9d0"),
                    MainText = "Does your organization have a Third-party Access policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b899a382-f2a7-4c7c-b969-6e96afa0a13c"),
                    MainText = "Does this policy include contractual language on the third-party's compliance with your organization's policies?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("7c26354c-4927-407e-889b-76eba85bf9d0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d8d68cb9-1af4-4591-a052-1bb3d386f645"),
                    MainText = "Have you defined which third-parties can access your corporate network?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("7c26354c-4927-407e-889b-76eba85bf9d0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("cd150f3b-0fbf-4f8b-adc1-296b11671f31"),
                    MainText = "Which third-parties can access the organization’s network?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("d8d68cb9-1af4-4591-a052-1bb3d386f645"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c228496c-5811-4006-a334-897c9bf7742c"),
                    MainText = "Are third-parties granted local access?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("d8d68cb9-1af4-4591-a052-1bb3d386f645"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("3bef6a19-1968-4fb1-808c-9bbbd9259aed"),
                    MainText = "Are third-parties granted remote access?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("d8d68cb9-1af4-4591-a052-1bb3d386f645"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("9b777245-af5f-4cd5-947a-96f0282d0938"),
                    MainText = "Does this remote access comply with the Remote Access policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("3bef6a19-1968-4fb1-808c-9bbbd9259aed"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("fdbbb033-8d7e-4415-b20f-17b85edfaf8e"),
                    MainText = "Are audits of the third-party's systems conducted prior to access being granted?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("7c26354c-4927-407e-889b-76eba85bf9d0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ae06506c-7f2f-42c9-bd65-f133b7cc7d18"),
                    MainText = "What is the process for conducting audits of the third-party's systems?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("fdbbb033-8d7e-4415-b20f-17b85edfaf8e"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d4af98d9-e928-40bf-9aa9-78e8d17047da"),
                    MainText = "Are these audits aligned with a specific industry framework?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("fdbbb033-8d7e-4415-b20f-17b85edfaf8e"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("059e8df5-17f6-4585-b35c-7f96c18f226e"),
                    MainText = "What specific industry frameworks are these audits aligned with?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("d4af98d9-e928-40bf-9aa9-78e8d17047da"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a45e0cbf-3584-4d92-89ca-a471fd673431"),
                    MainText = "How often are these audits conducted?",
                    OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("fdbbb033-8d7e-4415-b20f-17b85edfaf8e"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("05cce86b-d64b-45a7-9a81-14e10cee33ce"),
                    MainText = "How often is this policy reviewed?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("7c26354c-4927-407e-889b-76eba85bf9d0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("cc7eabc6-82eb-4536-84c3-762295f31609"),
                    MainText = "Do third-parties have remote API access?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1c562d9e-d8fd-49ad-977d-d8f344a5fc69"),
                    MainText = "Are these API calls authenticated?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("cc7eabc6-82eb-4536-84c3-762295f31609"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("509a76f4-87f7-4099-b2f1-899dbe69d76d"),
                    MainText = "How are the API calls authenticated?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("1c562d9e-d8fd-49ad-977d-d8f344a5fc69"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("62d14c5d-6cd0-46f8-a2cb-b7205a869067"),
                    MainText = "Are these API calls encrypted?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("cc7eabc6-82eb-4536-84c3-762295f31609"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0c7bd269-21dc-4225-b773-4e1b9c05ae53"),
                    MainText = "How are the API calls encrypted?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("62d14c5d-6cd0-46f8-a2cb-b7205a869067"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("cddf636b-c481-4b12-a6a4-2d68a0efb498"),
                    MainText = "Do you monitor the third-party's compliance with your organization's policies?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ea54cc5a-3f01-4231-b52b-dc6aa8935995"),
                    MainText = "How do you monitor for Data Protection policy violations?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("cddf636b-c481-4b12-a6a4-2d68a0efb498"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("5a78549a-94b1-4d09-93a3-c8e089201867"),
                    MainText = "How do you monitor for Access Management policy violations?",                    
                    Order = 2,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("cddf636b-c481-4b12-a6a4-2d68a0efb498"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("cf7c7916-acea-4882-bc47-35f54f97a01b"),
                    MainText = "How do you monitor for Security Architecture policy violations?",                    
                    Order = 3,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("cddf636b-c481-4b12-a6a4-2d68a0efb498"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("68803238-f646-4b92-bdbb-8a207a31c7b5"),
                    MainText = "How do you monitor for Incident Response policy violations?",                    
                    Order = 4,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("cddf636b-c481-4b12-a6a4-2d68a0efb498"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("098820dc-b598-445d-8fe7-378f32bd65a2"),
                    MainText = "Do you have a process to review third-party access activity?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b5b2c03f-5c20-4bb2-b192-5972024eede0"),
                    MainText = "What is the process for reviewing third-party access activity?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("098820dc-b598-445d-8fe7-378f32bd65a2"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f8af224d-6b60-4af1-b7f6-dd4286c301f3"),
                    MainText = "How often do you review remote access activity?",
                    OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("098820dc-b598-445d-8fe7-378f32bd65a2"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(thirdPartyAccessQuestions);

            #endregion

            return results.ToArray();
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.