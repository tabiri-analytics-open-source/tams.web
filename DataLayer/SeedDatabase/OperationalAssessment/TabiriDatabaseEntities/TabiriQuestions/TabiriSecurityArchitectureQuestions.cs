using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.TabiriDatabaseEntities.TabiriQuestions
{
    public static class TabiriSecurityArchitectureQuestions
    {
        public static Question[] Seed(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Empty;

            var results = new List<Question>();

            #region Network Protection

            subcategoryId = Guid.Parse("7ebe6f6c-001f-4022-bb69-6ebd6111338b");

            var networkProtectionQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("8ed4f592-0f40-45de-8ac5-65bf45e86a2d"),
                    MainText = "Does your organization have a Network Protection policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e6472728-52da-4379-89ad-015f39ab59df"),
                    MainText = "Have you defined processes and procedures for evaluating, deploying and managing network protection solutions?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("8ed4f592-0f40-45de-8ac5-65bf45e86a2d"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("8e22593e-d696-4f78-bc4e-396ef37fb7b4"),
                    MainText = "Have you defined processes and procedures for segmenting your network?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("8ed4f592-0f40-45de-8ac5-65bf45e86a2d"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e17a7139-a57f-4f09-aafb-3a4432a0faec"),
                    MainText = "How often is this policy reviewed?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("8ed4f592-0f40-45de-8ac5-65bf45e86a2d"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f45429e4-0e55-4531-a5f0-d772320d47ec"),
                    MainText = "Do you have network firewalls deployed at your organization?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("736b899e-4b86-46ed-ac08-c1a0bedb23ec"),
                    MainText = "What is the placement of the network firewalls?",
                    OptionChoices = "DMZ,Internal,Both",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f45429e4-0e55-4531-a5f0-d772320d47ec"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("e2bfe9f1-ee88-4fe6-81a9-251254bdbc85"),
                    MainText = "What Open Systems Interconnection (OSI) layers do your network firewalls support?",
                    OptionChoices = "Application layer,Network layer,Transport layer,Other",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("f45429e4-0e55-4531-a5f0-d772320d47ec"),
                    SubcategoryId = subcategoryId,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("201b76eb-a4c4-4654-8481-fcc2ceb2006e"),
                    MainText = "Are your network firewalls configured to block traffic that does not match an explicit allow rule?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f45429e4-0e55-4531-a5f0-d772320d47ec"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e79f9a27-a196-4cb0-9a8f-dfcd3a724fa2"),
                    MainText = "Are your network firewalls configured to silently drop blocked traffic?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f45429e4-0e55-4531-a5f0-d772320d47ec"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e89575d3-e7c0-4041-b269-a7ceffcda82f"),
                    MainText = "Are your network firewalls configured to send TCP Reset, ICMP Destination Unreachable, or other error codes on blocked traffic?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f45429e4-0e55-4531-a5f0-d772320d47ec"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("44d60782-a08e-45df-a905-f839274f0217"),
                    MainText = "Do you have a process for managing the firewall rules?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f45429e4-0e55-4531-a5f0-d772320d47ec"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("31ab46c6-450d-46c6-a1ad-5791a7ccb2d7"),
                    MainText = "What is the process for managing the firewall rules?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("44d60782-a08e-45df-a905-f839274f0217"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("209c493d-9807-4a5f-8fb2-bbf7d3394ed0"),
                    MainText = "Do you have a virtual/cloud computing infrastructure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 7,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f45429e4-0e55-4531-a5f0-d772320d47ec"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("00d47222-85e5-48b8-8a80-cd50b1befca1"),
                    MainText = "Do you have a similar network firewall configuration on your virtual/cloud computing infrastructure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("209c493d-9807-4a5f-8fb2-bbf7d3394ed0"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("226206a2-7e4c-4518-be22-2c50c6db6def"),
                    MainText = "Do you (or your provider) have a SIEM capability?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 8,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f45429e4-0e55-4531-a5f0-d772320d47ec"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("4d88bc24-edc9-468f-8eb2-56950be1427e"),
                    MainText = "Do you forward the network firewall logs to the SIEM?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("226206a2-7e4c-4518-be22-2c50c6db6def"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d59630ac-e56d-4046-b449-89d4831e7924"),
                    MainText = "Do you have a process to review network firewall logs?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 9,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f45429e4-0e55-4531-a5f0-d772320d47ec"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("5e370c1e-92d5-4dc2-898d-670d2f4d69c0"),
                    MainText = "What is the process for reviewing network firewall logs?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("d59630ac-e56d-4046-b449-89d4831e7924"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("be205769-ca38-49c2-8237-acdef0178318"),
                    MainText = "How often do you review network firewall logs?",
                    OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("d59630ac-e56d-4046-b449-89d4831e7924"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("85155451-075e-4e6e-8440-fafa6fe07370"),
                    MainText = "Do you have network Intrusion Detection Systems (IDS) or Intrusion Prevention Systems (IPS) deployed at your organization?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("55f55033-4e5c-4892-b0fe-e648185dfc98"),
                    MainText = "What is the placement of the network IDS/IPS?",
                    OptionChoices = "DMZ,Internal,Both",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("85155451-075e-4e6e-8440-fafa6fe07370"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("380bb9bb-ad93-452d-ba99-d49fab0696e7"),
                    MainText = "What mode is the network IDS/IPS deployed in?",
                    OptionChoices = "Block mode,Monitor only",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("85155451-075e-4e6e-8440-fafa6fe07370"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("af5b8901-42f7-4b24-9918-b75819019f18"),
                    MainText = "Do you have a process for managing the network IDS/IPS signatures?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("85155451-075e-4e6e-8440-fafa6fe07370"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a19720ad-b162-45a3-aa36-c075bf0d32d9"),
                    MainText = "What is the process for managing the network IDS/IPS signatures?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("af5b8901-42f7-4b24-9918-b75819019f18"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("79d3c133-a4e7-4763-baff-ee1a82de9536"),
                    MainText = "Does your network IDS/IPS have the capability to obtain full packet captures?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("85155451-075e-4e6e-8440-fafa6fe07370"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("b9d1cd37-2290-423a-a94f-1137c24b0155"),
                    MainText = "On average, how many days of network traffic are available from the full packet captures at any given time?",                    
                    Order = 1,
                    QuestionType = QuestionType.Integer,
                    ParentId = Guid.Parse("79d3c133-a4e7-4763-baff-ee1a82de9536"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("394fda23-f9ac-4203-b7a9-a04670222f3c"),
                    MainText = "Does your network IDS/IPS have the capability to decrypt SSL/TLS traffic prior to inspection?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("85155451-075e-4e6e-8440-fafa6fe07370"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("db93c757-7818-4792-8174-1238e04c14e0"),
                    MainText = "Does your network IDS/IPS have the capability to extract and analyze executable code in a malware sandbox?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("85155451-075e-4e6e-8440-fafa6fe07370"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ea7a000d-221e-4e24-a439-8bfc0a97d770"),
                    MainText = "Do you have a virtual/cloud computing infrastructure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 7,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("85155451-075e-4e6e-8440-fafa6fe07370"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b7d180bd-affa-45ed-beed-64c10e891053"),
                    MainText = "Do you have a similar network IDS/IPS configuration on your virtual/cloud computing infrastructure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("ea7a000d-221e-4e24-a439-8bfc0a97d770"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2bc25974-3e33-4cc8-b70d-740a41d2f6db"),
                    MainText = "Do you have a SIEM?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 8,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("85155451-075e-4e6e-8440-fafa6fe07370"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("0158c9d3-9fc6-4c6b-84fb-9fd9bf319aa2"),
                    MainText = "Do you forward the network IDS/IPS logs to the SIEM?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("2bc25974-3e33-4cc8-b70d-740a41d2f6db"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("71204591-5e3d-4d61-b40c-452a0784d7c5"),
                    MainText = "Do you have a process to review network IDS/IPS logs?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 9,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("85155451-075e-4e6e-8440-fafa6fe07370"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("45dd3c12-efb4-4576-9993-9b790e30b34a"),
                    MainText = "What is the process for reviewing network IDS/IPS logs?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("71204591-5e3d-4d61-b40c-452a0784d7c5"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0717191f-950b-4b17-a464-b746f0967025"),
                    MainText = "How often do you review network IDS/IPS logs?",
                    OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("71204591-5e3d-4d61-b40c-452a0784d7c5"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e6af3656-2eb8-439a-8e34-e8dd4af2f050"),
                    MainText = "Do you have network web proxies at your organization?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a37538b5-68aa-4dc9-9aca-2eeb3a4453e4"),
                    MainText = "What mode are the network web proxies deployed in?",
                    OptionChoices = "Block mode,Monitor only",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("e6af3656-2eb8-439a-8e34-e8dd4af2f050"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1fcd65ed-98d9-4b17-af47-52af8c4aa1ed"),
                    MainText = "What type of network web proxies are deployed?",
                    OptionChoices = "Transparent (inline),Non-transparent",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("e6af3656-2eb8-439a-8e34-e8dd4af2f050"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1dc2ba58-db9f-4e96-bfa7-0b189a5a5b0c"),
                    MainText = "Do you have any reverse web proxies deployed?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("e6af3656-2eb8-439a-8e34-e8dd4af2f050"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b552e285-a787-4bf2-b226-c699f8938a92"),
                    MainText = "What mode are the reverse web proxies deployed in?",
                    OptionChoices = "Block mode,Monitor only",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1dc2ba58-db9f-4e96-bfa7-0b189a5a5b0c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("add532d8-677d-49d4-8b41-755425df76df"),
                    MainText = "Are these reverse web proxies deployed on all publicly-facing web servers?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1dc2ba58-db9f-4e96-bfa7-0b189a5a5b0c"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("615a9b96-dfc3-4371-90bb-e031b7cb172e"),
                    MainText = "Do you have a process for managing the network web proxies rules?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("e6af3656-2eb8-439a-8e34-e8dd4af2f050"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("31668dca-04cc-43eb-8096-9161a8def627"),
                    MainText = "What is the process for managing the network web proxies rules?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("615a9b96-dfc3-4371-90bb-e031b7cb172e"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f3ea5928-079b-44dc-9b8d-30d8dc5e9812"),
                    MainText = "Do you have a virtual/cloud computing infrastructure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("e6af3656-2eb8-439a-8e34-e8dd4af2f050"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2caa1850-c68b-445f-a9a7-75660b75597a"),
                    MainText = "Do you have a similar network web proxies configuration on your virtual/cloud computing infrastructure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f3ea5928-079b-44dc-9b8d-30d8dc5e9812"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a2a8c8d7-69b9-4199-a3a8-dcb1622508c6"),
                    MainText = "Do you have a SIEM?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("e6af3656-2eb8-439a-8e34-e8dd4af2f050"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("1ec93417-cfb1-4248-839a-6cf0e1e5e843"),
                    MainText = "Do you forward the network web proxies logs to the SIEM?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("a2a8c8d7-69b9-4199-a3a8-dcb1622508c6"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("af88e1b2-b949-4a1f-bc41-a8bcc4122a40"),
                    MainText = "Do you have a process to review network web proxy logs?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 7,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("e6af3656-2eb8-439a-8e34-e8dd4af2f050"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c3a6f27e-d3e5-4357-93f1-5f2961d5ae3a"),
                    MainText = "What is the process for reviewing network web proxy logs?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("af88e1b2-b949-4a1f-bc41-a8bcc4122a40"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c3863fe1-fff0-4a41-8eca-f8e3457afb0f"),
                    MainText = "How often do you review network web proxy logs?",
                    OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("af88e1b2-b949-4a1f-bc41-a8bcc4122a40"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(networkProtectionQuestions);

            #endregion

            #region Endpoint Protection

            subcategoryId = Guid.Parse("72e5fa4d-5c0f-4d37-943c-d972393efdcb");

            var endpointProtectionQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("4d3a50a7-7769-43a8-b3db-74cb3b502228"),
                    MainText = "Does your organization have an Endpoint Protection policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ad293182-c873-42c6-9a7a-47b9dfbee64c"),
                    MainText = "Have you defined processes and procedures for evaluating, deploying and managing endpoint protection solutions?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("4d3a50a7-7769-43a8-b3db-74cb3b502228"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("09a78f5a-9fd2-4791-8f94-2c6cdb456216"),
                    MainText = "How often is this policy reviewed?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("4d3a50a7-7769-43a8-b3db-74cb3b502228"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("fc3145fe-70ca-4d61-9ea1-2a8dbc9b8de2"),
                    MainText = "Do you have a malware protection (antivirus) solution deployed on endpoints?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2c9420d9-6401-4dc2-a0c1-b9ae077a3fcb"),
                    MainText = "Does the malware protection solution have Endpoint Detection and Response capabilities?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("fc3145fe-70ca-4d61-9ea1-2a8dbc9b8de2"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("9bad0510-217d-449a-8575-46305a3d3e06"),
                    MainText = "What EDR capabilities are available?",
                    OptionChoices = "File forensics,Memory forensics,System containment",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("2c9420d9-6401-4dc2-a0c1-b9ae077a3fcb"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("bf0d5f9c-feb6-4c91-ba24-fcfacbe66097"),
                    MainText = "Does your malware protection solution have the capability to extract and analyze executable code in a malware sandbox?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("fc3145fe-70ca-4d61-9ea1-2a8dbc9b8de2"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("8453cc1f-417f-45f4-9f18-b027ed12ba1a"),
                    MainText = "What operating systems is the malware protection solution deployed on?",
                    OptionChoices = "Windows,macOS,Linux,Mobile (Android/iOS)",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 3,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("fc3145fe-70ca-4d61-9ea1-2a8dbc9b8de2"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2c7eaa4c-2e5e-4242-8bbb-d84e50a74145"),
                    MainText = "Is the malware protection solution centrally managed?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("fc3145fe-70ca-4d61-9ea1-2a8dbc9b8de2"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f20895a2-ca4a-4304-a9fb-58cb0b4ef57b"),
                    MainText = "What is the process for managing the malware protection solution?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("2c7eaa4c-2e5e-4242-8bbb-d84e50a74145"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("72b8e143-4626-481e-b227-94b3b0535446"),
                    MainText = "What components are centrally managed?",
                    OptionChoices = "Real-time scanning,Scheduled scans,Signature updates",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("2c7eaa4c-2e5e-4242-8bbb-d84e50a74145"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("220728b1-df98-4908-ba0b-4453f9017619"),
                    MainText = "Do you have a virtual/cloud computing infrastructure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("fc3145fe-70ca-4d61-9ea1-2a8dbc9b8de2"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e2a1421a-360f-4619-b8a8-a3a504a06254"),
                    MainText = "Do you have a similar malware protection configuration on your virtual/cloud computing infrastructure?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("220728b1-df98-4908-ba0b-4453f9017619"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("748a8011-8a1d-408e-b0ea-5b6f8c575dc5"),
                    MainText = "Do you have an endpoint firewall solution deployed on endpoints?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("6a24d8c1-f5c4-4b07-90cb-ec3426522933"),
                    MainText = "What operating systems is the endpoint firewall deployed on?",
                    OptionChoices = "Windows,macOS,Linux,Mobile (Android/iOS)",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("748a8011-8a1d-408e-b0ea-5b6f8c575dc5"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("15f359a9-9dfe-4e50-a467-93608eb5d651"),
                    MainText = "Is the endpoint firewall centrally managed?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("748a8011-8a1d-408e-b0ea-5b6f8c575dc5"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("6a9a6a90-cdce-4f63-a502-67cc2365532f"),
                    MainText = "What is the process for managing the endpoint firewall?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("15f359a9-9dfe-4e50-a467-93608eb5d651"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f77504eb-4c5d-4086-a890-7edbf3529749"),
                    MainText = "Do you have a tool to manage software patches on endpoints?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7ff7727b-7914-4696-8cf2-949eb261f7e4"),
                    MainText = "What operating systems is the patch management solution deployed on?",
                    OptionChoices = "Windows,macOS,Linux,Mobile (Android/iOS)",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("f77504eb-4c5d-4086-a890-7edbf3529749"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("dadb828b-472d-4a29-85c4-576c916bf308"),
                    MainText = "Is the patch management tool centrally managed?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f77504eb-4c5d-4086-a890-7edbf3529749"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("eee02c29-db5a-44c0-91c5-bafc8c8b5e87"),
                    MainText = "What is the process for managing the patch management tool?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("dadb828b-472d-4a29-85c4-576c916bf308"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("38a9fff6-55c9-4dd3-8a82-630866435e13"),
                    MainText = "Do you have a tool to manage the system configuration on endpoints?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0e780724-a238-4935-a4ab-d5b768fb60f2"),
                    MainText = "What operating systems is the system configuration solution compatible with?",
                    OptionChoices = "Windows,macOS,Linux,Mobile (Android/iOS)",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("38a9fff6-55c9-4dd3-8a82-630866435e13"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("29b27b55-c034-46f7-8869-a6f97322f5ad"),
                    MainText = "Is the system configuration tool centrally managed?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("38a9fff6-55c9-4dd3-8a82-630866435e13"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("06760177-b2e8-419d-8d0a-2697046dde93"),
                    MainText = "What is the process for managing the system configuration tool?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("29b27b55-c034-46f7-8869-a6f97322f5ad"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e0eef8bc-5add-4d20-9dd5-99ced58bca74"),
                    MainText = "Do you (or your managed security service provider) have a SIEM capability?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0e6edc15-5b9e-40f0-a782-a5723b72d6a2"),
                    MainText = "Do you forward endpoint logs to the SIEM?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("e0eef8bc-5add-4d20-9dd5-99ced58bca74"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c72a43d7-408b-4fbb-aa24-01ef71460ada"),
                    MainText = "What endpoint logs do you forward to the SIEM?",
                    OptionChoices = "Configuration changes,Endpoint firewall,Malware protection,Other",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("0e6edc15-5b9e-40f0-a782-a5723b72d6a2"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("60a91727-21e0-405d-bcc6-db829fc9ae18"),
                    MainText = "Do you have a process to review endpoint logs?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 7,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("950027df-e946-42c7-90f5-a55b63c0af2c"),
                    MainText = "What is the process for reviewing endpoint logs?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("60a91727-21e0-405d-bcc6-db829fc9ae18"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id =Guid.Parse("1d5d04e4-4815-4e6f-97da-03586531995d"),
                    MainText = "How often do you review endpoint activity?",
                    OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("60a91727-21e0-405d-bcc6-db829fc9ae18"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(endpointProtectionQuestions);

            #endregion

            #region Application Protection

            subcategoryId = Guid.Parse("0cf39325-3c91-453f-a254-a482d86575e6");

            var applicationProtectionQuestions = new List<Question>
            {
                new Question 
                {
                    Id = Guid.Parse("1653d5fb-f539-47d5-a5e3-176add784818"),
                    MainText = "Does your organization have an Application Protection policy?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("7bf80757-ee10-4402-8bad-9012d4a986d2"),
                    MainText = "Does the policy cover the Software Development Life Cycle (SDLC) for internally developed software applications?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1653d5fb-f539-47d5-a5e3-176add784818"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("490afb31-65ab-43f6-b30c-bbf874abeed3"),
                    MainText = "Does the policy cover training for your developers on secure software development practices?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1653d5fb-f539-47d5-a5e3-176add784818"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("123b5011-a7dc-4fda-8bf8-7ae3a63f869e"),
                    MainText = "Have you defined processes and procedures for certifying third-party software prior to acquisition?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1653d5fb-f539-47d5-a5e3-176add784818"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("9f862c9b-4199-48a4-a8a1-9733cb3730e6"),
                    MainText = "Have you defined processes and procedures for evaluating, deploying and managing application protection solutions?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1653d5fb-f539-47d5-a5e3-176add784818"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b79968fe-b075-422e-be3f-602fd68903af"),
                    MainText = "How often is this policy reviewed?",
                    OptionChoices = "Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("1653d5fb-f539-47d5-a5e3-176add784818"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d7b2bb2f-569f-4c96-8626-95f907fb2bea"),
                    MainText = "Does your organization develop software applications for internal or external use?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f1a3cd7c-c279-42f8-bf81-dcc8030c5781"),
                    MainText = "Do you have a process to review source code for security flaws?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("d7b2bb2f-569f-4c96-8626-95f907fb2bea"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("5c575f24-f217-473b-bb1e-e4732a0fa5a1"),
                    MainText = "What is the process for reviewing source code for security flaws?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("f1a3cd7c-c279-42f8-bf81-dcc8030c5781"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("94c7aa67-7014-44fe-8817-2e9bffd81b4e"),
                    MainText = "Do you use automated tools to review source code for security flaws?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f1a3cd7c-c279-42f8-bf81-dcc8030c5781"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("61355ea7-3437-425d-9010-7534e6a23937"),
                    MainText = "How often do you review source code for security flaws?",
                    OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("f1a3cd7c-c279-42f8-bf81-dcc8030c5781"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("e4b97b18-9ec2-4b33-82ef-55cb814bb7d1"),
                    MainText = "Do you have a process to digitally sign software applications developed by your organization?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("d7b2bb2f-569f-4c96-8626-95f907fb2bea"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("30ef1322-e537-4fea-b6b2-2d017d34a626"),
                    MainText = "What is the process to digitally sign software applications developed by your organization?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("e4b97b18-9ec2-4b33-82ef-55cb814bb7d1"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("03d80c45-6b82-433d-ae61-36bb603da264"),
                    MainText = "Do you maintain an inventory of software applications at your organization?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c12fb80c-150a-4e19-a05e-63711b64eaa5"),
                    MainText = "What is the process for managing this software inventory?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("03d80c45-6b82-433d-ae61-36bb603da264"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("0c7692b6-1101-4ea0-81d8-9fa5a927bb13"),
                    MainText = "Is the software inventory automatically updated?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("03d80c45-6b82-433d-ae61-36bb603da264"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("dcaf3f79-8acb-4735-a321-c024567b24b2"),
                    MainText = "How often is this software inventory updated?",
                    OptionChoices = "Daily,Weekly,Monthly,Quarterly,Semi-annually,Annually",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("03d80c45-6b82-433d-ae61-36bb603da264"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b6943c20-5c7c-4bb7-975d-0b17f749f3f2"),
                    MainText = "Do you have a tool to manage what software applications users can run (application whitelisting)?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 4,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("72568809-34e9-43e8-9c76-39ecb7996d56"),
                    MainText = "What mechanism does this tool use to whitelist applications?",
                    OptionChoices = "File names/paths,File hashes,Digital signatures",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b6943c20-5c7c-4bb7-975d-0b17f749f3f2"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("ba1b9608-5cce-4a7e-b742-81470e96f619"),
                    MainText = "What systems is this application whitelisting tool deployed on?",
                    OptionChoices = "Workstations/laptops,Servers,Mobile devices",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b6943c20-5c7c-4bb7-975d-0b17f749f3f2"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("182e08af-2298-451e-ae87-64bc3d362cde"),
                    MainText = "Is the application whitelisting tool centrally managed?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("b6943c20-5c7c-4bb7-975d-0b17f749f3f2"),
                    SubcategoryId = subcategoryId,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("9a3740a3-b702-47e4-975f-01b5e9314d10"),
                    MainText = "What is the process for managing the application whitelisting tool?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("182e08af-2298-451e-ae87-64bc3d362cde"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("a9c8b86b-e9ec-4be8-a25c-f896f806f0ff"),
                    MainText = "Have you enabled logging on critical applications?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 5,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("c9add263-64ac-403d-b9fd-a980481395c9"),
                    MainText = "Do you log activity on internal web applications?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("a9c8b86b-e9ec-4be8-a25c-f896f806f0ff"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("8168ad00-9525-4df7-bc41-8af5dbca60ec"),
                    MainText = "What logging level is enabled on web applications?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("c9add263-64ac-403d-b9fd-a980481395c9"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("1510d07f-e41a-47a8-8949-477c00d75e0d"),
                    MainText = "Do you log activity on email applications?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("a9c8b86b-e9ec-4be8-a25c-f896f806f0ff"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f1939b62-676c-4e06-bc75-9af96ae2dfa5"),
                    MainText = "What logging level is enabled on email applications?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("1510d07f-e41a-47a8-8949-477c00d75e0d"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("680288f9-519f-4ed0-9982-6c66c3605633"),
                    MainText = "Do you log activity on database applications?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("a9c8b86b-e9ec-4be8-a25c-f896f806f0ff"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("93d66348-4211-4191-b615-8fa3118da213"),
                    MainText = "What logging level is enabled on database applications?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("680288f9-519f-4ed0-9982-6c66c3605633"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("2a96327f-59da-49cb-bf68-771e8e4e44ab"),
                    MainText = "Have stored procedures and prepared statements been documented?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("680288f9-519f-4ed0-9982-6c66c3605633"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("d38bd2d0-0aa8-4467-80e8-663e6b70f041"),
                    MainText = "Do you have a tool to manage email encryption?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 6,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("f788cf1a-ab61-45a2-9d25-268bbdbdfb6f"),
                    MainText = "What is the process for managing email encryption?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("d38bd2d0-0aa8-4467-80e8-663e6b70f041"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("01ef7f66-dc7f-458b-b1a2-1fdbe014de28"),
                    MainText = "Are staff trained on how to use this email encryption tool?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("d38bd2d0-0aa8-4467-80e8-663e6b70f041"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("b1ef01c8-ec4e-45a8-a179-f4b740484766"),
                    MainText = "Is the email encryption tool centrally managed?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 3,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("d38bd2d0-0aa8-4467-80e8-663e6b70f041"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("72a68d17-9ca0-4317-99c2-b8f367a36c64"),
                    MainText = "Do you have a tool to manage email spam?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 7,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("4cb75dfc-7e05-42d9-9f9d-3788e358714b"),
                    MainText = "What is the process for managing email spam?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("72a68d17-9ca0-4317-99c2-b8f367a36c64"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("198448fb-6f1c-4d5d-aa04-79fd0e746725"),
                    MainText = "Have you implemented an email authentication solution?",
                    HintText = "e.g. Sender Policy Framework (SPF), DomainKeys Identified Mail (DKIM)",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("72a68d17-9ca0-4317-99c2-b8f367a36c64"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("c48b67ba-147d-437a-b852-2c0391a5d978"),
                    MainText = "Do you (or your managed security service provider) have a SIEM capability?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 8,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("3e72f64b-837e-4c7c-8171-398296103aef"),
                    MainText = "Do you forward application logs to the SIEM?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 1,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("c48b67ba-147d-437a-b852-2c0391a5d978"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("1bddf0ec-702e-445b-80c7-15c43e9afcdd"),
                    MainText = "What application logs do you forward to the SIEM?",
                    OptionChoices = "Web logs, Email logs, Database logs, Other critical application logs",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 1,
                    QuestionType = QuestionType.SelectMultiple,
                    ParentId = Guid.Parse("3e72f64b-837e-4c7c-8171-398296103aef"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("914cd1cf-24a8-431b-acb5-811e4fa23d03"),
                    MainText = "Do you have a process to review application logs?",
                    OptionChoices = "Yes,No",
                    OptionsOrientation = OptionsOrientation.Horizontal,
                    ActiveOptionIndex = 0,
                    Order = 9,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = null,
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question
                {
                    Id = Guid.Parse("1eba206c-2ded-4c37-b38e-0528b48e4d4e"),
                    MainText = "What is the process for reviewing application logs?",                    
                    Order = 1,
                    QuestionType = QuestionType.Text,
                    ParentId = Guid.Parse("914cd1cf-24a8-431b-acb5-811e4fa23d03"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Question 
                {
                    Id = Guid.Parse("8e8b319f-f56c-4e1b-9f84-cb612a268803"),
                    MainText = "How often do you review application activity?",
                    OptionChoices = "Monthly,Quarterly,Semi-annually,Annually,Other",
                    OptionsOrientation = OptionsOrientation.Vertical,
                    Order = 2,
                    QuestionType = QuestionType.SelectSingle,
                    ParentId = Guid.Parse("914cd1cf-24a8-431b-acb5-811e4fa23d03"),
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
            results.AddRange(applicationProtectionQuestions);

            #endregion

            return results.ToArray();
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.