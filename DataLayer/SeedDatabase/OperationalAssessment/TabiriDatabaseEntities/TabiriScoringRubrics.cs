using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.DataLayer.SeedDatabase.OperationalAssessment.TabiriDatabaseEntities
{
    public static class TabiriScoringRubrics
    {
        public static ScoringRubric[] Seed(Guid createdBy, DateTime currentTimestamp)
        {
            var scoringRubrics = new List<ScoringRubric>();  

            scoringRubrics.AddRange(StrategicPlanningScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(SecurityPolicyFrameworkScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(OrganizationalStructureScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(PerformanceMetricsScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(WorkforceManagementScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(RiskManagementFrameworkScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(ThreatManagementScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(SecurityAwarenessScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(DataProtectionFrameworkScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(DataClassificationScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(DataProtectionPoliciesScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(DataRetentionScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(DataLossScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(DataRecoveryScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(IdentityManagementScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(AccessControlsScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(SeparationOfDutiesScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(PrivilegedAccessManagementScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(RemoteAccessScoringRubrics(createdBy, currentTimestamp));
            scoringRubrics.AddRange(ThirdPartyAccessScoringRubrics(createdBy, currentTimestamp));    
            scoringRubrics.AddRange(NetworkProtectionScoringRubrics(createdBy, currentTimestamp));    
            scoringRubrics.AddRange(EndpointProtectionScoringRubrics(createdBy, currentTimestamp));   
            scoringRubrics.AddRange(ApplicationProtectionScoringRubrics(createdBy, currentTimestamp));   
            scoringRubrics.AddRange(IncidentReadinessScoringRubrics(createdBy, currentTimestamp));   
            scoringRubrics.AddRange(IncidentDetectionScoringRubrics(createdBy, currentTimestamp)); 
            scoringRubrics.AddRange(IncidentRemediationScoringRubrics(createdBy, currentTimestamp));

            return scoringRubrics.ToArray();
        }

        private static List<ScoringRubric> StrategicPlanningScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("5e0fdb74-9606-4776-ab23-24bf5898160d");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("9ac796fa-11c6-49bd-9d0d-65e186595f5e"),
                    Title = "Security department role and mission is not defined nor understood; organization does not recognize a need for dedicated security funding",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("14a01fe9-ea4b-4230-8948-a75c5a060609"),
                    Title = "Security department role and mission are defined but not well communicated; limited security budget exists but with no clear accountability",
                    InstructionText = "01, 05.a.i",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("84feb74a-c797-4a13-a609-918623d5fe25"),
                    Title = "Members of the security department clearly understand their role and mission; adequate security budget with limited accountability",
                    InstructionText = "01, 02, (05.a.ii or 05.a.iii), 05.b",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("10edf132-3c76-4b7f-b8f4-d34bd1ede6d5"),
                    Title = "Other departments understand the role and mission of the security department; goals are tracked and measured",
                    InstructionText = "01, 02.a, (05.a.ii or 05.a.iii), 05.b, 05.c",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("ee71ddae-2c3c-4b23-b201-c120981ae4a7"),
                    Title = "Management is continuously briefed on security department activities; goal outcomes drive future security planning",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };               
        }

        private static List<ScoringRubric> SecurityPolicyFrameworkScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("3df232a9-b172-4cd0-9d91-d3d025e5f36f");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("23520a52-cff7-4235-8fb8-e7258233e2fd"),
                    Title = "No documented policies, standards or procedures",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("eb8d0faa-03f6-46db-9879-4e2791bbd08f"),
                    Title = "Some documented policies, but few if any standards and procedures",
                    InstructionText = "01.a",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("d7b45a2a-d83f-46a0-b8ad-b2d7b84d7dea"),
                    Title = "Organization has documented policies, standards and procedures; benchmarked to industry frameworks",
                    InstructionText = "01.a, 01.b, 01.c, 02",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("d2608320-57e7-4ecc-9cb2-0855a25fa4bd"),
                    Title = "Regular reviews and revisions are carried out",
                    InstructionText = "01.a, 01.b, 01.c, 02, 01.f",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("dd61e311-6766-43ad-9cbb-a63a9a954a50"),
                    Title = "Clear accountability by linkage to individual employee performance",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }; 
        }

        private static List<ScoringRubric> OrganizationalStructureScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("14d74f5a-aae4-4958-aaf5-c10af094515f");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("ef1f1afd-4ef2-4231-a92c-3be85f496ebc"),
                    Title = "No formal IT Security team structure",
                    InstructionText = "01, 02, 05",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("60d20c13-e653-470b-b3c2-785bfd80ca99"),
                    Title = "IT Security team roles are not distinct nor well defined; insufficient staffing levels",
                    InstructionText = "(02.a or 02.d), 03<2",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("a9daa24b-81d8-4528-9e30-7634146e4718"),
                    Title = "IT Security team roles are separate from other IT functions; sufficient staffing levels",
                    InstructionText = "01, 03>2, 05",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("56e170cd-3d32-4471-8a8f-b987195a8f3a"),
                    Title = "IT Security department has an independent reporting structure",
                    InstructionText = "01, (02.b or 02.c), 03>=3, 04, 05",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("f52299c0-48d4-4826-b552-b871bc418e41"),
                    Title = "Excellent coordination between IT security, other business units and third parties, both locally and across geographic regions",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };   
        }

        private static List<ScoringRubric> PerformanceMetricsScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("dcbecea7-7a7e-4378-bd4c-acc6ac24afc1");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("09896d56-c00a-4be8-92ed-74b82aca9b17"),
                    Title = "Organization does not collect security metrics",
                    InstructionText = "01",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("7fb3a6f3-2fae-49c4-89bf-ed98e55974bf"),
                    Title = "Organization collects some metrics, but with no clear criteria; no management reporting",
                    InstructionText = "01.a",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("40a6696d-dec0-4a91-9b15-2933a70e0f0d"),
                    Title = "Metrics reported to management, but no defined KPIs; Collection and reporting are periodic",
                    InstructionText = "01.a, 01.c, 01.d.i(annual or quarterly)",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("2763da1a-2aa8-4ccb-85ea-9060b5f0e0da"),
                    Title = "Defined KPIs and real-time collection and reporting",
                    InstructionText = "01.a, 01.b, 01.c.ii, 01.d.i(>=monthly), 01.d.ii",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("904fe1c2-13cd-4783-8e2f-f35499019ad8"),
                    Title = "Metrics are independently audited and have a well defined role in process improvement and strategic planning",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };   
        }

        private static List<ScoringRubric> WorkforceManagementScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("7969918f-98f9-4436-8a07-ae7a9cccf9b5");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("e5c8ff08-2d9f-4637-8832-db677f8c89d6"),
                    Title = "Organization cannot determine if security team staff are adequately qualified",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("bdf30ad2-37af-4d4e-b58f-808fe5751394"),
                    Title = "Security team staff are qualified based on measured skills and knowledge",
                    InstructionText = "01, 06",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("50ec3191-981a-4f43-97c8-2fa07a91e415"),
                    Title = "A formal process exists to provide continuous professional training",
                    InstructionText = "01, 02, 06",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("f686c78b-d610-4030-a35a-e36a1b81227e"),
                    Title = "A clear career path exists with opportunities for job rotation",
                    InstructionText = "01, 02, 03, 05.c, 06",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("2bc4f2fa-2537-450c-982a-ac6cbfd98b88"),
                    Title = "Well defined job retention program with industry benchmarked incentive packages",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };  
        }

        private static List<ScoringRubric> RiskManagementFrameworkScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("38a12386-d26f-4921-9e4a-08abe7d8e4cb");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("83ad66c6-2ead-412d-92e2-d339d781f7a4"),
                    Title = "Organization has no formal risk management team, risk assessment framework, nor understanding of compliance requirements",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("b2f20220-406b-4d77-b255-7ebb7e01fd73"),
                    Title = "Risk management team exists, there is a documented risk assessment of critical systems and production systems, and an understanding of compliance requirements",
                    InstructionText = "01, 02.d[critical only], 02.k, 03",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("d2df13af-64fc-4070-b6b0-52d477ee8abb"),
                    Title = "IT Security team has representation on the Risk Management team, risk assessments conducted semi-annually or better, covers all systems, integrate with other IT activities, and  remeditian actively tracked",
                    InstructionText = "01, 01.b, 01.c, 02.b[>=semi-annual], 02.d[all systems], 02.e, 02.h, 02.k, 03",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("229eed93-dc00-4d0f-bf9c-7d61db1428b7"),
                    Title = "Risk assessment conducted quarterly or better, formal documentation of risks in a regularly updated risk register, clear ownership, progress tracking, immediate validation, third-party audits, and mitigating controls for identified risks",
                    InstructionText = "01, 01.b, 01.c, 02.b[>=quarterly], 02.c,  02.d[all systems], 02.e, 02.f, 02.g, 02.h, 02.i, 02.j, 02.k, 03",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("c3ea9499-c7ea-4e5d-b794-53fb2db666a9"),
                    Title = "IT security is a joint responsibility of all teams, real-time communication of IT security risks, integration with the overall Strategic Plan, use of automated risk assessment tools, use of automated compliance review tools",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }; 
        }

        private static List<ScoringRubric> ThreatManagementScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("7c0fa42c-e5e3-4d16-9599-963f9041ed9f");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("e8d2d40d-eaa4-44d4-891b-52d989bbe0f6"),
                    Title = "Organization has no structured threat management process",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("93ebddc3-28be-4829-ab85-fe2c2b7af80f"),
                    Title = "Organization conducts limited scope vulnerability scans once or twice a year",
                    InstructionText = "01, 02.a[>=semiannual], 02.b[unauthenticated only]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("01caefd1-4f87-4e10-8706-ceeeeb4198f1"),
                    Title = "Vulnerability scans cover all assets, automated scans run on a scheduled basis, annual penetration tests, and basic SIEM deployment",
                    InstructionText = "01, 02.a[>=monthly], 02.b[all], 03.a[annual], 06.a[network+endpoint logs]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("b5148112-4dcd-45a9-ab05-1eb86db512a1"),
                    Title = "Technical findings translated to business risk; remediation efforts tracked for completion; threat intel integration with SIEM",
                    InstructionText = "01, 02.a[>=weekly], 02.b[all], 03.a[semiannual], 05.b, 05.c, 06.a[all], 06.b",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("ab3b6325-88e3-4637-b51d-508ca98de60a"),
                    Title = "Emerging threats actively tracked and communicated, active collaboration with peers and authorities, and automated threat intel sharing",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };  
        }

        private static List<ScoringRubric> SecurityAwarenessScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("b9ec8ca6-7e6e-4fe6-8e8d-7b768eba2648");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("98f5c3c4-7580-4585-b406-8a4c07a261b5"),
                    Title = "Organization has no formal IT security awareness program",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("be669fcb-e006-4d1b-a680-81a3431602bf"),
                    Title = "Program exists, program budget allocated, limited training audience and scope, and conducted annually",
                    InstructionText = "01.b[some], 01.f.i[newsletters], 01.f.ii[annual], 01.h.i[some topics]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("de6cf51b-c1b7-4be1-a783-e4e6f711a0cf"),
                    Title = "Wider training audience and scope, clear progress tracking, and conducted semiannually",
                    InstructionText = "01.b[most], 01.f.i[workshops/courses], 01.f.ii[>=semiannual], 01.h.i[more topics], 01.i",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("a88a2a41-075f-4f99-a581-9e1804637814"),
                    Title = "Mandatory training for all employees and third-parties, role-specific training for employees in sensitive roles, and regular revisions to the program",
                    InstructionText = "01.b[all], 01.d, 01.e,  01.f.i[workshops/courses], 01.f.ii[>=quarterly], 01.g, 01.h.i[all topics], 01.i, 01.j.i[annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("e0b3e196-9230-4f43-a513-6b1b23982eb5"),
                    Title = "IT security awareness incorporated into employee performance, training content tracks emerging threats, and metrics have demonstrated improvement over time",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }; 
        }

        private static List<ScoringRubric> DataProtectionFrameworkScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("ec0ed13d-e887-41e2-a0cd-62fd1ca2de25");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("c04ee44b-e928-4e14-9093-76ba938d9353"),
                    Title = "Organization has no formal data protection framework",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("94002eea-bfa5-4e1a-a9b6-ecb88c89572a"),
                    Title = "Data protection framework exists, organization understands the types of data it processes, and framework covers some topics",
                    InstructionText = "01, 01.c[some topics], 02",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("0f25efd5-9dd3-4661-84b4-840f7d71f4d7"),
                    Title = "Organization has a process to track different types of data, framework covers all topics, based on industry standards, and reviewed yearly",
                    InstructionText = "01, 01.c[all topics], 01.d, 01.e[annual], 02, 02.b",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("4539c821-c566-4e7c-88d6-0019faf17d3d"),
                    Title = "Organization formally defines and tracks data owners, and conducted reviews at least twice a year",
                    InstructionText = "01, 01.c[all topics], 01.d, 01.e[>=semiannual], 02, 02.b, 03",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("252fb52b-8d61-441e-8d40-04bb628c2e02"),
                    Title = "Organization can track data in real-time and makes continuous adjustments to the framework based on business risk",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }; 
        }

        private static List<ScoringRubric> DataClassificationScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("8859dd17-94e3-433d-90d8-2fcf3cffaac9");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("e4cc41b0-235e-4b63-8e41-46d9c423def2"),
                    Title = "Organization has no formal data classification policy and data handling guidelines do not exist",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("b226e5ba-6a26-44b2-9784-fe377e17116d"),
                    Title = "Organization has a data classification policy and data handling guidelines exist",
                    InstructionText = "01, 02",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("f3d3bbe0-d395-4927-9e42-d7ce5fd432ed"),
                    Title = "Data owners are aware of and follow data handling guidelines, enforcement mapped to access control policies and annual reviews",
                    InstructionText = "01, 02, 02.a, 02.b, 02.b.ii, 03, 03.b[annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("d144398c-2b5f-45f6-8559-3189e8b942f1"),
                    Title = "Data marking and handling is consistent across the organization and frequent reviews",
                    InstructionText = "01, 02, 02.a, 02.b, 02.b.ii, 02.b.iii, 03, 03.b[>=quarterly]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("10e6e82c-6ca3-4ae5-86aa-6698d31aff63"),
                    Title = "Data handling enforced via automated DLP solution, automated notifications for data marking changes and real-time reviews",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };  
        }

        private static List<ScoringRubric> DataProtectionPoliciesScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("ed613e1b-6a1a-4d6b-8efc-216f9520226a");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("1e3241ce-7904-4d7a-9418-117740c174fe"),
                    Title = "Organization has no formal data protection policy",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("f9e745c1-15ce-4e8b-9cd5-4bcc38b8411d"),
                    Title = "Data protection policies exist with defined standards",
                    InstructionText = "01.a, 01.b, 02.a, 02.b",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("269bb047-7a21-4fdb-98e9-690fd1e3ddbb"),
                    Title = "Data owners understand how to protect data in some states, documented key management procedure, and annual reviews are conducted",
                    InstructionText = "01.a, 01.b, 01.f, 01.g.i[annual], 02.a, 02.b, 02.f.i[annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("ea32b606-6a9b-48e2-9809-ab921f349402"),
                    Title = "Data owners understand how to protect data in all states, and frequent reviews are conducted",
                    InstructionText = "01.a.ii, 01.b.ii, 01.c.ii, 01.d.ii, 01.e.ii, 01.f.ii, 01.g.i[>=quarterly], 02.a.ii, 02.b.ii, 02.c.ii, 02.d.ii, 02.e.ii, 02.f.i[>=quarterly]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("be34a4df-527c-4762-a621-7b58e6961a0f"),
                    Title = "Process in place to conduct automated reviews with DLP enforcement",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };
        }

        private static List<ScoringRubric> DataRetentionScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId =  Guid.Parse("b679ff1a-2121-45de-a695-9517c3b5319e");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("d51430d0-12f0-452a-ade5-ec227594b0ea"),
                    Title = "Organization has no defined data retention policy or disposal guidelines",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("13077fdb-2b32-4793-bfad-de2fcebb183e"),
                    Title = "Data retention policy and disposal guidelines are defined",
                    InstructionText = "01, 03",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("f7cf0b05-9907-4582-b74c-9d6c5af8f5b4"),
                    Title = "Data owners are aware, training is provided, and annual reviews are conducted",
                    InstructionText = "01, 01.c, 01.d[annual], 03, 03.b, 03.e[annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("336c6a15-69f3-4878-9a0f-6b523b9813b0"),
                    Title = "Organization has a procedure for securing retained data, securely erasing data, and frequent reviews are conducted",
                    InstructionText = "01, 01.c, 01.d[>=semiannual], 02, 03, 03.b, 03.c, 03.d, 03.e[>=semiannual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("b07595ea-53b5-4670-8b27-d4cbe178a371"),
                    Title = "Organization has automated systems to retain, secure and dispose data",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }; 
        }

        private static List<ScoringRubric> DataLossScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("95bfad59-1224-414a-8c51-d688abac70ed");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("e3efcdce-30a3-424f-bbde-e715678c30be"),
                    Title = "Organization has no formal data loss policy",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("7f5e7610-97e6-451a-80d6-c6f5a4539339"),
                    Title = "Data loss policy exists and includes a defined breach notification procedure",
                    InstructionText = "01.a",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("5b3c1c6f-7e1e-4cb2-aee5-ad12cc444f5d"),
                    Title = "Data owners are aware of this policy, organization relies on self-reporting, and conducts annual reviews",
                    InstructionText = "01.a, 01.b, 01.c[annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("869b09fb-81be-4274-9cb2-e9ca477c8772"),
                    Title = "Organization has technology solutions in place to detect data loss and critical systems, conducts frequent reviews, and has cyber insurance coverage",
                    InstructionText = "01.a, 01.b, 01.c[>=semiannual], 02.a.ii[critical systems]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("86384e1d-9ad6-42d7-a0b6-90cafc136a12"),
                    Title = "Technology solutions cover all systems with automated loss notification",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };  
        }

        private static List<ScoringRubric> DataRecoveryScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("673d41d1-e017-4b8a-ad8e-a0992882f385");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("4bcd78a2-d1bd-42e8-9578-94886ebf8ed0"),
                    Title = "Organization has no formal data recovery policy",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("eb2e105b-3925-4551-a4bf-c54de820de72"),
                    Title = "Data recovery policy exists and includes a disaster recovery plan",
                    InstructionText = "01.a",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("8822fb34-2b5f-45cc-986c-d21f1b702b60"),
                    Title = "Data owners are aware, delegates are trained, backups conducted monthly, annual policy reviews, and monthly data recovery tests",
                    InstructionText = "01.a, 01.a.ii, 01.b[annual], 02.a[monthly], 04.b[annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("73fdd083-876b-4876-949b-1a2c0242850d"),
                    Title = "Sensitive backups are encrypted, backups conducted weekly, off-site backup storage, frequent policy reviews, and frequent recovery tests",
                    InstructionText = "01.a, 01.a.ii, 01.b[>=semannual], 02.a[weekly], 04.b[annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("11248071-fbbc-4bc7-bad5-0db60e7db9ac"),
                    Title = "Backups conducted daily, daily data recovery tests, and real-time failover for critical systems",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }; 
        }

        private static List<ScoringRubric> IdentityManagementScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId =  Guid.Parse("a4c329d6-193e-441f-b0cb-97b9770c7265");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("4575f914-9825-45a0-b9e4-3ad5373da844"),
                    Title = "Organization has no identity management policies, and ad-hoc user provisioning/deprovisioning procedures",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("3c3d1271-0d09-4d58-9d22-e71be27cc49d"),
                    Title = "Policies and procedures are documented, multiple identity sources, and no IAM solution",
                    InstructionText = "01",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("05856843-2442-45a4-b0f0-85126ac6aa6b"),
                    Title = "Single authoritative identity source, background checks performed, IAM solution implemented, manual account provision/deprovision procedures, annual account reviews, and annual policy reviews",
                    InstructionText = "01.a, 01.e, 01.f[annual], 02, 05.b[annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("d1a6ad0c-80ff-4342-8545-5db781852d19"),
                    Title = "Common identities across systems, automated account provision/deprovision procedures, account deprovision validation,  frequent account reviews, and frequent policy reviews (YES to 01.a, 01.d, 01.e, 01.f[>=semiannual], 02, 03.c, 04.d, 04.f, 05.b[>=monthly])",
                    InstructionText = "01.a, 01.d, 01.e, 01.f[>=semiannual], 02, 03.c, 04.d, 04.f, 05.b[>=monthly]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("80163f73-8b92-4658-b62e-0c79bb8f34ce"),
                    Title = "Automated account reviews and notification of discrepancies",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }; 
        }

        private static List<ScoringRubric> AccessControlsScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("eb909bf7-654c-4314-b1c8-6356169a2c6f");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("33277d52-00c9-4a04-acae-ac2d04b4bd6d"),
                    Title = "Organization has no access control policies",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("321eea91-6a5f-4925-a5d1-8c9909ac6c59"),
                    Title = "Policy defined, and some standards defined",
                    InstructionText = "01.a[password complexity & account lockout], 01.c[annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("20a97b1f-e415-468d-aa00-5c046b5f7254"),
                    Title = "All password standards defined, standards applied uniformly, least privilege standard implemented, two-factor authentication implemented, access control monitoring procedures, and annual policy reviews",
                    InstructionText = "01.a[all], 01.a.i, 01.b, 01.c[annual], 02, 03",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("35d8283d-b78f-4a51-a7ba-0e1507d90e9e"),
                    Title = "Centralized Log Management solution implemented, with defined log retention schedule, log review procedures, and frequent policy reviews",
                    InstructionText = "01.a[all], 01.a.i, 01.b, 01.c[>=semiannual], 02, 03.a",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("0a832efe-2337-4fd7-91f2-0ccbeaa167fa"),
                    Title = "Strict least privilege implementation on all systems, two-factor authentication on all critical systems/applications, automated access control violation notification, CLM integrated with SIEM",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }; 
        }

        private static List<ScoringRubric> SeparationOfDutiesScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("5e1e6a79-046c-4fbd-89c0-3b41fc69e555");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("ba0b3d41-7559-4557-a5bb-86ec31570227"),
                    Title = "Organization has no separation of duties policy",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("a392218a-96bc-48a6-af37-02d324be047e"),
                    Title = "Policy defined and separation between production and dev/test environments",
                    InstructionText = "01.b",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("666b1dbe-3e09-45d1-854f-5cf2619d5fb1"),
                    Title = "User restriction on administrator level changes, similar controls across production and dev/test environments, developer access restrictions, and annual policy reviews (YES to 01.a, 01.b.i, 01.b.ii, 01.b.iii, 01.c[annual])",
                    InstructionText = "01.a, 01.b.i, 01.b.ii, 01.b.iii, 01.c[annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("f6aeddb0-1e35-4d76-8c05-5f276a5233ea"),
                    Title = "RBAC implemented, enforced reviews prior to code approvals, RBAC role reviews, frequent policy reviews",
                    InstructionText = "01.a, 01.b.i, 01.b.ii, 01.b.iii, 01.b.iii.3, 01.c[>=semiannual], 02, 02.e[>=annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("fbb45313-671c-49c5-af4a-62d021cda58e"),
                    Title = "RBAC integrated with IAM solution, automated role reviews and notification of discrepancies",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };  
        }

        private static List<ScoringRubric> PrivilegedAccessManagementScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("4b17e07a-b608-4406-bdeb-32e2653a26cb");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("dd174292-4d04-4727-bbbb-5b6f0c8480dc"),
                    Title = "Organization has no privileged access management policy",
                    InstructionText = "01, 03, 04, 05",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("8cf8e481-22fd-4cac-bdf8-6b49742e3c43"),
                    Title = "Policies are documented and an inventory of privileged accounts exists",
                    InstructionText = "01.a, 03",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("e304b2bb-c77b-46ed-be85-acc4243ed31d"),
                    Title = "Separate privileged/standard accounts, annual policy reviews, and regular privileged account activity reviews",
                    InstructionText = "01.a, 01.b.i, 01.c[annual], 03, 05.b[monthly]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("a8a162c0-b5a7-4291-8568-21efb039f680"),
                    Title = "PAM solution implemented, frequent privileged account activity reviews, and semiannual policy reviews",
                    InstructionText = "01.a, 01.b.i, 01.c[semiannual], 03, 04, 05.b[>=weekly]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("a429dd95-a660-4f47-a32d-4fa11a0392d6"),
                    Title = "PAM integration with IAM, automated validation against job roles, privileged credentials automatically reset, and real-time activity review with defined rules",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };  
        }

        private static List<ScoringRubric> RemoteAccessScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("9734a725-6f43-4a49-8b7c-fd008db3562d");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("ab56050c-74a2-4dea-b07a-07778741636e"),
                    Title = "Organization has no remote access policy",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("9f8757e3-64e7-44fe-b7c9-234fa68ea6e5"),
                    Title = "Policies are documented and defined criteria for granting remote access",
                    InstructionText = "01, 01.c",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("86006929-3b92-47a2-9893-04e77fc7708c"),
                    Title = "Two-factor authentication implemented, annual policy reviews, and regular activity reviews",
                    InstructionText = "01, 01.c, 01.d[annual], 02.d, 03.b[monthly]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("5d6ec5cd-a6de-40ea-881c-334025a24b05"),
                    Title = "All remote traffic monitored (no split tunneling), remote users segmented, remote systems authenticated, remote system security checks, frequent activity reviews, and semiannual policy reviews",
                    InstructionText = "01, 01.c, 01.d[>=semiannual], 02.b, 02.c, 02.d, 02.e, 02.f, 03.b[>=weekly]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("bf0d7275-d36e-4e33-89b0-9d2749eeaa01"),
                    Title = "Integration with IAM, automated validation against job roles, and real time activity reviews",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };   
        }

        private static List<ScoringRubric> ThirdPartyAccessScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("754ed961-0edd-45c5-87f6-2ff75e409fdd");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("a00bf39b-0a17-412c-80c8-7a51b59f6a55"),
                    Title = "Organization has no third party access policies",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("fd8c0a8a-c069-4fc0-83f6-bc374f27559e"),
                    Title = "Policies are documented, contractual language included, and third-parties defined",
                    InstructionText = "01, 01.a, 01.b",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("12e3e5a0-0b16-4b53-bb19-5485b37b081c"),
                    Title = "Secure API access, partial compliance monitoring, regular audits, regular activity reviews, and annual policy reviews",
                    InstructionText = "01, 01.a, 01.b, 01.c.iii[monthly], 01.d[annual], 02.a, 03[some], 02.b, 04.b[weekly]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("736a1d79-d694-49ef-9a11-dbe8200f2391"),
                    Title = "Full compliance monitoring, frequent audits, frequent activity reviews, and semiannual policy reviews",
                    InstructionText = "01, 01.a, 01.b, 01.b.iii.1, 01.c.iii[>=weekly], 01.d[>=semiannual], 02.a, 03[all], 02.b, 04.b[daily]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("7da1e5b5-6697-4191-a465-d3d657ad7c27"),
                    Title = "Integration with IAM, automated validation against job roles, real time audits, and real time activity reviews",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };  
        }

        private static List<ScoringRubric> NetworkProtectionScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("7ebe6f6c-001f-4022-bb69-6ebd6111338b");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("90d07e61-c112-4e36-9a50-f49255224084"),
                    Title = "Organization has no Network Protection policies",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("d535619e-2744-4098-84d9-a83992e1b9cc"),
                    Title = "Policies are documented, external network firewall deployed, and external network IDS/IPS deployed",
                    InstructionText = "01, 02.a[DMZ], 03.a[DMZ]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("789c7655-ad6e-4562-8173-fa20c0f58396"),
                    Title = "DMZ+internal network firewall and IDS/IPS deployed, silent network firewall block, IDS/IPS configured in block mode, forward network web proxy deployed, similar network protections on virtual/cloud infrastructure, forwarding of network logs to SIEM, regular log review, and annual policy reviews (YES to 01, 01.c[annual], 02.a[both], 02.c, 02.d, 02.g.i, 02.h.i, 02.i.ii[weekly], 03.a[both], 03.b[block], 03.g.i, 03.h.i, 03.i.ii[weekly], 04, 04.e.i, 04.f.i, 04.g.ii[weekly])",
                    InstructionText = "01, 01.c[annual], 02.a[both], 02.c, 02.d, 02.g.i, 02.h.i, 02.i.ii[weekly], 03.a[both], 03.b[block], 03.g.i, 03.h.i, 03.i.ii[weekly], 04, 04.e.i, 04.f.i, 04.g.ii[weekly]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("bc3d92bb-f1b0-489f-a894-21d30b3cd0b2"),
                    Title = "WAF or reverse web proxies deployed, full packet capture capability, SSL/TLS traffic inspection, malware sandbox analysis, transparent web proxies deployed, frequent log review, and semiannual policy review (YES to 01, 01.c[>=semiannual], 02.a[both], (02.b[Application] or 04.c), 02.c, 02.d, 02.g.i, 02.h.i, 02.i.ii[daily], 03.a[both], 03.b[block], 03.d, 03.e, 03.f, 03.g.i, 03.h.i, 03.i.ii[daily], 04, 04.b[transparent], 04.e.i, 04.f.i, 04.g.ii[daily])",
                    InstructionText = "01, 01.c[>=semiannual], 02.a[both], (02.b[Application] or 04.c), 02.c, 02.d, 02.g.i, 02.h.i, 02.i.ii[daily], 03.a[both], 03.b[block], 03.d, 03.e, 03.f, 03.g.i, 03.h.i, 03.i.ii[daily], 04, 04.b[transparent], 04.e.i, 04.f.i, 04.g.ii[daily]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("9d62a204-6a80-45e1-826c-720c2b6358a3"),
                    Title = "Automated threat intelligence integration with rules/signatures, real time network log review",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }; 
        }

        private static List<ScoringRubric> EndpointProtectionScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("72e5fa4d-5c0f-4d37-943c-d972393efdcb");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("f448deba-7490-4103-911d-3767869f9f01"),
                    Title = "Organization has no Endpoint Protection policies or technologies",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("59170347-b14a-4ee6-a027-0a43c10989b2"),
                    Title = "Policies are documented, signature-based malware protection deployed on Windows systems, endpoint firewall deployed",
                    InstructionText = "01, 02.c[Windows], 03.a[Windows]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("2847ea97-1d18-4770-abc4-18f9247805dc"),
                    Title = "Malware protection deployed on all systems, centralized management on Windows, similar endpoint protection on virtual/cloud infrastructure, forwarding of logs to SIEM, regular log review, and annual policy reviews (YES to 01, 01.b[annual], 02.c[Windows], 02.d.i[Windows], 03.a[Windows], 03.b, 04.a[Windows], 04.b, 05.a[Windows], 05.b, 06.a, 07.b[weekly])",
                    InstructionText = "01, 01.b[annual], 02.c[Windows], 02.d.i[Windows], 03.a[Windows], 03.b, 04.a[Windows], 04.b, 05.a[Windows], 05.b, 06.a, 07.b[weekly]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("8e7d5c82-2bd6-4b8a-a0e4-8630fe0072df"),
                    Title = "Centralized management on all operating systems, EDR capabilities, malware sandbox analysis, frequent log reviews, semiannual policy reviews (YES to 01, 01.b[>=semiannual], 02.a, 02.b, 02.c[all], 02.d.i[all], 03.a[all], 03.b, 04.a[all], 04.b, 05.a[all], 05.b, 06.a, 07.b[daily])",
                    InstructionText = "01, 01.b[>=semiannual], 02.a, 02.b, 02.c[all], 02.d.i[all], 03.a[all], 03.b, 04.a[all], 04.b, 05.a[all], 05.b, 06.a, 07.b[daily]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("8e101cbf-381f-4327-b4b6-7b45a22732c5"),
                    Title = "Automated threat intelligence integration with rules/signatures, real time endpoint log review",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };   
        }

        private static List<ScoringRubric> ApplicationProtectionScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("0cf39325-3c91-453f-a254-a482d86575e6");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("bebb2550-1a35-4508-a08b-3d2a91f841e0"),
                    Title = "Organization has no Application Security policies",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("30550d10-591d-4c4d-9278-e023f89b8b0e"),
                    Title = "Policies are documented, organization has a software inventory with manual software inventory updates",
                    InstructionText = "01, 03",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("ebcd3a02-175b-4083-a873-1c2b7cc06b32"),
                    Title = "Developers are trained on secure SDLC, software is certified prior to acquisition, automated software inventory updates, application whitelisting on servers, database logging, email spam filtering, forwarding of logs to SIEM, regular log reviews, and annual policy reviews (YES to 01, 01.a, 01.b, 01.c, 01.e[annual], 03, 03.b, 03.c[daily], 04.b[servers], 05.c, 07, 08.a, 09.b[weekly])",
                    InstructionText = "01, 01.a, 01.b, 01.c, 01.e[annual], 03, 03.b, 03.c[daily], 04.b[servers], 05.c, 07, 08.a, 09.b[weekly]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("2fc76dc8-07be-43ed-ab11-d379ac7e8ee3"),
                    Title = "Manual source code reviews, digital signing of code, digital signature capable application whitelisting, application whitelisting on all systems, centrally managed application whitelisting, logging on all critical applications, documented stored procedures and prepared statements, centrally managed email encryption, email authentication, frequent log reviews, and semiannual policy reviews (YES to 01, 01.a, 01.b, 01.c, 01.e[>=semiannual], 02.a.iii[>monthly], 02.b, 03, 03.b, 03.c[daily], 04.a[all], 04.b[all], 04.c, 05.a, 05.b, 05.c, 06.b, 06.c, 07, 07.b, 08.a, 09.b[daily])",
                    InstructionText = "01, 01.a, 01.b, 01.c, 01.e[>=semiannual], 02.a.iii[>monthly], 02.b, 03, 03.b, 03.c[daily], 04.a[all], 04.b[all], 04.c, 05.a, 05.b, 05.c, 06.b, 06.c, 07, 07.b, 08.a, 09.b[daily]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("224e7274-30a5-4e4e-adcb-991efbc5e71e"),
                    Title = "Automated source code reviews and real time application log review",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };  
        }

        private static List<ScoringRubric> IncidentReadinessScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("02a82cba-a900-4ca0-aa4e-810065e4c33c");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("f95bfa18-09e1-4d84-b76e-e05840bdd355"),
                    Title = "Organization has no Incident Response Plan",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("f229e568-41bb-43c5-b4a5-2c1d3676e6f5"),
                    Title = "IRP documented, IRP roles defined, incident reporting structure defined, incident escalation procedures defined, and team members aware of their assigned roles",
                    InstructionText = "01.a, 01.c, 01.d, 02.c",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("53cd1f5c-a2df-4201-9ec9-9daac168fb02"),
                    Title = "Security analyst roles defined, external party requirements defined, service retainers in place, staff training, annual drills, and annual IRP reviews",
                    InstructionText = "01.a, 01.b.i[some], 01.b.ii, 01.c, 01.d, 01.f[annual], 02.a.i[all], 02.c, 02.c.i, 03.b[annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("cf561319-aea1-46db-acd0-4ddb32b94306"),
                    Title = "24/7 staff coverage, mandatory leave, staff certification, staff performance reviews, ISAC participation, semiannual drills; semiannual IRP review",
                    InstructionText = "01.a, 02.a.i[all], 02.b, 01.b.i[all], 01.b.ii, 01.c, 01.d, 01.e, 01.f[>=semiannual], 02.c, 02.c.i, 02.c.ii, 02.d, 03.b[semiannual], 04",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("6e6c60d0-7f2e-4057-8fed-a604b4446bd7"),
                    Title = "CERT/CIRT/SOC team spans multiple business units, certification of all team members, quarterly drills with real time tracking, and drills integrated into staff performance review, ISAC threat intelligence sharing",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };   
        }

        private static List<ScoringRubric> IncidentDetectionScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("3b500510-0aa2-44bc-9600-f49ad7208113");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("43be24e7-c99c-45b6-9585-2d2a6779c8e1"),
                    Title = "Organization has no SOC/provider, SIEM capability, and detection procedures",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("39e08a02-3a86-44eb-b31b-29f00ccb8583"),
                    Title = "SOC/provider, SIEM capability with limited log collection, and defined detection procedures",
                    InstructionText = "01, 02, 02.a[some], 04",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("c3c7cf9f-0d4f-41b4-be40-b666c8f38907"),
                    Title = "SIEM capability with extensive log collection, 1 month log retention schedule, timestamp synchronization, CERT/CIRT/SOC staff training on detection procedures, and annual procedure reviews (YES to 01, 02, 02.a[all], 02.b[1 month], 02.e, 04, 04.b, 04.c[annual])",
                    InstructionText = "01, 02, 02.a[all], 02.b[1 month], 02.e, 04, 04.b, 04.c[annual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("0e90eb56-6f8e-4a7c-8990-7146e20e6f72"),
                    Title = "6 month log retention, threat intelligence feed integration, log tampering measures, partial automation of log correlation, and semiannual procedure reviews (YES to 01, 02, 02.a[all], 02.b[6 months], 02.c, 02.d.ii[partial], 02.e, 03, 04, 04.b, 04.c[semiannual])",
                    InstructionText = "01, 02, 02.a[all], 02.b[6 months], 02.c, 02.d.ii[partial], 02.e, 03, 04, 04.b, 04.c[semiannual]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("e1333cc2-7557-440e-8db1-e16e25e03496"),
                    Title = "1+ year log retention, fully automated log correlation, and continuous procedure reviews",
                    InstructionText = "01, 02, 02.a[all], 02.b[1+ year], 02.c, 02.d.ii[full], 02.e, 03, 04, 04.b, 04.c[>quarterly]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }; 
        }

        private static List<ScoringRubric> IncidentRemediationScoringRubrics(Guid createdBy, DateTime currentTimestamp)
        {
            var subcategoryId = Guid.Parse("6b4a7bed-c626-4369-87ab-1245d5560a38");

            return new List<ScoringRubric>
            {
                new ScoringRubric
                {
                    Id = Guid.Parse("a198b72d-e1de-4c76-b048-bc5cbf60e786"),
                    Title = "Organization has no incident remediation procedures or tools",
                    InstructionText = "all",
                    ScoringMethod = ScoringMethod.Auto_No,
                    Score = 1,
                    Order = 1,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("5a059dce-e935-4c2d-8685-dc4def61cfc6"),
                    Title = "Limited incident containment procedures documented with standalone containment tools",
                    InstructionText = "01.a[some]",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 2,
                    Order = 2,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("818379a9-62c1-44be-9264-7143162c08ad"),
                    Title = "Comprehensive containment procedures with staff training, centralized containment tools, limited forensics procedures, limited forensics analysis capabilities, case management tool, limited incident metrics tracking, and annual procedure reviews (YES to 01.a[all], 01.b, 01.c.i, 01.d[annual], 02.a[some], 02.c.ii[some], 02.d.ii[some], 02.f[annual], 03, 04)",
                    InstructionText = "01.a[all], 01.b, 01.c.i, 01.d[annual], 02.a[some], 02.c.ii[some], 02.d.ii[some], 02.f[annual], 03, 04",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 3,
                    Order = 3,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("6cf6b9ea-6c1f-475c-b8e5-1a95d6ea3baf"),
                    Title = "Comprehensive forensics procedures with staff training, comprehensive digital forensics tools, centralized digital forensics collection, comprehensive forensics analysis capabilities, dedicated forensics lab, comprehensive incident metrics tracking, metrics integrated into performance metrics + strategic planning, and semiannual procedure reviews (YES to 01.a[all], 01.b, 01.c.i, 01.d[semiannual], 02.a[all], 02.c.ii[all], 02.d.ii[all], 02.e, 02.f[semiannual], 03, 04.c, 04.d, 4.e, 4.f)",
                    InstructionText = "01.a[all], 01.b, 01.c.i, 01.d[semiannual], 02.a[all], 02.c.ii[all], 02.d.ii[all], 02.e, 02.f[semiannual], 03, 04.c, 04.d, 4.e, 4.f",
                    ScoringMethod = ScoringMethod.Auto_Yes,
                    Score = 4,
                    Order = 4,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new ScoringRubric
                {
                    Id = Guid.Parse("411bb469-6823-4279-b119-4745d5c84a55"),
                    Title = "Automated incident containment, automated digital forensics collection, automated forensic analysis (except static code analysis)",
                    InstructionText = null,
                    ScoringMethod = ScoringMethod.Manual,
                    Score = 5,
                    Order = 5,                    
                    SubcategoryId = subcategoryId,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            };  
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.