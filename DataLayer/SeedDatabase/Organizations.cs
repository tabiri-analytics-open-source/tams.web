using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels;

namespace Tams.Web.DataLayer
{
    public static class Organizations
    {
        public static Organization[] Seed(Guid createdBy, DateTime currentTimestamp)
        {
            return new List<Organization>
            {              
                new Organization
                {
                    Id = Guid.Parse("759344af-0e24-4673-852f-9b3e92742314"),
                    Name = "Tabiri Analytics, Inc",
                    OrganizationType = OrganizationType.CmuPartner,  
                    CreatedBy = createdBy,       
                    CreatedTimestamp = currentTimestamp
                },
                new Organization
                {
                    Id = Guid.Parse("d482e97e-83bd-4ae3-bcfc-09bd16289b98"),
                    Name = "SavcoSoft Corp",
                    OrganizationType = OrganizationType.TabiriCustomer,    
                    CreatedBy = createdBy,     
                    CreatedTimestamp = currentTimestamp
                },
                new Organization
                {
                    Id = Guid.Parse("d81018d9-e441-4f98-b5c5-38ee1434fb46"),
                    Name = "EdwinSoft Corp",
                    OrganizationType = OrganizationType.TabiriCustomer,      
                    CreatedBy = createdBy,   
                    CreatedTimestamp = currentTimestamp
                },
                new Organization
                {
                    Id = Guid.Parse("2e9a9635-16ba-41bd-aa87-3c9dc212e008"),
                    Name = "PaulSoft Corp",
                    OrganizationType = OrganizationType.TabiriCustomer,   
                    CreatedBy = createdBy,      
                    CreatedTimestamp = currentTimestamp
                }
            }.ToArray();
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.