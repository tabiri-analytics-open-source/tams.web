using System;
using System.Collections.Generic;
using Tams.Web.DataLayer.DomainModels;

namespace Tams.Web.DataLayer
{
    public static class Users
    {
        public static Person[] Seed(Guid createdBy, DateTime currentTimestamp)
        {
            return new List<Person>
            { 
                //Savannah Kadima's two test accounts             
                new Person
                {
                    Id = Guid.Parse("6118d3f5-2d54-4128-b9c1-5d4b888134e0"),
                    FirstName = "Savannah",
                    MiddleName = "O",
                    LastName = "Kadima",
                    OrgId = Guid.Parse("759344af-0e24-4673-852f-9b3e92742314"),
                    Role = Role.Auditor,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Person
                {
                    Id = Guid.Parse("d9e69d0f-80ff-4f65-b56a-c980643f4a00"),
                    FirstName = "Savannah",
                    MiddleName = "O",
                    LastName = "Kadima",
                    OrgId = Guid.Parse("d482e97e-83bd-4ae3-bcfc-09bd16289b98"),
                    Role = Role.User,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                //Edwin Kairu's two test accounts             
                new Person
                {
                    Id = Guid.Parse("e275dac4-ddcb-4851-ba6e-d2e0cf3f6056"),
                    FirstName = "Edwin",
                    MiddleName = "Kiaraho",
                    LastName = "Kairu",
                    OrgId = Guid.Parse("759344af-0e24-4673-852f-9b3e92742314"),
                    Role = Role.Auditor,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Person
                {
                    Id = Guid.Parse("22e61fb4-0c40-40c8-8a5d-9ebd1761461c"),
                    FirstName = "Edwin",
                    MiddleName = "Kiaraho",
                    LastName = "Kairu",
                    OrgId = Guid.Parse("d81018d9-e441-4f98-b5c5-38ee1434fb46"),
                    Role = Role.User,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                //Paul Nsonga's two test accounts
                new Person
                {
                    Id = Guid.Parse("388518d4-2d92-43c3-a805-e39e76fa2cef"),
                    FirstName = "Paul",
                    LastName = "Nsonga",
                    OrgId = Guid.Parse("759344af-0e24-4673-852f-9b3e92742314"),
                    Role = Role.Auditor,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                },
                new Person
                {
                    Id = Guid.Parse("03f9945b-4c12-4774-bb9a-8bc475f13d5e"),
                    FirstName = "Paul",
                    LastName = "Nsonga",
                    OrgId = Guid.Parse("2e9a9635-16ba-41bd-aa87-3c9dc212e008"),
                    Role = Role.User,
                    CreatedBy = createdBy,
                    CreatedTimestamp = currentTimestamp
                }
            }.ToArray();
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.