using Microsoft.EntityFrameworkCore;
using Tams.Web.DataLayer.DomainModels;
using Tams.Web.DataLayer.DomainModels.EntityConfigurations;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment.EntityConfigurations;
using Tams.Web.DataLayer.SeedDatabase;

namespace Tams.Web.DataLayer
{
  public class OperationalAssessmentContext : DbContext
  {
    public DbSet<Person> Person { get; set; }
    public DbSet<Organization> Organizations { get; set; }
    public DbSet<Questionnaire> Questionnaires { get; set; }
    public DbSet<Category> Categories { get; set; }
    public DbSet<Subcategory> Domains { get; set; }
    public DbSet<Question> Questions { get; set; }
    public DbSet<Response> Responses { get; set; }    
    public DbSet<CybersecurityStandard> CybersecurityStandards { get; set; }
    public DbSet<ScoringRubric> ScoringRubrics { get; set; }

    public OperationalAssessmentContext(DbContextOptions dbContextOptions)
      : base(dbContextOptions) { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {      
      modelBuilder.ApplyConfiguration(new OrganizationConfiguration());      
      modelBuilder.ApplyConfiguration(new QuestionnaireConfiguration());
      modelBuilder.ApplyConfiguration(new OrganizationQuestionnaireConfiguration());
      modelBuilder.ApplyConfiguration(new CategoryConfiguration());     
      modelBuilder.ApplyConfiguration(new SubcategoryConfiguration());      
      modelBuilder.ApplyConfiguration(new QuestionConfiguration());
      modelBuilder.ApplyConfiguration(new ResponseConfiguration());
      modelBuilder.ApplyConfiguration(new PersonConfiguration());
      modelBuilder.ApplyConfiguration(new CybersecurityStandardConfiguration());

      modelBuilder.Seed();

      base.OnModelCreating(modelBuilder);
    }
  }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.