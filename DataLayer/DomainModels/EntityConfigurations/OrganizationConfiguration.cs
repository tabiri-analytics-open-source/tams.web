using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Tams.Web.DataLayer.DomainModels.OperationalAssessment;

namespace Tams.Web.DataLayer.DomainModels.EntityConfigurations
{
    public class OrganizationConfiguration : IEntityTypeConfiguration<Organization>
    {
        public void Configure(EntityTypeBuilder<Organization> builder)
        {
            builder.ToTable("Organizations");

            #region Property configurations

            builder.HasKey(x => x.Id);            

            builder.Property(x => x.Id)
              .IsRequired();

            builder.Property(x => x.Name)
              .IsRequired();

            builder.Property(x => x.OrganizationType)
              .HasConversion<int>();

            builder.Property(x => x.CreatedBy)
              .IsRequired();

            builder.Property(x => x.CreatedTimestamp)
              .IsRequired();

            #endregion

            #region Ignore

            builder.Ignore(x => x.CurrentQuestionnaireId);

            #endregion

            #region Relationships

            builder.HasMany(x => x.Persons)
              .WithOne(y => y.Organization)
              .HasForeignKey(z => z.OrgId);

            builder.HasMany(x => x.Questionnaires)
              .WithMany(y => y.Organizations)
              .UsingEntity<OrganizationQuestionnaire>
                (bs => bs.HasOne<Questionnaire>().WithMany().HasForeignKey(cs => cs.QuestionnaireId),
                bs => bs.HasOne<Organization>().WithMany().HasForeignKey(cs => cs.OrgId));

            #endregion  
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.