using System;
using System.Collections.Generic;

namespace Tams.Web.DataLayer.DomainModels.OperationalAssessment
{
  public sealed class Subcategory : Entity
  {
    public string Title { get; set; }
    public string Objective { get; set; }
    public int Order { get; set; }
    public Guid CategoryId { get; set; }       

    #region Navigation properties

    public Category Category { get; set; }

    public ICollection<Question> Questions { get; set; }
    public ICollection<CybersecurityStandard> CybersecurityStandards { get; set; }
    public ICollection<ScoringRubric> ScoringRubrics { get; set; }

    #endregion
  }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.