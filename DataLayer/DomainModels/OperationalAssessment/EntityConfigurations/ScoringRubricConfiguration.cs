using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Tams.Web.DataLayer.DomainModels.OperationalAssessment.EntityConfigurations
{
    public class ScoringRubricConfigurationConfiguration : IEntityTypeConfiguration<ScoringRubric>
    {
        public void Configure(EntityTypeBuilder<ScoringRubric> builder)
        {
            builder.ToTable("ScoringRubrics");

            #region Property configurations

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
              .IsRequired();

            builder.Property(x => x.Title)
              .IsRequired();           

            builder.Property(x => x.ScoringMethod)
              .IsRequired();

            builder.Property(x => x.ScoringMethod)
              .HasConversion<int>();

            builder.Property(x => x.Score)
              .IsRequired();

            builder.Property(x => x.Order)
              .IsRequired();
            
            builder.Property(x => x.SubcategoryId)
              .IsRequired();

            builder.Property(x => x.CreatedBy)
              .IsRequired();

            builder.Property(x => x.CreatedTimestamp)
              .IsRequired();

            #endregion
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.