using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Tams.Web.DataLayer.DomainModels.OperationalAssessment.EntityConfigurations
{
    public class SubcategoryConfiguration : IEntityTypeConfiguration<Subcategory>
    {
        public void Configure(EntityTypeBuilder<Subcategory> builder)
        {
            builder.ToTable("Subcategories");

            #region Properties

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
              .IsRequired();

            builder.Property(x => x.Title)
              .IsRequired();

            builder.Property(x => x.Objective)
              .IsRequired();             

            // builder.Property(x => x.ScoringRubric)
            //   .IsRequired();

            builder.Property(x => x.Order)
              .IsRequired();

            builder.Property(x => x.CategoryId)
              .IsRequired();

            builder.Property(x => x.CreatedBy)
              .IsRequired();

            builder.Property(x => x.CreatedTimestamp)
              .IsRequired();

            #endregion

            #region Relationships     

            builder.HasOne(x => x.Category)
              .WithMany(y => y.Subcategories);

            builder.HasMany(x => x.Questions)
              .WithOne(y => y.Subcategory);

            builder.HasMany(x => x.CybersecurityStandards)
              .WithOne(y => y.Subcategory);      

            builder.HasMany(x => x.ScoringRubrics)
              .WithOne(y => y.Subcategory);      

            #endregion            
        }
    }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.