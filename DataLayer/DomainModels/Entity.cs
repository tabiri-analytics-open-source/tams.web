using System;

namespace Tams.Web.DataLayer.DomainModels
{
    public abstract class Entity
  {    
    public virtual Guid Id { get; set; }
    public bool IsTransient => Id == Guid.Empty;

    #region Logging properties
      public Guid CreatedBy { get; set; }
      public DateTime CreatedTimestamp { get; set; }
      public Guid? LastUpdatedBy { get; set; }
      public DateTime? LastUpdatedTimestamp { get; set; }

    #endregion 

    public override bool Equals(object obj)
    {
      var other = obj as Entity;

      if (ReferenceEquals(other, null))
        return false;

      if (ReferenceEquals(this, other))
        return true;

      if (GetRealType() != other.GetRealType())
        return false;

      if (Id == Guid.Empty || other.Id == Guid.Empty)
        return false;

      return Id == other.Id;
    }

    public static bool operator ==(Entity a, Entity b)
    {
      if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
        return true;

      if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
        return false;

      return a.Equals(b);
    }

    public static bool operator !=(Entity a, Entity b) => !(a == b);

    public override int GetHashCode() => (GetType().ToString() + Id).GetHashCode();

    private Type GetRealType() => GetType();
  }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.