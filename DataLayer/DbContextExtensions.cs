using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Tams.Web.DataLayer.DomainModels;
using System;

namespace Tams.Web.DataLayer
{
  public static class DbContextExtensions
  {
    public static void AddOrUpdate<T>(this DbContext context, IEnumerable<T> records, Guid currentUserId)
      where T : Entity
    {
      var currentTimestamp = DateTime.UtcNow;

      foreach (var record in records)
      {
        var local = context.Set<T>()
          .Local
          .FirstOrDefault(x => x.Id == record.Id);      

        if (local != null)
        {
          context.Entry(local).State = EntityState.Detached;
          context.Entry(record).State = EntityState.Modified;
          continue;
        }

        context.Add<T>(record);
      }
    } 

    public static void DeleteIfExists<T>(this DbSet<T> dbSet, IEnumerable<T> records)
      where T : Entity
    {
      foreach (var record in records)
      {
        var data = dbSet.SingleOrDefault(x => x.Id == record.Id);            
            
        if (data != null)
        {
          dbSet.Remove(data);          
          continue;
        }        
      }
    }
  }
}

// Copyright 2021 Tabiri Analytics, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.